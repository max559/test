import type { ReactElement } from 'react';

import { Heading, Logo, Paragraph, Placeholder } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';

interface NewsArticleProps {
  isUnlocked: boolean;
}

const NewsArticle = ({ isUnlocked }: NewsArticleProps): ReactElement => {
  return (
    <Flex flexDirection="column" gap={4}>
      <Flex alignItems="baseline" flexDirection="row" flexWrap="nowrap" gap={4}>
        <Logo className="!w-5 " name="brand-plus-main" />
        <Heading level={2}>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit rem similique neque
        </Heading>
      </Flex>

      <Placeholder aspectRatio="16:9" />

      <Paragraph weight="strong">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis autem laudantium ullam
        fuga, officiis voluptatum, eos repellat non, odit reprehenderit blanditiis esse soluta
        accusamus quos nisi totam atque corporis excepturi!
      </Paragraph>

      {isUnlocked && (
        <>
          <Paragraph>
            Curabitur suscipit sodales massa, quis aliquet orci sagittis et. Mauris tincidunt ex a
            massa cursus dictum. Ut vitae tristique neque. Pellentesque ac ligula quis libero
            ultricies finibus. Donec neque ex, venenatis a cursus sed, finibus vitae nibh. Morbi sit
            amet fringilla tortor. Curabitur urna mi, vulputate eget pellentesque a, ultricies sit
            amet quam. Mauris commodo, justo nec ultricies porttitor, lectus odio tincidunt massa,
            eget convallis metus ipsum a tellus. Vivamus vel leo velit.
          </Paragraph>

          <Paragraph>
            In ut pulvinar tellus. Pellentesque porta urna vel nisi ullamcorper, a dignissim arcu
            cursus. Nulla tellus leo, laoreet id tortor sit amet, tempus semper urna. Aliquam
            interdum augue ut molestie tincidunt. Fusce laoreet, massa quis porttitor vulputate,
            nunc arcu varius diam, a posuere ligula velit sit amet dolor. Curabitur sit amet libero
            quis tortor sollicitudin condimentum. Curabitur a eleifend lectus. Vivamus vel mattis
            urna. Donec ullamcorper nunc nec purus blandit feugiat. Nam pellentesque, mi quis
            rhoncus accumsan, dui lectus feugiat ligula, id iaculis nisi justo et dolor. In diam
            est, pellentesque sed eros sit amet, aliquet gravida tortor.
          </Paragraph>

          <Paragraph>
            Nullam nec commodo eros. Nulla vel mauris quis sapien viverra ultrices nec at arcu. Cras
            tincidunt tincidunt luctus. Ut efficitur orci id sagittis interdum. Vivamus non
            tincidunt velit. Nullam urna augue, mollis eget nunc ac, fringilla sagittis ex. Aliquam
            erat volutpat. Nulla viverra risus non magna tempus, interdum dictum ex venenatis. Proin
            ut ipsum non libero mollis tincidunt vitae a nisl. Ut vitae auctor purus, sit amet
            convallis mauris. Vestibulum vitae consectetur nibh, sit amet posuere urna. Cras
            condimentum et neque quis vestibulum. Pellentesque venenatis neque eu ullamcorper
            vestibulum.
          </Paragraph>

          <Paragraph>
            Suspendisse potenti. Proin sed risus id diam congue dapibus in et turpis. Quisque eu
            nibh non tellus pellentesque venenatis in sit amet nunc. Praesent justo sapien, euismod
            et eros id, elementum tincidunt felis. Vivamus pellentesque elementum tincidunt. Nunc
            euismod, arcu non ornare volutpat, nunc dolor placerat ante, quis bibendum odio ligula
            quis urna. Pellentesque mollis nunc magna, ut cursus sem tempor pretium. Suspendisse
            hendrerit rhoncus nunc. Praesent gravida maximus tellus, non tristique turpis luctus eu.
            Quisque ac nulla semper, iaculis lectus et, accumsan urna.
          </Paragraph>
        </>
      )}
    </Flex>
  );
};

export default NewsArticle;
