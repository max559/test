import type { ReactElement } from 'react';

import { Heading, List, ListItem } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';

const NewsAside = (): ReactElement => {
  return (
    <Flex flexDirection="column" gap={8}>
      <Flex flexDirection="column" gap={2}>
        <Heading level={6} size="md">
          Meest gelezen
        </Heading>
        <List as="ol">
          <ListItem>Lorem ipsum dolor sit amet</ListItem>
          <ListItem>Consectetur adipisicing elit</ListItem>
          <ListItem>Rem similique neque voluptatem expedita ea</ListItem>
        </List>
      </Flex>

      <Flex flexDirection="column" gap={2}>
        <Heading level={6} size="md">
          Meest recent
        </Heading>
        <List as="ol">
          <ListItem>Rem similique neque voluptatem expedita ea</ListItem>
          <ListItem>Eius facere impedit omnis tempora facilis</ListItem>
          <ListItem>Quisquam veritatis nulla doloribus</ListItem>
        </List>
      </Flex>
    </Flex>
  );
};

export default NewsAside;
