import type { ReactElement } from 'react';

import { Heading, Logo } from '@mediahuis/chameleon-react';
import { AUTHENTICATION_STATUS, useAuthentication } from '@subscriber/authentication';
import { Flex } from '@subscriber/subscriber-ui';
import dynamic from 'next/dynamic';

const DynamicButton = dynamic(
  () => import('@mediahuis/chameleon-react').then(module => module.Button),
  { ssr: false }
);

interface HeaderProps {
  onLogin: () => void;
  onLogout: () => void;
}

const Header = ({ onLogin, onLogout }: HeaderProps): ReactElement => {
  const { authenticationStatus, userInfo } = useAuthentication();

  const isAuthenticated = authenticationStatus === AUTHENTICATION_STATUS.AUTHENTICATED;

  function handleClick() {
    if (!isAuthenticated) {
      onLogin();
    } else {
      onLogout();
    }
  }

  return (
    <Flex
      alignItems="center"
      className="bg-slate-50 border border-slate-200 border-solid px-4 py-4 rounded"
      flexDirection="row"
      flexWrap="wrap"
      gap={4}
      justifyContent="space-between"
    >
      <Flex alignItems="center" flexDirection="row" gap={8}>
        <Logo className="!w-8 sm:!w-16" name="brand-square-main" />
        <Heading level={4}>Sandbox</Heading>
      </Flex>

      <DynamicButton
        appearance={isAuthenticated ? 'secondary' : 'primary'}
        loading={authenticationStatus === AUTHENTICATION_STATUS.LOADING}
        size="sm"
        onClick={handleClick}
      >
        {isAuthenticated ? `Logout ${userInfo?.EmailAddress}` : 'Login'}
      </DynamicButton>
    </Flex>
  );
};

export default Header;
