import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Heading, Paragraph, Select, Switch } from '@mediahuis/chameleon-react';
import { LoginWizard, SignedIn, SignedOut, useAuthentication } from '@subscriber/authentication';
import {
  DefaultCheckoutTemplateConfig,
  FastCheckoutTemplateConfig,
  HtmlTemplateConfig,
  Smartwall
} from '@subscriber/smartwall';
import { Banner, Flex, Sidebar } from '@subscriber/subscriber-ui';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { ChangeEvent, useState } from 'react';

import { NewsArticle } from 'components/NewsArticle';
import { NewsAside } from 'components/NewsAside';

const DynamicLoginWizard = dynamic(
  () => import('@subscriber/authentication').then(module => module.LoginWizard),
  { ssr: false }
);
const DynamicModal = dynamic(
  () => import('@subscriber/subscriber-ui').then(module => module.Modal),
  { ssr: false }
);

interface HomePageProps {
  orderId: number | null;
}

type ContainerOptionsType = 'modal' | 'popover';
type TemplateOptionsType = 'default-checkout' | 'fast-checkout' | 'html';

export const getServerSideProps: GetServerSideProps<HomePageProps> = async context => {
  const { sw_order_id } = context.query;

  return {
    props: {
      orderId: sw_order_id ? Number(sw_order_id) : null
    }
  };
};

const HomePage: NextPage<HomePageProps> = ({
  orderId
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { userInfo, redirectToSignOut } = useAuthentication();

  const containerOptions = [
    { label: 'Modal', value: 'modal' },
    { label: 'Popover', value: 'popover' }
  ];
  const templateOptions = [
    { label: 'Default Checkout', value: 'default-checkout' },
    { label: 'Fast Checkout', value: 'fast-checkout' }
  ];

  const [container, setContainer] = useState<ContainerOptionsType>('modal');
  const [template, setTemplate] = useState<TemplateOptionsType>('fast-checkout');
  const [isUnlocked, setIsUnlocked] = useState<boolean>(false);
  const [showLoginModal, setShowLoginModal] = useState<boolean>(false);
  const [showLoginwallModal, setShowLoginwallModal] = useState<boolean>(false);

  const getTemplate = ():
    | DefaultCheckoutTemplateConfig
    | FastCheckoutTemplateConfig
    | HtmlTemplateConfig => {
    switch (template) {
      case 'default-checkout':
        return {
          data: {
            articleTitle: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.',
            offerSlug: 'porous-maxedout-c'
          },
          type: 'default-checkout'
        };
      case 'fast-checkout':
        return {
          data: {
            articleTitle: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.',
            orderId: orderId || undefined
          },
          type: 'fast-checkout'
        };
      case 'html':
        return {
          data: {
            html: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.'
          },
          type: 'html'
        };
      default:
        return {
          data: {
            articleTitle: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit.',
            offerSlug: 'porous-maxedout-c'
          },
          type: 'default-checkout'
        };
    }
  };

  return (
    <>
      <Head>
        <title>Sandbox</title>
        <meta name="description" content="Sandbox" />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Sidebar gap={8}>
        <Sidebar.Main minWidthPercentage="55%">
          <Flex flexDirection="column" gap={8}>
            {!isUnlocked && (
              <SignedIn>
                <Banner>
                  <Paragraph>Als abonnee kan je dit artikel lezen</Paragraph>
                </Banner>
              </SignedIn>
            )}

            <NewsArticle isUnlocked={isUnlocked} />

            <SignedOut>
              <DynamicLoginWizard
                config={{
                  articleTitle: 'ARTICLE TITLE',
                  type: 'wall'
                }}
                onCompleteLogin={() => setShowLoginModal(false)}
                onCompleteRegistration={() => setShowLoginModal(false)}
              />
            </SignedOut>

            {!isUnlocked && (
              <SignedIn>
                <Smartwall
                  userInfo={userInfo}
                  brand={MH_BRAND}
                  container={{
                    options: {
                      closeOnOutsideClick: true
                    },
                    type: container
                  }}
                  environment={MH_ENV}
                  template={getTemplate()}
                />
              </SignedIn>
            )}
          </Flex>
        </Sidebar.Main>

        <Sidebar.Aside>
          <Flex flexDirection="column" flexGrow={1} gap={8}>
            <Flex
              className="border border-solid border-gray-300 p-4 rounded"
              flexDirection="column"
              gap={4}
            >
              <Heading level={6} size="md">
                Configuration
              </Heading>
              <Switch
                checked={isUnlocked}
                id="UnlockedSwitch"
                label="Unlocked"
                onChange={() => setIsUnlocked(prevValue => !prevValue)}
              />
              <Select
                id="container"
                value={container}
                placeholder="Select Container"
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setContainer(e.target.value as ContainerOptionsType)
                }
                label="Select Container"
                labelHidden
              >
                {containerOptions.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </Select>
              <Select
                id="template"
                value={template}
                placeholder="Select Template"
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setTemplate(e.target.value as TemplateOptionsType)
                }
                label="Select Template"
                labelHidden
              >
                {templateOptions.map(option => (
                  <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
              </Select>
            </Flex>

            <NewsAside />
          </Flex>
        </Sidebar.Aside>
      </Sidebar>

      <DynamicModal show={showLoginwallModal} onClose={() => setShowLoginwallModal(false)}>
        <div className="p-8">
          <LoginWizard
            config={{
              articleTitle: 'ARTICLE TITLE',
              type: 'wall'
            }}
            onCompleteLogin={() => setShowLoginwallModal(false)}
            onCompleteRegistration={() => setShowLoginwallModal(false)}
          />
        </div>
      </DynamicModal>
    </>
  );
};

export default HomePage;
