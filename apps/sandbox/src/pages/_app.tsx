import '@mediahuis/chameleon-reset';
import '@mediahuis/chameleon-theme-wl/fonts.css';
import 'styles/globals.css';

import { AuthProvider } from '@subscriber/authentication';
import type { Brand, Environment } from '@subscriber/globals';
import type { AppProps } from 'next/app';
import { PageLayout } from 'layouts';

declare global {
  const BRAND_NAME: string;
  const MH_BRAND: Brand;
  const MH_ENV: Environment;
  const MH_THEME: string;
  const SITE_URL: string;
  const STATIC_URL: string;
}

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <AuthProvider brand={MH_BRAND as 'gva'} environment={MH_ENV}>
      <PageLayout>
        <Component {...pageProps} />
      </PageLayout>
    </AuthProvider>
  );
};

export default App;
