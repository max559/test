import type { NextPage } from 'next';

import { Activation, ActivationEvent } from '@subscriber/activation';
import { SignedIn, SignedOut, useAuthentication } from '@subscriber/authentication';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { ReactElement, useState } from 'react';

import { trackingActivationEvent } from '../tracking/activation.events';

const DynamicLoginWizard = dynamic(
  () => import('@subscriber/authentication').then(module => module.LoginWizard),
  { ssr: false }
);

const ActivationPage: NextPage = (): ReactElement => {
  const { userInfo } = useAuthentication();

  const [showLoginModal, setShowLoginModal] = useState<boolean>(false);

  const onEvent = (event: ActivationEvent) => {
    const track = trackingActivationEvent(event);
    console.log(track);
  };

  return (
    <>
      <Head>
        <title>Sandbox - Activation</title>
        <meta name="description" content="Sandbox" />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <SignedOut>
        <div className="max-w-xl mx-auto p-8">
          <DynamicLoginWizard
            onCompleteLogin={() => setShowLoginModal(false)}
            onCompleteRegistration={() => setShowLoginModal(false)}
          />
        </div>
      </SignedOut>

      <SignedIn>
        {/* <Activation
          userInfo={userInfo}
          brand={MH_BRAND}
          environment={MH_ENV}
          onEvent={onEvent}
          withLayout
        /> */}
      </SignedIn>
    </>
  );
};

export default ActivationPage;
