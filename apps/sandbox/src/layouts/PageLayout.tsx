import { LoginWizard, useAuthentication } from '@subscriber/authentication';
import { Flex } from '@subscriber/subscriber-ui';
import { Header } from 'components/Header';
import dynamic from 'next/dynamic';
import { PropsWithChildren, ReactElement, useState } from 'react';

const DynamicModal = dynamic(
  () => import('@subscriber/subscriber-ui').then(module => module.Modal),
  { ssr: false }
);

const PageLayout = ({ children }: PropsWithChildren): ReactElement => {
  const { redirectToSignOut } = useAuthentication();
  const [showLoginModal, setShowLoginModal] = useState<boolean>(false);

  return (
    <>
      <Flex className="max-w-7xl mx-auto p-4" flexDirection="column" gap={8}>
        <Header onLogin={() => setShowLoginModal(true)} onLogout={() => redirectToSignOut()} />
        {children}
      </Flex>

      <DynamicModal show={showLoginModal} onClose={() => setShowLoginModal(false)}>
        <div className="p-8">
          <LoginWizard
            config={{
              type: 'default'
            }}
            onCompleteLogin={() => setShowLoginModal(false)}
            onCompleteRegistration={() => setShowLoginModal(false)}
          />
        </div>
      </DynamicModal>
    </>
  );
};

export default PageLayout;
