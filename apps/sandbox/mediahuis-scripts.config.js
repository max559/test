// eslint-disable-next-line @typescript-eslint/no-var-requires
const { getConfiguration } = require('@subscriber/globals');

module.exports = (defaultConfig, options) => {
  const { brand, env, theme } = options.context;

  const { brandName, siteUrl, staticUrl } = getConfiguration(brand, env);

  return {
    ...defaultConfig,
    extensionVars: {
      source: [brand, theme]
    },
    globals: {
      BRAND_NAME: brandName,
      MH_BRAND: brand,
      MH_ENV: env,
      MH_THEME: theme,
      SITE_URL: siteUrl,
      STATIC_URL: staticUrl
    }
  };
};
