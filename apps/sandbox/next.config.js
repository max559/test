/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    outputFileTracingRoot: path.join(__dirname, '../../')
  },
  output: 'standalone',
  reactStrictMode: true,
  swcMinify: true
};

module.exports = (...args) => {
  const mhScriptsConfig = require('@mediahuis/scripts-next/config')(...args);

  return {
    ...mhScriptsConfig,
    ...nextConfig
  };
};
