/* eslint-disable turbo/no-undeclared-env-vars */

let variants = [
  /* --- BE --- */
  /* DS - De Standaard */
  {
    brand: 'ds',
    env: 'test',
    theme: 'ds'
  },
  {
    brand: 'ds',
    env: 'preview',
    theme: 'ds'
  },
  {
    brand: 'ds',
    env: 'production',
    theme: 'ds'
  },
  /* GVA - Gazet van Antwerpen */
  {
    brand: 'gva',
    env: 'test',
    theme: 'gva'
  },
  {
    brand: 'gva',
    env: 'preview',
    theme: 'gva'
  },
  {
    brand: 'gva',
    env: 'production',
    theme: 'gva'
  },
  /* HBVL - Het Belang van Limburg */
  {
    brand: 'hbvl',
    env: 'test',
    theme: 'hbvl'
  },
  {
    brand: 'hbvl',
    env: 'preview',
    theme: 'hbvl'
  },
  {
    brand: 'hbvl',
    env: 'production',
    theme: 'hbvl'
  },
  /* NB - Nieuwsblad */
  {
    brand: 'nb',
    env: 'test',
    theme: 'nb'
  },
  {
    brand: 'nb',
    env: 'preview',
    theme: 'nb'
  },
  {
    brand: 'nb',
    env: 'production',
    theme: 'nb'
  },
  /* --- DE --- */
  /* AZ - Aachener Zeitung */
  {
    brand: 'az',
    env: 'test',
    theme: 'az'
  },
  {
    brand: 'az',
    env: 'preview',
    theme: 'az'
  },
  {
    brand: 'az',
    env: 'production',
    theme: 'az'
  },
  /* --- NL --- */
  /* DL - De Limburger */
  {
    brand: 'dl',
    env: 'test',
    theme: 'dl'
  },
  {
    brand: 'dl',
    env: 'preview',
    theme: 'dl'
  },
  {
    brand: 'dl',
    env: 'production',
    theme: 'dl'
  },
  /* --- LUX --- */
  /* CO - Contacto */
  {
    brand: 'co',
    env: 'test',
    theme: 'co'
  },
  {
    brand: 'co',
    env: 'preview',
    theme: 'co'
  },
  {
    brand: 'co',
    env: 'production',
    theme: 'co'
  },
  /* LT - Luxembourg Times */
  {
    brand: 'lt',
    env: 'test',
    theme: 'lt'
  },
  {
    brand: 'lt',
    env: 'preview',
    theme: 'lt'
  },
  {
    brand: 'lt',
    env: 'production',
    theme: 'lt'
  },
  /* LW - Luxemburger Wort */
  {
    brand: 'lw',
    env: 'test',
    theme: 'lw'
  },
  {
    brand: 'lw',
    env: 'preview',
    theme: 'lw'
  },
  {
    brand: 'lw',
    env: 'production',
    theme: 'lw'
  },
  /* TC - Télécran */
  {
    brand: 'tc',
    env: 'test',
    theme: 'tc'
  },
  {
    brand: 'tc',
    env: 'preview',
    theme: 'tc'
  },
  {
    brand: 'tc',
    env: 'production',
    theme: 'tc'
  }
];

if (process.env.MH_BRAND) {
  variants = variants.filter(
    variant => variant.brand === process.env.MH_BRAND && variant.env === 'test'
  );
}

module.exports = variants;
