module.exports = (defaultConfig, options) => {
  const { authentication, brand, ciamBrandGroup, ciamClientId, env, theme } = options.context;

  return {
    ...defaultConfig,
    extensionVars: {
      source: [brand, theme]
    },
    globals: {
      AUTHENTICATION: authentication || 'default',
      CIAM_BRAND_GROUP: ciamBrandGroup,
      CIAM_CLIENT_ID: ciamClientId,
      MH_BRAND: brand,
      MH_ENV: env,
      MH_THEME: theme
    }
  };
};
