/* eslint-disable turbo/no-undeclared-env-vars */

let variants = [
  /* --- BE --- */
  /* DS - De Standaard */
  {
    authentication: 'default',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '2u05G2DxWkoWydy1iD5s4IRoekEl9sKW',
    env: 'test',
    theme: 'ds'
  },
  {
    authentication: 'default',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'DscRc6rEbwP2uUZazvByGjXZflrjV0Z2',
    env: 'preview',
    theme: 'ds'
  },
  {
    authentication: 'default',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '',
    env: 'production',
    theme: 'ds'
  },
  /* GVA - Gazet van Antwerpen */
  {
    authentication: 'default',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'eD2SOHBZOtaPcp6gOUn9Yj9NESvXkaHE',
    env: 'test',
    theme: 'gva'
  },
  {
    authentication: 'default',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'dp0M2Q4Yx60U1pxYaLDX4YApkKnjr8Gn',
    env: 'preview',
    theme: 'gva'
  },
  {
    authentication: 'default',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '',
    env: 'production',
    theme: 'gva'
  },
  /* HBVL - Het Belang van Limburg */
  {
    authentication: 'default',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'QZQRRE2TzAgxXbiKX2E9J5lhpwuM609P',
    env: 'test',
    theme: 'hbvl'
  },
  {
    authentication: 'default',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'YpBmx62m4t03aGYcP0Y6IUYrkIX9NOyH',
    env: 'preview',
    theme: 'hbvl'
  },
  {
    authentication: 'default',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '',
    env: 'production',
    theme: 'hbvl'
  },
  /* NB - Nieuwsblad */
  {
    authentication: 'default',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'orAlhLDvDRp25jK1HZCcHv9xpclEDa1h',
    env: 'test',
    theme: 'nb'
  },
  {
    authentication: 'default',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'T9n4NLK2JMFR0XnhboXHXNzXrEJ3PDwq',
    env: 'preview',
    theme: 'nb'
  },
  {
    authentication: 'default',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '',
    env: 'production',
    theme: 'nb'
  },
  /* --- NL --- */
  /* DL - De Limburger */
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mnl',
    ciamClientId: '8HlgXnmSlF1y2E2ShHqUZ2snBypD5ma8',
    env: 'test',
    theme: 'dl'
  },
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mnl',
    ciamClientId: '6jAa0DSD2n7064M5RfNWCqKjoJCg46Sv',
    env: 'preview',
    theme: 'dl'
  },
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mnl',
    ciamClientId: '',
    env: 'production',
    theme: 'dl'
  }
];

if (process.env.MH_BRAND) {
  variants = variants.filter(
    variant => variant.brand === process.env.MH_BRAND && variant.env === 'test'
  );
}

module.exports = variants;
