import { ServicesCore } from '@subscriber/services-core';

export enum CJ_SELECTOR {
  ActivationWall = '[data-cj-root="activation-wall"]',
  InfoProposalBottom = '[data-cj-root="info-proposal-bottom"]',
  InfoProposalTop = '[data-cj-root="info-proposal-top"]',
  LoginToggle = '[data-cj-toggle="login-wall"]',
  SubscriptionWall = '[data-cj-root="subscription-wall"]'
}

export const coreInstance = new ServicesCore(MH_BRAND, MH_ENV);
