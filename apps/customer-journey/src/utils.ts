import type { CiamUserInfo } from '@subscriber/ciam';

import type { UserInfo } from './types';

export const createRootElement = () => {
  const rootElement = document.createElement('div');

  rootElement.className = 'cj-root';
  document.body.appendChild(rootElement);

  return rootElement;
};

export const isClientSide = (): boolean =>
  Boolean(typeof window !== 'undefined') && Boolean(window.document);

export const mapMediahuisUser = (user: Window['MEDIAHUIS']['user']): UserInfo => ({
  address: {
    box: undefined,
    city: user?.city || undefined,
    country: user?.countryCode || undefined,
    houseNumber: user?.houseNumber || undefined,
    postalCode: user?.zip || undefined,
    street: user?.street || undefined
  },
  email: user?.email || undefined,
  firstName: user?.firstName || undefined,
  id: user?.accountId as string,
  idToken: undefined,
  lastName: user?.lastName || undefined
});

export const mapCiamUserInfo = (ciamUserInfo: CiamUserInfo): UserInfo => ({
  address: {
    box: ciamUserInfo.fullIdentity.address?.houseNumberExtension || undefined,
    city: ciamUserInfo.fullIdentity.address?.city || undefined,
    country: ciamUserInfo.fullIdentity.address?.countryCode || undefined,
    houseNumber: ciamUserInfo.fullIdentity.address?.houseNumber || undefined,
    postalCode: ciamUserInfo.fullIdentity.address?.postalCode || undefined,
    street: ciamUserInfo.fullIdentity.address?.street || undefined
  },
  email: ciamUserInfo.idToken.email || undefined,
  firstName: ciamUserInfo.idToken.given_name || undefined,
  id: ciamUserInfo.idToken.sub as string,
  idToken: ciamUserInfo.tokens.id || undefined,
  lastName: ciamUserInfo.idToken.family_name || undefined
});
