import type { Brand as AuthBrand } from '@subscriber/authentication';
import type { ReactElement } from 'react';

import { AuthProvider, LoginWizard } from '@subscriber/authentication';
import { Modal } from '@subscriber/subscriber-ui';
import { useState } from 'react';

const completeLoginWizard = () => {
  window.location.reload();
};

interface LoginWallProps {
  articleTitle: string;
}

const LoginWall = ({ articleTitle }: LoginWallProps): ReactElement => {
  const [showModal, setShowModal] = useState<boolean>(true);

  if (showModal) {
    return (
      <AuthProvider brand={MH_BRAND as AuthBrand} environment={MH_ENV}>
        <Modal closeOnOutsideClick show onClose={() => setShowModal(false)}>
          <div style={{ padding: '2.5rem' }}>
            <LoginWizard
              config={{
                articleTitle,
                type: 'wall'
              }}
              onCompleteLogin={completeLoginWizard}
              onCompleteRegistration={completeLoginWizard}
            />
          </div>
        </Modal>
      </AuthProvider>
    );
  }

  return (
    <AuthProvider brand={MH_BRAND as AuthBrand} environment={MH_ENV}>
      <div style={{ border: '1px solid lightgray', padding: '2.5rem' }}>
        <LoginWizard
          config={{
            articleTitle,
            type: 'wall'
          }}
          onCompleteLogin={completeLoginWizard}
          onCompleteRegistration={completeLoginWizard}
        />
      </div>
    </AuthProvider>
  );
};

export default LoginWall;
