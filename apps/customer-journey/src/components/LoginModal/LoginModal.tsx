import type { Brand as AuthBrand } from '@subscriber/authentication';
import type { ReactElement } from 'react';

import { AuthProvider, LoginWizard } from '@subscriber/authentication';
import { Modal } from '@subscriber/subscriber-ui';

const completeLoginWizard = () => {
  window.location.reload();
};

interface LoginModalProps {
  onClose: () => void;
}

const LoginModal = ({ onClose }: LoginModalProps): ReactElement => {
  return (
    <AuthProvider brand={MH_BRAND as AuthBrand} environment={MH_ENV}>
      <Modal closeOnOutsideClick show onClose={onClose}>
        <div style={{ padding: '2.5rem' }}>
          <LoginWizard
            onCompleteLogin={completeLoginWizard}
            onCompleteRegistration={completeLoginWizard}
          />
        </div>
      </Modal>
    </AuthProvider>
  );
};

export default LoginModal;
