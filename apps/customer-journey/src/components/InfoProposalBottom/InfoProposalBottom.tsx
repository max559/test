import { Banner } from '@subscriber/subscriber-ui';
import { useState } from 'react';

const InfoProposalBottom = () => {
  const [showBanner, setShowBanner] = useState<boolean>(true);

  return (
    <Banner show={showBanner} onClose={() => setShowBanner(false)}>
      Info Bottom
    </Banner>
  );
};

export default InfoProposalBottom;
