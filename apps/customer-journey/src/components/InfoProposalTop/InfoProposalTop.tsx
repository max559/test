import { Banner } from '@subscriber/subscriber-ui';
import { ReactElement, useState } from 'react';

interface InfoProposalTop {
  text: string;
}

const InfoProposalTop = ({ text }: InfoProposalTop): ReactElement => {
  const [showBanner, setShowBanner] = useState<boolean>(true);

  return (
    <Banner show={showBanner} onClose={() => setShowBanner(false)}>
      {text}
    </Banner>
  );
};

export default InfoProposalTop;
