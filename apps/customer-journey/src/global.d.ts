/* eslint-disable @typescript-eslint/no-explicit-any */

import { CiamSDK } from '@subscriber/ciam';
import type { Brand, Environment } from '@subscriber/globals';

declare global {
  const AUTHENTICATION: 'ciam' | 'default' | 'none';
  const CIAM_BRAND_GROUP: string;
  const CIAM_CLIENT_ID: string;
  const MH_BRAND: Brand;
  const MH_ENV: Environment;

  interface Window {
    CIAM: CiamSDK | Array<() => void>;
    MEDIAHUIS: {
      abonnee?: {
        pageWontReload: boolean;
      };
      config: {
        access_loginshown: boolean;
        adblock: number;
        adblockSP: number;
        article_author: string;
        article_bodycharcount: number;
        article_cimpage: string;
        article_cimpage_free: string;
        article_cityname: string;
        article_contenttype: string;
        article_externalid: string;
        article_has_inline_media: boolean;
        article_id: string;
        article_introcharcount: number;
        article_ispluslayout: boolean;
        article_maintag: string;
        article_paper: number;
        article_positive: boolean;
        article_publicationdate: string;
        article_publicationdatetime_utc: string;
        article_publicationtime: string;
        article_section: string;
        article_sectionid: string;
        article_source: string;
        article_sourceoforigin: string;
        article_taglist: Array<string>;
        article_title: string;
        article_type: string;
        article_updatecountersurl: string;
        auth0: {
          clientId: string;
          loginIdentityLevel: string;
          premiumIdentityLevel: string;
          registerIdentityLevel: string;
        };
        brand: string;
        brooklynapiurl: string;
        cdnMarkup: string;
        cdnMarkupFull: string;
        cdnStatic: string;
        cdnStaticFull: string;
        cdn_markup: string;
        cdn_markupVersion: string;
        cdn_static: string;
        conversionFlow_variant: string;
        environment: string;
        features: {
          PERF_DisableArticleUpdateCounters: boolean;
          aboshop_pormax: boolean;
          accountConsent_showPopups: boolean;
          accountinfo_not_getidentity: boolean;
          adblockDetection: boolean;
          auth0: boolean;
          cimPhotoset: boolean;
          com_pushnotificationsOptinboxEnabled: boolean;
        };
        froomleServiceUrl: string;
        gdprServiceUrl: string;
        guid: string | null;
        interactiefPrefix: string;
        metered: boolean;
        oembedAccessToken: string;
        page_adv_dbg_sectiontree: string;
        page_adv_sectiontree: string;
      };
      pubsub: {
        publish: (topic: string, info: any) => void;
        subscribe: (topic: string, listener?: (info: any) => void) => void;
        unsubscribe: (topic: string, listener?: (info: any) => void) => void;
      };
      user?: {
        acceptCookie: boolean;
        accountId: string | null;
        accountType: string | null;
        age: number;
        authLevel: number;
        authType: string | null;
        birthDate: string;
        city: string | null;
        cookieBannerVisible: boolean;
        countryCode: string | null;
        email: string | null;
        facebookId: string | null;
        firstName: string | null;
        gender: string | null;
        hashA: string | null;
        hashI: string | null;
        houseNumber: string | null;
        identityHash: string | null;
        isAnonymous: boolean;
        isLoggedIn: boolean;
        isSubscriber: boolean;
        isTestUser: boolean;
        lastName: string | null;
        name: string | null;
        sessionExpired: boolean;
        showPersonalDetails: boolean;
        street: string | null;
        subscriptionStatus: string | null;
        subscriptionType: string | null;
        zip: string | null;
      };
    };
    utag: object;
  }
}
