import { LoginModal } from 'components/LoginModal';
import { CJ_SELECTOR } from 'globals';

const attachLoginActivation = () => {
  const activationWall = document.querySelector(CJ_SELECTOR.ActivationWall);

  const loginModalElement = window.React.createElement(LoginModal);

  window.ReactDOM.render(loginModalElement, activationWall);
};

export default attachLoginActivation;
