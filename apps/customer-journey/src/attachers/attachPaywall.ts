import { Smartwall } from '@subscriber/smartwall';

import { CJ_SELECTOR } from 'globals';

const attachPaywall = (orderId?: number) => {
  const subscriptionWall = document.querySelector(CJ_SELECTOR.SubscriptionWall);

  const smartwallElement = window.React.createElement(Smartwall, {
    userInfo: {
      Box: undefined,
      City: window.MEDIAHUIS.user?.city || undefined,
      Country: window.MEDIAHUIS.user?.countryCode || undefined,
      HouseNumber: window.MEDIAHUIS.user?.houseNumber || undefined,
      Zipcode: window.MEDIAHUIS.user?.zip || undefined,
      Street: window.MEDIAHUIS.user?.street || undefined,
      EmailAddress: window.MEDIAHUIS.user?.email || undefined,
      FirstName: window.MEDIAHUIS.user?.firstName || undefined,
      AccountGuid: window.MEDIAHUIS.user?.accountId as string,
      Name: window.MEDIAHUIS.user?.lastName || undefined
    },

    brand: MH_BRAND,
    container: {
      options: {
        closeOnOutsideClick: true
      },
      type: 'modal'
    },
    environment: MH_ENV,
    template: {
      data: {
        articleTitle: window.MEDIAHUIS.config.article_title,
        orderId
      },
      type: 'fast-checkout'
    }
  });

  window.ReactDOM.render(smartwallElement, subscriptionWall);
};

export default attachPaywall;
