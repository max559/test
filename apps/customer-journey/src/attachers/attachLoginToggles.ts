import { LoginModal } from 'components/LoginModal';
import { CJ_SELECTOR } from 'globals';
import { createRootElement } from 'utils';

const attachLoginToggles = () => {
  const loginWallToggles = document.querySelectorAll(CJ_SELECTOR.LoginToggle);
  const rootElement = createRootElement();

  const loginModalElement = window.React.createElement(LoginModal, {
    onClose: () => {
      window.ReactDOM.unmountComponentAtNode(rootElement);
    }
  });

  loginWallToggles.forEach(toggle => {
    toggle.addEventListener('click', event => {
      event.preventDefault();
      event.stopPropagation();

      window.ReactDOM.render(loginModalElement, rootElement);
    });
  });
};

export default attachLoginToggles;
