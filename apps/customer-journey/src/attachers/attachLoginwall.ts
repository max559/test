import { LoginWall } from 'components/LoginWall';
import { CJ_SELECTOR } from 'globals';

const attachLoginwall = () => {
  const subscriptionWall = document.querySelector(CJ_SELECTOR.SubscriptionWall);

  const loginModalElement = window.React.createElement(LoginWall, {
    articleTitle: window.MEDIAHUIS.config.article_title
  });

  window.ReactDOM.render(loginModalElement, subscriptionWall);
};

export default attachLoginwall;
