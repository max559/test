import { Activation, ActivationEvent } from '@subscriber/activation';

import { trackingActivationEvent } from '../tracking/activation.events';
import { UserInfo } from 'types';

const attachActivation = (user: UserInfo, attachElement: Element) => {
  const onEvent = (event: ActivationEvent) => {
    trackingActivationEvent(event);
  };

  const activationElement = window.React.createElement(Activation, {
    userInfo: user,
    brand: MH_BRAND,
    environment: MH_ENV,
    withLayout: true,
    onEvent: onEvent
  });

  window.ReactDOM.render(activationElement, attachElement);
};

export default attachActivation;
