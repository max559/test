import { InfoProposalBottom } from 'components/InfoProposalBottom';
import { CJ_SELECTOR } from 'globals';

const attachInfoProposalBottom = () => {
  const infoProposalBottom = document.querySelector(CJ_SELECTOR.InfoProposalBottom);
  const proposalBottomElement = window.React.createElement(InfoProposalBottom);

  window.ReactDOM.render(proposalBottomElement, infoProposalBottom);
};

export default attachInfoProposalBottom;
