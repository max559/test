import { InfoProposalTop } from 'components/InfoProposalTop';
import { CJ_SELECTOR } from 'globals';

const attachInfoProposalTop = (text: string) => {
  const infoProposalTop = document.querySelector(CJ_SELECTOR.InfoProposalTop);
  const proposalTopElement = window.React.createElement(InfoProposalTop, {
    text
  });

  window.ReactDOM.render(proposalTopElement, infoProposalTop);
};

export default attachInfoProposalTop;
