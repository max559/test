import { CiamSDK, CiamUserInfo } from '@subscriber/ciam';
import {
  attachActivation,
  attachInfoProposalBottom,
  attachInfoProposalTop,
  attachLoginActivation,
  attachLoginToggles,
  attachLoginwall,
  attachPaywall
} from 'attachers';
import { coreInstance, CJ_SELECTOR } from 'globals';
import { UserInfo } from 'types';
import { isClientSide, mapCiamUserInfo, mapMediahuisUser } from 'utils';

const activationWallElement = document.querySelector(CJ_SELECTOR.ActivationWall);
const currentUrl = new URL(window.location.href);
let user: UserInfo | null = null;

const initializeCustomerJourney = (isSubscribed = true) => {
  console.log('FR - CJ 2.0');

  if (isSubscribed) {
    window.MEDIAHUIS.pubsub.unsubscribe('account/getidentity/complete', initializeCustomerJourney);
  }

  user = mapMediahuisUser(window.MEDIAHUIS.user);

  const accountId = window.MEDIAHUIS.user?.accountId;
  const articleId = window.MEDIAHUIS.config.article_id;
  const isPlusLayout = window.MEDIAHUIS.config.article_ispluslayout;

  if (accountId) {
    const articleHash = currentUrl.searchParams.get('articlehash');
    const swOrderId = currentUrl.searchParams.get('sw_order_id');

    if (activationWallElement) {
      attachActivation(user, activationWallElement);
    }

    if (swOrderId) {
      attachInfoProposalTop(
        'Als abonnee kan je dit plusartikel lezen. Abonnee worden? Kies je leesformule'
      );
      attachPaywall(Number(swOrderId));
    } else if (articleHash) {
      attachInfoProposalTop(
        'Dit artikel is exclusief voor abonnees, maar we bieden het je gratis aan.'
      );
    } else if (isPlusLayout) {
      coreInstance.meteringService
        .postUnlock({
          accountId,
          articleId,
          brand: MH_BRAND
        })
        .then(response => {
          if (response.data.allowedState === 'Allowed') {
            currentUrl.searchParams.append('articlehash', response.data.rsa);
            window.location.href = currentUrl.toString();
          } else {
            attachInfoProposalTop(
              'Als abonnee kan je dit plusartikel lezen. Abonnee worden? Kies je leesformule'
            );
            attachPaywall();
          }
        })
        .catch(() => {
          attachInfoProposalTop(
            'Als abonnee kan je dit plusartikel lezen. Abonnee worden? Kies je leesformule'
          );
          attachInfoProposalBottom();
          attachPaywall();
        });
    }
  } else {
    attachLoginToggles();

    if (isPlusLayout) {
      attachLoginwall();
    } else {
      attachLoginActivation();
    }
  }
};

if (window.MEDIAHUIS.config.features.auth0) {
  console.log('AUTH0 - CJ 2.0');

  if (activationWallElement && isClientSide()) {
    window.CIAM = window.CIAM || [];
    window.CIAM.push(() =>
      Promise.all([
        (window.CIAM as CiamSDK).getDecodedAccessToken(window.MEDIAHUIS.config.auth0.clientId),
        (window.CIAM as CiamSDK).getFullUserProfile(window.MEDIAHUIS.config.auth0.clientId),
        (window.CIAM as CiamSDK).getUserInfo(window.MEDIAHUIS.config.auth0.clientId),
        (window.CIAM as CiamSDK).getUserSubscriptions(window.MEDIAHUIS.config.auth0.clientId)
      ])
        .then(([accessToken, fullIdentity, idToken, subscriptions]) => {
          if (!window.MEDIAHUIS.user?.isLoggedIn) {
            window.location.href = window.location.origin + `/auth/register?returnTo=${currentUrl}`;
          }

          if (accessToken && fullIdentity && idToken && subscriptions) {
            const userInfo: CiamUserInfo = {
              accessToken,
              fullIdentity,
              idToken,
              subscriptions: subscriptions ?? [],
              tokens: {
                access:
                  (window.CIAM as CiamSDK).getAccessToken(window.MEDIAHUIS.config.auth0.clientId) ??
                  '',
                id:
                  (window.CIAM as CiamSDK).getIdToken(window.MEDIAHUIS.config.auth0.clientId) ?? ''
              }
            };
            user = mapCiamUserInfo(userInfo);
            attachActivation(user, activationWallElement);
          }
        })
        .catch(error => {
          console.log('ERROR CIAM', error);
        })
    );
  }
} else if (window.MEDIAHUIS.user?.accountId) {
  initializeCustomerJourney(false);
} else {
  window.MEDIAHUIS.pubsub.subscribe('account/getidentity/complete', initializeCustomerJourney);
}
