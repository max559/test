export enum AUTHENTICATION_STATUS {
  AUTHENTICATED = 'authenticated',
  ERROR = 'error',
  LOADING = 'loading',
  UNAUTHENTICATED = 'unauthenticated',
  UNKNOWN = 'unknown'
}

export interface UserInfo {
  address: {
    box?: string;
    city?: string;
    country?: string;
    houseNumber?: string;
    postalCode?: string;
    street?: string;
  };
  email?: string;
  firstName?: string;
  id: string;
  idToken?: string;
  lastName?: string;
}
