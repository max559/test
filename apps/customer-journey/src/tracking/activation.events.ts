import {
  ActivationEvent,
  TRACKING_CLICK_EVENTS,
  TRACKING_SHOW_EVENTS
} from '@subscriber/activation';
import { EVENT_TYPE } from '@subscriber/globals';

type trackingClickConfigType = Record<TRACKING_CLICK_EVENTS, object>;
type trackingShowConfigType = Record<TRACKING_SHOW_EVENTS, object>;

const pubsub = window.MEDIAHUIS.pubsub;

export const trackingActivationEvent = (event: ActivationEvent, data?: object) => {
  let trackingObject;

  if (event.type === EVENT_TYPE.CLICK) {
    trackingObject = trackingClickConfig[event.key];
  }
  if (event.type === EVENT_TYPE.SHOW) {
    trackingObject = trackingShowConfig[event.key];
  }

  if (data) {
    trackingObject = { ...trackingObject, ...data };
  }

  track(trackingObject);
};

export const trackingClickConfig: trackingClickConfigType = {
  [TRACKING_CLICK_EVENTS.ACTIVATION_ERROR_CONTACT]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'click-contact',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT34'
  },
  [TRACKING_CLICK_EVENTS.ACTIVATION_HAS_ACCESS_CONTACT]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'click-contact',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT33'
  },
  [TRACKING_CLICK_EVENTS.ACTIVATION_NO_ACCESS_CONTACT]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'click-contact',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT32'
  },
  [TRACKING_CLICK_EVENTS.ACTIVATION_START_CONTACT]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'click-contact',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT20'
  },
  [TRACKING_CLICK_EVENTS.ACTIVATION_SUCCESS_CONTACT]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'click-contact',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT30'
  }
};

export const trackingShowConfig: trackingShowConfigType = {
  [TRACKING_SHOW_EVENTS.ACTIVATION_ERROR]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT34'
  },
  [TRACKING_SHOW_EVENTS.ACTIVATION_HAS_ACCESS]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT33'
  },
  [TRACKING_SHOW_EVENTS.ACTIVATION_NO_ACCESS]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT32'
  },
  [TRACKING_SHOW_EVENTS.ACTIVATION_START]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT20'
  },
  [TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/ACT30'
  },
  [TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS_MULTIBRAND]: {
    access_loginshown: true,
    access_walltype: 'subscription',
    event_action: 'show',
    event_category: 'conversionwall',
    event_label: '/conversion/activate/MultibrandSuccess'
  }
};

// Implementation from old CJ

const TRACKING_DEFAULTS = {
  access_loginshown: true,
  access_walltype: null,
  crm_campaigncode: 'CMP-02278-H8M9L6',
  event_category: 'gdpr',
  event_action: 'click',
  event_label: null,
  event_non_interaction: null,
  identity_gdprconsentlevel: null
};

const publish = (trackingObject: object) => {
  pubsub && pubsub.publish('tealium/event', Object.assign({}, TRACKING_DEFAULTS, trackingObject));
};

export const track = (trackingObject: object | undefined) => {
  if (pubsub && trackingObject) {
    if (window.utag) {
      // tealium is already loaded
      publish(trackingObject);
    } else {
      // wait for tealium to load before publish
      pubsub.subscribe('tealium/tagsLoaded', () => {
        publish(trackingObject);
      });
    }
  }
};
