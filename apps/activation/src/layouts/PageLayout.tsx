import type { PropsWithChildren, ReactElement } from 'react';

import { Divider, Logo } from '@mediahuis/chameleon-react';
import { Center, Flex } from '@subscriber/subscriber-ui';

import { CustomerServiceInfo } from 'components/CustomerServiceInfo';

const PageLayout = ({ children }: PropsWithChildren<Record<string, unknown>>): ReactElement => {
  return (
    <div className="bg-[var(--colorPrimary10)] min-h-screen md:py-[4%]">
      <Center
        className="bg-[var(--colorWhiteBase)] md:border md:border-solid md:border-primary20 md:rounded"
        maxInlineSize="750px"
      >
        <Flex className="p-10" flexDirection="column" gap={10}>
          <Logo className="h-9" />

          <Divider />

          <Center maxInlineSize="750px">{children}</Center>

          <Divider />

          <CustomerServiceInfo />
        </Flex>
      </Center>
    </div>
  );
};

export default PageLayout;
