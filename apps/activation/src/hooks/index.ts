export * from './useDigitalAccessStatus';
export { default as useDigitalAccessStatus } from './useDigitalAccessStatus';