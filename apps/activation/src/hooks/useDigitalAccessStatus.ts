import type { SubscriptionAddressRequest, SubscriptionStatus } from '@subscriber/services-core';

import { SUBSCRIPTION_STATUS } from '@subscriber/services-core';
import {
  useActivateSubscription,
  useLinkSubscription,
  useSubscriptionInfo
} from '@subscriber/services-hooks';
import { useEffect, useState } from 'react';

export enum DIGITAL_ACCESS_STATUS {
  Activate = 'Activate',
  AlreadyLinked = 'AlreadyLinked',
  Error = 'Error',
  Linked = 'Linked',
  NoAccess = 'NoAccess',
  NotFound = 'NotFound',
  NotLinked = 'NotLinked'
}

const useDigitalAccessStatus = (accountId?: string) => {
  const [status, setStatus] = useState<DIGITAL_ACCESS_STATUS | null>(null);

  const activateSubscription = useActivateSubscription();
  const linkSubscription = useLinkSubscription();
  const subscriptionInfoQuery = useSubscriptionInfo(accountId);

  useEffect(() => {
    if (linkSubscription.isError || subscriptionInfoQuery.isError) {
      setStatus(DIGITAL_ACCESS_STATUS.Error);
    }
  }, [linkSubscription.isError, subscriptionInfoQuery.isError]);

  useEffect(() => {
    if (linkSubscription.isSuccess) {
      if (linkSubscription.data.isLinked) {
        setStatus(DIGITAL_ACCESS_STATUS.Linked);
      } else {
        setStatus(DIGITAL_ACCESS_STATUS.NotLinked);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [linkSubscription.isSuccess]);

  useEffect(() => {
    if (!subscriptionInfoQuery.isFetching && subscriptionInfoQuery.isSuccess) {
      if (subscriptionInfoQuery.data.subscription) {
        setStatus(DIGITAL_ACCESS_STATUS.Linked);
      } else if (!status) {
        setStatus(DIGITAL_ACCESS_STATUS.Activate);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subscriptionInfoQuery.isFetching, subscriptionInfoQuery.isSuccess]);

  return {
    status,
    activate: (request: SubscriptionAddressRequest) => {
      activateSubscription
        .mutateAsync(request)
        .then((data: SubscriptionStatus) => {
          const isCurrentNumberLinked: boolean | undefined = data.subscriptions?.some(
            subscription => subscription.subscriptionNumber === request.subscriptionNumber
          );

          switch (data.status) {
            case SUBSCRIPTION_STATUS.FoundButLinked:
              if (isCurrentNumberLinked) {
                setStatus(DIGITAL_ACCESS_STATUS.Linked);
              } else {
                setStatus(DIGITAL_ACCESS_STATUS.AlreadyLinked);
              }
              break;
            case SUBSCRIPTION_STATUS.FoundNoAccess:
              setStatus(DIGITAL_ACCESS_STATUS.NoAccess);
              break;
            case SUBSCRIPTION_STATUS.FoundNotLinked:
              linkSubscription.mutate(request);
              break;
            case SUBSCRIPTION_STATUS.NotFound:
              setStatus(DIGITAL_ACCESS_STATUS.NotFound);
              break;
            default:
              setStatus(DIGITAL_ACCESS_STATUS.Error);
          }
        })
        .catch(() => {
          setStatus(DIGITAL_ACCESS_STATUS.Error);
        });
    },
    reset: () => {
      setStatus(null);
      subscriptionInfoQuery.remove();
    }
  };
};

export default useDigitalAccessStatus;
