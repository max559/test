import type { UserInfo } from '@subscriber/services-core';

import { create } from 'zustand';

export interface AuthenticationState {
  userInfo: UserInfo | undefined;
  isAuthenticated: boolean;
  setIsAuthenticated: (value: boolean) => void;
  setUserInfo: (value?: UserInfo) => void;
}

export const useAuthenticationStore = create<AuthenticationState>(set => ({
  userInfo: undefined,
  isAuthenticated: false,
  setIsAuthenticated: (isAuthenticated: boolean) => set(() => ({ isAuthenticated })),
  setUserInfo: (userInfo?: UserInfo) => set(() => ({ userInfo }))
}));

export default useAuthenticationStore;
