import type { LoginDto } from '@subscriber/services-core';
import type { PropsWithChildren, ReactElement } from 'react';

import type { AuthenticationState } from './Authentication.store';

import useTranslation from 'next-translate/useTranslation';
import { useEffect } from 'react';
import { shallow } from 'zustand/shallow';

import { useAccountInfoLux, useLogin } from '@subscriber/services-hooks';

import { useAuthenticationStore } from './Authentication.store';

interface AuthenticationProps {
  fallback?: ReactElement;
}

const Authentication = ({
  children,
  fallback
}: PropsWithChildren<AuthenticationProps>): ReactElement | null => {
  const { lang } = useTranslation('common');

  const { isAuthenticated, setIsAuthenticated, setUserInfo } = useAuthenticationStore(
    (state: AuthenticationState) => ({
      isAuthenticated: state.isAuthenticated,
      setIsAuthenticated: state.setIsAuthenticated,
      setUserInfo: state.setUserInfo
    }),
    shallow
  );

  const accountInfoQuery = useAccountInfoLux();
  const verifyLogin = useLogin();

  useEffect(() => {
    if (accountInfoQuery.isError) {
      if (accountInfoQuery.error.status === 401) {
        if (process.env.NODE_ENV === 'development') {
          // eslint-disable-next-line no-console
          console.error(
            new Error('Please authenticate on test environment when developing on localhost')
          );
        } else {
          verifyLogin
            .mutateAsync({ lang })
            .then((data: LoginDto) => {
              if (data.redirectUrl) {
                window.location.href = data.redirectUrl;
              }
            })
            .catch(() => {
              // eslint-disable-next-line no-console
              console.error(new Error('Could not retrieve redirectUrl'));
            });
        }
      } else {
        setIsAuthenticated(false);
        setUserInfo(undefined);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountInfoQuery.isError]);

  useEffect(() => {
    if (accountInfoQuery.isSuccess) {
      setIsAuthenticated(true);
      setUserInfo(accountInfoQuery.data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accountInfoQuery.isSuccess]);

  if (isAuthenticated) {
    return <>{children}</>;
  }

  if (fallback) {
    return fallback;
  }

  return null;
};

export default Authentication;
