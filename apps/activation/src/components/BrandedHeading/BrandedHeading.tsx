import type { PropsWithChildren, ReactElement } from 'react';

import { Heading, Logo } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';

const BrandedHeading = ({ children }: PropsWithChildren<Record<string, unknown>>): ReactElement => {
  return (
    <Flex alignItems="center" gap={4}>
      <Logo className="h-7 w-7" name="brand-plus-main" />
      <Heading level={3}>{children}</Heading>
    </Flex>
  );
};

export default BrandedHeading;
