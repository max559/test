import type { MouseEventHandler, ReactElement } from 'react';

import { Button, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import useTranslation from 'next-translate/useTranslation';

import { BrandedHeading } from 'components/BrandedHeading';

interface DigitalNoAccessProps {
  onCheckData: MouseEventHandler<HTMLButtonElement>;
}

const DigitalNoAccess = ({ onCheckData }: DigitalNoAccessProps): ReactElement => {
  const { t } = useTranslation('common');

  return (
    <Flex flexDirection="column" gap={10}>
      <BrandedHeading>{t('digitalAccess.noAccess.header')}</BrandedHeading>

      <Paragraph>{t('digitalAccess.noAccess.text')}</Paragraph>

      <Flex gap={2}>
        <Button appearance="secondary" id="CheckData" onClick={onCheckData}>
          {t('digitalAccess.noAccess.checkData')}
        </Button>
        <Button appearance="secondary" id="ContactCustomerService">
          {t('digitalAccess.noAccess.contactCustomerService')}
        </Button>
      </Flex>
    </Flex>
  );
};

export default DigitalNoAccess;
