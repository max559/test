import type { ReactElement } from 'react';

import { Button, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import useTranslation from 'next-translate/useTranslation';

import { BrandedHeading } from 'components/BrandedHeading';

const DigitalError = (): ReactElement => {
  const { t } = useTranslation('common');

  return (
    <Flex flexDirection="column" gap={10}>
      <BrandedHeading>{t('digitalAccess.error.header')}</BrandedHeading>

      <Paragraph>{t('digitalAccess.error.text')}</Paragraph>

      <Button appearance="secondary" id="ContactCustomerService">
        {t('digitalAccess.error.contactCustomerService')}
      </Button>
    </Flex>
  );
};

export default DigitalError;
