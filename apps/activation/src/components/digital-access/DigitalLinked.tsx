import type { UserInfo } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Button, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import Trans from 'next-translate/Trans';
import useTranslation from 'next-translate/useTranslation';

import { BrandedHeading } from 'components/BrandedHeading';

interface DigitalLinkedProps {
  userInfo: UserInfo;
}

const DigitalLinked = ({ userInfo }: DigitalLinkedProps): ReactElement => {
  const { t } = useTranslation('common');

  return (
    <Flex flexDirection="column" gap={10}>
      <BrandedHeading>{t('digitalAccess.linked.header')}</BrandedHeading>

      <Flex flexDirection="column" gap={1}>
        <Paragraph>
          <Trans
            // eslint-disable-next-line react/jsx-key
            components={[<span className="text-primaryLight" />]}
            i18nKey="common:digitalAccess.linked.textAccountCoupled"
            values={{
              email: userInfo.user.user.email
            }}
          />
        </Paragraph>
        <Paragraph>
          <Trans
            // eslint-disable-next-line react/jsx-key
            components={[<span className="text-primaryLight" />]}
            i18nKey="common:digitalAccess.linked.textPlusAccess"
            values={{
              email: userInfo.user.user.email
            }}
          />
        </Paragraph>
      </Flex>

      <Paragraph>{t('digitalAccess.linked.textEnjoyReading')}</Paragraph>

      <Button
        appearance="primary"
        id="StartReading"
        onClick={() => (window.location.href = SITE_URL)}
      >
        {t('digitalAccess.linked.startReading')}
      </Button>
    </Flex>
  );
};

export default DigitalLinked;
