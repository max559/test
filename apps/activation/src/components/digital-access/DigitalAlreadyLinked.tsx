import type { UserInfo } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Button, Caption, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import Trans from 'next-translate/Trans';
import useTranslation from 'next-translate/useTranslation';

import { BrandedHeading } from 'components/BrandedHeading';

interface DigitalAlreadyLinkedProps {
  userInfo: UserInfo;
}

const DigitalAlreadyLinked = ({ userInfo }: DigitalAlreadyLinkedProps): ReactElement => {
  const { t } = useTranslation('common');

  return (
    <Flex flexDirection="column" gap={10}>
      <BrandedHeading>{t('digitalAccess.alreadyLinked.header')}</BrandedHeading>

      <Flex flexDirection="column" gap={1}>
        <Paragraph>{t('digitalAccess.alreadyLinked.textAlreadyCoupled')}</Paragraph>
        <Paragraph>
          <Trans
            // eslint-disable-next-line react/jsx-key
            components={[<span className="text-primaryLight" />]}
            i18nKey="common:digitalAccess.alreadyLinked.textDigitalReading"
            values={{
              email: userInfo.user.user.email
            }}
          />
        </Paragraph>
        <Paragraph>{t('digitalAccess.alreadyLinked.textCustomerService')}</Paragraph>
      </Flex>

      <Button appearance="secondary" id="ContactCustomerService">
        {t('digitalAccess.alreadyLinked.contactCustomerService')}
      </Button>

      <Caption>{t('digitalAccess.alreadyLinked.caption')}</Caption>
    </Flex>
  );
};

export default DigitalAlreadyLinked;
