import type { UserInfo } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import { ReactElement, RefObject, useEffect } from 'react';

import type { SubscriptionFormValues } from 'forms';

import { Button, Caption, Paragraph } from '@mediahuis/chameleon-react';
import { useValidateAddress } from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import Trans from 'next-translate/Trans';
import useTranslation from 'next-translate/useTranslation';
import { useRef, useState } from 'react';

import { SubscriptionForm } from 'forms';
import { BrandedHeading } from 'components/BrandedHeading';

interface DigitalActivationProps {
  formValues?: SubscriptionFormValues;
  userInfo: UserInfo;
  onSubmit: (values: SubscriptionFormValues) => void;
}

const DigitalActivation = ({
  formValues,
  userInfo,
  onSubmit
}: DigitalActivationProps): ReactElement => {
  const { t } = useTranslation('common');

  const [isValidationError, setIsValidationError] = useState<boolean>(false);

  const validateAddress = useValidateAddress();

  const subscriptionFormRef: RefObject<FormikProps<SubscriptionFormValues>> =
    useRef<FormikProps<SubscriptionFormValues>>(null);

  useEffect(() => {
    if (validateAddress.isError) {
      setIsValidationError(true);
    }
  }, [validateAddress.isError]);

  return (
    <Flex flexDirection="column" gap={6}>
      <BrandedHeading>{t('digitalAccess.activation.header')}</BrandedHeading>

      <Paragraph>
        <Trans
          // eslint-disable-next-line react/jsx-key
          components={[<span className="text-primaryLight" />]}
          i18nKey="common:digitalAccess.activation.text"
          values={{
            email: userInfo.user.user.email
          }}
        />
      </Paragraph>

      <SubscriptionForm
        formRef={subscriptionFormRef}
        initialValues={
          formValues || {
            box: '',
            city: undefined,
            country: 'LU',
            houseNumber: '',
            postalCode: '',
            street: undefined,
            subscriptionNumber: ''
          }
        }
        onSubmit={values => {
          if (values.city && values.street) {
            validateAddress
              .mutateAsync({
                AddressToValidate: {
                  BoxNumber: values.box,
                  City: values?.city,
                  Country: values.country,
                  HouseNumber: values.houseNumber,
                  Street: values.street
                }
              })
              .then(data => {
                if (data.ValidationResult.IsValid) {
                  onSubmit(values);
                  setIsValidationError(false);
                } else {
                  setIsValidationError(true);
                }
              });
          }
        }}
      />

      <Flex flexDirection="column" gap={2}>
        <Button
          appearance="primary"
          className="!font-semibold"
          width="full"
          onClick={() => subscriptionFormRef.current?.submitForm()}
        >
          {t('digitalAccess.activation.submit')}
        </Button>

        {isValidationError && (
          <Caption color="RedBase">{t('digitalAccess.activation.validationError')}</Caption>
        )}

        <Caption className="text-center">{t('digitalAccess.activation.submitCaption')}</Caption>
      </Flex>
    </Flex>
  );
};

export default DigitalActivation;
