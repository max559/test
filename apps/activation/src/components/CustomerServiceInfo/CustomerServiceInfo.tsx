import type { ReactElement } from 'react';

import { Caption, Heading, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import useTranslation from 'next-translate/useTranslation';

import CustomerServiceInfoLink from './CustomerServiceInfoLink';

const CustomerServiceInfo = (): ReactElement => {
  const { t } = useTranslation('common');

  return (
    <Flex flexDirection="column" gap={2}>
      <Heading level={6}>{t('customerServiceInfo.title')}</Heading>

      <div>
        <Paragraph>{t('customerServiceInfo.phoneNumberText')}</Paragraph>
      </div>

      <div>
        <Paragraph weight="strong">+352/4993-9393</Paragraph>
        <Caption size="sm">{t('customerServiceInfo.phoneNumberCaption')}</Caption>
      </div>

      <Flex className="gap-1" flexDirection="column">
        <CustomerServiceInfoLink onClick={() => window.Didomi && window.Didomi.preferences.show()}>
          {t('customerServiceInfo.privacyPreferences')}
        </CustomerServiceInfoLink>

        <CustomerServiceInfoLink
          onClick={() => window && window.open(PRIVACY_POLICY_URL, '_blank')}
        >
          {t('customerServiceInfo.privacyPolicy')}
        </CustomerServiceInfoLink>
      </Flex>
    </Flex>
  );
};

export default CustomerServiceInfo;
