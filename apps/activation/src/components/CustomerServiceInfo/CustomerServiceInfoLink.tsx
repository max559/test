import type { PropsWithChildren, ReactElement } from 'react';

import { Caption } from '@mediahuis/chameleon-react';

interface CustomerServiceInfoLinkProps {
  onClick: () => void;
}

const CustomerServiceInfoLink = ({
  children,
  onClick
}: PropsWithChildren<CustomerServiceInfoLinkProps>): ReactElement => {
  return (
    <span className="cursor-pointer hover:underline" onClick={onClick}>
      <Caption size="lg">{children}</Caption>
    </span>
  );
};

export default CustomerServiceInfoLink;
