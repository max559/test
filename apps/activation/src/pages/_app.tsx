import '@mediahuis/chameleon-reset';
import 'styles/globals.css';

import type { AppProps } from 'next/app';

import { Didomi } from '@subscriber/didomi';
import { ServicesCore } from '@subscriber/services-core';
import { ServicesHooksProvider } from '@subscriber/services-hooks';
import { QueryClient } from '@tanstack/react-query';
import useTranslation from 'next-translate/useTranslation';

import { PageLayout } from 'layouts';

declare global {
  const MH_BRAND: 'lt' | 'lw';
  const MH_ENV: 'test' | 'preview' | 'production';
  const MH_THEME: string;
  const PRIVACY_POLICY_URL: string;
  const SITE_URL: string;
}

const didomiLanguages: Record<string, 'de' | 'en' | 'nl'> = {
  de: 'de',
  en: 'en',
  'nl-be': 'nl'
};

const queryClient = new QueryClient();

export const servicesCoreInstance = new ServicesCore(MH_BRAND, MH_ENV);

const App = ({ Component, pageProps }: AppProps) => {
  const { lang } = useTranslation();

  return (
    <ServicesHooksProvider coreInstance={servicesCoreInstance} queryClient={queryClient}>
      <PageLayout>
        <Component {...pageProps} />
      </PageLayout>

      <Didomi language={didomiLanguages[lang] ?? 'en'} />
    </ServicesHooksProvider>
  );
};

export default App;
