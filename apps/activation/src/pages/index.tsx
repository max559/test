import type { UserInfo } from '@subscriber/services-core';
import type { NextPage } from 'next';
import type { ReactElement } from 'react';

import type { SubscriptionFormValues } from 'forms';

import { Loader } from '@mediahuis/chameleon-react';
import Head from 'next/head';
import { useState } from 'react';
import { shallow } from 'zustand/shallow';

import { Authentication, useAuthenticationStore } from 'components/Authentication';
import {
  DigitalActivation,
  DigitalAlreadyLinked,
  DigitalError,
  DigitalLinked,
  DigitalNoAccess
} from 'components/digital-access';
import { DIGITAL_ACCESS_STATUS, useDigitalAccessStatus } from 'hooks';

const HomePageHead = (): ReactElement => {
  return (
    <Head>
      <title>Activation</title>
      <meta name="description" content="Activation" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
};

const HomePage: NextPage = () => {
  return (
    <Authentication fallback={<Loader size="lg" />}>
      <HomePageContent />
    </Authentication>
  );
};

const HomePageContent = (): ReactElement => {
  const { userInfo } = useAuthenticationStore(
    state => ({ userInfo: state.userInfo as UserInfo }),
    shallow
  );

  const [formValues, setFormValues] = useState<SubscriptionFormValues | undefined>();

  const {
    status: digitalAccessStatus,
    activate: activateDigitalAccess,
    reset: resetDigitalAccessStatus
  } = useDigitalAccessStatus(userInfo.user.user.id);

  if (digitalAccessStatus) {
    if (digitalAccessStatus === DIGITAL_ACCESS_STATUS.AlreadyLinked) {
      return (
        <>
          <HomePageHead />

          <DigitalAlreadyLinked userInfo={userInfo} />
        </>
      );
    }
    if (digitalAccessStatus === DIGITAL_ACCESS_STATUS.Error) {
      return (
        <>
          <HomePageHead />

          <DigitalError />
        </>
      );
    }
    if (digitalAccessStatus === DIGITAL_ACCESS_STATUS.Linked) {
      return (
        <>
          <HomePageHead />

          <DigitalLinked userInfo={userInfo as UserInfo} />
        </>
      );
    }
    if (
      digitalAccessStatus === DIGITAL_ACCESS_STATUS.NoAccess ||
      digitalAccessStatus === DIGITAL_ACCESS_STATUS.NotFound
    ) {
      return (
        <>
          <HomePageHead />

          <DigitalNoAccess onCheckData={resetDigitalAccessStatus} />
        </>
      );
    }

    return (
      <>
        <HomePageHead />

        <DigitalActivation
          formValues={formValues}
          userInfo={userInfo as UserInfo}
          onSubmit={(subscriptionFormValues: SubscriptionFormValues) => {
            activateDigitalAccess({
              accountGuid: userInfo.user.user.id,
              address: {
                boxNumber: subscriptionFormValues.box,
                city: subscriptionFormValues.city?.Name,
                countryCode: subscriptionFormValues.country,
                houseNumber: subscriptionFormValues.houseNumber,
                street: subscriptionFormValues.street?.Name,
                zipCode: subscriptionFormValues.postalCode
              },
              subscriptionNumber: subscriptionFormValues.subscriptionNumber
            });
            setFormValues(subscriptionFormValues);
          }}
        />
      </>
    );
  }

  return <Loader size="lg" />;
};

export default HomePage;
