import type { City, Country, Street } from '@subscriber/services-core';
import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { AutoComplete, Caption, Select, TextField } from '@mediahuis/chameleon-react';
import {
  useAutocompleteCities,
  useAutocompleteStreets,
  useCountriesByLanguage
} from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import { Field, Form, Formik } from 'formik';
import useTranslation from 'next-translate/useTranslation';
import * as Yup from 'yup';

const mapCitySuggestions = (cities: Array<City>): Array<string> => {
  return cities.reduce((acc: Array<string>, city: City) => {
    if (city.Name && city.PostalCode) {
      return acc.concat(`${city.Name} (${city.PostalCode})`);
    }

    return acc;
  }, []);
};

const mapStreetSuggestions = (streets: Array<Street>): Array<string> => {
  return streets.reduce((acc: Array<string>, street: Street) => {
    if (street.Name) {
      return acc.concat(street.Name);
    }

    return acc;
  }, []);
};

export interface SubscriptionFormValues {
  box: string;
  city?: City;
  country: string;
  houseNumber: string;
  postalCode: string;
  street?: Street;
  subscriptionNumber: string;
}

interface SubscriptionFormProps {
  formRef: RefObject<FormikProps<SubscriptionFormValues>>;
  initialValues: SubscriptionFormValues;
  onSubmit: (values: SubscriptionFormValues) => void;
}

const SubscriptionForm = ({
  formRef,
  initialValues,
  onSubmit
}: SubscriptionFormProps): ReactElement => {
  const { t } = useTranslation('common');

  const autocompleteCities = useAutocompleteCities();
  const autocompleteStreets = useAutocompleteStreets();
  const countriesByLanguageQuery = useCountriesByLanguage('en');

  const SubscriptionFormSchema = Yup.object().shape({
    box: Yup.string(),
    country: Yup.string().required(t('forms.SubscriptionForm.validations.required.country')),
    houseNumber: Yup.string().required(
      t('forms.SubscriptionForm.validations.required.houseNumber')
    ),
    postalCode: Yup.string().required(t('forms.SubscriptionForm.validations.required.postalCode')),
    street: Yup.object()
      .nullable()
      .required(t('forms.SubscriptionForm.validations.required.street')),
    subscriptionNumber: Yup.string().required(
      t('forms.SubscriptionForm.validations.required.subscriptionNumber')
    )
  });

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      validationSchema={SubscriptionFormSchema}
      onSubmit={onSubmit}
    >
      {({ setFieldValue }) => (
        <Form className="flex flex-col gap-4">
          <Flex flexDirection="column" gap={1}>
            <Field name="subscriptionNumber">
              {({ field, meta }: FieldProps<string>) => (
                <TextField
                  error={!!meta.touched && !!meta.error}
                  id="SubscriptionNumberInput"
                  label={t('forms.SubscriptionForm.labels.subscriptionNumber')}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  required
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>
            <Caption color="var(--colorGreyDark)" size="lg">
              {t('forms.SubscriptionForm.captions.subscriptionNumber')}
            </Caption>
          </Flex>

          <Field name="country">
            {({ field, meta }: FieldProps<string>) => (
              <Select
                disabled={!countriesByLanguageQuery.isSuccess}
                error={!!meta.touched && !!meta.error}
                id="CountrySelect"
                label={t('forms.SubscriptionForm.labels.country')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              >
                {countriesByLanguageQuery.isSuccess &&
                  countriesByLanguageQuery.data.map((country: Country) => (
                    <option key={country.Name} value={country.IsoCode}>
                      {country.Name}
                    </option>
                  ))}
              </Select>
            )}
          </Field>

          <Field name="postalCode">
            {({ field, form, meta }: FieldProps<string>) => (
              <AutoComplete
                disabled={!form.values.country}
                error={!!meta.touched && !!meta.error}
                id="PostalCodeAutoComplete"
                label={t('forms.SubscriptionForm.labels.postalCode')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                suggestions={
                  autocompleteCities.isSuccess
                    ? mapCitySuggestions(autocompleteCities.data.Cities)
                    : []
                }
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onInputChange={value =>
                  autocompleteCities.mutate({
                    body: { Key: value },
                    countryIsoCode: form.values.country
                  })
                }
                onSelect={value => {
                  if (value) {
                    const postalCode = value.split(' ')[1].slice(1, -1);

                    setFieldValue(
                      'city',
                      autocompleteCities.data?.Cities.find(city => city.PostalCode === postalCode)
                    );
                    setFieldValue('postalCode', postalCode);
                  } else {
                    setFieldValue('city', undefined);
                  }
                }}
              />
            )}
          </Field>

          <Field name="city">
            {({ field }: FieldProps<City>) => (
              <TextField
                disabled
                id="CityInput"
                label={t('forms.SubscriptionForm.labels.city')}
                name={field.name}
                required
                value={field.value?.Name}
              />
            )}
          </Field>

          <Field name="street">
            {({ field, form, meta }: FieldProps<Street>) => (
              <AutoComplete
                disabled={!form.values.city || !form.values.postalCode}
                error={!!meta.touched && !!meta.error}
                id="StreetAutoComplete"
                label={t('forms.SubscriptionForm.labels.street')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                suggestions={
                  autocompleteStreets.isSuccess
                    ? mapStreetSuggestions(autocompleteStreets.data.Streets)
                    : []
                }
                value={field.value?.Name}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onInputChange={value => {
                  autocompleteStreets.mutate({
                    body: { Key: value, PostalCode: form.values.postalCode },
                    countryIsoCode: form.values.country
                  });
                }}
                onSelect={value =>
                  setFieldValue(
                    'street',
                    autocompleteStreets.data?.Streets.find(street => street.Name === value)
                  )
                }
              />
            )}
          </Field>

          <Flex gap={4}>
            <Field name="houseNumber">
              {({ field, form, meta }: FieldProps<string>) => (
                <TextField
                  className="flex-grow"
                  disabled={!form.values.street}
                  error={!!meta.touched && !!meta.error}
                  id="HouseNumberInput"
                  label={t('forms.SubscriptionForm.labels.houseNumber')}
                  maxLength={5}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  required
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>
            <Field name="box">
              {({ field, form, meta }: FieldProps<string>) => (
                <TextField
                  className="flex-grow"
                  disabled={!form.values.street}
                  id="BoxInput"
                  label={t('forms.SubscriptionForm.labels.box')}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  required={false}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>
          </Flex>
        </Form>
      )}
    </Formik>
  );
};

export default SubscriptionForm;
