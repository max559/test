/* eslint-disable turbo/no-undeclared-env-vars */

module.exports = {
  plugins: {
    '@mediahuis/chameleon-postcss-plugin': {
      theme: process.env.__MEDIAHUIS_SCRIPTS_CONTEXT_THEME__
    },
    tailwindcss: {},
    autoprefixer: {}
  }
};
