/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable turbo/no-undeclared-env-vars */

const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    outputFileTracingRoot: path.join(__dirname, '../../')
  },
  output: 'standalone',
  reactStrictMode: true,
  swcMinify: true
};

module.exports = (...args) => {
  const defaultLocale = process.env.__MEDIAHUIS_SCRIPTS_CONTEXT_DEFAULT_LOCALE__;
  const mhScriptsConfig = require('@mediahuis/scripts-next/config')(...args);
  const nextTranslateConfig = require('next-translate');

  let configuration = nextTranslateConfig({
    ...mhScriptsConfig,
    ...nextConfig
  });

  if (defaultLocale) {
    configuration.i18n.defaultLocale = defaultLocale;
  }

  return configuration;
};
