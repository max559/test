module.exports = (defaultConfig, options) => {
  const { brand, env, privacyPolicyUrl, siteUrl, theme } = options.context;

  return {
    ...defaultConfig,
    extensionVars: {
      source: [brand, theme]
    },
    globals: {
      MH_BRAND: brand,
      MH_ENV: env,
      MH_THEME: theme,
      PRIVACY_POLICY_URL: privacyPolicyUrl,
      SITE_URL: siteUrl
    }
  };
};
