/* eslint-disable turbo/no-undeclared-env-vars */

let variants = [
  {
    brand: 'lw',
    defaultLocale: 'de',
    env: 'test',
    privacyPolicyUrl: 'https://www.wort.lu/de/privacy',
    siteUrl: 'https://www.wort.lu',
    theme: 'lw'
  },
  {
    brand: 'lw',
    defaultLocale: 'de',
    env: 'preview',
    privacyPolicyUrl: 'https://www.wort.lu/de/privacy',
    siteUrl: 'https://www.wort.lu',
    theme: 'lw'
  },
  {
    brand: 'lw',
    defaultLocale: 'de',
    env: 'production',
    privacyPolicyUrl: 'https://www.wort.lu/de/privacy',
    siteUrl: 'https://www.wort.lu',
    theme: 'lw'
  },
  {
    brand: 'lt',
    defaultLocale: 'en',
    env: 'test',
    privacyPolicyUrl: 'https://www.luxtimes.lu/en/privacy',
    siteUrl: 'https://www.luxtimes.lu',
    theme: 'lt'
  },
  {
    brand: 'lt',
    defaultLocale: 'en',
    env: 'preview',
    privacyPolicyUrl: 'https://www.luxtimes.lu/en/privacy',
    siteUrl: 'https://www.luxtimes.lu',
    theme: 'lt'
  },
  {
    brand: 'lt',
    defaultLocale: 'en',
    env: 'production',
    privacyPolicyUrl: 'https://www.luxtimes.lu/en/privacy',
    siteUrl: 'https://www.luxtimes.lu',
    theme: 'lt'
  }
];

if (process.env.MH_BRAND) {
  variants = variants.filter(
    variant => variant.brand === process.env.MH_BRAND && variant.env === 'test'
  );
}

module.exports = variants;
