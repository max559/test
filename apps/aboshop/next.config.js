/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable turbo/no-undeclared-env-vars */

const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    outputFileTracingRoot: path.join(__dirname, '../../')
  },
  images: {
    domains: [
      /* BE */
      'teststatic.standaard.be',
      'previewstatic.standaard.be',
      'static.standaard.be',
      'teststatic.gva.be',
      'previewstatic.gva.be',
      'static.gva.be',
      'teststatic.hbvl.be',
      'previewstatic.hbvl.be',
      'static.hbvl.be',
      'teststatic.nieuwsblad.be',
      'previewstatic.nieuwsblad.be',
      'static.nieuwsblad.be',
      /* DE */
      'teststatic.aachener-zeitung.de',
      'previewstatic.aachener-zeitung.de',
      'static.aachener-zeitung.de',
      /* NL */
      'teststatic.limburger.nl',
      'previewstatic.limburger.nl',
      'static.limburger.nl',
      /* LUX */
      'teststatic.contacto.lu',
      'previewstatic.contacto.lu',
      'static.contacto.lu',
      'teststatic.luxtimes.lu',
      'previewstatic.luxtimes.lu',
      'static.luxtimes.lu',
      'teststatic.wort.lu',
      'previewstatic.wort.lu',
      'static.wort.lu',
      'teststatic.telecran.lu',
      'previewstatic.telecran.lu',
      'static.telecran.lu'
    ]
  },
  output: 'standalone',
  reactStrictMode: true,
  swcMinify: true
};

module.exports = (...args) => {
  const defaultLocale = process.env.__MEDIAHUIS_SCRIPTS_CONTEXT_DEFAULT_LOCALE__;
  const mhScriptsConfig = require('@mediahuis/scripts-next/config')(...args);
  const nextTranslateConfig = require('next-translate');

  let configuration = nextTranslateConfig({
    ...mhScriptsConfig,
    ...nextConfig
  });

  if (defaultLocale) {
    configuration.i18n.defaultLocale = defaultLocale;
  }

  return configuration;
};
