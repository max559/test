module.exports = (defaultConfig, options) => {
  const {
    authentication,
    brand,
    ciamBrandGroup,
    ciamClientId,
    env,
    logo,
    paymentProvider,
    siteName,
    siteUrl,
    theme
  } = options.context;

  return {
    ...defaultConfig,
    extensionVars: {
      source: [brand, theme]
    },
    globals: {
      AUTHENTICATION: authentication || 'default',
      CIAM_BRAND_GROUP: ciamBrandGroup,
      CIAM_CLIENT_ID: ciamClientId,
      LOGO: logo || 'default',
      PAYMENT_PROVIDER: paymentProvider || 'mollie',
      MH_BRAND: brand,
      MH_ENV: env,
      MH_THEME: theme,
      SITE_NAME: siteName,
      SITE_URL: siteUrl
    }
  };
};
