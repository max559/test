import type { LogoProps } from '@mediahuis/chameleon-react';
import type { ContentLink } from '@subscriber/services-core';
import type { PropsWithChildren, ReactElement } from 'react';

import { Icon, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { PhoneFill } from '@mediahuis/chameleon-theme-wl/icons';
import { Center, Flex } from '@subscriber/subscriber-ui';
import dynamic from 'next/dynamic';
import Link from 'next/link';

import { Footer } from 'components/Footer';

import { useTranslation } from 'hooks';

const resolveLogoProps = (logoVariant: 'default' | 'plus' | 'square'): LogoProps => {
  const logoProps = {
    default: {
      className: 'h-6',
      name: 'brand-main'
    },
    plus: {
      className: 'h-10',
      name: 'brand-plus-main'
    },
    square: {
      className: 'h-16',
      name: 'brand-square-main'
    }
  };

  return logoProps[logoVariant];
};

const translationNamespace = 'layouts';

const DynamicContentFrame = dynamic(() => import('components/ContentFrame/ContentFrame'), {
  ssr: false
});

interface PageLayoutProps {
  contentLinksBottom?: Array<ContentLink>;
  contentLinksTop?: Array<ContentLink>;
}

const PageLayout = ({
  children,
  contentLinksBottom,
  contentLinksTop
}: PropsWithChildren<PageLayoutProps>): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <Flex className="min-h-screen w-full" flexDirection="column" justifyContent="space-between">
      <header className="bg-neutral10 h-16 px-[5%]">
        <Center className="flex h-full items-center justify-between" maxInlineSize="80rem">
          <Link href="/">
            <a className="flex">
              <Logo {...resolveLogoProps(LOGO)} />
            </a>
          </Link>

          {MH_BRAND === 'ds' ||
            MH_BRAND === 'dl' ||
            MH_BRAND === 'gva' ||
            MH_BRAND === 'hbvl' ||
            (MH_BRAND === 'nb' && (
              <Flex flexGrow="0" gap={8}>
                <Flex alignItems="center" flexDirection="row" className="gap-2 !hidden md:!flex">
                  <Icon color="colorBlackBase" as={PhoneFill} />
                  <Paragraph className="!ml-0" font="alt">
                    {t('pageLayout.headerPrimary')}
                  </Paragraph>
                </Flex>
              </Flex>
            ))}
        </Center>
      </header>

      {contentLinksTop && (
        <DynamicContentFrame
          srcDoc={contentLinksTop.map(link => link.content.value).join('') || ''}
        />
      )}

      <main className="basis-0 grow px-[5%] py-10 shrink-0" style={{ flex: '1 0 0' }}>
        <Center className="h-full" maxInlineSize="80rem">
          {children}
        </Center>
      </main>

      {contentLinksBottom && (
        <DynamicContentFrame
          srcDoc={contentLinksBottom.map(link => link.content.value).join('') || ''}
        />
      )}

      <Footer />
    </Flex>
  );
};

export default PageLayout;
