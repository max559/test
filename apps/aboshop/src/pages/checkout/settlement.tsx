import type { Order, OrderSettlement } from '@subscriber/services-core';
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Spinner } from 'components/Spinner';
import { Authentication, RedirectToSignIn, SignedIn, SignedOut } from 'features/authentication';
import { SettlementWizard } from 'features/checkout';
import { ValidatorGate } from 'features/validators';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/checkout/settlement';

interface SettlementPageProps {
  order: Order;
  orderSettlement: OrderSettlement;
}

export const getServerSideProps: GetServerSideProps<SettlementPageProps> = async context => {
  const { orderId, paymentReference } = context.query;

  let orderResponse = null;
  let orderSettlementResponse = null;

  if (orderId) {
    [orderResponse, orderSettlementResponse] = await Promise.all([
      servicesCoreInstance.orderService.getOrder({ id: orderId as string }),
      servicesCoreInstance.orderSettleService.getOrderSettlement({ orderId: orderId as string })
    ]);
  } else if (paymentReference) {
    orderSettlementResponse = await servicesCoreInstance.orderSettleService.getOrderSettlement({
      paymentReference: paymentReference as string
    });
    orderResponse = await servicesCoreInstance.orderService.getOrder({
      id: orderSettlementResponse.data.orderId
    });
  }

  if (orderResponse && orderSettlementResponse) {
    return {
      props: {
        order: orderResponse.data,
        orderSettlement: orderSettlementResponse.data
      }
    };
  }

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  };
};

const SettlementPage: NextPage<SettlementPageProps> = ({
  order,
  orderSettlement
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { t } = useTranslation(translationNamespace);

  if (order.loginValidation) {
    return (
      <Authentication>
        <SignedIn>
          <ValidatorGate
            fallback={<Spinner text={t('validationGateFallbackText')} />}
            loginValidation={order.loginValidation}
            preconditionsValidation={order.preconditionsValidation}
          >
            <SettlementWizard order={order} orderSettlement={orderSettlement} />
          </ValidatorGate>
        </SignedIn>

        <SignedOut>
          <RedirectToSignIn />
        </SignedOut>
      </Authentication>
    );
  }

  return (
    <ValidatorGate
      fallback={<Spinner text={t('validationGateFallbackText')} />}
      preconditionsValidation={order.preconditionsValidation}
    >
      <SettlementWizard order={order} orderSettlement={orderSettlement} />
    </ValidatorGate>
  );
};

export default SettlementPage;
