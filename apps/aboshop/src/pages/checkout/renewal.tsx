import type { Order } from '@subscriber/services-core';
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Spinner } from 'components/Spinner';
import { Authentication, RedirectToSignIn, SignedIn, SignedOut } from 'features/authentication';
import { RenewalWizard } from 'features/checkout';
import { ValidatorGate } from 'features/validators';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/checkout/renewal';

interface RenewalPageProps {
  order: Order;
  paymentReference: string | null;
}

export const getServerSideProps: GetServerSideProps<RenewalPageProps> = async context => {
  const { orderHash, orderId, paymentReference } = context.query;

  if (orderHash && orderId) {
    const orderResponse = await servicesCoreInstance.orderService.getOrder({
      id: orderId as string,
      orderHash: orderHash as string
    });

    return {
      props: {
        order: {
          ...orderResponse.data,
          orderHash: orderHash as string
        },
        paymentReference: (paymentReference as string) || null
      }
    };
  }

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  };
};

const RenewalPage: NextPage<RenewalPageProps> = ({
  order,
  paymentReference
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { t } = useTranslation(translationNamespace);

  if (order.loginValidation) {
    return (
      <Authentication>
        <SignedIn>
          <ValidatorGate
            fallback={<Spinner text={t('validationGateFallbackText')} />}
            loginValidation={order.loginValidation}
            preconditionsValidation={order.preconditionsValidation}
          >
            <RenewalWizard order={order} paymentReference={paymentReference} />
          </ValidatorGate>
        </SignedIn>

        <SignedOut>
          <RedirectToSignIn />
        </SignedOut>
      </Authentication>
    );
  }

  return (
    <ValidatorGate
      fallback={<Spinner text={t('validationGateFallbackText')} />}
      loginValidation={order.loginValidation}
      preconditionsValidation={order.preconditionsValidation}
    >
      <RenewalWizard order={order} paymentReference={paymentReference} />
    </ValidatorGate>
  );
};

export default RenewalPage;
