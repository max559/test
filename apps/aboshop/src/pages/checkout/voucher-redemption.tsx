import type { Order } from '@subscriber/services-core';
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Spinner } from 'components/Spinner';
import { Authentication, RedirectToSignIn, SignedIn, SignedOut } from 'features/authentication';
import { VoucherRedemptionWizard } from 'features/checkout';
import { ValidatorGate } from 'features/validators';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/checkout/voucher-redemption';

interface VoucherRedemptionPageProps {
  order: Order;
}

export const getServerSideProps: GetServerSideProps<VoucherRedemptionPageProps> = async context => {
  const { orderHash, orderId } = context.query;

  if (orderHash && orderId) {
    const orderResponse = await servicesCoreInstance.orderService.getOrder({
      id: orderId as string,
      orderHash: orderHash as string
    });

    return {
      props: {
        order: {
          ...orderResponse.data,
          orderHash: orderHash as string
        }
      }
    };
  }

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  };
};

const VoucherRedemptionPage: NextPage<VoucherRedemptionPageProps> = ({
  order
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { t } = useTranslation(translationNamespace);

  if (order.loginValidation) {
    return (
      <Authentication>
        <SignedIn>
          <ValidatorGate
            fallback={<Spinner text={t('validationGateFallbackText')} />}
            loginValidation={order.loginValidation}
            preconditionsValidation={order.preconditionsValidation}
          >
            <VoucherRedemptionWizard order={order} />
          </ValidatorGate>
        </SignedIn>

        <SignedOut>
          <RedirectToSignIn />
        </SignedOut>
      </Authentication>
    );
  }

  return (
    <ValidatorGate
      fallback={<Spinner text={t('validationGateFallbackText')} />}
      preconditionsValidation={order.preconditionsValidation}
    >
      <VoucherRedemptionWizard order={order} />
    </ValidatorGate>
  );
};

export default VoucherRedemptionPage;
