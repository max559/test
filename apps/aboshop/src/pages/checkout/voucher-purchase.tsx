import type { Order } from '@subscriber/services-core';
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { OFFER_ITEM_TYPE, PRICE_TYPE } from '@subscriber/services-core';

import { Spinner } from 'components/Spinner';
import { Authentication, RedirectToSignIn, SignedIn, SignedOut } from 'features/authentication';
import { VoucherPurchaseWizard } from 'features/checkout';
import { ValidatorGate } from 'features/validators';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/checkout/voucher-purchase';

interface VoucherPurchasePageProps {
  order: Order;
  paymentReference: string | null;
}

export const getServerSideProps: GetServerSideProps<VoucherPurchasePageProps> = async context => {
  const { formulaId, orderHash, orderId, paymentReference } = context.query;

  if (orderHash && orderId) {
    const orderResponse = await servicesCoreInstance.orderService.getOrder({
      id: orderId as string,
      orderHash: orderHash as string
    });

    return {
      props: {
        order: {
          ...orderResponse.data,
          orderHash: orderHash as string
        },
        paymentReference: (paymentReference as string) || null
      }
    };
  }
  if (formulaId) {
    const offerFormulaItemResponse = await servicesCoreInstance.offerService.getOfferItem({
      id: formulaId as string,
      type: OFFER_ITEM_TYPE.SubscriptionFormula
    });

    if (offerFormulaItemResponse.data.priceType === PRICE_TYPE.VoucherPurchase) {
      const order = servicesCoreInstance.orderUtil.generateOrder(
        offerFormulaItemResponse.data,
        'voucherpurchase'
      );
      const createOrderResponse = await servicesCoreInstance.orderService.createOrder(order);

      return {
        redirect: {
          destination: `/checkout/voucher-purchase?orderId=${createOrderResponse.data.id}&orderHash=${createOrderResponse.data.orderHash}`,
          permanent: false
        }
      };
    }
  }

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  };
};

const VoucherPurchasePage: NextPage<VoucherPurchasePageProps> = ({
  order,
  paymentReference
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const { t } = useTranslation(translationNamespace);

  if (order.loginValidation) {
    return (
      <Authentication>
        <SignedIn>
          <ValidatorGate
            fallback={<Spinner text={t('validationGateFallbackText')} />}
            loginValidation={order.loginValidation}
            preconditionsValidation={order.preconditionsValidation}
          >
            <VoucherPurchaseWizard order={order} paymentReference={paymentReference} />
          </ValidatorGate>
        </SignedIn>

        <SignedOut>
          <RedirectToSignIn />
        </SignedOut>
      </Authentication>
    );
  }

  return (
    <ValidatorGate
      fallback={<Spinner text={t('validationGateFallbackText')} />}
      preconditionsValidation={order.preconditionsValidation}
    >
      <VoucherPurchaseWizard order={order} paymentReference={paymentReference} />
    </ValidatorGate>
  );
};

export default VoucherPurchasePage;
