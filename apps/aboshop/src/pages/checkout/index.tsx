import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Banner, Button, Heading, Paragraph } from '@mediahuis/chameleon-react';
import { useCreateOrder, useOfferItemBySubscriptionFormulaId } from '@subscriber/services-hooks';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { Spinner } from 'components/Spinner';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/checkout/index';

interface CheckoutPageProps {
  formulaId: string | null;
}

export const getServerSideProps: GetServerSideProps<CheckoutPageProps> = async context => {
  const formulaId = context.query.formulaId ? String(context.query.formulaId) : null;
  const formula_id = context.query.formula_id ? String(context.query.formula_id) : null;

  if (formulaId || formula_id) {
    return {
      props: {
        formulaId: formulaId || formula_id
      }
    };
  }

  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  };
};

const CheckoutPage: NextPage<CheckoutPageProps> = ({
  formulaId
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();
  const { t } = useTranslation(translationNamespace);

  const createOrder = useCreateOrder();
  const formulaQuery = useOfferItemBySubscriptionFormulaId(formulaId ?? undefined);

  useEffect(() => {
    if (formulaQuery.isSuccess) {
      createOrder
        .mutateAsync(servicesCoreInstance.orderUtil.generateOrder(formulaQuery.data))
        .then(order =>
          router.push(`/checkout/${order.type}?orderId=${order.id}&orderHash=${order.orderHash}`)
        );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formulaQuery.isSuccess]);

  if (formulaQuery.isError || createOrder.isError) {
    return (
      <Banner appearance="error" closeHidden show>
        <div className="flex flex-col gap-2">
          <Heading size="sm">Error</Heading>

          <Paragraph>{t('errorText')}</Paragraph>

          <span>
            <Button appearance="primary" size="sm" onClick={() => router.push('/')}>
              {t('errorButton')}
            </Button>
          </span>
        </div>
      </Banner>
    );
  }

  return <Spinner text={t('spinnerText')} />;
};

export default CheckoutPage;
