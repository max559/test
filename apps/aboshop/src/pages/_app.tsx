import '@mediahuis/chameleon-reset';
import '@mediahuis/chameleon-theme-wl/fonts.css';
import 'styles/globals.css';
import 'styles/mollie.css';
import 'styles/react-phone-number-input.css';

import type { Brand } from '@subscriber/globals';
import type { ContentLink } from '@subscriber/services-core';
import type { AppProps } from 'next/app';

import type { PageHeadProps } from 'components/PageHead';

import { Didomi } from '@subscriber/didomi';
import { GtmScript } from '@subscriber/gtm';
import { ServicesHooksProvider } from '@subscriber/services-hooks';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import useTranslation from 'next-translate/useTranslation';

import { PageHead } from 'components/PageHead';
import { AuthenticationProvider } from 'features/authentication';
import { PaymentProvider } from 'features/payment';
import { PageViewTracker, TrackingProvider } from 'features/tracking';
import { queryClient, servicesCoreInstance } from 'globals';
import { PageLayout } from 'layouts';

dayjs.extend(customParseFormat);

const defaultHeadConfig: Record<Brand, PageHeadProps> = {
  az: {
    description:
      'Jetzt unser Digital-Abo, Digital-Extra-Abo oder Komplett-Abo kennenlernen: Im Monatsabo, mit oder ohne Laufzeit, zum Verschenken, für Studenten oder im Probeabo schon ab 0,99 €.',
    openGraph: {
      locale: 'de_DE',
      siteName: 'Medienhaus Aachen GmbH'
    },
    title: 'Unsere Angebote kennenlernen | Aachener Zeitung'
  },
  co: {
    description:
      'Deseja o melhor semanário de língua portuguesa do Luxemburgo? Todas as semanas, entregamos a última edição à sua porta. Descubra-o agora!',
    openGraph: {
      locale: 'pt_LU',
      siteName: 'Mediahuis Luxembourg S.A.'
    },
    title: 'Contacto Assinatura - O jornal português do Luxemburgo'
  },
  dl: {
    description:
      'Neem nu een abonnement op De Limburger en blijf dagelijks op de hoogte van het laatste nieuws. Maak de klik met jouw wereld en blijf op de hoogte via je mobiel, tablet, desktop of de papieren krant. Bekijk meer over de 4 verschillende abonnementen. Je bent al abonnee vanaf € 7,50 per maand.',
    openGraph: {
      image: 'https://markup.limburger.nl/extra/static/img/website/DL_OG_image.png',
      locale: 'nl_NL',
      siteName: 'De Limburger'
    },
    title: 'De Limburger abonnementen | Lees vanaf 7,50 per maand'
  },
  ds: {
    description:
      'Kies uw abonnement en lees De Standaard op papier en/of digitaal. Helder beschreven feiten en inzichten, van politiek over economie tot cultuur en wetenschap.',
    facebook: {
      admins: ['1031588996', '100006909940815'],
      appId: '155215551216365',
      pages: '7133374462'
    },
    openGraph: {
      image: 'https://markup.standaard.be/extra/assets/img/dummy-social.gif',
      locale: 'nl_BE',
      siteName: 'De Standaard'
    },
    title: 'Abonnement De Standaard - Alle formules en prijzen - De Standaard'
  },
  gva: {
    description:
      'Vergelijk abonnementsformules en kies voor papier en/of digitaal. Je dagelijkse portie grote nieuwsfeiten, regio- en sportnieuws.',
    facebook: {
      admins: ['1031588996', '100006909940815'],
      appId: '122351784513266',
      pages: '25170247890'
    },
    openGraph: {
      image: 'https://markup.gva.be/extra/assets/img/dummy-social.gif',
      locale: 'nl_BE',
      siteName: 'Gazet van Antwerpen'
    },
    title: 'Abonnement Gazet van Antwerpen - Alle formules en prijzen - Gazet van Antwerpen'
  },
  hbvl: {
    description:
      'Vergelijk abonnementsformules en kies voor papier en/of digitaal. Nieuws uit Limburg en ver daarbuiten. Grote nieuwsfeiten, regio- en sportnieuws.',
    facebook: {
      admins: ['1031588996', '100006909940815'],
      appId: '193878777324687',
      pages: '14619952769'
    },
    openGraph: {
      image: 'https://markup.hbvl.be/extra/assets/img/dummy-social.gif',
      locale: 'nl_BE',
      siteName: 'Het Belang van Limburg'
    },
    title: 'Aboshop - Het Belang van Limburg - Alle formules en prijzen'
  },
  lt: {
    description: 'Luxembourg Times subscription shop',
    openGraph: {
      image: 'https://abos.wort.lu/wp-content/uploads/2021/05/luxtimes_plus_yellow.png',
      locale: 'en_US',
      siteName: 'Mediahuis Luxembourg S.A.'
    },
    title: 'Luxembourg Times - Comparison - Mediahuis Luxembourg S.A.'
  },
  lw: {
    description:
      'Sie möchten das Luxemburger Wort als Druckausgabe, digital oder als Druckausgabe und digital? Hier finden Sie das perfekte Abo für Sie. Jetzt entdecken!',
    openGraph: {
      locale: 'de_DE',
      siteName: 'Mediahuis Luxembourg S.A.'
    },
    title: 'Alle Luxemburger Wort Abonnements auf einen Blick'
  },
  nb: {
    description:
      'Vergelijk abonnementsformules en kies voor papier en/of digitaal. Grote nieuwsfeiten, regio- en sportnieuws en consumentenadvies waar je écht wat aan hebt.',
    facebook: {
      admins: ['1031588996', '100006909940815'],
      appId: '152476521481961',
      pages: '37823307325'
    },
    openGraph: {
      image: 'https://markup.nieuwsblad.be/extra/assets/img/dummy-social.gif',
      locale: 'nl_BE',
      siteName: 'Het Nieuwsblad'
    },
    title: 'Abonnement Het Nieuwsblad - Alle formules en prijzen - Het Nieuwsblad'
  },
  tc: {
    description: 'Abo-Shop des Luxemburger Wort',
    openGraph: {
      locale: 'de_DE',
      siteName: 'Mediahuis Luxembourg S.A.'
    },
    title: 'Télécran - Mediahuis Luxembourg S.A.'
  }
};

const didomiLanguages: Record<string, 'de' | 'en' | 'nl'> = {
  'de-de': 'de',
  'en-gb': 'en',
  'nl-be': 'nl',
  'nl-nl': 'nl'
};

const mergePageHeadProps = (headProps: PageHeadProps | null): PageHeadProps => {
  const pageHeadProps: PageHeadProps = defaultHeadConfig[MH_BRAND];

  if (headProps) {
    Object.entries(headProps).forEach(([key, value]) => {
      if (value) {
        pageHeadProps[key as keyof PageHeadProps] = value;
      }
    });
  }

  return pageHeadProps;
};

export interface DefaultPageProps {
  contentBottom: Array<ContentLink>;
  contentTop: Array<ContentLink>;
  headProps: PageHeadProps | null;
}

export type WithDefaultPageProps<T> = T & DefaultPageProps;

const App = ({ Component, pageProps }: AppProps<DefaultPageProps>) => {
  const { contentBottom, contentTop, headProps } = pageProps;

  const { lang } = useTranslation();

  return (
    <AuthenticationProvider>
      <ServicesHooksProvider coreInstance={servicesCoreInstance} queryClient={queryClient}>
        <TrackingProvider>
          <PaymentProvider serviceProvider={PAYMENT_PROVIDER}>
            <PageViewTracker pageProps={pageProps} />

            <PageLayout contentLinksBottom={contentBottom} contentLinksTop={contentTop}>
              <PageHead {...mergePageHeadProps(headProps)} />
              <Component {...pageProps} />
            </PageLayout>

            <Didomi language={didomiLanguages[lang] ?? 'en'} />
            <GtmScript brand={MH_BRAND} enviroment={MH_ENV} />
          </PaymentProvider>
        </TrackingProvider>
      </ServicesHooksProvider>
    </AuthenticationProvider>
  );
};

export default App;
