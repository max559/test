import type { Order, Voucher, VoucherValidation } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';
import type { RefObject } from 'react';

import type { VoucherFormValues } from 'forms';

import { Button, Heading, Paragraph } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE } from '@subscriber/services-core';
import {
  useCreateOrder,
  useOfferItem,
  useOfferItemBySlug,
  useValidateVoucher
} from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';

import { OrderSummary } from 'components/OrderSummary';
import { useTracking } from 'features/tracking';
import { VoucherForm } from 'forms';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/voucher';

interface VoucherPageProps {
  code: string | null;
}

export const getServerSideProps: GetServerSideProps<VoucherPageProps> = async context => {
  const { code } = context.query;

  return {
    props: {
      code: code ? (code as string) : null
    }
  };
};

const VoucherPage: NextPage<VoucherPageProps> = ({
  code
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();
  const { utmSessionData } = useTracking();
  const { t } = useTranslation(translationNamespace);

  const [voucher, setVoucher] = useState<Voucher | undefined>();

  const createOrderMutation = useCreateOrder();
  const redeemFormulaQuery = useOfferItem({
    id: voucher?.redeemFormulaId,
    type: OFFER_ITEM_TYPE.SubscriptionFormula
  });
  const offerTypeQuery = useOfferItemBySlug(redeemFormulaQuery.data?.parent);
  const validateVoucher = useValidateVoucher();

  const voucherFormRef: RefObject<FormikProps<VoucherFormValues>> =
    useRef<FormikProps<VoucherFormValues>>(null);

  useEffect(() => {
    if (createOrderMutation.isSuccess && validateVoucher.isSuccess) {
      router.push({
        pathname: '/checkout/voucher-redemption',
        query: {
          orderId: createOrderMutation.data.id,
          orderHash: createOrderMutation.data.orderHash
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createOrderMutation.isSuccess, validateVoucher.isSuccess]);

  return (
    <>
      <Heading className="mb-8" level={3}>
        {t('title')}
      </Heading>

      <Sidebar gap={4}>
        <Sidebar.Main>
          <div className="flex flex-col gap-4">
            <Paragraph className="mb-4">{t('headerText')}</Paragraph>

            <VoucherForm
              autoSubmit
              formRef={voucherFormRef}
              initialValues={{ code: code || '' }}
              onSubmit={setVoucher}
              onValidate={(isValid: boolean) => {
                if (!isValid) {
                  setVoucher(undefined);
                }
              }}
            />

            <Button
              appearance="primary"
              disabled={
                createOrderMutation.isError ||
                createOrderMutation.isLoading ||
                validateVoucher.isError ||
                validateVoucher.isLoading
              }
              onClick={() => {
                if (redeemFormulaQuery.isSuccess) {
                  const order = servicesCoreInstance.orderUtil.generateOrder(
                    redeemFormulaQuery.data,
                    'voucherredemption',
                    undefined,
                    utmSessionData
                  );
                  createOrderMutation.mutateAsync(order).then((createdOrder: Order) => {
                    const voucherValidation = createdOrder.voucherValidation as VoucherValidation;
                    voucherValidation.data = { voucherCode: voucher?.code };
                    validateVoucher.mutate(voucherValidation);
                  });
                }
              }}
            >
              {t('offerSummary.primary')}
            </Button>
          </div>
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {offerTypeQuery.isSuccess && <OrderSummary subscriptionTypeItem={offerTypeQuery.data} />}
        </Sidebar.Aside>
      </Sidebar>
    </>
  );
};

export default VoucherPage;
