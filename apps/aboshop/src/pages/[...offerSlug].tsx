import type {
  ContentLinkResult,
  Offer,
  OfferItem,
  SortableFeature
} from '@subscriber/services-core';
import type { GetStaticPaths, GetStaticProps, InferGetStaticPropsType, NextPage } from 'next';
import type { ReactElement } from 'react';

import type { WithDefaultPageProps } from 'pages/_app';

import { Button, Caption, Heading } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE } from '@subscriber/services-core';
import { useCreateOrder } from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';
import Markdown from 'markdown-to-jsx';
import { useRouter } from 'next/router';
import { Fragment, useEffect, useState } from 'react';

import { OfferFormulas } from 'components/OfferFormulas';
import { OrderSummary } from 'components/OrderSummary';
import { Spinner } from 'components/Spinner';
import { Vitrine } from 'components/Vitrine';
import { useTracking } from 'features/tracking';
import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

const translationNamespace = 'pages/offer';

interface OfferPageProps {
  formulas: OfferPageFormulasProps | null;
  vitrine: OfferPageVitrineProps | null;
}

export const getStaticPaths: GetStaticPaths = async () => {
  let possibleNextSteps: Array<Array<string>> = [];
  let vitrineOffer: Offer | null = null;

  vitrineOffer = await servicesCoreInstance.offerService
    .getVitrineOffer()
    .then(response => response.data)
    .catch(() => null);

  if (vitrineOffer) {
    possibleNextSteps =
      vitrineOffer.items?.reduce((acc: Array<Array<string>>, offerItem: OfferItem) => {
        if (offerItem.nextStep && offerItem.nextStep.indexOf('checkout') === -1) {
          try {
            new URL(offerItem.nextStep);
          } catch {
            return [
              ...acc,
              offerItem.nextStep.split('/').filter(splitValue => Boolean(splitValue))
            ];
          }
        }
        return acc;
      }, []) || [];
  }

  return {
    fallback: true,
    paths: possibleNextSteps.map((nextStep: Array<string>) => ({
      params: { offerSlug: nextStep }
    }))
  };
};

export const getStaticProps: GetStaticProps<WithDefaultPageProps<OfferPageProps>> = async ({
  params
}) => {
  const offerSlug = encodeURIComponent((params?.offerSlug as Array<string>).join('/'));

  let contentLinkResult: ContentLinkResult | null = null;
  let formulas: OfferPageFormulasProps | null = null;
  let vitrine: OfferPageVitrineProps | null = null;

  const offer = await servicesCoreInstance.offerService
    .getOfferBySlug({ slug: offerSlug })
    .then(response => response.data)
    .catch(() => null);

  if (offer?.active === false) {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    };
  }

  if (offer) {
    const hasSubscriptionTypeItems = Boolean(
      offer.items?.find(item => item.type === OFFER_ITEM_TYPE.SubscriptionType)
    );

    if (hasSubscriptionTypeItems) {
      vitrine = {
        offer
      };
    } else {
      const parentSlugs =
        offer.items?.reduce((acc: Record<string, OfferItem | null>, offerItem) => {
          if (offerItem.parent && !acc[offerItem.parent]) {
            acc[offerItem.parent] = null;
          }

          return acc;
        }, {}) ?? {};

      const parentResults = await Promise.all(
        Object.keys(parentSlugs).map(slug =>
          servicesCoreInstance.offerService
            .getOfferItemBySlug(slug)
            .then(response => response.data)
            .catch(() => null)
        )
      );

      formulas = {
        offer,
        parents: Object.keys(parentSlugs).reduce(
          (acc: Record<string, OfferItem | null>, slug, slugIndex) => {
            acc[slug] = parentResults[slugIndex];

            return acc;
          },
          {}
        )
      };
    }

    contentLinkResult = await servicesCoreInstance.contentService
      .getContentLinks(`Offer_${offer.id}`)
      .then(response => response.data)
      .catch(() => null);
  }

  return {
    props: {
      contentBottom: contentLinkResult?.contentLinks?.bottom || [],
      contentTop: contentLinkResult?.contentLinks?.top || [],
      formulas,
      headProps: {
        description: offer?.metaDescription || '',
        title: offer?.metaTitle || ''
      },
      vitrine
    },
    revalidate: 60
  };
};

const OfferPage: NextPage<WithDefaultPageProps<OfferPageProps>> = ({
  formulas,
  vitrine
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  const router = useRouter();

  useEffect(() => {
    if (formulas === null && vitrine === null && !router.isFallback) {
      router.push('/');
    }
  }, [formulas, vitrine, router]);

  if (formulas) {
    return <OfferPageFormulas offer={formulas.offer} parents={formulas.parents} />;
  }

  if (vitrine) {
    return <OfferPageVitrine offer={vitrine.offer} />;
  }

  return <Spinner text="" />;
};

/* Offer Page - Vitrine */

interface OfferPageVitrineProps {
  offer: Offer;
}

const OfferPageVitrine = ({ offer }: OfferPageVitrineProps): ReactElement => {
  const offerFeatures: Array<SortableFeature> = (offer.items ?? []).reduce(
    (acc: Array<SortableFeature>, offerItem: OfferItem) => {
      if (offerItem.features) {
        return acc.concat(
          offerItem.features.filter(
            (feature: SortableFeature) =>
              !acc.find((accItem: SortableFeature) => feature.id === accItem.id)
          )
        );
      }

      return acc;
    },
    []
  );

  return <Vitrine combinedFeatures={offerFeatures} offer={offer} />;
};

/* Offer Page - Formulas */

interface OfferPageFormulasProps {
  offer: Offer;
  parents: Record<string, OfferItem | null>;
}

const OfferPageFormulas = ({ offer, parents }: OfferPageFormulasProps): ReactElement => {
  const router = useRouter();
  const {
    utmSessionData,
    trackGlitrEvent,
    trackGtmEcommerceEvent,
    trackGtmEcommerceMutipleItemsEvent
  } = useTracking();
  const { t } = useTranslation(translationNamespace);

  const [checkedFormula, setCheckedFormula] = useState<OfferItem | undefined>(
    offer.items ? offer.items[0] : undefined
  );
  const [isCreating, setIsCreating] = useState<boolean>(false);

  const createOrderMutation = useCreateOrder();

  const parentType: OfferItem | null = parents[checkedFormula?.parent ?? ''];

  useEffect(() => {
    trackGlitrEvent({
      eventAction: 'show',
      eventLabel: `subscriptionshop/cta-offer-formule?typeproduct=${parentType?.title ?? ''}`
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (checkedFormula && parentType) {
      trackGtmEcommerceEvent('view_item', offer, parentType, checkedFormula);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checkedFormula]);

  useEffect(() => {
    trackGtmEcommerceMutipleItemsEvent('view_item_list', offer, parents);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offer]);

  useEffect(() => {
    if (createOrderMutation.isSuccess) {
      if (createOrderMutation.data.type === 'acquisition') {
        router.push({
          pathname: '/checkout/acquisition',
          query: {
            orderId: createOrderMutation.data.id,
            orderHash: createOrderMutation.data.orderHash
          }
        });
      }
      if (createOrderMutation.data.type === 'renewal') {
        router.push({
          pathname: '/checkout/renewal',
          query: {
            orderId: createOrderMutation.data.id,
            orderHash: createOrderMutation.data.orderHash
          }
        });
      }
      if (createOrderMutation.data.type === 'winback') {
        router.push({
          pathname: '/checkout/winback',
          query: {
            orderId: createOrderMutation.data.id,
            orderHash: createOrderMutation.data.orderHash
          }
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [createOrderMutation.isSuccess]);

  function handleBackToVitrine() {
    if (checkedFormula && parentType) {
      trackGlitrEvent({
        eventAction: 'click-edit-product',
        eventLabel: `subscriptionshop/order?typeproduct=${parentType.title ?? ''}`
      });
    }

    router.push('/');
  }

  function handleCreateOrder() {
    if (checkedFormula) {
      setIsCreating(true);

      if (parentType) {
        trackGlitrEvent({
          eventAction: 'click-next-step',
          eventLabel: `subscriptionshop/cta-offer-formule?typeproduct=${parentType.title ?? ''}`
        });
        trackGtmEcommerceEvent('add_to_cart', offer, parentType, checkedFormula);
      }

      if (checkedFormula.nextStep) {
        try {
          const url = new URL(checkedFormula.nextStep);

          url.search = Object.entries(utmSessionData)
            .reduce((acc, [utmKey, utmValue]) => {
              if (utmValue) {
                acc.append(utmKey, utmValue.toString());
              }

              return acc;
            }, new URLSearchParams())
            .toString();
          window.location.assign(url);
        } catch {
          window.location.assign(checkedFormula.nextStep);
        }
      } else {
        const order = servicesCoreInstance.orderUtil.generateOrder(
          checkedFormula,
          offer.type,
          offer.id,
          utmSessionData
        );

        createOrderMutation.mutate(order);
      }
    }
  }

  return (
    <>
      <Heading className="pb-6" level={4} size="lg">
        {offer.title}
      </Heading>

      <Sidebar gap="5vw">
        <Sidebar.Main>
          <div className="flex flex-col gap-4">
            <OfferFormulas
              checkedItem={checkedFormula}
              items={offer.items || []}
              onChange={(item: OfferItem) => setCheckedFormula(item)}
            />

            <div className="flex gap-6 justify-end">
              <Button
                appearance="secondary"
                data-testid="btn-back-home"
                width="full"
                onClick={handleBackToVitrine}
              >
                {t('offerSummary.secondary')}
              </Button>
              <Button
                appearance="primary"
                data-testid="btn-create-order"
                disabled={!checkedFormula}
                loading={isCreating}
                width="full"
                onClick={handleCreateOrder}
              >
                {t('offerSummary.primary')}
              </Button>
            </div>

            <Caption className="text-center" size="sm">
              <Markdown options={{ forceInline: true, wrapper: Fragment }}>
                {offer.extra || ''}
              </Markdown>
            </Caption>
          </div>
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {parentType && <OrderSummary subscriptionTypeItem={parentType} />}
        </Sidebar.Aside>
      </Sidebar>
    </>
  );
};

export default OfferPage;
