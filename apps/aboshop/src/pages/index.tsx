import type {
  ContentLinkResult,
  Offer,
  OfferItem,
  SortableFeature
} from '@subscriber/services-core';
import type { GetStaticProps, InferGetStaticPropsType, NextPage } from 'next';

import type { WithDefaultPageProps } from 'pages/_app';

import { Vitrine } from 'components/Vitrine';
import { servicesCoreInstance } from 'globals';

interface HomePageProps {
  offerFeatures: Array<SortableFeature>;
  offer: Offer | null;
}

export const getStaticProps: GetStaticProps<WithDefaultPageProps<HomePageProps>> = async () => {
  let contentLinkResult: ContentLinkResult | null = null;
  let offerFeatures: Array<SortableFeature> = [];
  let vitrineOffer: Offer | null = null;

  vitrineOffer = await servicesCoreInstance.offerService
    .getVitrineOffer()
    .then(response => response.data)
    .catch(() => null);

  if (vitrineOffer) {
    contentLinkResult = await servicesCoreInstance.contentService
      .getContentLinks(`Offer_${vitrineOffer.id}`)
      .then(response => response.data)
      .catch(() => null);

    offerFeatures = (vitrineOffer.items ?? []).reduce(
      (acc: Array<SortableFeature>, offerItem: OfferItem) => {
        if (offerItem.features) {
          return acc.concat(
            offerItem.features.filter(
              (feature: SortableFeature) =>
                !acc.find((accItem: SortableFeature) => feature.id === accItem.id)
            )
          );
        }

        return acc;
      },
      []
    );
  }

  return {
    props: {
      contentBottom: contentLinkResult?.contentLinks?.bottom || [],
      contentTop: contentLinkResult?.contentLinks?.top || [],
      headProps: {
        description: vitrineOffer?.metaDescription || '',
        title: vitrineOffer?.metaTitle || ''
      },
      offerFeatures,
      offer: vitrineOffer
    },
    revalidate: 60
  };
};

const HomePage: NextPage<WithDefaultPageProps<HomePageProps>> = ({
  offerFeatures,
  offer
}: InferGetStaticPropsType<typeof getStaticProps>) => {
  if (offer) {
    return <Vitrine combinedFeatures={offerFeatures} offer={offer} />;
  }

  return null;
};

export default HomePage;
