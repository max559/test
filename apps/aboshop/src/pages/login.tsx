import type { GetServerSideProps, InferGetServerSidePropsType, NextPage } from 'next';

import { Heading, Paragraph } from '@mediahuis/chameleon-react';
import { LoginWizard } from '@subscriber/authentication';
import { Center, Flex } from '@subscriber/subscriber-ui';
import { useRouter } from 'next/router';

import { RedirectToSignIn } from 'features/authentication';

interface LoginPageProps {
  authHash: string | null;
  redirectUrl: string | null;
}

export const getServerSideProps: GetServerSideProps<LoginPageProps> = async context => {
  const { hash, redirectUrl } = context.query;

  if (AUTHENTICATION === 'none') {
    return {
      redirect: {
        destination: '/',
        permanent: false
      }
    };
  }

  return {
    props: {
      authHash: hash ? (hash as string) : null,
      redirectUrl: redirectUrl ? (redirectUrl as string) : null
    }
  };
};

const LoginPage: NextPage<LoginPageProps> = ({
  authHash,
  redirectUrl
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();

  if (AUTHENTICATION === 'default') {
    if (process.env.NODE_ENV === 'development') {
      return (
        <Flex flexDirection="column" gap={2}>
          <Heading level={3}>Development mode</Heading>
          <Paragraph>
            It is not possible to login in dev mode. Authenticate on the test environment and use
            that cookie for local development.
          </Paragraph>
        </Flex>
      );
    }

    return (
      <Center maxInlineSize="40rem">
        <LoginWizard
          authHash={authHash || undefined}
          onCompleteLogin={() => router.push(redirectUrl || '/')}
          onCompleteRegistration={() => router.push(redirectUrl || '/')}
        />
      </Center>
    );
  }

  return <RedirectToSignIn />;
};

export default LoginPage;
