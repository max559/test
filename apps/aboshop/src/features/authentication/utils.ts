import type { CiamUserInfo } from '@subscriber/ciam';
import type { AccountInfoResponse, UserInfo as LuxUserInfo } from '@subscriber/services-core';

import type { UserInfo } from './types';

import { CIAM_AUTHENTICATION_STATUS } from '@subscriber/ciam';

import { AUTHENTICATION_STATUS } from './types';

export const mapAccountInfoResponse = (accountInfoResponse: AccountInfoResponse): UserInfo => ({
  address: {
    box: accountInfoResponse.Box,
    city: accountInfoResponse.City,
    country: accountInfoResponse.Country,
    houseNumber: accountInfoResponse.HouseNumber,
    postalCode: accountInfoResponse.Zipcode,
    street: accountInfoResponse.Street
  },
  email: accountInfoResponse.EmailAddress,
  firstName: accountInfoResponse.FirstName,
  id: accountInfoResponse.AccountGuid,
  lastName: accountInfoResponse.Name
});

export const mapCiamAuthenticationStatus = (
  ciamAuthStatus: CIAM_AUTHENTICATION_STATUS
): AUTHENTICATION_STATUS => {
  switch (ciamAuthStatus) {
    case CIAM_AUTHENTICATION_STATUS.AUTHENTICATED:
      return AUTHENTICATION_STATUS.AUTHENTICATED;
    case CIAM_AUTHENTICATION_STATUS.ERROR:
      return AUTHENTICATION_STATUS.ERROR;
    case CIAM_AUTHENTICATION_STATUS.LOADING:
      return AUTHENTICATION_STATUS.LOADING;
    case CIAM_AUTHENTICATION_STATUS.UNAUTHENTICATED:
      return AUTHENTICATION_STATUS.UNAUTHENTICATED;
    default:
      return AUTHENTICATION_STATUS.UNKNOWN;
  }
};

export const mapCiamUserInfo = (ciamUserInfo: CiamUserInfo): UserInfo => ({
  address: {
    box: ciamUserInfo.fullIdentity.address?.houseNumberExtension,
    city: ciamUserInfo.fullIdentity.address?.city,
    country: ciamUserInfo.fullIdentity.address?.countryCode,
    houseNumber: ciamUserInfo.fullIdentity.address?.houseNumber,
    postalCode: ciamUserInfo.fullIdentity.address?.postalCode,
    street: ciamUserInfo.fullIdentity.address?.street
  },
  email: ciamUserInfo.idToken.email,
  firstName: ciamUserInfo.idToken.given_name,
  id: ciamUserInfo.idToken.sub as string,
  idToken: ciamUserInfo.tokens.id || undefined,
  lastName: ciamUserInfo.idToken.family_name
});

export const mapLuxUserInfo = (luxUserInfo: LuxUserInfo): UserInfo => ({
  address: {
    country: 'LU'
  },
  email: luxUserInfo.user.user.email,
  firstName: luxUserInfo.user.user.contactDetails.firstName,
  id: luxUserInfo.user.user.id || '0',
  lastName: luxUserInfo.user.user.contactDetails.lastName
});
