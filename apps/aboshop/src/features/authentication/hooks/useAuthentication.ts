import type { UserInfo } from '../types';

import { useAuthentication as useAuth } from '@subscriber/authentication';
import { useAuthenticationLux as useAuthLux } from '@subscriber/authentication-lux';
import { useCiam } from '@subscriber/ciam';

import { AUTHENTICATION_STATUS } from '../types';
import {
  mapAccountInfoResponse,
  mapCiamAuthenticationStatus,
  mapCiamUserInfo,
  mapLuxUserInfo
} from '../utils';

export const useAuthentication = () => {
  const {
    authenticationStatus: authStatus,
    userInfo: authUserInfo,
    getUserInfo: getUserInfoBe,
    redirectToSignOut: redirectToSignOutBe
  } = useAuth();
  const {
    authenticationStatus: authLuxStatus,
    userInfo: authLuxUserInfo,
    getUserInfo: getUserInfoLux,
    redirectToSignOut: redirectToSignOutLux
  } = useAuthLux();
  const {
    authenticationStatus: ciamAuthStatus,
    userInfo: ciamUserInfo,
    getUserInfo: getUserInfoCiam,
    redirectToSignOut: redirectToSignOutCiam
  } = useCiam();

  function getUserInfo(): Promise<UserInfo | null> {
    if (AUTHENTICATION === 'ciam') {
      return getUserInfoCiam().then(ciamUserInfo =>
        ciamUserInfo ? mapCiamUserInfo(ciamUserInfo) : null
      );
    }
    if (AUTHENTICATION === 'default') {
      return getUserInfoBe().then(accountInfoResponse =>
        accountInfoResponse ? mapAccountInfoResponse(accountInfoResponse) : null
      );
    }
    if (AUTHENTICATION === 'lux') {
      return getUserInfoLux().then(userInfoLux =>
        userInfoLux ? mapLuxUserInfo(userInfoLux) : null
      );
    }

    return Promise.reject();
  }

  function redirectToSignOut(): void {
    if (AUTHENTICATION === 'ciam') {
      redirectToSignOutCiam();
    }
    if (AUTHENTICATION === 'default') {
      redirectToSignOutBe();
    }
    if (AUTHENTICATION === 'lux') {
      redirectToSignOutLux();
    }

    return;
  }

  function resolveAuthenticationStatus(): AUTHENTICATION_STATUS {
    if (AUTHENTICATION === 'ciam') {
      return mapCiamAuthenticationStatus(ciamAuthStatus);
    }
    if (AUTHENTICATION === 'default') {
      return authStatus;
    }
    if (AUTHENTICATION === 'lux') {
      return authLuxStatus;
    }

    return AUTHENTICATION_STATUS.UNKNOWN;
  }

  function resolveUserInfo(): UserInfo | null {
    if (AUTHENTICATION === 'ciam' && ciamUserInfo) {
      return mapCiamUserInfo(ciamUserInfo);
    }
    if (AUTHENTICATION === 'default' && authUserInfo) {
      return mapAccountInfoResponse(authUserInfo);
    }
    if (AUTHENTICATION === 'lux' && authLuxUserInfo) {
      return mapLuxUserInfo(authLuxUserInfo);
    }

    return null;
  }

  return {
    authenticationStatus: resolveAuthenticationStatus(),
    userInfo: resolveUserInfo(),
    getUserInfo,
    redirectToSignOut
  };
};
