import type { PropsWithChildren, ReactElement } from 'react';

import { Brand as AuthBrand, AuthProvider } from '@subscriber/authentication';
import { Brand as AuthLuxBrand, AuthLuxProvider } from '@subscriber/authentication-lux';
import { Brand as CiamBrand, BrandGroup as CiamBrandGroup, CiamProvider } from '@subscriber/ciam';

const AuthenticationProvider = ({ children }: PropsWithChildren): ReactElement => {
  return (
    <AuthProvider autoAuthentication={false} brand={MH_BRAND as AuthBrand} environment={MH_ENV}>
      <AuthLuxProvider
        autoAuthentication={false}
        brand={MH_BRAND as AuthLuxBrand}
        environment={MH_ENV}
      >
        <CiamProvider
          autoAuthentication={false}
          brand={MH_BRAND as CiamBrand}
          brandGroup={CIAM_BRAND_GROUP as CiamBrandGroup}
          clientId={CIAM_CLIENT_ID}
          environment={MH_ENV}
        >
          {children}
        </CiamProvider>
      </AuthLuxProvider>
    </AuthProvider>
  );
};

export default AuthenticationProvider;
