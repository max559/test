import type { ReactElement } from 'react';

import { RedirectToSignIn as RedirectToSignInLux } from '@subscriber/authentication-lux';
import { CIAM_IDENTITY_LEVEL, RedirectToSignIn as RedirectToSignInCiam } from '@subscriber/ciam';
import { useRouter } from 'next/router';

const RedirectToSignIn = (): ReactElement | null => {
  const router = useRouter();

  if (AUTHENTICATION === 'ciam') {
    return <RedirectToSignInCiam options={{ registerIdentityLevel: CIAM_IDENTITY_LEVEL.MH5 }} />;
  }

  if (AUTHENTICATION === 'default') {
    router.push({
      pathname: '/login',
      query: {
        redirectUrl: router.asPath
      }
    });
  }

  if (AUTHENTICATION === 'lux') {
    return <RedirectToSignInLux />;
  }

  return null;
};

export default RedirectToSignIn;
