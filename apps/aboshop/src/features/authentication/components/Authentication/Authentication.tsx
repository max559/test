import type { PropsWithChildren, ReactElement } from 'react';

import { Authentication as DefaultAuthentication } from '@subscriber/authentication';
import { Authentication as LuxAuthentication } from '@subscriber/authentication-lux';
import {
  CIAM_AUTHENTICATION_STATUS,
  CIAM_IDENTITY_LEVEL,
  Authentication as CiamAuthentication,
  RedirectToSignIn,
  useCiam
} from '@subscriber/ciam';

import { Spinner } from 'components/Spinner';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/authentication';

const Authentication = ({ children }: PropsWithChildren): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const { authenticationStatus: ciamAuthStatus, userInfo: ciamUserInfo } = useCiam();

  if (AUTHENTICATION === 'ciam') {
    return (
      <CiamAuthentication fallback={<Spinner text={t('authenticating')} />}>
        {ciamAuthStatus === CIAM_AUTHENTICATION_STATUS.AUTHENTICATED &&
        !ciamUserInfo?.accessToken['https://mediahuis.com/identity-levels']?.includes(
          CIAM_IDENTITY_LEVEL.MH5
        ) ? (
          <RedirectToSignIn options={{ loginIdentityLevel: CIAM_IDENTITY_LEVEL.MH5 }} />
        ) : (
          children
        )}
      </CiamAuthentication>
    );
  }

  if (AUTHENTICATION === 'default') {
    return (
      <DefaultAuthentication fallback={<Spinner text={t('authenticating')} />}>
        {children}
      </DefaultAuthentication>
    );
  }

  if (AUTHENTICATION === 'lux') {
    return (
      <LuxAuthentication fallback={<Spinner text={t('authenticating')} />}>
        {children}
      </LuxAuthentication>
    );
  }

  return <>{children}</>;
};

export default Authentication;
