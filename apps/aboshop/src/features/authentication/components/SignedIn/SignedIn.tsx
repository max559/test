import type { PropsWithChildren, ReactElement } from 'react';

import { AUTHENTICATION_STATUS } from '@subscriber/authentication';

import { useAuthentication } from '../../hooks/useAuthentication';

const SignedIn = ({ children }: PropsWithChildren): ReactElement | null => {
  const { authenticationStatus } = useAuthentication();

  if (authenticationStatus === AUTHENTICATION_STATUS.AUTHENTICATED) {
    return <>{children}</>;
  }

  return null;
};

export default SignedIn;
