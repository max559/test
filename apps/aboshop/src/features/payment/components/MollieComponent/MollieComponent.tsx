import type { ReactElement } from 'react';

import type {
  MollieComponent as IMollieComponent,
  MollieComponentEvent,
  MollieComponentType
} from '../../types';

import { useEffect, useRef } from 'react';

import { usePaymentStore } from '../../context';
import { useElementVisibility } from '../../hooks/useElementVisibility';

const containerId: Record<MollieComponentType, string> = {
  cardHolder: 'mollie-card-number',
  cardNumber: 'mollie-card-number',
  expiryDate: 'mollie-expiry-date',
  verificationCode: 'mollie-verification-code'
};

interface MollieComponentProps {
  type: MollieComponentType;
  onChange: (event: MollieComponentEvent) => void;
}

const MollieComponent = ({ type, onChange }: MollieComponentProps): ReactElement => {
  const isMollieReady = usePaymentStore(state => state.isReady);

  const componentRef = useRef<IMollieComponent | null>(null);
  const containerRef = useRef<HTMLDivElement | null>(null);
  const isMountedRef = useRef<boolean>(false);

  const isElementVisible = useElementVisibility(containerRef);

  useEffect(() => {
    if (isMollieReady) {
      createComponent();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isMollieReady]);

  useEffect(() => {
    if (isElementVisible) {
      mountComponent();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isElementVisible]);

  function createComponent() {
    if (!componentRef.current && !isMountedRef.current) {
      componentRef.current = window.MollieObject?.createComponent(type) || null;
    }
  }

  function mountComponent() {
    if (componentRef.current && containerRef.current && !isMountedRef.current) {
      componentRef.current.mount(containerRef.current);
      componentRef.current.addEventListener('change', onChange);

      isMountedRef.current = true;
    }
  }

  return <div id={containerId[type]} ref={containerRef}></div>;
};

export default MollieComponent;
