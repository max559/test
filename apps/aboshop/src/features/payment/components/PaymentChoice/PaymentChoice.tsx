import type { ReactElement } from 'react';

import { Banner, Choice, Heading, List, ListItem } from '@mediahuis/chameleon-react';
import { PAYMENT_OPTION } from '@subscriber/services-core';
import Trans from 'next-translate/Trans';
import { useState } from 'react';

import { useTranslation } from 'hooks';

import { PaymentForm } from '../PaymentForm';
import config from './config';

const translationNamespace = 'features/payment';

interface PaymentChoiceProps {
  checked: boolean;
  name: string;
  option: PAYMENT_OPTION;
  onChange: () => void;
}

const PaymentChoice = ({ checked, name, option, onChange }: PaymentChoiceProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [paymentError, setPaymentError] = useState<boolean>(false);

  return (
    <Choice
      checked={checked}
      id={option}
      imageAlt={config[option].imageAlt}
      imageSrc={config[option].imageSrc}
      message={t(`paymentChoice.message.${config[option].i18nKey}`)}
      name={name}
      title={t(`paymentChoice.title.${config[option].i18nKey}`)}
      value={option}
      onChange={onChange}
    >
      <div className="flex flex-col gap-4">
        <Banner appearance="error" show={paymentError} onClose={() => setPaymentError(false)}>
          {t('paymentChoice.error')}
        </Banner>

        <PaymentForm option={option} onError={() => setPaymentError(true)} />

        <div className="flex flex-col gap-4">
          <Heading level={4} size="sm">
            {t(`paymentChoice.listHeading.${config[option].i18nKey}`)}
          </Heading>

          <List>
            <Trans
              components={{
                ListItem: <ListItem className="font-normal text-base" />
              }}
              i18nKey={`${translationNamespace}:${MH_BRAND}.paymentChoice.listItems.${config[option].i18nKey}`}
              fallback={`${translationNamespace}:base.paymentChoice.listItems.${config[option].i18nKey}`}
            />
          </List>
        </div>
      </div>
    </Choice>
  );
};

export default PaymentChoice;
