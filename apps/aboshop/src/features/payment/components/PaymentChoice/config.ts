import { PAYMENT_OPTION } from '@subscriber/services-core';

interface PaymentChoiceConfig {
  i18nKey: string;
  imageAlt: string;
  imageSrc: string;
}

const paymentChoiceConfig: Record<PAYMENT_OPTION, PaymentChoiceConfig> = {
  Bancontact: {
    i18nKey: 'bancontact',
    imageAlt: 'Bancontact',
    imageSrc: '/images/bancontact.svg'
  },
  Belfius: {
    i18nKey: 'belfius',
    imageAlt: 'Belfius',
    imageSrc: '/images/belfius.svg'
  },
  CreditCard: {
    i18nKey: 'creditcard',
    imageAlt: 'Creditcard',
    imageSrc: '/images/creditCard.svg'
  },
  DirectDebit: {
    i18nKey: 'directdebit',
    imageAlt: 'Direct Debit',
    imageSrc: '/images/directDebit.svg'
  },
  Ideal: {
    i18nKey: 'ideal',
    imageAlt: 'Ideal',
    imageSrc: '/images/ideal.svg'
  },
  KBC: {
    i18nKey: 'kbc',
    imageAlt: 'KBC',
    imageSrc: '/images/kbc.svg'
  },
  PayPal: {
    i18nKey: 'paypal',
    imageAlt: 'PayPal',
    imageSrc: '/images/paypal.svg'
  },
  WireTransfer: {
    i18nKey: 'wiretransfer',
    imageAlt: 'WireTransfer',
    imageSrc: '/images/transfer.svg'
  }
};

export default paymentChoiceConfig;
