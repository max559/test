import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { BankAccountFormValues, UserTermsFormValues } from '../../forms';

import { Button } from '@mediahuis/chameleon-react';
import { PAYMENT_OPTION } from '@subscriber/services-core';
import { useRef } from 'react';

import { Spinner } from 'components/Spinner';
import { useTranslation } from 'hooks';

import { usePaymentStore } from '../../context';
import {
  BankAccountForm,
  DatatransCreditcardForm,
  MollieCreditcardForm,
  UserTermsForm
} from '../../forms';
import { usePaymentSessionStore } from '../../session';

const translationNamespace = 'features/payment';

interface PaymentFormProps {
  option: PAYMENT_OPTION;
  onError: () => void;
}

const PaymentForm = ({ option, onError }: PaymentFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const serviceProvider = usePaymentStore(state => state.serviceProvider);
  const isSessionLoading = usePaymentSessionStore(state => state.isLoading);
  const sessionPayment = usePaymentSessionStore(state => state.payment);
  const {
    createPayment,
    submitBancontact,
    submitBelfius,
    submitDatatransCreditcard,
    submitDirectDebit,
    submitIdeal,
    submitKbc,
    submitMollieCreditcard,
    submitPayPal,
    submitWireTransfer
  } = usePaymentSessionStore(state => ({
    createPayment: state.createPayment,
    submitBancontact: state.submitBancontact,
    submitBelfius: state.submitBelfius,
    submitDatatransCreditcard: state.submitDatatransCreditcard,
    submitDirectDebit: state.submitDirectDebit,
    submitIdeal: state.submitIdeal,
    submitKbc: state.submitKbc,
    submitMollieCreditcard: state.submitMollieCreditcard,
    submitPayPal: state.submitPayPal,
    submitWireTransfer : state.submitWireTransfer
  }));

  const bankAccountFormRef = useRef<FormikProps<BankAccountFormValues>>(null);
  const datatransCreditcardFormRef = useRef<HTMLFormElement | null>(null);
  const mollieCreditcardFormRef = useRef<HTMLFormElement | null>(null);
  const userTermsFormRef = useRef<FormikProps<UserTermsFormValues>>(null);

  function handleFormSubmit() {
    bankAccountFormRef.current?.submitForm();
    mollieCreditcardFormRef.current?.dispatchEvent(
      new Event('submit', { bubbles: true, cancelable: true })
    );
    datatransCreditcardFormRef.current?.dispatchEvent(
      new Event('submit', { bubbles: true, cancelable: true })
    );
    userTermsFormRef.current?.submitForm();
  }

  if (isSessionLoading) {
    return <Spinner text={t('paymentForm.sessionLoading')} />;
  }

  return (
    <>
      {option === PAYMENT_OPTION.Bancontact && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitBancontact}
        />
      )}

      {option === PAYMENT_OPTION.Belfius && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitBelfius}
        />
      )}

      {option === PAYMENT_OPTION.CreditCard &&
        serviceProvider === 'datatrans' &&
        sessionPayment?.id && (
          <DatatransCreditcardForm
            formRef={datatransCreditcardFormRef}
            transactionId={sessionPayment.id}
            onError={() => {
              createPayment(PAYMENT_OPTION.CreditCard);
              onError();
            }}
            onSubmit={submitDatatransCreditcard}
          />
        )}

      {option === PAYMENT_OPTION.CreditCard && serviceProvider === 'mollie' && (
        <MollieCreditcardForm formRef={mollieCreditcardFormRef} onSubmit={submitMollieCreditcard} />
      )}

      {option === PAYMENT_OPTION.DirectDebit && (
        <BankAccountForm
          formRef={bankAccountFormRef}
          initialValues={{ acceptedTerms: false, ibanNumber: 'BE' }}
          onSubmit={submitDirectDebit}
        />
      )}

      {option === PAYMENT_OPTION.Ideal && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitIdeal}
        />
      )}

      {option === PAYMENT_OPTION.KBC && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitKbc}
        />
      )}

      {option === PAYMENT_OPTION.PayPal && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitPayPal}
        />
      )}

      {option === PAYMENT_OPTION.WireTransfer && (
        <UserTermsForm
          formRef={userTermsFormRef}
          initialValues={{ acceptedTerms: false }}
          onSubmit={submitWireTransfer}
        />
      )}

      <Button appearance="primary" onClick={handleFormSubmit}>
        {t('paymentForm.submit')}
      </Button>
    </>
  );
};

export default PaymentForm;
