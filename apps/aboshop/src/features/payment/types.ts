export type PaymentServiceProvider = 'datatrans' | 'mollie';

// Datatrans

export interface DatatransErrorEvent {
  action: 'submit';
  error:
    | 'blocked_card'
    | 'client_error'
    | 'declined'
    | 'expired_card'
    | 'invalid_alias'
    | 'invalid_card'
    | 'invalid_cvv'
    | 'invalid_json_payload'
    | 'invalid_property'
    | 'invalid_setup'
    | 'invalid_sign'
    | 'invalid_transaction_status'
    | 'server_error'
    | 'soft_declined'
    | 'transaction_not_found'
    | 'unauthorized'
    | 'unknown_error'
    | 'unrecognized_property'
    | 'unsupported_card';
}

export interface DatatransSuccessEvent {
  cardInfo: {
    brand: string;
    country: string;
    issuer: string;
    type: string;
    usage: string;
  };
  redirect?: string;
  transactionId: string;
}

export interface DatatransValidateEvent {
  fields: Record<string, { length: number; valid: boolean }>;
  hasErrors: boolean;
}

// Mollie

export type Mollie = (
  profileId: string,
  options?: {
    locale: MollieLocale;
    testmode: boolean;
  }
) => MollieObject;

export type MollieLocale = 'en_US' | 'nl_NL' | 'nl_BE' | 'fr_FR' | 'fr_BE' | 'de_DE';
export type MollieComponentType = 'cardHolder' | 'cardNumber' | 'expiryDate' | 'verificationCode';

export interface MollieComponent {
  addEventListener: (
    event: 'blur' | 'change' | 'focus',
    callback: (event: MollieComponentEvent) => void
  ) => void;
  mount: (targetElement: HTMLElement | string) => void;
  unmount: () => void;
}

export interface MollieComponentEvent {
  dirty: boolean;
  error: string | null;
  touched: boolean;
  valid: boolean;
}

export interface MollieComponentStyles {
  backgroundColor?: string;
  color?: string;
  fontSize?: string | number;
  fontWeight?: string | number;
  letterSpacing?: number;
  lineHeight?: number;
  textAlign?: string;
  textDecoration?: string;
  textTransform?: string;
}

export interface MollieObject {
  createComponent: (
    type: MollieComponentType,
    options?: {
      styles: {
        base?: MollieComponentStyles;
        invalid?: MollieComponentStyles;
        valid?: MollieComponentStyles;
      };
    }
  ) => MollieComponent;
  createToken: () => Promise<{ error?: { message: string }; token?: string }>;
}
