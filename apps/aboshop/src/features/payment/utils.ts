import type { PaymentRequest, PaymentValidation } from '@subscriber/services-core';

import type { UserInfo } from 'features/authentication';

import { PAYMENT_OPTION, PAYMENT_TYPE } from '@subscriber/services-core';

interface CreatePaymentRequestParams {
  paymentInfo?: string;
  paymentOption: PAYMENT_OPTION;
  paymentType: PAYMENT_TYPE;
  paymentValidation: PaymentValidation;
  redirectUrl?: string;
  userInfo?: UserInfo | null;
}

export const createPaymentRequest = ({
  paymentInfo,
  paymentOption,
  paymentType,
  paymentValidation,
  redirectUrl,
  userInfo
}: CreatePaymentRequestParams): PaymentRequest => {
  const paymentRequest: PaymentRequest = {
    amount: paymentValidation.amount,
    currency: 'EUR',
    // description: offerTypeName,
    email: userInfo?.email,
    methods: [paymentOption],
    name:
      userInfo?.firstName && userInfo?.lastName
        ? `${userInfo.firstName} ${userInfo.lastName}`
        : undefined,
    paymentInfo,
    paymentReceiver: MH_BRAND,
    paymentType,
    redirectUrl,
    source: 'Aboshop',
    sourceReference: paymentValidation.id.toString()
  };

  if (paymentRequest.paymentType === PAYMENT_TYPE.FirstRecurring) {
    if (paymentRequest.methods[0] === PAYMENT_OPTION.Bancontact) {
      paymentRequest.amount = 0.02;
    }
    if (paymentRequest.methods[0] === PAYMENT_OPTION.Ideal) {
      paymentRequest.amount = 0.01;
    }
  }

  return paymentRequest;
};

export const resolvePaymentOptions = (
  paymentType: PAYMENT_TYPE,
  paymentValidation: PaymentValidation
): Array<PAYMENT_OPTION> => {
  if (paymentType === PAYMENT_TYPE.FirstRecurring && paymentValidation.recurringPaymentOptions) {
    return paymentValidation.recurringPaymentOptions;
  }
  if (paymentType === PAYMENT_TYPE.OneShot && paymentValidation.oneShotPaymentOptions) {
    return paymentValidation.oneShotPaymentOptions.map(option => option === PAYMENT_OPTION.DirectDebit ? PAYMENT_OPTION.WireTransfer : option);
  }

  return [];
};
