import type { StoreApi } from 'zustand';

import type {
  Mollie,
  MollieComponent,
  MollieComponentType,
  MollieObject,
  PaymentServiceProvider
} from '../types';

import { createContext } from 'react';
import { createStore } from 'zustand';

declare global {
  interface Window {
    Mollie: Mollie;
    MollieObject: MollieObject | null;
    MollieComponents: Record<MollieComponentType, MollieComponent | null>;
    SecureFields: any;
  }
}

export interface PaymentStore {
  isError: boolean;
  isReady: boolean;
  serviceProvider: PaymentServiceProvider;
  initializeDatatrans: (isError: boolean) => void;
  initializeMollie: (isError: boolean) => void;
}

export const PaymentContext = createContext<StoreApi<PaymentStore> | null>(null);

export const createPaymentStore = (serviceProvider: PaymentServiceProvider) => {
  const paymentStore = createStore<PaymentStore>()((set, get) => ({
    isError: false,
    isReady: false,
    serviceProvider,
    initializeDatatrans: isError => {
      set({ isError, isReady: !isError });
    },
    initializeMollie: isError => {
      window.MollieObject = window.Mollie('pfl_AaTsUVacFp', {
        locale: 'nl_BE',
        testmode: true
      });

      set({ isError, isReady: !isError });
    }
  }));

  return paymentStore;
};
