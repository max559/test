import type { PaymentStore } from './PaymentContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { PaymentContext } from './PaymentContext';

const usePaymentStore = <T>(selector: (state: PaymentStore) => T): T => {
  const paymentContext = useContext(PaymentContext);

  if (!paymentContext) {
    throw new Error('The usePaymentStore hook can only be used in a PaymentProvider');
  }

  return useStore(paymentContext, selector);
};

export default usePaymentStore;
