import type { PropsWithChildren, ReactElement } from 'react';

import type { PaymentServiceProvider } from '../types';

import Script from 'next/script';
import { useRef } from 'react';

import { createPaymentStore, PaymentContext } from './PaymentContext';

interface PaymentProviderProps {
  serviceProvider: PaymentServiceProvider;
}

const PaymentProvider = ({
  children,
  serviceProvider
}: PropsWithChildren<PaymentProviderProps>): ReactElement => {
  const store = useRef(createPaymentStore(serviceProvider)).current;

  return (
    <PaymentContext.Provider value={store}>
      {children}

      {serviceProvider === 'datatrans' && (
        <Script
          src="https://pay.sandbox.datatrans.com/upp/payment/js/secure-fields-2.0.0.min.js"
          strategy="lazyOnload"
          onError={() => store.getState().initializeDatatrans(true)}
          onLoad={() => store.getState().initializeDatatrans(false)}
        />
      )}
      {serviceProvider === 'mollie' && (
        <Script
          src="https://js.mollie.com/v1/mollie.js"
          strategy="lazyOnload"
          onError={() => store.getState().initializeMollie(true)}
          onLoad={() => store.getState().initializeMollie(false)}
        />
      )}
    </PaymentContext.Provider>
  );
};

export default PaymentProvider;
