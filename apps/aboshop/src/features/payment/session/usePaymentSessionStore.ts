import type { PaymentSessionStore } from './PaymentSessionContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { PaymentSessionContext } from './PaymentSessionContext';

const usePaymentSessionStore = <T>(selector: (state: PaymentSessionStore) => T): T => {
  const paymentSessionContext = useContext(PaymentSessionContext);

  if (!paymentSessionContext) {
    throw new Error(
      'The usePaymentSessionStore hook can only be used in a PaymentSession component'
    );
  }

  return useStore(paymentSessionContext, selector);
};

export default usePaymentSessionStore;
