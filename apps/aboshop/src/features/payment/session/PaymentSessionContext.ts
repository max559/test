import type { Payment, PaymentRequest, PaymentValidation } from '@subscriber/services-core';
import type { StoreApi } from 'zustand';

import type { UserInfo } from 'features/authentication';

import type {
  BankAccountFormValues,
  MollieCreditcardFormValues,
  UserTermsFormValues
} from '../forms';
import type { DatatransSuccessEvent, PaymentServiceProvider } from '../types';

import { PAYMENT_OPTION, PAYMENT_STATUS, PAYMENT_TYPE } from '@subscriber/services-core';
import { createContext } from 'react';
import { createStore } from 'zustand';

import { servicesCoreInstance } from 'globals';

import { createPaymentRequest, resolvePaymentOptions } from '../utils';

export interface PaymentSessionStore {
  isLoading: boolean;
  options: Array<PAYMENT_OPTION>;
  payment?: Payment;
  createPayment: (
    option: PAYMENT_OPTION,
    partialRequest?: Partial<PaymentRequest>
  ) => Promise<Payment>;
  submitBancontact: (formValues: UserTermsFormValues) => void;
  submitBelfius: (formValues: UserTermsFormValues) => void;
  submitDatatransCreditcard: (event: DatatransSuccessEvent) => void;
  submitDirectDebit: (formValues: BankAccountFormValues) => void;
  submitIdeal: (formValues: UserTermsFormValues) => void;
  submitKbc: (formValues: UserTermsFormValues) => void;
  submitMollieCreditcard: (formValues: MollieCreditcardFormValues) => void;
  submitPayPal: (formValues: UserTermsFormValues) => void;
  submitWireTransfer: (formValues: UserTermsFormValues) => void;
  validateDatatransPayment: (paymentId: string) => Promise<'success' | 'error'>;
}

export const PaymentSessionContext = createContext<StoreApi<PaymentSessionStore> | null>(null);

export const createPaymentSessionStore = (
  serviceProvider: PaymentServiceProvider,
  paymentType: PAYMENT_TYPE,
  paymentValidation: PaymentValidation,
  userInfo: UserInfo | null,
  onCreate: () => Partial<PaymentRequest>,
  onSubmit: (payment: Payment) => void
) => {
  const activeOptions = resolvePaymentOptions(paymentType, paymentValidation);

  const paymentSessionStore = createStore<PaymentSessionStore>()((set, get) => ({
    isLoading: false,
    options: activeOptions,
    createPayment: (option, partialRequest) => {
      return new Promise((resolve, reject) => {
        set({ isLoading: true });

        const paymentRequest = createPaymentRequest({
          paymentOption: option,
          paymentType,
          paymentValidation,
          redirectUrl: window.location.href,
          userInfo
        });

        servicesCoreInstance.paymentService
          .createPayment({ ...paymentRequest, ...partialRequest, ...onCreate() })
          .then(response => {
            set({ payment: response.data });
            resolve(response.data);
          })
          .catch(reject)
          .finally(() => {
            set({ isLoading: false });
          });
      });
    },
    submitBancontact: () => {
      get().createPayment(PAYMENT_OPTION.Bancontact).then(onSubmit);
    },
    submitBelfius: () => {
      get().createPayment(PAYMENT_OPTION.Belfius).then(onSubmit);
    },
    submitDatatransCreditcard: event => {
      const payment = get().payment;

      if (payment) {
        if (event.redirect) {
          // 3D secure
          payment.redirectUrl = event.redirect;

          onSubmit(payment);
        } else {
          // without 3D secure
          get()
            .validateDatatransPayment(payment.id as string)
            .then(status => {
              payment.redirectUrl = `${payment.redirectUrl}&status=${status}`;

              onSubmit(payment);
            });
        }
      }
    },
    submitDirectDebit: formValues => {
      get()
        .createPayment(PAYMENT_OPTION.DirectDebit, { paymentInfo: formValues.ibanNumber })
        .then(onSubmit);
    },
    submitIdeal: () => {
      get().createPayment(PAYMENT_OPTION.Ideal).then(onSubmit);
    },
    submitKbc: () => {
      get().createPayment(PAYMENT_OPTION.KBC).then(onSubmit);
    },
    submitMollieCreditcard: formValues => {
      get()
        .createPayment(PAYMENT_OPTION.CreditCard, { paymentInfo: formValues.token })
        .then(onSubmit);
    },
    submitPayPal: () => {
      get().createPayment(PAYMENT_OPTION.PayPal).then(onSubmit);
    },
    submitWireTransfer: () => {
      get().createPayment(PAYMENT_OPTION.WireTransfer).then(onSubmit);
    },
    validateDatatransPayment: async paymentId => {
      try {
        set({ isLoading: true });

        const paymentStatus = await servicesCoreInstance.paymentService
          .getPaymentStatus(paymentId)
          .then(response => response.data);

        if (paymentStatus.status === PAYMENT_STATUS.Authenticated) {
          await servicesCoreInstance.paymentService.createPaymentCallback(paymentId);

          set({ isLoading: false });

          return 'success';
        } else {
          set({ isLoading: false });

          return 'error';
        }
      } catch {
        set({ isLoading: false });

        return 'error';
      }
    }
  }));

  if (
    serviceProvider === 'datatrans' &&
    paymentSessionStore.getState().options.includes(PAYMENT_OPTION.CreditCard)
  ) {
    paymentSessionStore.getState().createPayment(PAYMENT_OPTION.CreditCard);
  }

  return paymentSessionStore;
};
