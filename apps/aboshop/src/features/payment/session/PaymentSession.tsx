import type { Payment, PaymentRequest, PaymentValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { PAYMENT_OPTION, PAYMENT_TYPE } from '@subscriber/services-core';
import { useRef } from 'react';

import { useAuthentication } from 'features/authentication';

import { usePaymentStore } from '../context';
import { createPaymentSessionStore, PaymentSessionContext } from './PaymentSessionContext';

interface PaymentSessionProps {
  paymentType: PAYMENT_TYPE;
  paymentValidation: PaymentValidation;
  onCreate: () => Partial<PaymentRequest>;
  onSubmit: (payment: Payment) => void;
  render: (options: Array<PAYMENT_OPTION>) => ReactElement;
}

const PaymentSession = ({
  paymentType,
  paymentValidation,
  onCreate,
  onSubmit,
  render
}: PaymentSessionProps): ReactElement => {
  const { userInfo } = useAuthentication();

  const serviceProvider = usePaymentStore(state => state.serviceProvider);

  const store = useRef(
    createPaymentSessionStore(
      serviceProvider,
      paymentType,
      paymentValidation,
      userInfo,
      onCreate,
      onSubmit
    )
  ).current;

  return (
    <PaymentSessionContext.Provider value={store}>
      {render(store.getState().options)}
    </PaymentSessionContext.Provider>
  );
};

export default PaymentSession;
