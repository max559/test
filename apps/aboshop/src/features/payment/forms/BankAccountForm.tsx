import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { Checkbox, TextField } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/payment';

export interface BankAccountFormValues {
  acceptedTerms: boolean;
  ibanNumber: string;
}

interface BankAccountFormProps {
  formRef: RefObject<FormikProps<BankAccountFormValues>>;
  initialValues: BankAccountFormValues;
  onSubmit: (values: BankAccountFormValues) => void;
}

const BankAccountForm = ({
  initialValues,
  formRef,
  onSubmit
}: BankAccountFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const BankAccountFormSchema = Yup.object().shape({
    acceptedTerms: Yup.boolean().oneOf([true], t('forms.bankAccount.acceptedTermsError')),
    ibanNumber: Yup.string()
      .matches(
        /^([A-Z]{2}[ \\-]?[0-9]{2})(?=(?:[ \\-]?[A-Z0-9]){9,30}$)((?:[ \\-]?[A-Z0-9]{3,5}){2,7})([ \\-]?[A-Z0-9]{1,3})?$/,
        t('forms.bankAccount.ibanNumberRequired')
      )
      .required(t('forms.bankAccount.ibanNumberRequired'))
  });

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={BankAccountFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <Flex flexDirection="column" gap={4}>
          <Field name="ibanNumber">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                error={!!meta.touched && !!meta.error}
                id="IbanNumberInput"
                label={t('forms.bankAccount.ibanNumber')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                required
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              />
            )}
          </Field>

          <Field name="acceptedTerms">
            {({ field, meta }: FieldProps<string>) => (
              <Checkbox
                error={!!meta.touched && !!meta.error}
                id="AcceptedTermsCheck"
                label={t('forms.bankAccount.acceptedTerms')}
                labelProps={{
                  size: 'Caption1'
                }}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                required
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              />
            )}
          </Field>
        </Flex>
      </Form>
    </Formik>
  );
};

export default BankAccountForm;
