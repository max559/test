import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { Checkbox } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/payment';

export interface UserTermsFormValues {
  acceptedTerms: boolean;
}

interface UserTermsFormProps {
  formRef: RefObject<FormikProps<UserTermsFormValues>>;
  initialValues: UserTermsFormValues;
  onSubmit: (values: UserTermsFormValues) => void;
}

const UserTermsForm = ({ formRef, initialValues, onSubmit }: UserTermsFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const UserTermsFormSchema = Yup.object().shape({
    acceptedTerms: Yup.boolean().oneOf([true], t('forms.userTerms.acceptedTermsError'))
  });

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={UserTermsFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <Field name="acceptedTerms">
          {({ field, meta }: FieldProps<string>) => (
            <Checkbox
              error={!!meta.touched && !!meta.error}
              id="AcceptedTermsCheck"
              label={t('forms.userTerms.acceptedTerms')}
              labelProps={{
                size: 'Caption1'
              }}
              message={meta.touched && meta.error ? meta.error : undefined}
              name={field.name}
              required
              value={field.value}
              onBlur={field.onBlur}
              onChange={field.onChange}
            />
          )}
        </Field>
      </Form>
    </Formik>
  );
};

export default UserTermsForm;
