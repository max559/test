import type { MutableRefObject, ReactElement } from 'react';

import type { MollieComponentEvent, MollieComponentType } from '../types';

import { Caption, Icon, Paragraph } from '@mediahuis/chameleon-react';
import { Error } from '@mediahuis/chameleon-theme-wl/icons';
import { Flex } from '@subscriber/subscriber-ui';
import { useState } from 'react';

import { useTranslation } from 'hooks';

import { MollieComponent } from '../components/MollieComponent';

const translationNamespace = 'features/payment';

export interface MollieCreditcardFormValues {
  token: string;
}

interface MollieCreditcardFormProps {
  formRef: MutableRefObject<HTMLFormElement | null>;
  onSubmit: (values: MollieCreditcardFormValues) => void;
}

const MollieCreditcardForm = ({ formRef, onSubmit }: MollieCreditcardFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [cardHolderError, setCardHolderError] = useState<string>('');
  const [cardNumberError, setCardNumberError] = useState<string>('');
  const [expiryDateError, setExpiryDateError] = useState<string>('');
  const [verificationCodeError, setVerificationCodeError] = useState<string>('');

  function handleMollieComponentChange(type: MollieComponentType, event: MollieComponentEvent) {
    if (type === 'cardHolder') {
      if (event.error && event.touched) {
        setCardHolderError(event.error);
      } else {
        setCardHolderError('');
      }
    }
    if (type === 'cardNumber') {
      if (event.error && event.touched) {
        setCardNumberError(event.error);
      } else {
        setCardNumberError('');
      }
    }
    if (type === 'expiryDate') {
      if (event.error && event.touched) {
        setExpiryDateError(event.error);
      } else {
        setExpiryDateError('');
      }
    }
    if (type === 'verificationCode') {
      if (event.error && event.touched) {
        setVerificationCodeError(event.error);
      } else {
        setVerificationCodeError('');
      }
    }
  }

  return (
    <form
      ref={formRef}
      onSubmit={event => {
        event.preventDefault();

        window.MollieObject?.createToken().then(result => {
          if (result.token) {
            onSubmit({ token: result.token });
          }
        });
      }}
    >
      <Flex flexDirection="column" gap={4}>
        <Flex flexDirection="column" gap={1}>
          <Paragraph as="label" className="!text-grey80" weight="strong">
            {t('forms.mollieCreditcard.cardNumberLabel')}
          </Paragraph>

          <MollieComponent
            type="cardNumber"
            onChange={e => handleMollieComponentChange('cardNumber', e)}
          />

          {cardNumberError && (
            <Flex alignItems="center" className="!text-redBase" gap={1}>
              <Icon as={Error} size="sm" />
              <Caption>{cardNumberError}</Caption>
            </Flex>
          )}
        </Flex>

        <Flex flexDirection="column" gap={1}>
          <Paragraph as="label" className="!text-grey80" weight="strong">
            {t('forms.mollieCreditcard.cardHolderLabel')}
          </Paragraph>

          <MollieComponent
            type="cardHolder"
            onChange={e => handleMollieComponentChange('cardHolder', e)}
          />

          {cardHolderError && (
            <Flex alignItems="center" className="!text-redBase" gap={1}>
              <Icon as={Error} size="sm" />
              <Caption>{cardHolderError}</Caption>
            </Flex>
          )}
        </Flex>

        <Flex gap={4} justifyContent="space-between">
          <Flex className="w-full" flexDirection="column" gap={1}>
            <Paragraph as="label" className="!text-grey80" weight="strong">
              <label>{t('forms.mollieCreditcard.expiryDateLabel')}</label>
            </Paragraph>

            <MollieComponent
              type="expiryDate"
              onChange={e => handleMollieComponentChange('expiryDate', e)}
            />

            {expiryDateError && (
              <Flex alignItems="center" className="!text-redBase" gap={1}>
                <Icon as={Error} size="sm" />
                <Caption>{expiryDateError}</Caption>
              </Flex>
            )}
          </Flex>

          <Flex className="w-full" flexDirection="column" gap={1}>
            <Paragraph as="label" className="!text-grey80" weight="strong">
              <label>{t('forms.mollieCreditcard.verificationCodeLabel')}</label>
            </Paragraph>

            <MollieComponent
              type="verificationCode"
              onChange={e => handleMollieComponentChange('verificationCode', e)}
            />

            {verificationCodeError && (
              <Flex alignItems="center" className="!text-redBase" gap={1}>
                <Icon as={Error} size="sm" />
                <Caption>{verificationCodeError}</Caption>
              </Flex>
            )}
          </Flex>
        </Flex>
      </Flex>
    </form>
  );
};

export default MollieCreditcardForm;
