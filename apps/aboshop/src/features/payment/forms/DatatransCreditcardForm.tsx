import type { MutableRefObject, ReactElement } from 'react';

import type { DatatransErrorEvent, DatatransSuccessEvent, DatatransValidateEvent } from '../types';

import { Caption, TextField } from '@mediahuis/chameleon-react';
import {
  colorFocus,
  colorNeutral50,
  colorNeutral90,
  colorNeutralWhite,
  colorSecondary90,
  fontFamilySystem,
  fontSizeMd,
  scale12,
  scale3,
  scale6,
  scale7
} from '@mediahuis/chameleon-theme-wl/tokens';
import { useEffect, useRef, useState } from 'react';

import { ErrorMessage } from 'components/ErrorMessage';
import { useTranslation } from 'hooks';

const deconstructExpiryDate = (expiryDate: string): { expm: number; expy: number } => {
  let expm = 0;
  let expy = 0;

  if (expiryDate) {
    const [month, year] = expiryDate.split('/');

    expm = parseInt(month);
    expy = parseInt(year);
  }

  return {
    expm: isNaN(expm) ? 0 : expm,
    expy: isNaN(expy) ? 0 : expy
  };
};

const validateExpiryDate = (expiryMonth: number, expiryYear: number): boolean => {
  if (expiryMonth >= 1 && expiryMonth <= 12 && expiryYear > 0) {
    return true;
  }

  return false;
};

const inputStyles = `
  background-color: ${colorNeutralWhite};
  border: 1px solid ${colorNeutral50};
  border-radius: ${scale3};
  color: ${colorNeutral90};
  font-family: ${fontFamilySystem};
  font-size: ${fontSizeMd};
  padding: ${scale6} ${scale7};
  height: ${scale12};
  outline: 0;
`;

const translationNamespace = 'features/payment';

interface DatatransCreditcardFormProps {
  formRef: MutableRefObject<HTMLFormElement | null>;
  transactionId: string;
  onError: (event: DatatransErrorEvent) => void;
  onSubmit: (event: DatatransSuccessEvent) => void;
}

const DatatransCreditcardForm = ({
  formRef,
  transactionId,
  onError,
  onSubmit
}: DatatransCreditcardFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [expiryDate, setExpiryDate] = useState<string>('');
  const [fieldErrors, setFieldErrors] = useState<{
    cardNumber: boolean;
    cvv: boolean;
    expiryDate: boolean;
  }>({
    cardNumber: false,
    cvv: false,
    expiryDate: false
  });
  const [isReady, setIsReady] = useState<boolean>(false);

  const secureFieldsRef = useRef<any>(null);

  useEffect(() => {
    if (!secureFieldsRef.current) {
      secureFieldsRef.current = initializeSecureFields();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function initializeSecureFields() {
    const secureFields = new window.SecureFields();

    secureFields.init(
      transactionId,
      {
        cardNumber: 'CardNumber',
        cvv: 'Cvv'
      },
      {
        styles: {
          'cardNumber:hover': `border-color: ${colorSecondary90};`,
          'cardNumber:focus': `border-color: ${colorFocus};`,
          // 'cardNumber.invalid': `border-color: ${colorRedBase};`,
          // 'cardNumber.invalid:hover': `border-color: ${colorRedBase};`,
          // 'cardNumber.invalid:focus': `border-color: ${colorPrimaryBase};`,
          'cvv:hover': `border-color: ${colorSecondary90};`,
          'cvv:focus': `border-color: ${colorFocus};`
          // 'cvv.invalid': `border-color: ${colorRedBase};`,
          // 'cvv.invalid:hover': `border-color: ${colorRedBase};`,
          // 'cvv.invalid:focus': `border-color: ${colorPrimaryBase};`
        }
      }
    );

    secureFields.on('error', onError);

    secureFields.on('ready', () => {
      secureFields.setStyle('cardNumber', inputStyles);
      secureFields.setStyle('cvv', inputStyles);

      setIsReady(true);
    });

    secureFields.on('success', onSubmit);

    secureFields.on('validate', (event: DatatransValidateEvent) => {
      setFieldErrors(prevFieldErrors => ({
        ...prevFieldErrors,
        cardNumber: !event.fields.cardNumber.valid,
        cvv: !event.fields.cvv.valid
      }));
    });

    return secureFields;
  }

  return (
    <form
      ref={formRef}
      style={{ display: isReady ? 'block' : 'none' }}
      onSubmit={event => {
        event.preventDefault();

        const { expm, expy } = deconstructExpiryDate(expiryDate);

        setFieldErrors(prevFieldErrors => ({
          ...prevFieldErrors,
          expiryDate: !validateExpiryDate(expm, expy)
        }));

        secureFieldsRef.current?.submit({
          expm,
          expy
        });
      }}
    >
      <div className="flex flex-col gap-2">
        <div>
          <label htmlFor="CardNumber">{t('forms.datatransCreditcard.cardNumberLabel')}</label>
          <div id="CardNumber" style={{ height: scale12 }}></div>
          <Caption>{t('forms.datatransCreditcard.cardNumberCaption')}</Caption>
          {fieldErrors.cardNumber && (
            <ErrorMessage>{t('forms.datatransCreditcard.cardNumberError')}</ErrorMessage>
          )}
        </div>

        <div className="flex gap-4 justify-between">
          <TextField
            className="grow"
            error={fieldErrors.expiryDate}
            id="ExpiryDate"
            label={t('forms.datatransCreditcard.expiryDateLabel')}
            mask="99 / 99"
            message={
              fieldErrors.expiryDate ? t('forms.datatransCreditcard.expiryDateError') : undefined
            }
            value={expiryDate}
            onChange={event => setExpiryDate(event.target.value)}
          />

          <div className="grow">
            <label htmlFor="Cvv">{t('forms.datatransCreditcard.cvvLabel')}</label>
            <div id="Cvv" style={{ height: scale12 }}></div>
            {fieldErrors.cardNumber && (
              <ErrorMessage>{t('forms.datatransCreditcard.cvvError')}</ErrorMessage>
            )}
          </div>
        </div>
      </div>
    </form>
  );
};

export default DatatransCreditcardForm;
