import type { Order } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Stepper } from '@mediahuis/chameleon-react';
import { ORDER_STATE, PAYMENT_STATUS } from '@subscriber/services-core';
import { usePaymentStatus } from '@subscriber/services-hooks';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { PaymentStatusBanner } from 'components/PaymentStatusBanner';
import { useSessionStorage, useTranslation } from 'hooks';

import WinbackWizard10 from './WinbackWizard10';
import WinbackWizard20 from './WinbackWizard20';

const PAYMENT_REFERENCE_SESSION_KEY = 'mh_payment_reference';

const translationNamespace = 'features/checkout/winback';

interface WinbackWizardProps {
  order: Order;
  paymentReference: string | null;
}

const WinbackWizard = ({ order, paymentReference }: WinbackWizardProps): ReactElement => {
  const router = useRouter();
  const { getItem, removeItem, setItem } = useSessionStorage();
  const { t } = useTranslation(translationNamespace);

  const [activeStep, setActiveStep] = useState<0 | 1>(
    order.state === ORDER_STATE.Confirmed ? 1 : 0
  );

  const paymentRef: string | undefined =
    paymentReference || getItem(PAYMENT_REFERENCE_SESSION_KEY) || undefined;

  const paymentStatusQuery = usePaymentStatus(paymentRef);

  useEffect(() => {
    if (paymentStatusQuery.isSuccess) {
      if (paymentStatusQuery.data?.status === PAYMENT_STATUS.Paid) {
        setActiveStep(1);
      }
      if (
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Canceled ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Expired ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Failed
      ) {
        setActiveStep(0);
      }

      removeItem(PAYMENT_REFERENCE_SESSION_KEY);
      router.replace({
        pathname: '/checkout/winback',
        query: {
          orderId: order.id,
          orderHash: order.orderHash,
          paymentReference: paymentRef
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paymentStatusQuery.isSuccess]);

  return (
    <div className="flex flex-col gap-10">
      <Stepper activeStepIndex={activeStep} steps={[t('step10'), t('step20')]} />

      {paymentStatusQuery.data?.status && (
        <PaymentStatusBanner paymentStatus={paymentStatusQuery.data.status} />
      )}

      {activeStep === 0 && (
        <WinbackWizard10
          orderHash={order.orderHash || ''}
          orderId={order.id.toString()}
          paymentValidation={order.paymentValidation}
          subscriptionFormulaId={order.subscriptionFormulaId}
          onSubmit={payment => {
            const { id, redirectUrl } = payment;

            if (id) {
              setItem(PAYMENT_REFERENCE_SESSION_KEY, id);
            }
            if (redirectUrl && !redirectUrl.startsWith(window.location.origin)) {
              window.location.href = redirectUrl;
            } else {
              setActiveStep(1);
            }
          }}
        />
      )}
      {activeStep === 1 && <WinbackWizard20 />}
    </div>
  );
};

export default WinbackWizard;
