import { useEffect, type ReactElement } from 'react';

import { Button, Heading, Icon, Paragraph } from '@mediahuis/chameleon-react';
import { Checkmark } from '@mediahuis/chameleon-theme-wl/icons';
import { useOffer, useOfferItemBySlug } from '@subscriber/services-hooks';
import { Center } from '@subscriber/subscriber-ui';

import { PurchaseSummary } from 'components/PurchaseSummary';
import { useTracking } from 'features/tracking';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/settlement';

interface SettlementWizard30Props {
  created: Date;
  formulaOfferId?: number;
  subscriptionFormulaId?: number;
}

const SettlementWizard30 = ({
  created,
  formulaOfferId,
  subscriptionFormulaId
}: SettlementWizard30Props): ReactElement => {
  const { clearSessionData } = useTracking();
  const { t } = useTranslation(translationNamespace);

  const offerQuery = useOffer(formulaOfferId);
  const offerTypeQuery = useOfferItemBySlug(
    offerQuery.data?.items?.find(item => item.id === subscriptionFormulaId)?.parent
  );

  useEffect(() => {
    clearSessionData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Center maxInlineSize="40rem">
      <div className="flex flex-col gap-6">
        <div className="flex gap-4 items-center">
          <div className="bg-[var(--)colorGreenBase] p-2 rounded-[50%]">
            <Icon as={Checkmark} color="colorWhiteBase" />
          </div>
          <Heading as="span" color="var(--color-green-base)" level={6} size="sm">
            {t('heading30')}
          </Heading>
        </div>

        <Paragraph>{t('text30', { siteName: SITE_NAME })}</Paragraph>

        <Button
          appearance="primary"
          id="StartReading"
          onClick={() => (window.location.href = SITE_URL)}
        >
          {t('startReading')}
        </Button>

        {offerTypeQuery.isSuccess && (
          <PurchaseSummary
            creationDate={created}
            offerFormula={offerQuery.data?.items?.find(item => item.id === subscriptionFormulaId)}
            offerType={offerTypeQuery.data}
          />
        )}
      </div>
    </Center>
  );
};

export default SettlementWizard30;
