import type { Payment, PaymentValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Button, Heading } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE, PAYMENT_OPTION, PAYMENT_TYPE } from '@subscriber/services-core';
import { useOfferItem } from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';
import { useState } from 'react';

import { OrderSummary } from 'components/OrderSummary';
import { PaymentChoice, PaymentSession } from 'features/payment';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/settlement';

interface SettlementWizard20Props {
  orderHash: string;
  orderId: string;
  paymentInterval: 'oneShot' | 'recurring';
  paymentValidation?: PaymentValidation;
  subscriptionTypeId?: number;
  onPreviousStep: () => void;
  onSubmit: (payment: Payment) => void;
}

const SettlementWizard20 = ({
  orderHash,
  orderId,
  paymentInterval,
  paymentValidation,
  subscriptionTypeId,
  onPreviousStep,
  onSubmit
}: SettlementWizard20Props): ReactElement => {
  const oneShotOptions = paymentValidation?.oneShotPaymentOptions ?? [];
  const recurringOptions = paymentValidation?.recurringPaymentOptions ?? [];

  const { t } = useTranslation(translationNamespace);

  const [selectedOneShotOption, setSelectedOneShotOption] = useState<PAYMENT_OPTION | undefined>(
    oneShotOptions[0]
  );
  const [selectedRecurringOption, setSelectedRecurringOption] = useState<
    PAYMENT_OPTION | undefined
  >(recurringOptions[0]);

  const offerTypeQuery = useOfferItem({
    id: subscriptionTypeId,
    type: OFFER_ITEM_TYPE.SubscriptionType
  });

  return (
    <div className="flex flex-col gap-10">
      <Heading level={3}>{t('heading20')}</Heading>

      <Sidebar gap={4}>
        <Sidebar.Main>
          <div className="flex flex-col gap-4">
            {paymentValidation && paymentInterval === 'oneShot' && (
              <PaymentSession
                paymentType={PAYMENT_TYPE.OneShot}
                paymentValidation={paymentValidation}
                onCreate={() => ({
                  description: offerTypeQuery.data?.name,
                  redirectUrl: `${window.location.origin}/checkout/acquisition?orderId=${orderId}&orderHash=${orderHash}`
                })}
                onSubmit={onSubmit}
                render={options => (
                  <div className="flex flex-col gap-2">
                    {options.map(option => (
                      <PaymentChoice
                        checked={selectedOneShotOption === option}
                        key={option}
                        name="OneShotOptions"
                        option={option}
                        onChange={() => setSelectedOneShotOption(option)}
                      />
                    ))}
                  </div>
                )}
              />
            )}

            {paymentValidation && paymentInterval === 'recurring' && (
              <PaymentSession
                paymentType={PAYMENT_TYPE.FirstRecurring}
                paymentValidation={paymentValidation}
                onCreate={() => ({
                  description: offerTypeQuery.data?.name,
                  redirectUrl: `${window.location.origin}/checkout/acquisition?orderId=${orderId}&orderHash=${orderHash}`
                })}
                onSubmit={onSubmit}
                render={options => (
                  <div className="flex flex-col gap-2">
                    {options.map(option => (
                      <PaymentChoice
                        checked={selectedRecurringOption === option}
                        key={option}
                        name="RecurringOptions"
                        option={option}
                        onChange={() => setSelectedRecurringOption(option)}
                      />
                    ))}
                  </div>
                )}
              />
            )}

            <Button appearance="secondary" width="full" onClick={onPreviousStep}>
              {t('previousStep')}
            </Button>
          </div>
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {offerTypeQuery.isSuccess && <OrderSummary subscriptionTypeItem={offerTypeQuery.data} />}
        </Sidebar.Aside>
      </Sidebar>
    </div>
  );
};

export default SettlementWizard20;
