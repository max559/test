import type { Order, OrderSettlement } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Stepper } from '@mediahuis/chameleon-react';
import { ORDER_STATE } from '@subscriber/services-core';
import { useRouter } from 'next/router';
import { useState } from 'react';

import { useSessionStorage, useTranslation } from 'hooks';

import SettlementWizard10 from './SettlementWizard10';
import SettlementWizard20 from './SettlementWizard20';
import SettlementWizard30 from './SettlementWizard30';

const PAYMENT_REFERENCE_SESSION_KEY = 'mh_payment_reference';

const translationNamespace = 'features/checkout/settlement';

interface SettlementWizardProps {
  order: Order;
  orderSettlement: OrderSettlement;
}

const SettlementWizard = ({ order, orderSettlement }: SettlementWizardProps): ReactElement => {
  const router = useRouter();
  const { setItem } = useSessionStorage();
  const { t } = useTranslation(translationNamespace);

  const [activeStep, setActiveStep] = useState<0 | 1 | 2>(
    order.state === ORDER_STATE.Confirmed ? 2 : 0
  );
  const [paymentInterval, setPaymentInterval] = useState<'oneShot' | 'recurring'>('recurring');

  return (
    <div className="flex flex-col gap-10">
      <Stepper activeStepIndex={activeStep} steps={[t('step10'), t('step20'), t('step30')]} />

      {activeStep === 0 && (
        <SettlementWizard10
          defaultInterval="recurring"
          subscriptionTypeId={orderSettlement.subscriptionTypeId}
          onSubmit={paymentInterval => {
            setActiveStep(1);
            setPaymentInterval(paymentInterval);
          }}
        />
      )}

      {activeStep === 1 && (
        <SettlementWizard20
          orderHash={order.orderHash || ''}
          orderId={order.id.toString()}
          paymentInterval={paymentInterval}
          paymentValidation={order.paymentValidation}
          subscriptionTypeId={orderSettlement.subscriptionTypeId}
          onPreviousStep={() => setActiveStep(0)}
          onSubmit={payment => {
            const { id, redirectUrl } = payment;

            if (id) {
              setItem(PAYMENT_REFERENCE_SESSION_KEY, id);
            }
            if (redirectUrl && !redirectUrl.startsWith(window.location.origin)) {
              window.location.href = redirectUrl;
            } else {
              router.push({
                pathname: '/checkout/settlement',
                query: { orderId: order.id }
              });
            }
          }}
        />
      )}

      {activeStep === 2 && (
        <SettlementWizard30
          created={new Date(order.creationDate)}
          formulaOfferId={order.formulaOfferId}
          subscriptionFormulaId={order.subscriptionFormulaId}
        />
      )}
    </div>
  );
};

export default SettlementWizard;
