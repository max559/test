import type { ReactElement } from 'react';

import { Button, Heading } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE } from '@subscriber/services-core';
import { useOfferItem } from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';
import { useState } from 'react';

import { OrderSummary } from 'components/OrderSummary';
import { PaymentIntervalChoices } from 'components/PaymentIntervalChoices';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/settlement';

interface SettlementWizard10Props {
  defaultInterval: 'oneShot' | 'recurring';
  subscriptionTypeId?: number;
  onSubmit: (paymentInterval: 'oneShot' | 'recurring') => void;
}

const SettlementWizard10 = ({
  defaultInterval,
  subscriptionTypeId,
  onSubmit
}: SettlementWizard10Props): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [selectedInterval, setSelectedInterval] = useState<'oneShot' | 'recurring'>(
    defaultInterval
  );

  const offerTypeQuery = useOfferItem({
    id: subscriptionTypeId,
    type: OFFER_ITEM_TYPE.SubscriptionType
  });

  return (
    <div className="flex flex-col gap-10">
      <Heading level={3}>{t('heading10')}</Heading>

      <Sidebar gap="5vw">
        <Sidebar.Main>
          <div className="flex flex-col gap-6">
            <PaymentIntervalChoices
              checkedOption={selectedInterval}
              onChange={setSelectedInterval}
            />

            <Button appearance="primary" width="full" onClick={() => onSubmit(selectedInterval)}>
              {t('nextStep')}
            </Button>
          </div>
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {offerTypeQuery.isSuccess && <OrderSummary subscriptionTypeItem={offerTypeQuery.data} />}
        </Sidebar.Aside>
      </Sidebar>
    </div>
  );
};

export default SettlementWizard10;
