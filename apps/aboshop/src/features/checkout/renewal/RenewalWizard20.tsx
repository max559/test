import { useEffect, type ReactElement } from 'react';

import { Button, Heading, Icon, Paragraph } from '@mediahuis/chameleon-react';
import { Checkmark } from '@mediahuis/chameleon-theme-wl/icons';
import { Center } from '@subscriber/subscriber-ui';

import { useTracking } from 'features/tracking';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/renewal';

const RenewalWizard20 = (): ReactElement => {
  const { clearSessionData } = useTracking();
  const { t } = useTranslation(translationNamespace);

  useEffect(() => {
    clearSessionData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Center maxInlineSize="40rem">
      <div className="flex flex-col gap-6">
        <div className="flex gap-4 items-center">
          <div className="bg-[var(--colorGreenBase)] p-2 rounded-[50%]">
            <Icon as={Checkmark} color="colorWhiteBase" />
          </div>

          <Heading as="span" color="colorGreenBase" level={6}>
            {t('heading20')}
          </Heading>
        </div>

        <Paragraph>{t('text20', { siteName: SITE_NAME })}</Paragraph>

        <Button
          appearance="primary"
          id="StartReading"
          onClick={() => (window.location.href = SITE_URL)}
        >
          {t('StartReading')}
        </Button>
      </div>
    </Center>
  );
};

export default RenewalWizard20;
