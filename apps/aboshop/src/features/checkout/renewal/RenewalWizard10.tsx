import type { Payment, PaymentValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Heading } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE, PAYMENT_OPTION, PAYMENT_TYPE } from '@subscriber/services-core';
import { useOfferItem, useOfferItemBySlug } from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';
import { useState } from 'react';

import { OrderSummary } from 'components/OrderSummary';
import { PaymentChoice, PaymentSession } from 'features/payment';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/renewal';

interface RenewalWizard10Props {
  orderHash: string;
  orderId: string;
  paymentValidation?: PaymentValidation;
  subscriptionFormulaId?: number;
  onSubmit: (payment: Payment) => void;
}

const RenewalWizard10 = ({
  orderHash,
  orderId,
  paymentValidation,
  subscriptionFormulaId,
  onSubmit
}: RenewalWizard10Props): ReactElement => {
  const oneShotOptions = paymentValidation?.oneShotPaymentOptions ?? [];
  const recurringOptions = paymentValidation?.recurringPaymentOptions ?? [];

  const { t } = useTranslation(translationNamespace);

  const [selectedOneShotOption, setSelectedOneShotOption] = useState<PAYMENT_OPTION | undefined>(
    oneShotOptions[0]
  );
  const [selectedRecurringOption, setSelectedRecurringOption] = useState<
    PAYMENT_OPTION | undefined
  >(recurringOptions[0]);

  const subscriptionFormulaItemQuery = useOfferItem({
    id: subscriptionFormulaId,
    type: OFFER_ITEM_TYPE.SubscriptionFormula
  });
  const subscriptionTypeItemQuery = useOfferItemBySlug(subscriptionFormulaItemQuery.data?.parent);

  return (
    <div className="flex flex-col gap-10">
      <Heading level={3}>{t('heading20')}</Heading>

      <Sidebar gap="5vw">
        <Sidebar.Main>
          <div className="flex flex-col gap-4">
            {paymentValidation && oneShotOptions.length > 0 && (
              <PaymentSession
                paymentType={PAYMENT_TYPE.OneShot}
                paymentValidation={paymentValidation}
                onCreate={() => ({
                  description: subscriptionTypeItemQuery.data?.name,
                  redirectUrl: `${window.location.origin}/checkout/renewal?orderId=${orderId}&orderHash=${orderHash}`
                })}
                onSubmit={onSubmit}
                render={options => (
                  <div className="flex flex-col gap-2">
                    {options.map(option => (
                      <PaymentChoice
                        checked={selectedOneShotOption === option}
                        key={option}
                        name="OneShotOptions"
                        option={option}
                        onChange={() => setSelectedOneShotOption(option)}
                      />
                    ))}
                  </div>
                )}
              />
            )}

            {paymentValidation && recurringOptions.length > 0 && (
              <PaymentSession
                paymentType={PAYMENT_TYPE.FirstRecurring}
                paymentValidation={paymentValidation}
                onCreate={() => ({
                  description: subscriptionTypeItemQuery.data?.name,
                  redirectUrl: `${window.location.origin}/checkout/renewal?orderId=${orderId}&orderHash=${orderHash}`
                })}
                onSubmit={onSubmit}
                render={options => (
                  <div className="flex flex-col gap-2">
                    {options.map(option => (
                      <PaymentChoice
                        checked={selectedRecurringOption === option}
                        key={option}
                        name="RecurringOptions"
                        option={option}
                        onChange={() => setSelectedRecurringOption(option)}
                      />
                    ))}
                  </div>
                )}
              />
            )}
          </div>
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {subscriptionTypeItemQuery.isSuccess && (
            <OrderSummary
              subscriptionFormulaItem={subscriptionFormulaItemQuery.data}
              subscriptionTypeItem={subscriptionTypeItemQuery.data}
            />
          )}
        </Sidebar.Aside>
      </Sidebar>
    </div>
  );
};

export default RenewalWizard10;
