import type { Order } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import type { Validation } from 'features/validators';

import { Stepper } from '@mediahuis/chameleon-react';
import { ORDER_STATE } from '@subscriber/services-core';
import { useState } from 'react';

import { useTranslation } from 'hooks';

import VoucherRedemptionWizard10 from './VoucherRedemptionWizard10';
import VoucherRedemptionWizard20 from './VoucherRedemptionWizard20';

const translationNamespace = 'features/checkout/voucher-redemption';

interface VoucherRedemptionWizardProps {
  order: Order;
}

const VoucherRedemptionWizard = ({ order }: VoucherRedemptionWizardProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [activeStep, setActiveStep] = useState<0 | 1>(
    order.state === ORDER_STATE.Confirmed ? 1 : 0
  );
  const [step10Validations, setStep10Validations] = useState<Array<Validation>>(
    [
      order.addressAreaValidations ?? [],
      order.booleanValidations ?? [],
      order.choiceValidations ?? [],
      order.dateValidations ?? [],
      order.emailValidations ?? [],
      order.fileValidations ?? [],
      order.invoiceDetailsValidation ?? [],
      order.textfieldValidations ?? []
    ].flat()
  );

  return (
    <div className="flex flex-col gap-10">
      <Stepper activeStepIndex={activeStep} steps={[t('step10'), t('step20')]} />

      {activeStep === 0 && (
        <VoucherRedemptionWizard10
          subscriptionFormulaId={order.subscriptionFormulaId}
          validations={step10Validations}
          onSubmit={validations => {
            setActiveStep(1);
            setStep10Validations(validations);
          }}
        />
      )}

      {activeStep === 1 && (
        <VoucherRedemptionWizard20
          created={new Date(order.creationDate)}
          subscriptionFormulaId={order.subscriptionFormulaId}
        />
      )}
    </div>
  );
};

export default VoucherRedemptionWizard;
