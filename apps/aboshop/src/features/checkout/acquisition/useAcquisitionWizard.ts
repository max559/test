import type { Order } from '@subscriber/services-core';

import { ORDER_STATE } from '@subscriber/services-core';
import { useReducer } from 'react';

export enum ACQUISITION_STEP {
  Validations = 'Validations',
  Payment = 'Payment',
  Confirmation = 'Confirmation'
}

export type AcquisitionAction =
  | { type: 'index'; index: number }
  | {
      type: 'next';
    }
  | {
      type: 'previous';
    }
  | {
      type: 'step';
      id: ACQUISITION_STEP;
    };

export interface AcquisitionState {
  currentStep: AcquisitionStep;
  steps: Array<AcquisitionStep>;
}

export interface AcquisitionStep {
  id: ACQUISITION_STEP;
  index: number;
  isActive: boolean;
  i18nKey: string;
}

const getDefaultAcquisitionState = (order: Order): AcquisitionState => {
  const acquisitionSteps: Array<AcquisitionStep> = [
    {
      id: ACQUISITION_STEP.Validations,
      index: 0,
      isActive: true,
      i18nKey: 'step10'
    },
    {
      id: ACQUISITION_STEP.Payment,
      index: 1,
      isActive: Boolean(order.paymentValidation),
      i18nKey: 'step20'
    },
    {
      id: ACQUISITION_STEP.Confirmation,
      index: 2,
      isActive: true,
      i18nKey: 'step30'
    }
  ];

  return {
    currentStep: acquisitionSteps[order.state === ORDER_STATE.Confirmed ? 2 : 0],
    steps: acquisitionSteps
  };
};

const acquisitionReducer = (
  state: AcquisitionState,
  action: AcquisitionAction
): AcquisitionState => {
  switch (action.type) {
    case 'index':
      return {
        ...state,
        currentStep:
          state.steps.find(step => step.index === action.index && step.isActive) ??
          state.currentStep
      };
    case 'next':
      return {
        ...state,
        currentStep:
          state.steps.find(step => step.index > state.currentStep.index && step.isActive) ??
          state.currentStep
      };
    case 'previous':
      return {
        ...state,
        currentStep:
          state.steps.find(step => step.index < state.currentStep.index && step.isActive) ??
          state.currentStep
      };
    case 'step':
      return {
        ...state,
        currentStep:
          state.steps.find(step => step.id === action.id && step.isActive) ?? state.currentStep
      };
    default:
      return state;
  }
};

export const useAcquisitionWizard = (order: Order) => {
  const [{ currentStep, steps }, dispatch] = useReducer(
    acquisitionReducer,
    getDefaultAcquisitionState(order)
  );

  return {
    currentStep,
    steps,
    goToIndex: (stepIndex: number) => dispatch({ type: 'index', index: stepIndex }),
    goToNextStep: () => dispatch({ type: 'next' }),
    goToPreviousStep: () => dispatch({ type: 'previous' }),
    goToStep: (stepId: ACQUISITION_STEP) => dispatch({ type: 'step', id: stepId })
  };
};

export default useAcquisitionWizard;
