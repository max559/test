import type { Order } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import type { Validation } from 'features/validators';

import { Stepper } from '@mediahuis/chameleon-react';
import { PAYMENT_STATUS } from '@subscriber/services-core';
import { usePaymentStatus } from '@subscriber/services-hooks';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { PaymentStatusBanner } from 'components/PaymentStatusBanner';
import { Spinner } from 'components/Spinner';
import { useSessionStorage, useTranslation } from 'hooks';

import AcquisitionWizard10 from './AcquisitionWizard10';
import AcquisitionWizard20 from './AcquisitionWizard20';
import AcquisitionWizard30 from './AcquisitionWizard30';
import { ACQUISITION_STEP, useAcquisitionWizard } from './useAcquisitionWizard';

const PAYMENT_REFERENCE_SESSION_KEY = 'mh_payment_reference';

const translationNamespace = 'features/checkout/acquisition';

interface AcquisitionWizardProps {
  order: Order;
  paymentReference: string | null;
}

const AcquisitionWizard = ({ order, paymentReference }: AcquisitionWizardProps): ReactElement => {
  const router = useRouter();
  const { getItem, removeItem, setItem } = useSessionStorage();
  const { t } = useTranslation(translationNamespace);

  const { currentStep, steps, goToNextStep, goToPreviousStep, goToStep } =
    useAcquisitionWizard(order);

  const [step10Validations, setStep10Validations] = useState<Array<Validation>>(
    [
      order.addressAreaValidations ?? [],
      order.booleanValidations ?? [],
      order.choiceValidations ?? [],
      order.dateValidations ?? [],
      order.emailValidations ?? [],
      order.fileValidations ?? [],
      order.invoiceDetailsValidation ?? [],
      order.textfieldValidations ?? []
    ].flat()
  );

  const paymentRef = paymentReference || getItem(PAYMENT_REFERENCE_SESSION_KEY) || undefined;

  const paymentStatusQuery = usePaymentStatus(paymentRef);

  useEffect(() => {
    if (paymentStatusQuery.isSuccess) {
      if (paymentStatusQuery.data?.status === PAYMENT_STATUS.Paid) {
        goToStep(ACQUISITION_STEP.Confirmation);
      }
      if (
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Canceled ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Expired ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Failed
      ) {
        goToStep(ACQUISITION_STEP.Payment);
      }

      removeItem(PAYMENT_REFERENCE_SESSION_KEY);
      router.replace({
        pathname: '/checkout/acquisition',
        query: {
          orderId: order.id,
          orderHash: order.orderHash,
          paymentReference: paymentRef
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paymentStatusQuery.isSuccess]);

  if (paymentStatusQuery.isInitialLoading) {
    return <Spinner text={t('loading')} />;
  }

  return (
    <div className="flex flex-col gap-10">
      <Stepper
        activeStepIndex={currentStep.index}
        steps={steps.filter(step => step.isActive).map(step => t(step.i18nKey))}
      />

      {paymentStatusQuery.data?.status && (
        <PaymentStatusBanner paymentStatus={paymentStatusQuery.data.status} />
      )}

      {currentStep.id === ACQUISITION_STEP.Validations && (
        <AcquisitionWizard10
          subscriptionFormulaId={order.subscriptionFormulaId}
          validations={step10Validations}
          onSubmit={validations => {
            setStep10Validations(validations);
            goToNextStep();
          }}
        />
      )}

      {currentStep.id === ACQUISITION_STEP.Payment && (
        <AcquisitionWizard20
          orderHash={order.orderHash || ''}
          orderId={order.id.toString()}
          paymentValidation={order.paymentValidation}
          subscriptionFormulaId={order.subscriptionFormulaId}
          onPreviousStep={() => goToPreviousStep()}
          onSubmit={payment => {
            const { id, redirectUrl } = payment;

            if (id) {
              setItem(PAYMENT_REFERENCE_SESSION_KEY, id);
            }
            if (redirectUrl && !redirectUrl.startsWith(window.location.origin)) {
              window.location.href = redirectUrl;
            } else {
              goToNextStep();
            }
          }}
        />
      )}

      {currentStep.id === ACQUISITION_STEP.Confirmation && (
        <AcquisitionWizard30
          created={new Date(order.creationDate)}
          formulaOfferId={order.formulaOfferId}
          subscriptionFormulaId={order.subscriptionFormulaId}
        />
      )}
    </div>
  );
};

export default AcquisitionWizard;
