import type { Order } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import type { Validation } from 'features/validators';

import { Stepper } from '@mediahuis/chameleon-react';
import { ORDER_STATE, PAYMENT_STATUS } from '@subscriber/services-core';
import { usePaymentStatus } from '@subscriber/services-hooks';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { PaymentStatusBanner } from 'components/PaymentStatusBanner';
import { useSessionStorage, useTranslation } from 'hooks';

import VoucherPurchaseWizard10 from './VoucherPurchaseWizard10';
import VoucherPurchaseWizard20 from './VoucherPurchaseWizard20';
import VoucherPurchaseWizard30 from './VoucherPurchaseWizard30';

const PAYMENT_REFERENCE_SESSION_KEY = 'mh_payment_reference';

const getInitialStep = (hasPaymentReference: boolean, isOrderConfirmed: boolean): 0 | 1 | 2 => {
  if (isOrderConfirmed) {
    return 2;
  }
  if (hasPaymentReference) {
    return 1;
  }

  return 0;
};

const translationNamespace = 'features/checkout/voucher-purchase';

interface VoucherPurchaseWizardProps {
  order: Order;
  paymentReference: string | null;
}

const VoucherPurchaseWizard = ({
  order,
  paymentReference
}: VoucherPurchaseWizardProps): ReactElement => {
  const router = useRouter();
  const { getItem, removeItem, setItem } = useSessionStorage();
  const { t } = useTranslation(translationNamespace);

  const [activeStep, setActiveStep] = useState<0 | 1 | 2>(
    getInitialStep(Boolean(paymentReference), order.state === ORDER_STATE.Confirmed)
  );
  const [step10Validations, setStep10Validations] = useState<Array<Validation>>(
    [
      order.addressAreaValidations ?? [],
      order.booleanValidations ?? [],
      order.choiceValidations ?? [],
      order.dateValidations ?? [],
      order.emailValidations ?? [],
      order.fileValidations ?? [],
      order.invoiceDetailsValidation ?? [],
      order.textfieldValidations ?? []
    ].flat()
  );

  const paymentRef = paymentReference || getItem(PAYMENT_REFERENCE_SESSION_KEY) || undefined;

  const paymentStatusQuery = usePaymentStatus(paymentRef);

  useEffect(() => {
    if (paymentStatusQuery.isSuccess) {
      if (paymentStatusQuery.data?.status === PAYMENT_STATUS.Paid) {
        setActiveStep(2);
      }
      if (
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Canceled ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Expired ||
        paymentStatusQuery.data?.status === PAYMENT_STATUS.Failed
      ) {
        setActiveStep(1);
      }

      removeItem(PAYMENT_REFERENCE_SESSION_KEY);
      router.replace({
        pathname: '/checkout/voucher-purchase',
        query: {
          orderId: order.id,
          orderHash: order.orderHash,
          paymentReference: paymentRef
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [paymentStatusQuery.isSuccess]);

  return (
    <div className="flex flex-col gap-10">
      <Stepper activeStepIndex={activeStep} steps={[t('step10'), t('step20'), t('step30')]} />

      {paymentStatusQuery.data?.status && (
        <PaymentStatusBanner paymentStatus={paymentStatusQuery.data.status} />
      )}

      {activeStep === 0 && (
        <VoucherPurchaseWizard10
          subscriptionFormulaId={order.subscriptionFormulaId}
          validations={step10Validations}
          onSubmit={validations => {
            setActiveStep(1);
            setStep10Validations(validations);
          }}
        />
      )}

      {activeStep === 1 && (
        <VoucherPurchaseWizard20
          orderHash={order.orderHash || ''}
          orderId={order.id.toString()}
          paymentValidation={order.paymentValidation}
          subscriptionFormulaId={order.subscriptionFormulaId}
          onPreviousStep={() => setActiveStep(0)}
          onSubmit={payment => {
            const { id, redirectUrl } = payment;

            if (id) {
              setItem(PAYMENT_REFERENCE_SESSION_KEY, id);
            }
            if (redirectUrl && !redirectUrl.startsWith(window.location.origin)) {
              window.location.href = redirectUrl;
            } else {
              setActiveStep(2);
            }
          }}
        />
      )}

      {activeStep === 2 && (
        <VoucherPurchaseWizard30
          created={new Date(order.creationDate)}
          formulaOfferId={order.formulaOfferId}
          subscriptionFormulaId={order.subscriptionFormulaId}
        />
      )}
    </div>
  );
};

export default VoucherPurchaseWizard;
