import type { ReactElement } from 'react';

import type { Validation } from 'features/validators';

import { Button, Heading, Paragraph } from '@mediahuis/chameleon-react';
import { OFFER_ITEM_TYPE } from '@subscriber/services-core';
import { useOfferItem, useOfferItemBySlug } from '@subscriber/services-hooks';
import { Sidebar } from '@subscriber/subscriber-ui';

import { OrderSummary } from 'components/OrderSummary';
import { useAuthentication } from 'features/authentication';
import { ValidatorsProvider, ValidatorsSubmit } from 'features/validators';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/checkout/voucher-purchase';

interface VoucherPurchaseWizard10Props {
  subscriptionFormulaId?: number;
  validations: Array<Validation>;
  onSubmit: (validations: Array<Validation>) => void;
}

const VoucherPurchaseWizard10 = ({
  subscriptionFormulaId,
  validations,
  onSubmit
}: VoucherPurchaseWizard10Props): ReactElement => {
  const { userInfo } = useAuthentication();
  const { t } = useTranslation(translationNamespace);

  const subscriptionFormulaItemQuery = useOfferItem({
    id: subscriptionFormulaId,
    type: OFFER_ITEM_TYPE.SubscriptionFormula
  });
  const subscriptionTypeItemQuery = useOfferItemBySlug(subscriptionFormulaItemQuery.data?.parent);

  return (
    <div className="flex flex-col gap-10">
      <div>
        <Heading level={3}>{t('heading10', { firstName: userInfo?.firstName || '' })}</Heading>
        <Paragraph>{t('heading10Sub')}</Paragraph>
      </div>

      <Sidebar gap="5vw">
        <Sidebar.Main>
          <ValidatorsProvider
            validations={validations}
            onSubmit={onSubmit}
            render={validators => (
              <div className="flex flex-col gap-8">
                <div className="flex flex-col gap-4">{validators}</div>

                <ValidatorsSubmit
                  render={({ isSubmitting, submit }) => (
                    <Button
                      appearance="primary"
                      loading={isSubmitting}
                      width="full"
                      onClick={submit}
                    >
                      {t('nextStep')}
                    </Button>
                  )}
                />
              </div>
            )}
          />
        </Sidebar.Main>

        <Sidebar.Aside width="25rem">
          {subscriptionTypeItemQuery.isSuccess && (
            <OrderSummary
              subscriptionFormulaItem={subscriptionFormulaItemQuery.data}
              subscriptionTypeItem={subscriptionTypeItemQuery.data}
            />
          )}
        </Sidebar.Aside>
      </Sidebar>
    </div>
  );
};

export default VoucherPurchaseWizard10;
