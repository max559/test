import type { DefaultPageProps } from 'pages/_app';

import { useEffect } from 'react';

import { useTracking } from '../../hooks/useTracking';

interface PageViewTrackerProps {
  pageProps: DefaultPageProps;
}

const PageViewTracker = ({ pageProps }: PageViewTrackerProps) => {
  const { trackPageView } = useTracking();

  useEffect(() => {
    trackPageView(window.location);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageProps]);

  return null;
};

export default PageViewTracker;
