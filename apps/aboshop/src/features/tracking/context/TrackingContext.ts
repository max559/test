import type { StoreApi } from 'zustand';

import type { UtmSessionData } from '../types';

import Cookies from 'js-cookie';
import { createContext } from 'react';
import { createStore } from 'zustand';

import { UTM_KEY } from '../types';

export interface TrackingStore {
  isReady: boolean;
  utmSessionData: UtmSessionData;
  clearUtmSessionData: () => void;
  setUtmSessionData: (utmSessionData: UtmSessionData) => void;
}

export const TrackingContext = createContext<StoreApi<TrackingStore> | null>(null);

export const createTrackingStore = (routerPath: string) => {
  const urlSearchParams = new URLSearchParams(routerPath.split('?')[1]);

  return createStore<TrackingStore>()(set => ({
    isReady: false,
    utmSessionData: {
      // Getting the articleid from the cookie is a temp solution until the green platform adds the articleid in the querystring (fix currently only on their test env)
      utm_artid:
        urlSearchParams.get(UTM_KEY.ARTICLE_ID) ?? Cookies.get('lastPaywallArticleId') ?? null,
      utm_campaign: urlSearchParams.get(UTM_KEY.CAMPAIGN),
      utm_content: urlSearchParams.get(UTM_KEY.CONTENT),
      utm_internal: urlSearchParams.get(UTM_KEY.INTERNAL),
      utm_medium: urlSearchParams.get(UTM_KEY.MEDIUM),
      utm_source: urlSearchParams.get(UTM_KEY.SOURCE),
      utm_term: urlSearchParams.get(UTM_KEY.TERM)
    },
    clearUtmSessionData: () => {
      set({
        utmSessionData: {
          utm_artid: null,
          utm_campaign: null,
          utm_content: null,
          utm_internal: null,
          utm_medium: null,
          utm_source: null,
          utm_term: null
        }
      });
    },
    setUtmSessionData: utmSessionData => {
      set({
        isReady: true,
        utmSessionData
      });
    }
  }));
};
