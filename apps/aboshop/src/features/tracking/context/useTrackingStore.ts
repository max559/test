import type { TrackingStore } from './TrackingContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { TrackingContext } from './TrackingContext';

const useTrackingStore = <T>(selector: (state: TrackingStore) => T): T => {
  const trackingContext = useContext(TrackingContext);

  if (!trackingContext) {
    throw new Error('The useTrackingStore hook can only be used in TrackingProvider');
  }

  return useStore(trackingContext, selector);
};

export default useTrackingStore;
