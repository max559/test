import type { PropsWithChildren, ReactElement } from 'react';

import type { UtmSessionData } from '../types';

import { useRouter } from 'next/router';
import { useEffect, useRef } from 'react';

import { UTM_KEY, UTM_SESSION_STORAGE_KEY } from '../types';
import { getUtmSessionStorageData } from '../utils';
import { TrackingContext, createTrackingStore } from './TrackingContext';

const TrackingProvider = ({ children }: PropsWithChildren): ReactElement => {
  const router = useRouter();

  const store = useRef(createTrackingStore(router.asPath)).current;

  useEffect(() => {
    if (router.isReady) {
      const sessionStorageData = getUtmSessionStorageData();

      const utmSessionData: UtmSessionData = {
        utm_artid: router.query[UTM_KEY.ARTICLE_ID] || sessionStorageData?.utm_artid || null,
        utm_campaign: router.query[UTM_KEY.CAMPAIGN] || sessionStorageData?.utm_campaign || null,
        utm_content: router.query[UTM_KEY.CONTENT] || sessionStorageData?.utm_content || null,
        utm_internal: router.query[UTM_KEY.INTERNAL] || sessionStorageData?.utm_internal || null,
        utm_medium: router.query[UTM_KEY.MEDIUM] || sessionStorageData?.utm_medium || null,
        utm_source: router.query[UTM_KEY.SOURCE] || sessionStorageData?.utm_source || null,
        utm_term: router.query[UTM_KEY.TERM] || sessionStorageData?.utm_term || null
      };

      sessionStorage.setItem(UTM_SESSION_STORAGE_KEY, JSON.stringify(utmSessionData));
      store.getState().setUtmSessionData(utmSessionData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.isReady]);

  return <TrackingContext.Provider value={store}>{children}</TrackingContext.Provider>;
};

export default TrackingProvider;
