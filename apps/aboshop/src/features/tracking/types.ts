export const UTM_SESSION_STORAGE_KEY = '_mhtc_utm';

export enum UTM_KEY {
  ARTICLE_ID = 'utm_artid',
  CAMPAIGN = 'utm_campaign',
  CONTENT = 'utm_content',
  INTERNAL = 'utm_internal',
  MEDIUM = 'utm_medium',
  SOURCE = 'utm_source',
  TERM = 'utm_term'
}

export type UtmSessionData = Record<UTM_KEY, string | Array<string> | null>;

export interface GlitrEvent {
  eventAction: string;
  eventCategory?: string;
  eventLabel: string;
  eventType?: string;
}
