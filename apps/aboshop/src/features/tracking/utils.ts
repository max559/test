import type { IDidomiObject } from '@subscriber/didomi';

import type { UtmSessionData } from './types';

import { UTM_SESSION_STORAGE_KEY } from './types';

export const getDidomiConsent = (): Promise<{
  disabled: Array<string>;
  enabled: Array<string>;
}> => {
  return new Promise((resolve, reject) => {
    getDidomiObject()
      .then(didomiObject => {
        const userConsent = didomiObject.getUserStatus().purposes.consent;

        if (userConsent.enabled.length === 0 && userConsent.disabled.length === 0) {
          window.addEventListener('didomi-consent', () => {
            if (window.Didomi) {
              resolve(window.Didomi.getUserStatus().purposes.consent);
            } else {
              reject();
            }
          });
        } else {
          resolve(userConsent);
        }
      })
      .catch(() => reject());
  });
};

export const getDidomiObject = (): Promise<IDidomiObject> => {
  return new Promise((resolve, reject) => {
    if (typeof window !== 'undefined') {
      if (window.Didomi) {
        resolve(window.Didomi);
      } else {
        window.addEventListener('didomi-ready', () => {
          if (window.Didomi) {
            resolve(window.Didomi);
          } else {
            reject();
          }
        });
        setTimeout(() => {
          if (window.Didomi) {
            resolve(window.Didomi);
          } else {
            reject();
          }
        }, 1000);
      }
    } else {
      reject();
    }
  });
};

export const getUtmSessionStorageData = (): UtmSessionData | null => {
  if (typeof window !== 'undefined') {
    const utmSessionStorageItem = sessionStorage.getItem(UTM_SESSION_STORAGE_KEY);

    if (utmSessionStorageItem) {
      const utmSessionStorageData: UtmSessionData = JSON.parse(utmSessionStorageItem);

      return utmSessionStorageData;
    }
  }

  return null;
};

export const mergeUtmSessionData = (utmStoreData: UtmSessionData): UtmSessionData => {
  const utmSessionStorageData = getUtmSessionStorageData();

  if (utmSessionStorageData) {
    return {
      utm_artid: utmStoreData.utm_artid || utmSessionStorageData.utm_artid,
      utm_campaign: utmStoreData.utm_campaign || utmSessionStorageData.utm_campaign,
      utm_content: utmStoreData.utm_content || utmSessionStorageData.utm_content,
      utm_internal: utmStoreData.utm_internal || utmSessionStorageData.utm_internal,
      utm_medium: utmStoreData.utm_medium || utmSessionStorageData.utm_medium,
      utm_source: utmStoreData.utm_source || utmSessionStorageData.utm_source,
      utm_term: utmStoreData.utm_term || utmSessionStorageData.utm_term
    };
  }

  return utmStoreData;
};

export const removeUtmSessionStorageData = (): void => {
  if (typeof window !== 'undefined') {
    sessionStorage.removeItem(UTM_SESSION_STORAGE_KEY);
  }
};
