import type { EcommerceEventName } from '@subscriber/gtm';
import type { Offer, OfferItem } from '@subscriber/services-core';

import type { GlitrEvent, UtmSessionData } from '../types';

import { GlitrTracker } from '@subscriber/glitr';
import { GoogleTagManager } from '@subscriber/gtm';
import { OFFER_ITEM_TYPE } from '@subscriber/services-core';

import { AUTHENTICATION_STATUS, useAuthentication } from 'features/authentication';

import { useTrackingStore } from '../context';
import { getDidomiConsent, mergeUtmSessionData, removeUtmSessionStorageData } from '../utils';

const glitrTracker = new GlitrTracker(MH_BRAND, MH_ENV);
const googleTagManager = new GoogleTagManager(MH_BRAND, MH_ENV);

export const useTracking = () => {
  const { clearUtmSessionData, isStoreReady, utmStoreData } = useTrackingStore(state => ({
    clearUtmSessionData: state.clearUtmSessionData,
    isStoreReady: state.isReady,
    utmStoreData: state.utmSessionData
  }));

  const { authenticationStatus, userInfo, getUserInfo } = useAuthentication();

  function clearSessionData(): void {
    clearUtmSessionData();
    removeUtmSessionStorageData();
  }

  function getUtmSessionData(): UtmSessionData {
    return isStoreReady ? utmStoreData : mergeUtmSessionData(utmStoreData);
  }

  function trackGlitrEvent(event: GlitrEvent): void {
    const trigger = () => {
      const utmSessionData = getUtmSessionData();

      const eventTypeReplaced = (event.eventType ?? 'subscriptionshop')
        .replaceAll(' ', '-')
        .replaceAll('%20', '-')
        .replaceAll('\n', '')
        .replaceAll('&nbsp;', '');
      const eventCategoryReplaced = (event.eventCategory ?? 'subscriptionshop')
        .replaceAll(' ', '-')
        .replaceAll('%20', '-')
        .replaceAll('\n', '')
        .replaceAll('&nbsp;', '');
      const eventActionReplaced = event.eventAction
        .replaceAll(' ', '-')
        .replaceAll('%20', '-')
        .replaceAll('\n', '')
        .replaceAll('&nbsp;', '');
      const eventLabelReplaced = event.eventLabel
        .replaceAll(' ', '-')
        .replaceAll('%20', '-')
        .replaceAll('\n', '')
        .replaceAll('&nbsp;', '');

      if (authenticationStatus === AUTHENTICATION_STATUS.UNKNOWN) {
        getUserInfo()
          .then(user => {
            glitrTracker.registerEvent(
              eventTypeReplaced,
              eventCategoryReplaced,
              eventActionReplaced,
              eventLabelReplaced,
              user?.id,
              utmSessionData
            );
          })
          .catch(() => {
            glitrTracker.registerEvent(
              eventTypeReplaced,
              eventCategoryReplaced,
              eventActionReplaced,
              eventLabelReplaced,
              undefined,
              utmSessionData
            );
          });
      } else {
        glitrTracker.registerEvent(
          eventTypeReplaced,
          eventCategoryReplaced,
          eventActionReplaced,
          eventLabelReplaced,
          userInfo?.id,
          utmSessionData
        );
      }
    };

    getDidomiConsent()
      .then(() => {
        trigger();
      })
      .catch(() => {
        trigger();
      });
  }

  function trackGtmEvent(eventName: string, eventData?: any): void {
    googleTagManager.registerEvent(eventName, eventData);
  }

  function trackGtmEcommerceEvent(
    eventName: EcommerceEventName,
    offer: Offer,
    type: OfferItem,
    formula: OfferItem
  ): void {
    googleTagManager.registerEcommerceEvent('Aboshop', eventName, [
      {
        offer: offer,
        type: type,
        formula: formula
      }
    ]);
  }

  function trackGtmEcommerceMutipleItemsEvent(
    eventName: EcommerceEventName,
    offer: Offer,
    parents?: Record<string, OfferItem | null>
  ): void {
    const offerItems = offer.items ?? [];
    const subscriptionTypes = offerItems.filter(i => i.type === OFFER_ITEM_TYPE.SubscriptionType);
    const subscriptionFormulas = offerItems.filter(
      i => i.type === OFFER_ITEM_TYPE.SubscriptionFormula
    );

    if (subscriptionFormulas.length === 0) {
      googleTagManager.registerEcommerceEvent(
        'Aboshop',
        eventName,
        subscriptionTypes.map(t => ({
          offer: offer,
          type: t,
          formula: undefined
        }))
      );
    } else {
      googleTagManager.registerEcommerceEvent(
        'Aboshop',
        eventName,
        subscriptionFormulas.map(f => ({
          offer: offer,
          type: f.parent && parents && parents[f.parent],
          formula: f
        }))
      );
    }
  }

  function trackPageView(location: Location): void {
    const trigger = () => {
      const utmSessionData = getUtmSessionData();

      if (authenticationStatus === AUTHENTICATION_STATUS.UNKNOWN) {
        getUserInfo()
          .then(user => {
            googleTagManager.registerPageView(user?.id);
            glitrTracker.registerPageView(location.href, user?.id, utmSessionData);
          })
          .catch(() => {
            googleTagManager.registerPageView(undefined);
            glitrTracker.registerPageView(location.href, undefined, utmSessionData);
          });
      } else {
        googleTagManager.registerPageView(userInfo?.id);
        glitrTracker.registerPageView(location.href, userInfo?.id, utmSessionData);
      }
    };

    getDidomiConsent()
      .then(() => {
        trigger();
      })
      .catch(() => {
        trigger();
      });
  }

  return {
    utmSessionData: getUtmSessionData(),
    clearSessionData,
    trackGlitrEvent,
    trackGtmEvent,
    trackGtmEcommerceEvent,
    trackGtmEcommerceMutipleItemsEvent,
    trackPageView
  };
};
