import type { ReactElement } from 'react';

import type { Validation } from '../types';

import { useRef } from 'react';

import { ValidatorsRender } from '../components/ValidatorsRender';
import { ValidatorsContext, createValidatorsStore } from './ValidatorsContext';

interface ValidatorsProviderProps {
  validations: Array<Validation>;
  onSubmit: (validations: Array<Validation>) => void;
  render: (validators: Array<ReactElement>) => ReactElement;
}

const ValidatorsProvider = ({
  validations,
  onSubmit,
  render
}: ValidatorsProviderProps): ReactElement => {
  const store = useRef(createValidatorsStore(validations, onSubmit)).current;

  return (
    <ValidatorsContext.Provider value={store}>
      <ValidatorsRender render={render} />
    </ValidatorsContext.Provider>
  );
};

export default ValidatorsProvider;
