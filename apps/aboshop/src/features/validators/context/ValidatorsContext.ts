import type { StoreApi } from 'zustand';

import type { Validation } from '../types';

import { VALIDATION_STATE } from '@subscriber/services-core';
import { createContext } from 'react';
import { createStore } from 'zustand';

import { validateValidation } from '../utils';

export interface ValidatorsStore {
  isSubmitting: boolean;
  subscribers: Record<number, () => Promise<Validation>>;
  validations: Array<Validation>;
  submit: () => void;
  subscribe: (id: number, submit: () => Promise<Validation>) => void;
}

export const ValidatorsContext = createContext<StoreApi<ValidatorsStore> | null>(null);

export const createValidatorsStore = (
  validations: Array<Validation>,
  onSubmit: (validations: Array<Validation>) => void
): StoreApi<ValidatorsStore> => {
  return createStore<ValidatorsStore>()((set, get) => ({
    isSubmitting: false,
    subscribers: {},
    validations,
    submit: () => {
      set({ isSubmitting: true });

      Promise.all(Object.values(get().subscribers).map(s => s()))
        .then(validatorResults => {
          Promise.allSettled(validatorResults.map(result => validateValidation(result))).then(
            validationResults => {
              const areValidationsCompliant = !validationResults.some(
                result =>
                  result.status === 'rejected' || result.value.state === VALIDATION_STATE.NotValid
              );
              const updatedValidations = get().validations.map(currentValidation => {
                const resultsMatch = validationResults.find(
                  result =>
                    result.status === 'fulfilled' && result.value.id === currentValidation.id
                );

                if (resultsMatch && resultsMatch.status === 'fulfilled') {
                  return resultsMatch.value;
                }

                return currentValidation;
              });

              if (areValidationsCompliant) {
                onSubmit(updatedValidations);
              }

              set({
                isSubmitting: false,
                validations: updatedValidations
              });
            }
          );
        })
        .catch(() => {
          set({ isSubmitting: false });
        });
    },
    subscribe: (id, submit) => {
      set({
        subscribers: {
          ...get().subscribers,
          [id]: submit
        }
      });
    }
  }));
};
