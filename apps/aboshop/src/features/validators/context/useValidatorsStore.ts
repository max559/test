import type { ValidatorsStore } from './ValidatorsContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { ValidatorsContext } from './ValidatorsContext';

const useValidatorsStore = <T>(selector: (state: ValidatorsStore) => T): T => {
  const validatorsContext = useContext(ValidatorsContext);

  if (!validatorsContext) {
    throw new Error('The useValidatorsStore hook can only be used in ValidatorsProvider');
  }

  return useStore(validatorsContext, selector);
};

export default useValidatorsStore;
