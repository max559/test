import type { FieldProps, FormikProps } from 'formik';

import { TextField } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import { ReactElement, RefObject } from 'react';
import * as Yup from 'yup';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

export interface InvoiceFormValues {
  companyName: string;
  vatNumber: string;
}

interface InvoiceFormProps {
  formRef: RefObject<FormikProps<InvoiceFormValues>>;
  initialValues: InvoiceFormValues;
  onSubmit: (values: InvoiceFormValues) => void;
}

export const InvoiceForm = ({
  formRef,
  initialValues,
  onSubmit
}: InvoiceFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const AddressFormSchema = Yup.object().shape({
    companyName: Yup.string().required(t('forms.invoice.companyNameRequired')),
    vatNumber: Yup.string().required(t('forms.invoice.vatNumberRequired'))
  });

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      validationSchema={AddressFormSchema}
      onSubmit={(values, actions) => {
        actions.setSubmitting(false);
        onSubmit(values);
      }}
    >
      <Form>
        <div className="flex gap-4">
          <Field name="vatNumber">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                className="grow"
                error={!!meta.touched && !!meta.error}
                id="VatNumberInput"
                label={t('forms.invoice.vatNumber')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                required
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              />
            )}
          </Field>

          <Field name="companyName">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                className="grow"
                error={!!meta.touched && !!meta.error}
                id="CompanyNameInput"
                label={t('forms.invoice.companyName')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                required
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              />
            )}
          </Field>
        </div>
      </Form>
    </Formik>
  );
};

export default InvoiceForm;
