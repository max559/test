import type { City, Country, Street } from '@subscriber/services-core';
import type { FieldProps, FormikProps } from 'formik';

import { AutoComplete, Select, TextField } from '@mediahuis/chameleon-react';
import { useAutocompleteCities, useAutocompleteStreets } from '@subscriber/services-hooks';
import { Field, Form, Formik } from 'formik';
import { ReactElement, RefObject } from 'react';
import * as Yup from 'yup';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

const overwriteInitialValues = (
  initialValues: AddressFormValues,
  countries: Array<Country>
): AddressFormValues => {
  if (!initialValues.country && countries.length === 1) {
    return {
      ...initialValues,
      country: countries[0].IsoCode
    };
  }

  return initialValues;
};

const mapCitySuggestions = (cities: Array<City>): Array<string> => {
  return cities.reduce((acc: Array<string>, city: City) => {
    if (city.Name && city.PostalCode) {
      return acc.concat(`${city.Name} (${city.PostalCode})`);
    }

    return acc;
  }, []);
};

const mapStreetSuggestions = (streets: Array<Street>): Array<string> => {
  return streets.reduce((acc: Array<string>, street: Street) => {
    if (street.Name) {
      return acc.concat(street.Name);
    }

    return acc;
  }, []);
};

export interface AddressFormValues {
  box: string;
  city: string;
  country: string;
  houseNumber: string;
  postalCode: string;
  street: string;
}

interface AddressFormProps {
  countries: Array<Country>;
  formRef: RefObject<FormikProps<AddressFormValues>>;
  initialValues: AddressFormValues;
  onSubmit: (values: AddressFormValues) => void;
}

export const AddressForm = ({
  countries,
  formRef,
  initialValues,
  onSubmit
}: AddressFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const autocompleteCities = useAutocompleteCities();
  const autocompleteStreets = useAutocompleteStreets();

  const AddressFormSchema = Yup.object().shape({
    box: Yup.string(),
    city: Yup.string().required(t('forms.address.cityRequired')),
    country: Yup.string().required(t('forms.address.countryRequired')),
    houseNumber: Yup.string().required(t('forms.address.houseNumberRequired')),
    postalCode: Yup.string().required(t('forms.address.postalCodeRequired')),
    street: Yup.string().nullable().required(t('forms.address.streetRequired'))
  });

  return (
    <Formik
      initialValues={overwriteInitialValues(initialValues, countries)}
      innerRef={formRef}
      validationSchema={AddressFormSchema}
      onSubmit={(values, actions) => {
        actions.setSubmitting(false);
        onSubmit(values);
      }}
    >
      {({ setFieldTouched, setFieldValue }) => (
        <Form>
          <div className="flex flex-col gap-2">
            <Field name="country">
              {({ field, meta }: FieldProps<string>) => (
                <Select
                  disabled={countries.length <= 1}
                  error={!!meta.touched && !!meta.error}
                  id="CountrySelect"
                  label={t('forms.address.country')}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                >
                  {countries.map((country: Country) => (
                    <option key={country.Name} value={country.IsoCode}>
                      {country.Name}
                    </option>
                  ))}
                </Select>
              )}
            </Field>

            <div className="flex gap-4">
              <Field name="postalCode">
                {({ field, form, meta }: FieldProps<string>) => (
                  <AutoComplete
                    className="basis-1/2"
                    disabled={!form.values.country}
                    error={!!meta.touched && !!meta.error}
                    id="PostalCodeAutoComplete"
                    label={t('forms.address.postalCode')}
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    suggestions={
                      autocompleteCities.isSuccess
                        ? mapCitySuggestions(autocompleteCities.data.Cities)
                        : []
                    }
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    onInputChange={value =>
                      autocompleteCities.mutate({
                        body: { Key: value },
                        countryIsoCode: form.values.country
                      })
                    }
                    onSelect={value => {
                      if (value) {
                        const splitValues = value.split(' ');

                        setFieldValue('city', splitValues[0]);
                        setFieldValue('postalCode', splitValues[1].slice(1, -1));
                      } else {
                        setFieldValue('postalCode', '');
                        setFieldValue('city', '');
                        setFieldTouched('city', false, false);
                        setFieldValue('street', '');
                        setFieldTouched('street', false, false);
                        setFieldValue('houseNumber', '');
                        setFieldTouched('houseNumber', false, false);
                        setFieldValue('box', '');
                      }
                    }}
                  />
                )}
              </Field>

              <Field name="city">
                {({ field, meta }: FieldProps<string>) => (
                  <TextField
                    className="basis-1/2"
                    disabled
                    error={!!meta.touched && !!meta.error}
                    id="CityInput"
                    label={t('forms.address.city')}
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    required
                    value={field.value}
                  />
                )}
              </Field>
            </div>

            <Field name="street">
              {({ field, form, meta }: FieldProps<string>) => (
                <AutoComplete
                  disabled={!form.values.city || !form.values.postalCode}
                  error={!!meta.touched && !!meta.error}
                  id="StreetAutoComplete"
                  label={t('forms.address.street')}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  suggestions={
                    autocompleteStreets.isSuccess
                      ? mapStreetSuggestions(autocompleteStreets.data.Streets)
                      : []
                  }
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                  onInputChange={value => {
                    autocompleteStreets.mutate({
                      body: { Key: value, PostalCode: form.values.postalCode },
                      countryIsoCode: form.values.country
                    });
                  }}
                  onSelect={value => {
                    if (value) {
                      setFieldValue('street', value);
                    } else {
                      setFieldValue('street', '');
                      setFieldValue('houseNumber', '');
                      setFieldTouched('houseNumber', false, false);
                      setFieldValue('box', '');
                    }
                  }}
                />
              )}
            </Field>

            <div className="flex gap-4">
              <Field name="houseNumber">
                {({ field, form, meta }: FieldProps<string>) => (
                  <TextField
                    className="basis-1/2"
                    disabled={!form.values.street}
                    error={!!meta.touched && !!meta.error}
                    id="HouseNumberInput"
                    label={t('forms.address.houseNumber')}
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    required
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                  />
                )}
              </Field>
              <Field name="box">
                {({ field, form, meta }: FieldProps<string>) => (
                  <TextField
                    className="basis-1/2"
                    disabled={!form.values.street}
                    error={!!meta.touched && !!meta.error}
                    id="BoxInput"
                    label={t('forms.address.box')}
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    required={false}
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                  />
                )}
              </Field>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default AddressForm;
