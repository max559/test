import type {
  AddressAreaValidation,
  BooleanValidation,
  ChoiceValidation,
  DateValidation,
  EmailValidation,
  FileValidation,
  InvoiceDetailsValidation,
  TextfieldValidation
} from '@subscriber/services-core';

import type { ReactElement } from 'react';

import type { Validation } from './types';

import { REQUIREMENT_TYPE, VALIDATION_STATE } from '@subscriber/services-core';
import { isValidPhoneNumber } from 'react-phone-number-input';

import { servicesCoreInstance } from 'globals';

import { AddressValidator } from './components/AddressValidator';
import { BooleanValidator } from './components/BooleanValidator';
import { ChoiceValidator } from './components/ChoiceValidator';
import { DateValidator } from './components/DateValidator';
import { DeliveryValidator } from './components/DeliveryValidator';
import { EmailValidator } from './components/EmailValidator';
import { FileValidator } from './components/FileValidator';
import { InvoiceValidator } from './components/InvoiceValidator';
import { PhoneValidator } from './components/PhoneValidator';
import { TextfieldValidator } from './components/TextfieldValidator';
import { ValidatorBlock } from './components/ValidatorBlock';

export const convertValidationsToValidators = (
  validations: Array<Validation>
): Array<ReactElement> => {
  const sortedValidations = [
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.Email),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.AddressArea),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.Textfield),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.Choice),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.Date),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.InvoiceDetails),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.File),
    validations.filter(validation => validation.type === REQUIREMENT_TYPE.Boolean)
  ].flat();

  return sortedValidations.map(mapValidationToValidator);
};

export const mapValidationToValidator = (validation: Validation): ReactElement => {
  if (validation.type === REQUIREMENT_TYPE.AddressArea) {
    if (validation.pickUpAllowed) {
      return <DeliveryValidator key={validation.id} validation={validation} />;
    }

    return <AddressValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.Boolean) {
    return <BooleanValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.Choice) {
    return <ChoiceValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.Date) {
    return <DateValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.Email) {
    return <EmailValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.File) {
    return <FileValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.InvoiceDetails) {
    return <InvoiceValidator key={validation.id} validation={validation} />;
  }
  if (validation.type === REQUIREMENT_TYPE.Textfield) {
    if (validation.key === 'phoneNumber') {
      return <PhoneValidator key={validation.id} validation={validation} />;
    }

    return <TextfieldValidator key={validation.id} validation={validation} />;
  }

  return (
    <ValidatorBlock key={0} title="">
      {null}
    </ValidatorBlock>
  );
};

export const validateValidation = (validation: Validation): Promise<Validation> => {
  if (validation.type === REQUIREMENT_TYPE.AddressArea) {
    return validateAddressArea(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.Boolean) {
    return validateBoolean(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.Choice) {
    return validateChoice(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.Date) {
    return validateDate(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.Email) {
    return validateEmail(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.File) {
    return validateFile(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.InvoiceDetails) {
    return validateInvoice(validation);
  }
  if (validation.type === REQUIREMENT_TYPE.Textfield) {
    return validateTextfield(validation);
  }

  return Promise.reject();
};

export const validateAddressArea = (
  addressAreaValidation: AddressAreaValidation
): Promise<AddressAreaValidation> => {
  return servicesCoreInstance.locationValidatorService
    .postAddressAreaValidation(addressAreaValidation)
    .then(response => response.data);
};

export const validateBoolean = (
  booleanValidation: BooleanValidation
): Promise<BooleanValidation> => {
  return servicesCoreInstance.defaultValidatorService
    .postBoolValidation(booleanValidation)
    .then(response => response.data);
};

export const validateChoice = (choiceValidation: ChoiceValidation): Promise<ChoiceValidation> => {
  return servicesCoreInstance.defaultValidatorService
    .postChoiceValidation(choiceValidation)
    .then(response => response.data);
};

export const validateDate = (dateValidation: DateValidation): Promise<DateValidation> => {
  return servicesCoreInstance.defaultValidatorService
    .postDateValidation(dateValidation)
    .then(response => response.data);
};

export const validateEmail = (emailValidation: EmailValidation): Promise<EmailValidation> => {
  return servicesCoreInstance.defaultValidatorService
    .postEmailValidation(emailValidation)
    .then(response => response.data);
};

export const validateFile = (fileValidation: FileValidation): Promise<FileValidation> => {
  return servicesCoreInstance.fileValidatorService
    .postFileValidation(fileValidation)
    .then(response => response.data);
};

export const validateInvoice = (
  invoiceValidation: InvoiceDetailsValidation
): Promise<InvoiceDetailsValidation> => {
  return servicesCoreInstance.locationValidatorService
    .postInvoiceDetailsValidation(invoiceValidation)
    .then(response => response.data);
};

export const validateTextfield = (
  textfieldValidation: TextfieldValidation
): Promise<TextfieldValidation> => {
  if (textfieldValidation.key === 'phoneNumber') {
    if (!isValidPhoneNumber(textfieldValidation?.data.value ?? '')) {
      textfieldValidation.state = VALIDATION_STATE.NotValid;

      return Promise.resolve(textfieldValidation);
    }
  }

  return servicesCoreInstance.textfieldValidatorService
    .postTextfieldValidation(textfieldValidation)
    .then(response => response.data);
};
