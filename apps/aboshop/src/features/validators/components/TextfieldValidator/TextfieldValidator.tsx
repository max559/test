import type { TextfieldValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import { useEffect, useState } from 'react';

import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

interface TextfieldValidatorProps {
  validation: TextfieldValidation;
}

const TextfieldValidator = ({ validation }: TextfieldValidatorProps): ReactElement => {
  const isError = validation.state === VALIDATION_STATE.NotValid;

  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [textValue, setTextValue] = useState<string | undefined>(validation.data.value);

  useEffect(() => {
    subscribe(validation.id, () => Promise.resolve({ ...validation, data: { value: textValue } }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [textValue]);

  return (
    <ValidatorBlock
      description={validation.description}
      error={isError ? t('textfield.error') : undefined}
      title={validation.name || t('textfield.title')}
    >
      <TextField
        error={isError}
        id={validation.id.toString()}
        label=""
        labelHidden
        value={textValue}
        onChange={event => setTextValue(event.target.value)}
      />
    </ValidatorBlock>
  );
};

export default TextfieldValidator;
