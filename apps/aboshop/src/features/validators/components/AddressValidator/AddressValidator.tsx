import type { AddressAreaValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { VALIDATION_STATE } from '@subscriber/services-core';

import { useTranslation } from 'hooks';

import { ValidatorBlock } from '../ValidatorBlock';
import AddressValidatorDisplay from './AddressValidatorDisplay';
import AddressValidatorEdit from './AddressValidatorEdit';
import { useAddressValidator } from './useAddressValidator';

const translationNamespace = 'features/validators';

interface AddressValidatorProps {
  validation: AddressAreaValidation;
}

const AddressValidator = ({ validation }: AddressValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const { addressFormRef, defaultAddressFormValues, isEditing, setIsEditing } =
    useAddressValidator(validation);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('address.error') : undefined}
      title={t('address.title')}
    >
      {isEditing ? (
        <AddressValidatorEdit
          allowedCountries={validation.countries ?? []}
          formRef={addressFormRef}
          initialValues={defaultAddressFormValues}
        />
      ) : (
        <AddressValidatorDisplay
          addressFormValues={defaultAddressFormValues}
          onEdit={() => setIsEditing(true)}
        />
      )}
    </ValidatorBlock>
  );
};

export default AddressValidator;
