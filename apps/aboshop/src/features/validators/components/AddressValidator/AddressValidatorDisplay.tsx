import type { ReactElement } from 'react';

import type { AddressFormValues } from '../../forms';

import { Button, Paragraph } from '@mediahuis/chameleon-react';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

interface AddressValidatorDisplayProps {
  addressFormValues: AddressFormValues;
  onEdit: () => void;
}

const AddressValidatorDisplay = ({
  addressFormValues,
  onEdit
}: AddressValidatorDisplayProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <div className="flex flex-col gap-4">
      <div>
        <Paragraph>
          {`${addressFormValues.street ?? ''} ${addressFormValues.houseNumber ?? ''} ${
            addressFormValues.box ?? ''
          }`}
        </Paragraph>
        <Paragraph>{`${addressFormValues.postalCode ?? ''} ${
          addressFormValues.city ?? ''
        }`}</Paragraph>
        <Paragraph>{addressFormValues.country}</Paragraph>
      </div>

      <span>
        <Button appearance="primary" size="sm" onClick={onEdit}>
          {t('address.editButton')}
        </Button>
      </span>
    </div>
  );
};

export default AddressValidatorDisplay;
