import type { AddressAreaValidation } from '@subscriber/services-core';
import type { FormikProps } from 'formik';

import type { UserInfo } from 'features/authentication';

import type { AddressFormValues } from '../../forms';

import { useEffect, useRef, useState } from 'react';

import { useAuthentication } from 'features/authentication';

import { useValidatorsStore } from '../../context';

const getDefaultAddressFormValues = (
  validation: AddressAreaValidation,
  userInfo: UserInfo | null
): AddressFormValues => ({
  box: validation.data.busNumber || userInfo?.address.box || '',
  city: validation.data.city || userInfo?.address.city || '',
  country: validation.data.countryCode || userInfo?.address.country || '',
  houseNumber: validation.data.houseNumber || userInfo?.address.houseNumber || '',
  postalCode: validation.data.postalCode || userInfo?.address.postalCode || '',
  street: validation.data.street || userInfo?.address.street || ''
});

export const useAddressValidator = (validation: AddressAreaValidation) => {
  const { userInfo } = useAuthentication();

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isEditing, setIsEditing] = useState<boolean>(!validation.data.street);

  const addressFormRef = useRef<FormikProps<AddressFormValues>>(null);

  const defaultAddressFormValues = getDefaultAddressFormValues(validation, userInfo);

  useEffect(() => {
    subscribe(
      validation.id,
      () =>
        new Promise((resolve, reject) => {
          if (addressFormRef.current) {
            addressFormRef.current?.submitForm().then(() => {
              if (addressFormRef.current?.isValid) {
                const { box, city, country, houseNumber, postalCode, street } =
                  addressFormRef.current.values;

                resolve({
                  ...validation,
                  data: {
                    addressee: `${userInfo?.lastName} ${userInfo?.firstName}`,
                    busNumber: box,
                    city,
                    countryCode: country,
                    houseNumber,
                    postalCode,
                    street
                  }
                });
              } else {
                reject();
              }
            });
          } else {
            resolve(validation);
          }
        })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addressFormRef.current]);

  return {
    addressFormRef,
    defaultAddressFormValues,
    isEditing,
    setIsEditing
  };
};
