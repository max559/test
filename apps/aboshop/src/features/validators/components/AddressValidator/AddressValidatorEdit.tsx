import type { FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import type { AddressFormValues } from '../../forms';

import { useCountriesByLanguage } from '@subscriber/services-hooks';

import { Spinner } from 'components/Spinner';
import { useTranslation } from 'hooks';

import { AddressForm } from '../../forms';

const translationNamespace = 'features/validators';

interface AddressValidatorEditProps {
  allowedCountries: Array<string>;
  formRef: RefObject<FormikProps<AddressFormValues>>;
  initialValues: AddressFormValues;
}

const AddressValidatorEdit = ({
  allowedCountries,
  formRef,
  initialValues
}: AddressValidatorEditProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const countriesByLanguageQuery = useCountriesByLanguage('nl');

  if (countriesByLanguageQuery.isSuccess) {
    const countries =
      allowedCountries.length > 0
        ? countriesByLanguageQuery.data.filter(country =>
            allowedCountries.includes(country.IsoCode)
          )
        : countriesByLanguageQuery.data;

    return (
      <AddressForm
        countries={countries}
        formRef={formRef}
        initialValues={initialValues}
        onSubmit={() => undefined}
      />
    );
  }

  return <Spinner text={t('address.loading')} />;
};

export default AddressValidatorEdit;
