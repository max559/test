import type { EmailValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Banner, Button, Caption, Paragraph } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import { useEffect, useState } from 'react';

import { useAuthentication } from 'features/authentication';
import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

interface EmailValidatorProps {
  validation: EmailValidation;
}

const EmailValidator = ({ validation }: EmailValidatorProps): ReactElement => {
  const { userInfo, redirectToSignOut } = useAuthentication();
  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isChanging, setIsChanging] = useState<boolean>(false);

  useEffect(() => {
    subscribe(validation.id, () =>
      Promise.resolve({ ...validation, data: { value: userInfo?.email } })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('email.error') : undefined}
      title={t('email.title')}
    >
      <div className="flex flex-col gap-2">
        <Paragraph weight="strong">{validation.data.value || userInfo?.email}</Paragraph>

        <Caption color="colorGrey60" size="sm">
          {t('email.caption')}
        </Caption>

        {isChanging ? (
          <Banner appearance="error" closeHidden show>
            <div className="flex flex-col gap-2">
              <Paragraph>{t('email.textConfirmChange')}</Paragraph>

              <span className="flex gap-2">
                <Button appearance="primary" size="sm" onClick={() => redirectToSignOut()}>
                  {t('email.buttonConfirm')}
                </Button>
                <Button appearance="secondary" size="sm" onClick={() => setIsChanging(false)}>
                  {t('email.buttonCancel')}
                </Button>
              </span>
            </div>
          </Banner>
        ) : (
          <span>
            <Button appearance="primary" size="sm" onClick={() => setIsChanging(true)}>
              {t('email.buttonChange')}
            </Button>
          </span>
        )}
      </div>
    </ValidatorBlock>
  );
};

export default EmailValidator;
