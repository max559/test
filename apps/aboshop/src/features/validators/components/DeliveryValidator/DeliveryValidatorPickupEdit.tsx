import type { City, Shop } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { AutoComplete, Button, Caption, Paragraph } from '@mediahuis/chameleon-react';
import { useAutocompleteCities, useShops } from '@subscriber/services-hooks';
import { useState } from 'react';

import { Spinner } from 'components/Spinner';
import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

const mapCitySuggestions = (cities: Array<City>): Array<string> => {
  return cities.reduce((acc: Array<string>, city: City) => {
    if (city.Name && city.PostalCode) {
      return acc.concat(`${city.Name} (${city.PostalCode})`);
    }

    return acc;
  }, []);
};
interface DeliveryValidatorPickupEditProps {
  defaultPostalCode?: string;
  onSelect: (shop: Shop) => void;
}

const DeliveryValidatorPickupEdit = ({
  defaultPostalCode,
  onSelect
}: DeliveryValidatorPickupEditProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const [postalCode, setPostalCode] = useState<string | undefined>(defaultPostalCode);

  const autocompleteCities = useAutocompleteCities();
  const shopsQuery = useShops(MH_BRAND, postalCode || '');

  return (
    <div className="flex flex-col gap-4">
      <AutoComplete
        // error={!!meta.touched && !!meta.error}
        id="PostalCodeAutoComplete"
        label={t('delivery.postalCodeAutoCompleteLabel')}
        // message={meta.touched && meta.error ? meta.error : undefined}
        // name={field.name}
        suggestions={
          autocompleteCities.isSuccess ? mapCitySuggestions(autocompleteCities.data.Cities) : []
        }
        value={postalCode}
        onChange={value => setPostalCode(value)}
        onInputChange={value =>
          autocompleteCities.mutate({
            body: { Key: value },
            countryIsoCode: 'BE'
          })
        }
        onSelect={value => {
          if (value) {
            const postalCodeFromValue = value.split(' ')[1].slice(1, -1);

            setPostalCode(postalCodeFromValue);
          }
        }}
      />

      {shopsQuery.isLoading ? (
        <Spinner text="lkdf" />
      ) : (
        <div className="flex flex-col gap-2">
          {shopsQuery.data?.map(shop => (
            <div
              className="border border-solid flex flex-wrap gap-0.5 p-2 rounded"
              key={shop.shopId}
            >
              <div className="basis-60 flex flex-col gap-0.5 grow">
                <Paragraph>{shop.deliveryStore}</Paragraph>

                <div className="flex flex-col">
                  <Caption>{`${shop.street ?? ''} ${shop.houseNumber ?? ''} ${
                    shop.bus ?? ''
                  }`}</Caption>
                  <Caption>{`${shop.postalCode ?? ''} ${shop.city ?? ''}`}</Caption>
                </div>
              </div>

              <div className="basis-28 flex items-center justify-center">
                <Button appearance="primary" size="sm" width="full" onClick={() => onSelect(shop)}>
                  {t('delivery.chooseShopButton')}
                </Button>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default DeliveryValidatorPickupEdit;
