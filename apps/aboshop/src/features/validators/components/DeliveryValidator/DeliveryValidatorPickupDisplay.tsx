import type { Shop } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Button, Paragraph } from '@mediahuis/chameleon-react';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

interface DeliveryValidatorPickupDisplayProps {
  shop: Shop;
  onEdit: () => void;
}

const DeliveryValidatorPickupDisplay = ({
  shop,
  onEdit
}: DeliveryValidatorPickupDisplayProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <div className="flex flex-col gap-4">
      <div>
        <Paragraph>{shop.deliveryStore}</Paragraph>
        <Paragraph>{`${shop.street ?? ''} ${shop.houseNumber ?? ''} ${shop.bus ?? ''}`}</Paragraph>
        <Paragraph>{`${shop.postalCode ?? ''} ${shop.city ?? ''}`}</Paragraph>
      </div>

      <span>
        <Button appearance="primary" size="sm" onClick={onEdit}>
          {t('delivery.editButton')}
        </Button>
      </span>
    </div>
  );
};

export default DeliveryValidatorPickupDisplay;
