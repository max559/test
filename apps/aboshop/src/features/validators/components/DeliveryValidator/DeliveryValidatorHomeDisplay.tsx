import type { ReactElement } from 'react';

import type { AddressFormValues } from '../../forms/AddressForm';

import { Button, Paragraph } from '@mediahuis/chameleon-react';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

interface DeliveryValidatorHomeDisplayProps {
  addressFormValues: AddressFormValues;
  onClick: () => void;
}

const DeliveryValidatorHomeDisplay = ({
  addressFormValues,
  onClick
}: DeliveryValidatorHomeDisplayProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <div className="flex flex-col gap-4">
      <div>
        <Paragraph>
          {`${addressFormValues.street ?? ''} ${addressFormValues.houseNumber ?? ''} ${
            addressFormValues.box ?? ''
          }`}
        </Paragraph>
        <Paragraph>{`${addressFormValues.postalCode ?? ''} ${
          addressFormValues.city ?? ''
        }`}</Paragraph>
        <Paragraph>{addressFormValues.country}</Paragraph>
      </div>

      <span>
        <Button appearance="primary" size="sm" onClick={onClick}>
          {t('delivery.editButton')}
        </Button>
      </span>
    </div>
  );
};

export default DeliveryValidatorHomeDisplay;
