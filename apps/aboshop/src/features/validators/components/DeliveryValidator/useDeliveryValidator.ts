import type { AddressAreaValidation, Shop } from '@subscriber/services-core';
import type { FormikProps } from 'formik';

import type { UserInfo } from 'features/authentication';

import type { AddressFormValues } from '../../forms';

import { useEffect, useRef, useState } from 'react';

import { useAuthentication } from 'features/authentication';

import { useValidatorsStore } from '../../context';

const insertUserInfoInAddressFormValues = (
  addressFormValues: AddressFormValues,
  userInfo: UserInfo | null
): AddressFormValues => ({
  box: userInfo?.address.box || addressFormValues.box,
  city: userInfo?.address.city || addressFormValues.city,
  country: userInfo?.address.country || addressFormValues.country,
  houseNumber: userInfo?.address.houseNumber || addressFormValues.houseNumber,
  postalCode: userInfo?.address.postalCode || addressFormValues.postalCode,
  street: userInfo?.address.street || addressFormValues.street
});

const insertValidationInAddressFormValues = (
  addressFormValues: AddressFormValues,
  validation: AddressAreaValidation
): AddressFormValues => ({
  box: validation.data?.busNumber || addressFormValues.box,
  city: validation.data.city || addressFormValues.city,
  country: validation.data.countryCode || addressFormValues.country,
  houseNumber: validation.data.busNumber || addressFormValues.houseNumber,
  postalCode: validation.data.postalCode || addressFormValues.postalCode,
  street: validation.data.street || addressFormValues.street
});

const getDefaultAddressFormValues = (
  validation: AddressAreaValidation,
  userInfo: UserInfo | null
): AddressFormValues => {
  const defaultFormValues: AddressFormValues = {
    box: '',
    city: '',
    country: '',
    houseNumber: '',
    postalCode: '',
    street: ''
  };
  const hasPickupData = Boolean(validation.data.shopId);

  if (hasPickupData) {
    return insertUserInfoInAddressFormValues(defaultFormValues, userInfo);
  }

  return insertValidationInAddressFormValues(defaultFormValues, validation);
};

export const useDeliveryValidator = (validation: AddressAreaValidation) => {
  const hasPickupData = Boolean(validation.data.shopId);

  const { userInfo } = useAuthentication();

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isEditingDelivery, setIsEditingDelivery] = useState<boolean>(
    !hasPickupData && !validation.data.street
  );
  const [isEditingPickup, setIsEditingPickup] = useState<boolean>(!hasPickupData);
  const [selectedMethod, setSelectedMethod] = useState<'delivery' | 'pickup'>(
    hasPickupData ? 'pickup' : 'delivery'
  );
  const [selectedShop, setSelectedShop] = useState<Shop | undefined>(
    hasPickupData
      ? {
          city: validation.data.city,
          bus: validation.data.busNumber,
          countryCode: validation.data.countryCode,
          deliveryStore: validation.data.deliveryStore,
          houseNumber: validation.data.houseNumber,
          postalCode: validation.data.postalCode,
          shopId: validation.data.shopId as number,
          street: validation.data.street
        }
      : undefined
  );

  const addressFormRef = useRef<FormikProps<AddressFormValues>>(null);

  const defaultAddressFormValues = getDefaultAddressFormValues(validation, userInfo);

  useEffect(() => {
    if (selectedMethod === 'delivery') {
      if (isEditingDelivery) {
        subscribe(
          validation.id,
          () =>
            new Promise((resolve, reject) => {
              if (addressFormRef.current) {
                addressFormRef.current.submitForm().then(() => {
                  if (addressFormRef.current?.isValid) {
                    const { box, city, country, houseNumber, postalCode, street } =
                      addressFormRef.current.values;

                    resolve({
                      ...validation,
                      data: {
                        addressee: `${userInfo?.lastName} ${userInfo?.firstName}`,
                        busNumber: box,
                        city,
                        countryCode: country,
                        houseNumber,
                        postalCode,
                        street
                      }
                    });
                  } else {
                    reject();
                  }
                });
              } else {
                resolve(validation);
              }
            })
        );
      } else {
        // TODO: Filter out empty strings in defaultAddressFormValues
        subscribe(validation.id, () =>
          Promise.resolve({ ...validation, data: defaultAddressFormValues })
        );
      }
    }

    if (selectedMethod === 'pickup') {
      if (isEditingDelivery) {
        setIsEditingDelivery(false);
      }

      subscribe(validation.id, () =>
        Promise.resolve({
          ...validation,
          data: selectedShop ? { ...selectedShop } : {}
        })
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEditingDelivery, isEditingPickup, selectedMethod, selectedShop]);

  return {
    addressFormRef,
    defaultAddressFormValues,
    isEditingDelivery,
    isEditingPickup,
    selectedMethod,
    selectedShop,
    setIsEditingDelivery,
    setIsEditingPickup,
    setSelectedMethod,
    setSelectedShop
  };
};
