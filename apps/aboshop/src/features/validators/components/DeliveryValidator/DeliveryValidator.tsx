import type { AddressAreaValidation, Shop } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Choice } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';

import { useTranslation } from 'hooks';

import { ValidatorBlock } from '../ValidatorBlock';
import DeliveryValidatorHomeDisplay from './DeliveryValidatorHomeDisplay';
import DeliveryValidatorHomeEdit from './DeliveryValidatorHomeEdit';
import DeliveryValidatorPickupDisplay from './DeliveryValidatorPickupDisplay';
import DeliveryValidatorPickupEdit from './DeliveryValidatorPickupEdit';
import { useDeliveryValidator } from './useDeliveryValidator';

const translationNamespace = 'features/validators';

interface DeliveryValidatorProps {
  validation: AddressAreaValidation;
}

const DeliveryValidator = ({ validation }: DeliveryValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const {
    addressFormRef,
    defaultAddressFormValues,
    isEditingDelivery,
    isEditingPickup,
    selectedMethod,
    selectedShop,
    setIsEditingDelivery,
    setIsEditingPickup,
    setSelectedMethod,
    setSelectedShop
  } = useDeliveryValidator(validation);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('delivery.error') : undefined}
      title={t('delivery.title')}
    >
      <div className="flex flex-col gap-2">
        <Choice
          checked={selectedMethod === 'delivery'}
          id="AddressAreaDelivery"
          message={t('delivery.deliveryMessage')}
          name="delivery-validator-choice"
          title={t('delivery.deliveryLabel')}
          value="delivery"
          onChange={() => setSelectedMethod('delivery')}
        >
          {isEditingDelivery ? (
            <DeliveryValidatorHomeEdit
              allowedCountries={validation.countries ?? []}
              formRef={addressFormRef}
              initialValues={defaultAddressFormValues}
            />
          ) : (
            <DeliveryValidatorHomeDisplay
              addressFormValues={defaultAddressFormValues}
              onClick={() => setIsEditingDelivery(true)}
            />
          )}
        </Choice>

        <Choice
          checked={selectedMethod === 'pickup'}
          id="AddressAreaPickup"
          message={t('delivery.pickupMessage')}
          name="delivery-validator-choice"
          title={t('delivery.pickupLabel')}
          value="pickup"
          onChange={() => setSelectedMethod('pickup')}
        >
          {isEditingPickup ? (
            <DeliveryValidatorPickupEdit
              defaultPostalCode={validation.data.postalCode}
              onSelect={shop => {
                setIsEditingPickup(false);
                setSelectedShop(shop);
              }}
            />
          ) : (
            <DeliveryValidatorPickupDisplay
              shop={selectedShop as Shop}
              onEdit={() => setIsEditingPickup(true)}
            />
          )}
        </Choice>
      </div>
    </ValidatorBlock>
  );
};

export default DeliveryValidator;
