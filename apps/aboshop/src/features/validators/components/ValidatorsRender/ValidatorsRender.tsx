import type { ReactElement } from 'react';

import { useValidatorsStore } from '../../context';
import { convertValidationsToValidators } from '../../utils';

interface ValidatorsRenderProps {
  render: (validators: Array<ReactElement>) => ReactElement;
}

const ValidatorsRender = ({ render }: ValidatorsRenderProps) => {
  const validations = useValidatorsStore(state => state.validations);

  return <>{render(convertValidationsToValidators(validations))}</>;
};

export default ValidatorsRender;
