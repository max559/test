import type { DateValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Choice, DatePicker } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';

import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

const getDefaultStartDate = (validation: DateValidation): Date => {
  if (validation.data.value) {
    return dayjs(validation.data.value, 'DD/MM/YYYY').toDate();
  }

  return new Date(validation.timeFrameStart || '');
};

interface DateValidatorProps {
  validation: DateValidation;
}

const DateValidator = ({ validation }: DateValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [selectedDate, setSelectedDate] = useState<Date>(getDefaultStartDate(validation));
  const [selectedRadio, setSelectedRadio] = useState<'asap' | 'specific'>(
    validation.data.value ? 'specific' : 'asap'
  );

  const defaultStartDate = new Date(validation.timeFrameStart || '');

  useEffect(() => {
    subscribe(validation.id, () =>
      Promise.resolve({
        ...validation,
        data: {
          ...validation.data,
          value: selectedRadio === 'specific' ? dayjs(selectedDate).format('DD/MM/YYYY') : undefined
        }
      })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDate, selectedRadio]);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('date.error') : undefined}
      title={t('date.title')}
    >
      <div className="flex flex-col gap-2">
        <Choice
          checked={selectedRadio === 'asap'}
          id="DateAsap"
          name="date-validator-choice"
          value="asap"
          title={t('date.choiceTitleAsap')}
          onChange={() => setSelectedRadio('asap')}
        />

        <Choice
          checked={selectedRadio === 'specific'}
          id="DateSpecific"
          name="date-validator-choice"
          value="specific"
          title={t('date.choiceTitleSpecific')}
          onChange={() => setSelectedRadio('specific')}
        >
          <DatePicker
            disabled={selectedRadio === 'asap'}
            disabledDays={{
              after: new Date(validation.timeFrameEnd || ''),
              before: new Date(validation.timeFrameStart || '')
            }}
            id="DateSpecificPicker"
            label={t('date.datePickerLabel')}
            labelHidden
            value={selectedDate}
            onChange={date => setSelectedDate(date || defaultStartDate)}
          />
        </Choice>
      </div>
    </ValidatorBlock>
  );
};

export default DateValidator;
