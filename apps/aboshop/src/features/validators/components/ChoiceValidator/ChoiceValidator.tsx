import type { ChoiceValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Select } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import { useEffect, useState } from 'react';

import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

interface ChoiceValidatorProps {
  validation: ChoiceValidation;
}

const ChoiceValidator = ({ validation }: ChoiceValidatorProps): ReactElement => {
  const isError = validation.state === VALIDATION_STATE.NotValid;

  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [selectValue, setSelectValue] = useState<string | undefined>(validation.data.value);

  useEffect(() => {
    subscribe(validation.id, () =>
      Promise.resolve({ ...validation, data: { value: selectValue } })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectValue]);

  return (
    <ValidatorBlock
      description={validation.description}
      error={isError ? t('choice.error') : undefined}
      title={validation.name || t('choice.title')}
    >
      <Select
        error={validation.state === VALIDATION_STATE.NotValid}
        id={validation.id.toString()}
        label=""
        placeholder={t('choice.selectPlaceholder')}
        value={selectValue}
        onChange={event => setSelectValue(event.target.value)}
      >
        {(validation.choices ?? []).map(option => (
          <option key={option}>{option}</option>
        ))}
      </Select>
    </ValidatorBlock>
  );
};

export default ChoiceValidator;
