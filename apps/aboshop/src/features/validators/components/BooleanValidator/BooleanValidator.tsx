import type { BooleanValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Checkbox, LinkText } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import Markdown from 'markdown-to-jsx';
import { useEffect, useState } from 'react';

import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';

const translationNamespace = 'features/validators';

interface BooleanValidatorProps {
  validation: BooleanValidation;
}

const BooleanValidator = ({ validation }: BooleanValidatorProps): ReactElement => {
  const isError = validation.state === VALIDATION_STATE.NotValid;

  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isChecked, setIsChecked] = useState<boolean>(
    validation.isPrechecked ? true : validation.data.value
  );

  useEffect(() => {
    subscribe(validation.id, () => Promise.resolve({ ...validation, data: { value: isChecked } }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isChecked]);

  return (
    <Checkbox
      checked={isChecked}
      error={isError}
      id={validation.id.toString()}
      label={
        <Markdown options={{ forceInline: true, overrides: { a: { component: LinkText } } }}>
          {validation.description ?? ''}
        </Markdown>
      }
      message={isError ? t('boolean.error') : undefined}
      onChange={() => setIsChecked(prevIsChecked => !prevIsChecked)}
    />
  );
};

export default BooleanValidator;
