import type { FileValidation, FileshareDto } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Button, Paragraph } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';
import { useEffect, useState } from 'react';

import { servicesCoreInstance } from 'globals';
import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

interface FileValidatorProps {
  validation: FileValidation;
}

const FileValidator = ({ validation }: FileValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isEditing, setIsEditing] = useState<boolean>(Boolean(!validation.data.filePath));
  const [uploadedFile, setUploadedFile] = useState<FileshareDto | null>(null);

  useEffect(() => {
    subscribe(validation.id, () => {
      const filePath = uploadedFile ? uploadedFile.id : validation.data.filePath;

      return Promise.resolve({
        ...validation,
        data: {
          filePath
        }
      });
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [uploadedFile]);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('file.error') : undefined}
      title={t('file.title')}
    >
      {isEditing ? (
        <input
          type="file"
          onChange={event => {
            if (event.target.files) {
              servicesCoreInstance.fileshareService
                .uploadImage(event.target.files[0])
                .then(response => {
                  setUploadedFile(response.data);
                })
                .catch(() => {
                  setUploadedFile(null);
                });
            } else {
              setUploadedFile(null);
            }
          }}
        />
      ) : (
        <div className="flex flex-col gap-2">
          <Paragraph>{t('file.text')}</Paragraph>

          <span>
            <Button appearance="primary" size="sm" onClick={() => setIsEditing(true)}>
              {t('file.editButton')}
            </Button>
          </span>
        </div>
      )}
    </ValidatorBlock>
  );
};

export default FileValidator;
