import type { ReactElement } from 'react';

import { useValidatorsStore } from '../../context';

interface ValidatorsSubmitProps {
  render: ({ isSubmitting, submit }: { isSubmitting: boolean; submit: () => void }) => ReactElement;
}

const ValidatorsSubmit = ({ render }: ValidatorsSubmitProps): ReactElement => {
  const isSubmitting = useValidatorsStore(state => state.isSubmitting);
  const submit = useValidatorsStore(state => state.submit);

  return <>{render({ isSubmitting, submit })}</>;
};

export default ValidatorsSubmit;
