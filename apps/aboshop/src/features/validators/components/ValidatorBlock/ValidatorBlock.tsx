import type { PropsWithChildren, ReactElement } from 'react';

import { Caption, Divider, Heading } from '@mediahuis/chameleon-react';

import { ErrorMessage } from 'components/ErrorMessage';

interface ValidatorBlockProps {
  description?: string;
  error?: string;
  title: string;
}

const ValidatorBlock = ({
  children,
  description,
  error,
  title
}: PropsWithChildren<ValidatorBlockProps>): ReactElement => {
  return (
    <div>
      <span className="flex flex-wrap items-center justify-between">
        <Heading level={6} size="xs">
          {title}
        </Heading>

        {description && <Caption>{description}</Caption>}
      </span>

      {Boolean(error) && <ErrorMessage>{error}</ErrorMessage>}

      <Divider className="!border-blackBase" />

      <div className={`${error ? 'bg-red10' : 'bg-neutral10'} p-6 rounded`}>{children}</div>
    </div>
  );
};

export default ValidatorBlock;
