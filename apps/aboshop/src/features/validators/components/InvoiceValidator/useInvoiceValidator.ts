import type { InvoiceDetailsValidation } from '@subscriber/services-core';
import type { FormikProps } from 'formik';

import type { UserInfo } from 'features/authentication';

import type { AddressFormValues, InvoiceFormValues } from '../../forms';

import { useEffect, useRef, useState } from 'react';

import { useAuthentication } from 'features/authentication';

import { useValidatorsStore } from '../../context';

const getDefaultAddressFormValues = (
  validation: InvoiceDetailsValidation,
  userInfo: UserInfo | null
): AddressFormValues => ({
  box: validation.data?.busNumber || userInfo?.address.box || '',
  city: validation.data?.city || userInfo?.address.city || '',
  country: validation.data?.countryCode || userInfo?.address.country || '',
  houseNumber: validation.data?.houseNumber || userInfo?.address.houseNumber || '',
  postalCode: validation.data?.postalCode || userInfo?.address.postalCode || '',
  street: validation.data?.street || userInfo?.address.street || ''
});

const getDefaultInvoiceFormValues = (validation: InvoiceDetailsValidation): InvoiceFormValues => ({
  companyName: validation.data?.companyName || '',
  vatNumber: validation.data?.vatNumber || ''
});

const submitAddressForm = (
  addressFormProps: FormikProps<AddressFormValues>,
  validation: InvoiceDetailsValidation
): Promise<InvoiceDetailsValidation> => {
  return new Promise((resolve, reject) => {
    addressFormProps.submitForm().then(() => {
      if (addressFormProps.isValid) {
        const { box, city, country, houseNumber, postalCode, street } = addressFormProps.values;

        resolve({
          ...validation,
          data: {
            busNumber: box,
            city,
            countryCode: country,
            houseNumber,
            postalCode,
            street
          }
        });
      } else {
        reject();
      }
    });
  });
};

const submitInvoiceForm = (
  invoiceFormProps: FormikProps<InvoiceFormValues>,
  validation: InvoiceDetailsValidation
): Promise<InvoiceDetailsValidation> => {
  return new Promise((resolve, reject) => {
    invoiceFormProps.submitForm().then(() => {
      if (invoiceFormProps.isValid) {
        const { companyName, vatNumber } = invoiceFormProps.values;

        resolve({
          ...validation,
          data: {
            companyName,
            vatNumber
          }
        });
      } else {
        reject();
      }
    });
  });
};

const submitForms = (
  validation: InvoiceDetailsValidation,
  addressFormProps: FormikProps<AddressFormValues>,
  invoiceFormProps: FormikProps<InvoiceFormValues>
): Promise<InvoiceDetailsValidation> => {
  return new Promise((resolve, reject) => {
    Promise.all([
      submitAddressForm(addressFormProps, validation),
      submitInvoiceForm(invoiceFormProps, validation)
    ])
      .then(results => {
        resolve({ ...validation, data: { ...results[0].data, ...results[1].data } });
      })
      .catch(() => {
        reject();
      });
  });
};

export const useInvoiceValidator = (validation: InvoiceDetailsValidation) => {
  const hasDataValues = Object.values(validation.data ?? {}).some(value => Boolean(value));

  const { userInfo } = useAuthentication();

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [isEditing, setIsEditing] = useState<boolean>(!hasDataValues);
  const [selectedChoice, setSelectedChoice] = useState<'yes' | 'no'>(hasDataValues ? 'yes' : 'no');

  const addressFormRef = useRef<FormikProps<AddressFormValues>>(null);
  const invoiceFormRef = useRef<FormikProps<InvoiceFormValues>>(null);

  const defaultAddressFormValues = getDefaultAddressFormValues(validation, userInfo);
  const defaultInvoiceFormValues = getDefaultInvoiceFormValues(validation);

  useEffect(() => {
    if (selectedChoice === 'no') {
      subscribe(validation.id, () => Promise.resolve({ ...validation, data: undefined }));
    }
    if (selectedChoice === 'yes') {
      if (isEditing) {
        if (addressFormRef.current && invoiceFormRef.current) {
          subscribe(validation.id, () =>
            submitForms(
              validation,
              addressFormRef.current as FormikProps<AddressFormValues>,
              invoiceFormRef.current as FormikProps<InvoiceFormValues>
            )
          );
        } else {
          subscribe(validation.id, () => Promise.resolve({ ...validation }));
        }
      } else {
        subscribe(validation.id, () => Promise.resolve({ ...validation }));
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isEditing, selectedChoice]);

  return {
    addressFormRef,
    defaultAddressFormValues,
    defaultInvoiceFormValues,
    invoiceFormRef,
    isEditing,
    selectedChoice,
    setIsEditing,
    setSelectedChoice
  };
};
