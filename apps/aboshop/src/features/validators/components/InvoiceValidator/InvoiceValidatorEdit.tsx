import type { FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import type { AddressFormValues, InvoiceFormValues } from '../../forms';

import { useCountriesByLanguage } from '@subscriber/services-hooks';

import { Spinner } from 'components/Spinner';

import { useTranslation } from 'hooks';

import { AddressForm, InvoiceForm } from '../../forms';

const translationNamespace = 'features/validators';

interface InvoiceValidatorEditProps {
  addressFormRef: RefObject<FormikProps<AddressFormValues>>;
  initialAddressValues: AddressFormValues;
  initialInvoiceValues: InvoiceFormValues;
  invoiceFormRef: RefObject<FormikProps<InvoiceFormValues>>;
}

const InvoiceValidatorEdit = ({
  addressFormRef,
  initialAddressValues,
  initialInvoiceValues,
  invoiceFormRef
}: InvoiceValidatorEditProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const countriesByLanguageQuery = useCountriesByLanguage('nl');

  if (countriesByLanguageQuery.isSuccess) {
    return (
      <div className="flex flex-col gap-2">
        <InvoiceForm
          formRef={invoiceFormRef}
          initialValues={initialInvoiceValues}
          onSubmit={() => undefined}
        />

        <AddressForm
          countries={countriesByLanguageQuery.data}
          formRef={addressFormRef}
          initialValues={initialAddressValues}
          onSubmit={() => undefined}
        />
      </div>
    );
  }

  return <Spinner text={t('invoice.loading')} />;
};

export default InvoiceValidatorEdit;
