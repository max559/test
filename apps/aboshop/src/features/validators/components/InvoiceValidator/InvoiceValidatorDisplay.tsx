import type { ReactElement } from 'react';

import type { AddressFormValues, InvoiceFormValues } from '../../forms';

import { Button, Paragraph } from '@mediahuis/chameleon-react';

import { useTranslation } from 'hooks';

const translationNamespace = 'features/validators';

interface InvoiceValidatorDisplayProps {
  addressFormValues: AddressFormValues;
  invoiceFormValues: InvoiceFormValues;
  onEdit: () => void;
}

const InvoiceValidatorDisplay = ({
  addressFormValues,
  invoiceFormValues,
  onEdit
}: InvoiceValidatorDisplayProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <div className="flex flex-col gap-4">
      <div>
        <Paragraph>{`${invoiceFormValues.companyName} (${invoiceFormValues.vatNumber})`}</Paragraph>
        <Paragraph>
          {`${addressFormValues.street ?? ''} ${addressFormValues.houseNumber ?? ''} ${
            addressFormValues.box ?? ''
          }`}
        </Paragraph>
        <Paragraph>{`${addressFormValues.postalCode ?? ''} ${
          addressFormValues.city ?? ''
        }`}</Paragraph>
        <Paragraph>{addressFormValues.country}</Paragraph>
      </div>

      <span>
        <Button appearance="primary" size="sm" onClick={onEdit}>
          {t('invoice.editButton')}
        </Button>
      </span>
    </div>
  );
};

export default InvoiceValidatorDisplay;
