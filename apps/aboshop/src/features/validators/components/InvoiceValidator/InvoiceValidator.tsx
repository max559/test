import type { InvoiceDetailsValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Choice } from '@mediahuis/chameleon-react';
import { VALIDATION_STATE } from '@subscriber/services-core';

import { useTranslation } from 'hooks';

import { ValidatorBlock } from '../ValidatorBlock';
import InvoiceValidatorDisplay from './InvoiceValidatorDisplay';
import InvoiceValidatorEdit from './InvoiceValidatorEdit';
import { useInvoiceValidator } from './useInvoiceValidator';

const translationNamespace = 'features/validators';

interface InvoiceValidatorProps {
  validation: InvoiceDetailsValidation;
}

const InvoiceValidator = ({ validation }: InvoiceValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const {
    addressFormRef,
    defaultAddressFormValues,
    defaultInvoiceFormValues,
    invoiceFormRef,
    isEditing,
    selectedChoice,
    setIsEditing,
    setSelectedChoice
  } = useInvoiceValidator(validation);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('invoice.error') : undefined}
      title={t('invoice.title')}
    >
      <div className="flex flex-col gap-2">
        <Choice
          checked={selectedChoice === 'no'}
          id="InvoiceNo"
          name="invoice-validator-no"
          value="no"
          title={t('invoice.choiceTitleNo')}
          onChange={() => setSelectedChoice('no')}
        />

        <Choice
          checked={selectedChoice === 'yes'}
          id="InvoiceYes"
          name="invoice-validator-yes"
          value="yes"
          title={t('invoice.choiceTitleYes')}
          onChange={() => setSelectedChoice('yes')}
        >
          {isEditing ? (
            <InvoiceValidatorEdit
              addressFormRef={addressFormRef}
              initialAddressValues={defaultAddressFormValues}
              initialInvoiceValues={defaultInvoiceFormValues}
              invoiceFormRef={invoiceFormRef}
            />
          ) : (
            <InvoiceValidatorDisplay
              addressFormValues={defaultAddressFormValues}
              invoiceFormValues={defaultInvoiceFormValues}
              onEdit={() => setIsEditing(true)}
            />
          )}
        </Choice>
      </div>
    </ValidatorBlock>
  );
};

export default InvoiceValidator;
