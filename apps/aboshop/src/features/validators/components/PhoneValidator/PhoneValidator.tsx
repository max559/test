import 'react-phone-number-input/style.css';

import type { Brand } from '@subscriber/globals';
import type { TextfieldValidation } from '@subscriber/services-core';
import type { ReactElement } from 'react';
import type { Country } from 'react-phone-number-input';

import { VALIDATION_STATE } from '@subscriber/services-core';
import { useEffect, useState } from 'react';
import PhoneInput from 'react-phone-number-input';

import { useTranslation } from 'hooks';

import { useValidatorsStore } from '../../context';
import { ValidatorBlock } from '../ValidatorBlock';

const translationNamespace = 'features/validators';

const countryConfig: Record<Brand, Country> = Object.freeze({
  az: 'DE',
  co: 'LU',
  dl: 'NL',
  ds: 'BE',
  gva: 'BE',
  hbvl: 'BE',
  lt: 'LU',
  lw: 'LU',
  nb: 'BE',
  tc: 'LU'
});

interface PhoneValidatorProps {
  validation: TextfieldValidation;
}

const PhoneValidator = ({ validation }: PhoneValidatorProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const subscribe = useValidatorsStore(state => state.subscribe);

  const [phoneNumber, setPhoneNumber] = useState<string | undefined>(validation.data.value);

  useEffect(() => {
    subscribe(validation.id, () =>
      Promise.resolve({ ...validation, data: { value: phoneNumber } })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [phoneNumber]);

  return (
    <ValidatorBlock
      description={validation.description}
      error={validation.state === VALIDATION_STATE.NotValid ? t('phone.error') : undefined}
      title={t('phone.title')}
    >
      <PhoneInput
        className={`react-phone-number-input ${
          validation.state === VALIDATION_STATE.NotValid ? 'error' : ''
        }`}
        countryCallingCodeEditable={false}
        defaultCountry={countryConfig[MH_BRAND]}
        id={validation.id}
        international
        value={phoneNumber}
        onChange={setPhoneNumber}
      />
    </ValidatorBlock>
  );
};

export default PhoneValidator;
