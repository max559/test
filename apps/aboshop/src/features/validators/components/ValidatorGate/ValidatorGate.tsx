import type { LoginValidation, PreconditionsValidation } from '@subscriber/services-core';
import type { PropsWithChildren, ReactElement } from 'react';

import type { UserInfo } from 'features/authentication';

import { VALIDATION_STATE } from '@subscriber/services-core';
import { useValidateLogin, useValidatePrecondition } from '@subscriber/services-hooks';
import { useEffect } from 'react';

import { useAuthentication } from 'features/authentication';

const mergeUserInfoInLoginValidation = (
  loginValidation: LoginValidation,
  userInfo: UserInfo | null
): LoginValidation => ({
  ...loginValidation,
  data: {
    account: loginValidation.data.account || userInfo?.id || '',
    brand: loginValidation.data.brand || MH_BRAND,
    firstName: loginValidation.data.firstName || userInfo?.firstName || '',
    lastName: loginValidation.data.lastName || userInfo?.lastName || ''
  }
});

const mergeUserInfoInPreconditionsValidation = (
  preconditionsValidation: PreconditionsValidation,
  userInfo: UserInfo | null
): PreconditionsValidation => ({
  ...preconditionsValidation,
  data: {
    accountId: userInfo?.id,
    boxNumber: userInfo?.address.box,
    email: userInfo?.email,
    houseNumber: userInfo?.address.houseNumber,
    postalCode: userInfo?.address.postalCode,
    street: userInfo?.address.street
    // TODO: subscriptionFormulaId: ...
  }
});

interface ValidatorGateProps {
  fallback: ReactElement;
  loginValidation?: LoginValidation;
  preconditionsValidation?: PreconditionsValidation;
}

const ValidatorGate = ({
  children,
  fallback,
  loginValidation,
  preconditionsValidation
}: PropsWithChildren<ValidatorGateProps>): ReactElement => {
  const { userInfo } = useAuthentication();

  const validateLogin = useValidateLogin(userInfo?.idToken);
  const validatePreconditions = useValidatePrecondition(userInfo?.idToken);

  useEffect(() => {
    if (loginValidation && loginValidation.state !== VALIDATION_STATE.Compliant) {
      validateLogin.mutate(mergeUserInfoInLoginValidation(loginValidation, userInfo));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loginValidation]);

  useEffect(() => {
    if (preconditionsValidation && preconditionsValidation.state !== VALIDATION_STATE.Compliant) {
      validatePreconditions.mutate(
        mergeUserInfoInPreconditionsValidation(preconditionsValidation, userInfo)
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [preconditionsValidation]);

  if (
    !preconditionsValidation ||
    preconditionsValidation.state === VALIDATION_STATE.Compliant ||
    validatePreconditions.data?.state === VALIDATION_STATE.Compliant
  ) {
    return <>{children}</>;
  }

  return fallback;
};

export default ValidatorGate;
