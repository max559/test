import type {
  AddressAreaValidation,
  BooleanValidation,
  ChoiceValidation,
  DateValidation,
  EmailValidation,
  FileValidation,
  InvoiceDetailsValidation,
  TextfieldValidation
} from '@subscriber/services-core';

export type Validation =
  | AddressAreaValidation
  | BooleanValidation
  | ChoiceValidation
  | DateValidation
  | EmailValidation
  | FileValidation
  | InvoiceDetailsValidation
  | TextfieldValidation;
