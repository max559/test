import type { Voucher } from '@subscriber/services-core';
import type { FieldProps, FormikHelpers, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { VOUCHER_STATE } from '@subscriber/services-core';
import dayjs from 'dayjs';
import { Field, Form, Formik, useFormikContext } from 'formik';
import { useEffect, useRef, useState } from 'react';

import { useTranslation } from 'hooks';

import { servicesCoreInstance } from 'globals';

const translationNamespace = 'forms';

export interface VoucherFormValues {
  code: string;
}

interface VoucherFormProps {
  autoSubmit?: boolean;
  formRef: RefObject<FormikProps<VoucherFormValues>>;
  initialValues: VoucherFormValues;
  onSubmit: (voucher: Voucher) => void;
  onValidate?: (isValid: boolean) => void;
}

export const VoucherForm = ({
  autoSubmit,
  formRef,
  initialValues,
  onSubmit,
  onValidate
}: VoucherFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const voucherDataRef = useRef<Voucher | null>(null);

  function handleSubmit(
    _values: VoucherFormValues,
    formikHelpers: FormikHelpers<VoucherFormValues>
  ) {
    if (voucherDataRef.current) {
      onSubmit(voucherDataRef.current);
    }
    formikHelpers.setSubmitting(false);
  }

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      validate={(values: VoucherFormValues) => {
        const errors: { code?: string } = {};

        if (!values.code) {
          errors.code = t('voucher.errors.required');
        } else if (!/^[A-Z0-9]{4}[-][A-Z0-9]{4}[-][A-Z0-9]{4}$/.test(values.code)) {
          errors.code = t('voucher.errors.valid');
        } else {
          return servicesCoreInstance.voucherService
            .getVoucherByCode(values.code)
            .then(response => {
              const { expirationDate, state } = response.data;

              if (state !== VOUCHER_STATE.New) {
                errors.code = t('voucher.errors.state');
              } else if (dayjs().isAfter(expirationDate)) {
                errors.code = t('voucher.errors.expired');
              }
              voucherDataRef.current = response.data;
              onValidate && onValidate(Object.keys(errors).length === 0);

              return errors;
            })
            .catch(() => {
              voucherDataRef.current = null;
              onValidate && onValidate(false);

              return {
                code: t('voucher.errors.valid')
              };
            });
        }
        onValidate && onValidate(Object.keys(errors).length === 0);

        return errors;
      }}
      onSubmit={handleSubmit}
    >
      <Form>
        <Field name="code">
          {({ field, meta }: FieldProps<string>) => (
            <>
              <TextField
                error={meta.error ? true : false}
                id="codeInput"
                label={t('voucher.code')}
                mask="****-****-****"
                message={meta.error ? meta.error : undefined}
                name={field.name}
                required
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
              />
            </>
          )}
        </Field>

        {autoSubmit && <AutoSubmit />}
      </Form>
    </Formik>
  );
};

const AutoSubmit = (): null => {
  const { isValid, values, submitForm, validateForm } = useFormikContext<VoucherFormValues>();

  const [isValidated, setIsValidated] = useState<boolean>(false);

  const isValuesEmpty = Object.values(values).some(value => !value);

  useEffect(() => {
    if (!isValidated && !isValuesEmpty) {
      validateForm().then(() => setIsValidated(true));
    }
  }, [isValidated, isValuesEmpty, validateForm]);

  useEffect(() => {
    if (isValid && isValidated) {
      submitForm();
    }
  }, [isValid, isValidated, submitForm]);

  return null;
};
