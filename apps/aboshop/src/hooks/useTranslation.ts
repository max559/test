import type { I18n, Translate } from 'next-translate';

import useNextTranslation from 'next-translate/useTranslation';

const useTranslation = (defaultNS?: string): I18n => {
  const { lang, t } = useNextTranslation(defaultNS);

  const translate: Translate = (key, query, options) => {
    return t(`${MH_BRAND}.${key}`, query, { ...options, fallback: `base.${key}` });
  };

  return {
    lang,
    t: translate
  };
};

export default useTranslation;
