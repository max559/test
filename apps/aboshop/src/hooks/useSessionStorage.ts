const useSessionStorage = () => {
  const isBrowser: boolean = (() => typeof window !== 'undefined')();

  const getItem = (key: string): string | null =>
    isBrowser ? window.sessionStorage.getItem(key) : null;
  const removeItem = (key: string): boolean => {
    if (isBrowser) {
      window.sessionStorage.removeItem(key);

      return true;
    }

    return false;
  };
  const setItem = (key: string, value: string): boolean => {
    if (isBrowser) {
      window.sessionStorage.setItem(key, value);

      return true;
    }

    return false;
  };

  return {
    getItem,
    removeItem,
    setItem
  };
};

export default useSessionStorage;
