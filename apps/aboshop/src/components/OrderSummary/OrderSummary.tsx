import type { OfferItem } from '@subscriber/services-core';
import type { FeaturesListItem } from '@subscriber/subscriber-ui';
import type { ReactElement } from 'react';

import { Caption, Divider, Heading, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { FeaturesList, MultibrandBanner } from '@subscriber/subscriber-ui';
import Markdown from 'markdown-to-jsx';
import { Fragment } from 'react';

import { useTranslation } from 'hooks';

const typeSummaryNamespace = 'components';

interface OrderSummaryProps {
  subscriptionFormulaItem?: OfferItem;
  subscriptionTypeItem: OfferItem;
}

const OrderSummary = ({
  subscriptionFormulaItem,
  subscriptionTypeItem
}: OrderSummaryProps): ReactElement => {
  const { t } = useTranslation(typeSummaryNamespace);

  const featuresListItems: Array<FeaturesListItem> =
    subscriptionTypeItem.features?.map(feature => ({
      isAvailable: true,
      key: feature.id,
      text: {
        render: value => (
          <Markdown
            options={{
              overrides: {
                img: {
                  component: 'img',
                  props: {
                    alt: 'FeaturesList-markdown-img',
                    className: 'align-middle inline',
                    height: 16,
                    width: 16
                  }
                }
              },
              wrapper: Fragment
            }}
          >
            {value}
          </Markdown>
        ),
        value: feature.description || ''
      }
    })) || [];

  return (
    <div
      className="border border-solid flex flex-col rounded w-full"
      style={{
        borderColor: 'var(--semantic-border-base-static-default-fill)'
      }}
    >
      <div className="px-6 py-4">
        <Logo className="h-7" />
      </div>

      <div
        className="flex flex-col gap-1 px-6 py-4"
        style={{
          backgroundColor: 'var(--semantic-background-brand-adaptive-soft-fill)'
        }}
      >
        <Heading className="capitalize" data-testid="offerdetail-title" level={5} size="md">
          <Markdown options={{ forceInline: true }}>{subscriptionTypeItem.title ?? ''}</Markdown>
        </Heading>

        <Paragraph>
          <Markdown options={{ forceInline: true }}>
            {subscriptionTypeItem.description ?? ''}
          </Markdown>
        </Paragraph>
      </div>

      <div className="flex flex-col gap-6 py-6">
        {subscriptionFormulaItem && (
          <div className="flex flex-col gap-2 px-6">
            <Paragraph>
              <Markdown options={{ forceInline: true }}>
                {subscriptionFormulaItem.priceSentence ?? ''}
              </Markdown>
            </Paragraph>
            <Caption>{subscriptionFormulaItem.extra}</Caption>
          </div>
        )}

        <FeaturesList className="px-6" gap={4} items={featuresListItems} />

        {subscriptionTypeItem.isMultiBrand &&
          (MH_BRAND === 'ds' || MH_BRAND === 'gva' || MH_BRAND === 'hbvl' || MH_BRAND === 'nb') && (
            <MultibrandBanner activeBrand={MH_BRAND} text={t('multibrandText')} />
          )}

        {subscriptionTypeItem.extra && (
          <div className="flex flex-col gap-2 px-6">
            <Divider className="w-full" />

            <Caption className="text-center">
              <Markdown options={{ forceInline: true }}>{subscriptionTypeItem.extra}</Markdown>
            </Caption>
          </div>
        )}
      </div>
    </div>
  );
};

export default OrderSummary;
