import type { Offer, OfferItem, SortableFeature } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Heading, Paragraph } from '@mediahuis/chameleon-react';
import Markdown from 'markdown-to-jsx';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import { useTracking } from 'features/tracking';

import { createCompactCard, createDefaultCard, resolveTemplateColumns } from './Vitrine.util';

export interface VitrineProps {
  combinedFeatures: Array<SortableFeature>;
  offer: Offer;
}

const Vitrine = ({ offer, combinedFeatures }: VitrineProps): ReactElement => {
  const router = useRouter();
  const { trackGlitrEvent, trackGtmEcommerceMutipleItemsEvent } = useTracking();

  const [clickedOfferItemId, setClickedOfferItemId] = useState<number | undefined>();

  const compactCards = (offer.items ?? []).map(offerItem =>
    createCompactCard(offerItem, handleCardClick)
  );
  const defaultCards = (offer.items ?? []).map(offerItem =>
    createDefaultCard(
      offerItem,
      combinedFeatures,
      offerItem.id === clickedOfferItemId,
      handleCardClick
    )
  );

  useEffect(() => {
    trackGlitrEvent({
      eventAction: 'show',
      eventLabel: 'subscriptionshop/cta-offer-product'
    });
    trackGtmEcommerceMutipleItemsEvent('view_item_list', offer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offer]);

  function handleCardClick(offerItem: OfferItem) {
    setClickedOfferItemId(offerItem.id);

    trackGlitrEvent({
      eventAction: `click-choose-${offerItem.title}`,
      eventLabel: 'subscriptionshop/cta-offer-product'
    });

    offerItem.nextStep && router.push(offerItem.nextStep);
  }

  return (
    <div className="flex flex-col gap-10 items-center">
      {(offer.title || offer.description) && (
        <div className="text-center">
          {offer.title && (
            <Heading level={3} size="md">
              {offer.title}
            </Heading>
          )}
          {offer.description && (
            <Paragraph>
              {<Markdown options={{ forceInline: true }}>{offer.description}</Markdown>}
            </Paragraph>
          )}
        </div>
      )}

      <div className="block flex flex-col gap-4 w-full sm:hidden" data-testid="vitrine">
        {compactCards}
      </div>
      <div
        className={`hidden justify-center sm:gap-8 sm:grid xl:gap-6 w-full ${resolveTemplateColumns(
          defaultCards.length
        )}`}
        data-testid="vitrine"
      >
        {defaultCards}
      </div>
    </div>
  );
};

export default Vitrine;
