import type { OfferItem, SortableFeature } from '@subscriber/services-core';
import type { FeaturesListItem } from '@subscriber/subscriber-ui';
import type { ReactElement } from 'react';

import { CompactCard, DefaultCard } from '@subscriber/subscriber-ui';
import Markdown from 'markdown-to-jsx';
import { Fragment } from 'react';

const mapSortableFeatureToFeaturesListItem = (
  sortableFeature: SortableFeature,
  isAvailable: boolean
): FeaturesListItem => {
  return {
    isAvailable,
    key: sortableFeature.id,
    text: {
      render: value => (
        <Markdown
          options={{
            overrides: {
              img: {
                component: 'img',
                props: {
                  alt: 'FeaturesList-markdown-img',
                  className: 'align-middle inline',
                  height: 14,
                  width: 14
                }
              }
            },
            wrapper: Fragment
          }}
        >
          {value}
        </Markdown>
      ),
      value: sortableFeature.description || ''
    }
  };
};

export const createCompactCard = (
  offerItem: OfferItem,
  onClick: (offerItem: OfferItem) => void
): ReactElement => {
  return (
    <CompactCard
      featuresListItems={(offerItem.features ?? []).map((sortableFeature: SortableFeature) =>
        mapSortableFeatureToFeaturesListItem(sortableFeature, true)
      )}
      highlight={offerItem.isHighlighted}
      key={offerItem.id}
      label={offerItem.label}
      multibrandProps={
        offerItem.isMultiBrand
          ? {
              activeBrand: MH_BRAND as 'dl' | 'ds' | 'gva' | 'hbvl' | 'nb',
              text: 'Gratis alle plusartikels'
            }
          : undefined
      }
      price={{
        render: value => <Markdown options={{ forceInline: true }}>{value}</Markdown>,
        value: offerItem.priceSentence || ''
      }}
      testId={`vitrine-card-${offerItem.id}`}
      title={{
        render: value => <Markdown options={{ forceInline: true }}>{value}</Markdown>,
        value: offerItem.title || ''
      }}
      onPaperClick={() => onClick(offerItem)}
    />
  );
};

export const createDefaultCard = (
  offerItem: OfferItem,
  combinedFeatures: Array<SortableFeature>,
  isLoading: boolean,
  onClick: (offerItem: OfferItem) => void
): ReactElement => {
  return (
    <DefaultCard
      buttonCaption={
        offerItem.extra
          ? {
              value: offerItem.extra
            }
          : undefined
      }
      buttonText={offerItem.buttonLabel || ''}
      description={{
        render: value => (
          <Markdown
            options={{
              forceInline: true,
              overrides: {
                img: {
                  component: 'img',
                  props: {
                    alt: 'DefaultVitrineCard-markdown-img',
                    className: 'align-middle inline',
                    height: 16,
                    width: 16
                  }
                }
              }
            }}
          >
            {value}
          </Markdown>
        ),
        value: offerItem.description || ''
      }}
      featuresListItems={combinedFeatures.map((feature: SortableFeature) => {
        const isAvailable = !!offerItem.features?.find(
          (itemFeature: SortableFeature) => itemFeature.id === feature.id
        );

        return mapSortableFeatureToFeaturesListItem(feature, isAvailable);
      })}
      highlight={offerItem.isHighlighted}
      imageProps={
        offerItem.image
          ? {
              alt: offerItem.title,
              src: offerItem.image
            }
          : undefined
      }
      key={offerItem.id}
      label={offerItem.label}
      loading={isLoading}
      multibrandProps={
        offerItem.isMultiBrand
          ? {
              activeBrand: MH_BRAND as 'ds' | 'gva' | 'hbvl' | 'nb',
              text: 'Lees gratis alle plusartikels op sites en apps'
            }
          : undefined
      }
      priceCaption={
        offerItem.priceExtra
          ? {
              value: offerItem.priceExtra
            }
          : undefined
      }
      priceText={{
        render: value => <Markdown options={{ forceInline: true }}>{value}</Markdown>,
        value: offerItem.priceSentence || ''
      }}
      testId={`vitrine-card-${offerItem.id}`}
      title={{
        render: value => <Markdown options={{ forceInline: true }}>{value}</Markdown>,
        value: offerItem.title || ''
      }}
      onButtonClick={() => onClick(offerItem)}
      onPaperClick={() => onClick(offerItem)}
    />
  );
};

const gridTemplateColumnsSm = {
  '1': 'sm:[grid-template-columns:repeat(1,300px)]',
  '2': 'sm:[grid-template-columns:repeat(2,300px)]',
  '4': 'sm:[grid-template-columns:repeat(4,300px)]'
};
const gridTemplateColumnsXl = {
  '1': 'xl:[grid-template-columns:repeat(1,300px)]',
  '2': 'xl:[grid-template-columns:repeat(2,300px)]',
  '3': 'xl:[grid-template-columns:repeat(3,300px)]',
  '4': 'xl:[grid-template-columns:repeat(4,300px)]'
};

export const resolveTemplateColumns = (numberOfCards: number): string => {
  if (numberOfCards === 1) {
    return `${gridTemplateColumnsSm[1]} ${gridTemplateColumnsXl[1]}`;
  }
  if (numberOfCards === 2) {
    return `${gridTemplateColumnsSm[2]} ${gridTemplateColumnsXl[2]}`;
  }
  if (numberOfCards === 3) {
    return `${gridTemplateColumnsSm[2]} ${gridTemplateColumnsXl[3]}`;
  }

  return `${gridTemplateColumnsSm[2]} ${gridTemplateColumnsXl[4]}`;
};
