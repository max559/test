import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

import { useTranslation } from 'hooks';

const translationNamespace = 'components';

export interface VatNumberFormValues {
  vatNumber: string;
}

interface VatNumberFormProps {
  formRef: RefObject<FormikProps<VatNumberFormValues>>;
  initialValues: VatNumberFormValues;
  labelHidden?: boolean;
  onSubmit: (values: VatNumberFormValues) => void;
}

export const VatNumberForm = ({
  formRef,
  initialValues,
  labelHidden,
  onSubmit
}: VatNumberFormProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const VatNumberFormSchema = Yup.object().shape({
    vatNumber: Yup.string()
      .required(t('enterpriseLookup.forms.vatNumber.vatNumberRequired'))
      .matches(
        /^(BE)[ \t][0-9]{4}[.][0-9]{3}[.][0-9]{3}$/,
        t('enterpriseLookup.forms.vatNumber.vatNumberError')
      )
  });

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      validationSchema={VatNumberFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <Field name="vatNumber">
          {({ field, meta }: FieldProps<string>) => (
            <TextField
              error={!!meta.touched && !!meta.error}
              id="VatNumberInput"
              label={t('enterpriseLookup.forms.vatNumber.vatNumber')}
              labelHidden={labelHidden}
              mask="BE 9999.999.999"
              message={meta.touched && meta.error ? meta.error : undefined}
              name={field.name}
              required
              value={field.value}
              onBlur={field.onBlur}
              onChange={field.onChange}
            />
          )}
        </Field>
      </Form>
    </Formik>
  );
};
