import type { Enterprise } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import type { VatNumberFormValues } from './forms/VatNumberForm';

import { Button } from '@mediahuis/chameleon-react';
import { useEnterprise } from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import { useEffect, useRef, useState } from 'react';

import { useTranslation } from 'hooks';

import { VatNumberForm } from './forms/VatNumberForm';

import { convertToExpressiveVatNumber, convertToSimpleVatNumber } from './EnterpriseLookup.util';

const translationNamespace = 'components';

interface EnterpriseLookupProps {
  initialValue?: string;
  onLookup: (enterprise?: Enterprise) => void;
}

const EnterpriseLookup = ({ initialValue, onLookup }: EnterpriseLookupProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);
  const [vatNumber, setVatNumber] = useState<string | undefined>();

  const vatNumberFormRef: RefObject<FormikProps<VatNumberFormValues>> =
    useRef<FormikProps<VatNumberFormValues>>(null);

  const enterpriseQuery = useEnterprise(vatNumber);

  useEffect(() => {
    if (enterpriseQuery.isError) {
      onLookup();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enterpriseQuery.isError]);

  useEffect(() => {
    if (enterpriseQuery.isSuccess) {
      onLookup(enterpriseQuery.data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enterpriseQuery.isSuccess]);

  return (
    <Flex gap={2}>
      <span style={{ flexGrow: 1 }}>
        <VatNumberForm
          formRef={vatNumberFormRef}
          initialValues={{
            vatNumber: initialValue ? convertToExpressiveVatNumber(initialValue) : ''
          }}
          labelHidden
          onSubmit={values => setVatNumber(convertToSimpleVatNumber(values.vatNumber))}
        />
      </span>

      <Button appearance="primary" onClick={() => vatNumberFormRef.current?.submitForm()}>
        {t('enterpriseLookup.submit')}
      </Button>
    </Flex>
  );
};

export default EnterpriseLookup;
