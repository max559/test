export const convertToExpressiveVatNumber = (simpleVatNumber: string): string => {
  const countryCode: string = simpleVatNumber.slice(0, 2);
  const firstRangeNumbers: string = simpleVatNumber.slice(2, 6);
  const secondRangeNumbers: string = simpleVatNumber.slice(6, 9);
  const thirdRangeNumbers: string = simpleVatNumber.slice(9, 12);

  return `${countryCode} ${firstRangeNumbers}.${secondRangeNumbers}.${thirdRangeNumbers}`;
};

export const convertToSimpleVatNumber = (expressiveVatNumber: string): string => {
  return expressiveVatNumber.replaceAll('.', '').replace(' ', '');
};
