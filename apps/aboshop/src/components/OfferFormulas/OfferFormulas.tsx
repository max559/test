import type { OfferItem } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Choice, Paragraph } from '@mediahuis/chameleon-react';
import Markdown from 'markdown-to-jsx';

interface OfferFormulasProps {
  checkedItem?: OfferItem;
  items: Array<OfferItem>;
  onChange: (item: OfferItem) => void;
}

const OfferFormulas = ({ checkedItem, items, onChange }: OfferFormulasProps): ReactElement => {
  return (
    <div className="flex flex-col gap-2" data-testid="offerformulas">
      {items.map((item: OfferItem) => (
        <Choice
          aria-checked={item.id === checkedItem?.id}
          checked={item.id === checkedItem?.id}
          data-testid={`offerformulas-choice-${item.id}`}
          id={item.id.toString()}
          info={
            <Markdown options={{ forceInline: true }}>{`${item.title ?? ''}${
              item.label ? ' - ' : ''
            }${item.label ?? ''}`}</Markdown>
          }
          key={item.id}
          labelProps={{
            weight: 'strong'
          }}
          message={<Markdown options={{ forceInline: true }}>{item.extra || ''}</Markdown>}
          name="OfferFormulas"
          title={
            <Markdown options={{ forceInline: true, forceWrapper: true, wrapper: 'strong' }}>
              {item.priceSentence || ''}
            </Markdown>
          }
          value={item.id.toString()}
          onChange={() => onChange(item)}
        >
          {item.description && (
            <Paragraph>
              <Markdown options={{ forceInline: true }}>{item.description}</Markdown>
            </Paragraph>
          )}
        </Choice>
      ))}
    </div>
  );
};

export default OfferFormulas;
