import type { OfferItem } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import { Caption, Heading, Logo, Paragraph, useMediaQuery } from '@mediahuis/chameleon-react';
import {
  FeaturesList,
  FeaturesListItem,
  Flex,
  MultibrandBannerInline
} from '@subscriber/subscriber-ui';
import dayjs from 'dayjs';
import Markdown from 'markdown-to-jsx';
import { Fragment } from 'react';

import { useTranslation } from 'hooks';

const translationNamespace = 'components';

interface PurchaseSummaryProps {
  creationDate: Date;
  offerFormula?: OfferItem;
  offerType: OfferItem;
}

const PurchaseSummary = ({
  creationDate,
  offerFormula,
  offerType
}: PurchaseSummaryProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  const priceStyle = useMediaQuery({
    sm: { textAlign: 'right' }
  });

  const pricePadding = useMediaQuery({
    xs: 4,
    sm: 0
  });

  const featuresListItems: Array<FeaturesListItem> = offerType.features
    ? offerType.features.map(feature => ({
        isAvailable: true,
        key: feature.id,
        text: {
          render: value => (
            <Markdown
              options={{
                overrides: {
                  img: {
                    component: 'img',
                    props: {
                      alt: 'FeaturesList-markdown-img',
                      className: 'align-middle inline',
                      height: 16,
                      width: 16
                    }
                  }
                },
                wrapper: Fragment
              }}
            >
              {value}
            </Markdown>
          ),
          value: feature.description || ''
        }
      }))
    : [];

  return (
    <Flex
      className="border-[var(--color-neutral-10)] border-2 border-solid p-5"
      flexDirection="column"
      gap={6}
    >
      <Flex alignItems="center" justifyContent="space-between">
        <div>
          <Caption>{t('purchaseSummary.orderDate')}</Caption>
          <Paragraph>{dayjs(creationDate).format('DD/MM/YYYY')}</Paragraph>
        </div>
        <div>
          <Logo className="max-h-6 max-w-6" name="brand-main" />
        </div>
      </Flex>

      <Flex alignItems="center" justifyContent="space-between" flexWrap="wrap">
        <div>
          <Heading level={2}>{offerType.title}</Heading>
          <Caption>
            {t('purchaseSummary.duration', {
              duration: offerFormula?.duration,
              durationType: offerFormula?.durationType
            })}
          </Caption>
        </div>
        <div className={`pt-${pricePadding}`}>
          <Caption as="span" size="lg" style={priceStyle}>
            <Markdown
              options={{
                wrapper: Caption
              }}
            >
              {offerFormula?.priceSentence || ''}
            </Markdown>
          </Caption>
        </div>
      </Flex>

      <div className="pl-4 mt-4">
        <FeaturesList gap={4} items={featuresListItems} />
      </div>

      {offerFormula?.isMultiBrand && MH_BRAND === ('ds' || 'gva' || 'hbvl' || 'nb') && (
        <MultibrandBannerInline activeBrand={MH_BRAND} text={t('purchaseSummary.multibrandText')} />
      )}
    </Flex>
  );
};

export default PurchaseSummary;
