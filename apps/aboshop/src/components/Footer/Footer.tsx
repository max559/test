import type { ReactElement } from 'react';

import { Divider, Heading, LinkText, Paragraph } from '@mediahuis/chameleon-react';

import { Center } from '@subscriber/subscriber-ui';

import { useTranslation } from 'hooks';

const translationNamespace = 'components';

const Footer = (): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <footer className="bg-neutral10 px-[5%] py-8">
      <Center className="flex flex-col gap-4" maxInlineSize="80rem">
        <div className="flex flex-col gap-2">
          <Heading level={3} size="md">
            {t('footer.heading')}
          </Heading>

          <div>
            <Paragraph>{t('footer.contactPhone')}</Paragraph>
            <Paragraph size="lg" weight="strong">
              <a href={`tel:${t('footer.customerServicePhoneNumber')}`}>
                {t('footer.customerServicePhoneNumber')}
              </a>
            </Paragraph>
          </div>

          <Paragraph color="var(--color-neutral-base)">
            {t('footer.contactPhoneClosingHour')}
          </Paragraph>
        </div>

        <Divider style={{ width: '100%' }} />

        <div className="flex flex-col gap-2">
          <LinkText href={t('footer.privacyPolicyUrl')} target="_blank">
            {t('footer.privacyPolicy')}
          </LinkText>

          <LinkText onClick={() => window.Didomi && window.Didomi.preferences.show()}>
            {t('footer.managePrivacySettings')}
          </LinkText>
        </div>
      </Center>
    </footer>
  );
};

export default Footer;
