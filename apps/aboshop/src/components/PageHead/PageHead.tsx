import type { ReactElement } from 'react';

import Head from 'next/head';

export interface PageHeadProps {
  description: string;
  facebook?: {
    admins?: Array<string>;
    appId?: string;
    pages?: string;
  };
  openGraph?: {
    description?: string;
    image?: string;
    locale?: string;
    siteName?: string;
    title?: string;
  };
  title: string;
  twitter?: {
    description?: string;
    title?: string;
  };
}

export const PageHead = ({
  description,
  facebook,
  openGraph,
  title,
  twitter
}: PageHeadProps): ReactElement => {
  const isClientSide = typeof window !== 'undefined';

  return (
    <Head>
      <title>{title}</title>

      <meta name="cXenseParse:mhu-article_ispaidcontent" content="false" />
      <meta name="cXenseParse:mhu-domain" content={MH_BRAND} />
      <meta name="cXenseParse:Taxonomy" content={MH_BRAND} />
      <meta name="description" content={description} />
      {facebook?.admins?.map(admin => (
        <meta content={admin} key={admin} property="fb:admins" />
      ))}
      {facebook?.appId && <meta content={facebook.appId} property="fb:app_id" />}
      {facebook?.pages && <meta content={facebook.pages} property="fb:pages" />}
      <meta content={openGraph?.description || description} property="og:description" />
      {openGraph?.image && <meta content={openGraph.image} property="og:image" />}
      {openGraph?.locale && <meta content={openGraph.locale} property="og:locale" />}
      {openGraph?.siteName && <meta content={openGraph.siteName} property="og:site_name" />}
      <meta content={openGraph?.title || title} property="og:title" />
      <meta content="article" property="og:type" />
      {isClientSide && (
        <meta name="og:url" content={`${window.location.origin}${window.location.pathname}`} />
      )}
      {MH_ENV !== 'production' && <meta name="robots" content="noindex, nofollow" />}
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content={twitter?.description || description} />
      <meta name="twitter:title" content={twitter?.title || title} />
      <meta name="viewport" content="width=device-width, initial-scale=1" />

      <link rel="icon" href={`/favicons/favicon.${MH_BRAND}.ico`} />
    </Head>
  );
};

export default PageHead;
