import type { PropsWithChildren, ReactElement } from 'react';

import { Icon, Paragraph } from '@mediahuis/chameleon-react';
import { Error } from '@mediahuis/chameleon-theme-wl/icons';
import { semanticForegroundErrorAdaptiveDefaultFill } from '@mediahuis/chameleon-theme-wl/tokens';

const ErrorMessage = ({ children }: PropsWithChildren): ReactElement => {
  return (
    <div className="flex gap-2 items-center">
      <Icon as={Error} color={semanticForegroundErrorAdaptiveDefaultFill} size="sm" />
      <Paragraph color={semanticForegroundErrorAdaptiveDefaultFill} size="sm">
        {children}
      </Paragraph>
    </div>
  );
};

export default ErrorMessage;
