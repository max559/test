import type { ReactElement } from 'react';

import { Banner } from '@mediahuis/chameleon-react';
import { PAYMENT_STATUS } from '@subscriber/services-core';
import { useState } from 'react';

import { useTranslation } from 'hooks';

const translationNamespace = 'components';

interface PaymentStatusBannerProps {
  paymentStatus: PAYMENT_STATUS;
}

const PaymentStatusBanner = ({ paymentStatus }: PaymentStatusBannerProps): ReactElement | null => {
  const { t } = useTranslation(translationNamespace);

  const [isVisible, setIsVisible] = useState<boolean>(true);

  if (
    paymentStatus === PAYMENT_STATUS.Canceled ||
    paymentStatus === PAYMENT_STATUS.Expired ||
    paymentStatus === PAYMENT_STATUS.Failed
  ) {
    return (
      <Banner appearance="error" show={isVisible} onClose={() => setIsVisible(false)}>
        {paymentStatus === PAYMENT_STATUS.Canceled && t('paymentStatusBanner.canceled')}
        {paymentStatus === PAYMENT_STATUS.Expired && t('paymentStatusBanner.expired')}
        {paymentStatus === PAYMENT_STATUS.Failed && t('paymentStatusBanner.failed')}
      </Banner>
    );
  }

  return null;
};

export default PaymentStatusBanner;
