import type { ReactElement } from 'react';

import IframeResizer from 'iframe-resizer-react';

const contentWindowScriptUrl =
  'https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/4.2.10/iframeResizer.contentWindow.min.js';

interface ContentFrameProps {
  srcDoc: string;
}

const ContentFrame = ({ srcDoc }: ContentFrameProps): ReactElement => {
  const srcDocument = `
  <style type="text/css">
    html, body {
      margin: 0;
      padding: 0;
    }
  </style>
  ${srcDoc}
  <div data-iframe-height></div>
  <script type='text/javascript' src=${contentWindowScriptUrl}></script>
  `;

  return (
    <IframeResizer
      checkOrigin={false}
      heightCalculationMethod="taggedElement"
      srcDoc={srcDocument}
      style={{ height: 0, minHeight: 0, minWidth: '100%', overflow: 'hidden', width: '1px' }}
    />
  );
};

export default ContentFrame;
