import type { ReactElement } from 'react';

import { Choice } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';

import { useTranslation } from 'hooks';

const translationNamespace = 'components';

interface PaymentIntervalChoicesProps {
  checkedOption?: 'oneShot' | 'recurring';
  onChange: (interval: 'oneShot' | 'recurring') => void;
}

const PaymentIntervalChoices = ({
  checkedOption,
  onChange
}: PaymentIntervalChoicesProps): ReactElement => {
  const { t } = useTranslation(translationNamespace);

  return (
    <Flex flexDirection="column" gap={4}>
      <Choice
        checked={checkedOption === 'recurring'}
        id="Recurring"
        name="PaymentIntervalChoices"
        title={t('paymentIntervalChoices.recurringTitle')}
        value="recurring"
        onChange={() => onChange('recurring')}
      />

      <Choice
        checked={checkedOption === 'oneShot'}
        id="OneShot"
        name="PaymentIntervalChoices"
        title={t('paymentIntervalChoices.oneShotTitle')}
        value="oneShot"
        onChange={() => onChange('oneShot')}
      />
    </Flex>
  );
};

export default PaymentIntervalChoices;
