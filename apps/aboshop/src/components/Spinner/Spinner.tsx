import type { ReactElement } from 'react';

import { Loader, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';

interface SpinnerProps {
  size?: 'sm' | 'md' | 'lg' | 'xl' | '2xl';
  text: string;
}

const Spinner = ({ size = 'lg', text }: SpinnerProps): ReactElement => {
  return (
    <Flex
      alignItems="center"
      className="h-full w-full"
      flexDirection="column"
      gap={4}
      justifyContent="center"
    >
      <Loader size={size} />
      <Paragraph color="var(--color-primary-base)" weight="strong">
        {text}
      </Paragraph>
    </Flex>
  );
};

export default Spinner;
