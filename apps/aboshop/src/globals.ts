import type { Brand, Environment } from '@subscriber/globals';

import { ServicesCore } from '@subscriber/services-core';
import { QueryClient } from '@tanstack/react-query';

declare global {
  const AUTHENTICATION: 'ciam' | 'default' | 'lux' | 'none';
  const CIAM_BRAND_GROUP: string;
  const CIAM_CLIENT_ID: string;
  const LOGO: 'default' | 'plus' | 'square';
  const PAYMENT_PROVIDER: 'datatrans' | 'mollie';
  const MH_BRAND: Brand;
  const MH_ENV: Environment;
  const MH_THEME: string;
  const SITE_NAME: string;
  const SITE_URL: string;
}

export const queryClient = new QueryClient();
export const servicesCoreInstance = new ServicesCore(MH_BRAND, MH_ENV);
