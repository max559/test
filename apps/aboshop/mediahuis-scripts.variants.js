/* eslint-disable turbo/no-undeclared-env-vars */

let variants = [
  /* --- BE --- */
  /* DS - De Standaard */
  {
    authentication: 'ciam',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '2u05G2DxWkoWydy1iD5s4IRoekEl9sKW',
    defaultLocale: 'nl-be',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Standaard',
    siteUrl: 'https://test.standaard.be',
    theme: 'ds'
  },
  {
    authentication: 'default',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'DscRc6rEbwP2uUZazvByGjXZflrjV0Z2',
    defaultLocale: 'nl-be',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Standaard',
    siteUrl: 'https://preview.standaard.be',
    theme: 'ds'
  },
  {
    authentication: 'default',
    brand: 'ds',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'pJTg6XsYm9I2M5c9Qbk8RY3Xo505Fc9L',
    defaultLocale: 'nl-be',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Standaard',
    siteUrl: 'https://www.standaard.be',
    theme: 'ds'
  },
  /* GVA - Gazet van Antwerpen */
  {
    authentication: 'ciam',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'eD2SOHBZOtaPcp6gOUn9Yj9NESvXkaHE',
    defaultLocale: 'nl-be',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Gazet van Antwerpen',
    siteUrl: 'https://test.gva.be',
    theme: 'gva'
  },
  {
    authentication: 'default',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'dp0M2Q4Yx60U1pxYaLDX4YApkKnjr8Gn',
    defaultLocale: 'nl-be',
    env: 'preview',
    logo: 'square',
    paymentProvider: '',
    siteName: 'Gazet van Antwerpen',
    siteUrl: 'https://preview.gva.be',
    theme: 'gva'
  },
  {
    authentication: 'default',
    brand: 'gva',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '8NjKhF9D8P2Nm4wjQBB91Ike71FGGARa',
    defaultLocale: 'nl-be',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Gazet van Antwerpen',
    siteUrl: 'https://www.gva.be',
    theme: 'gva'
  },
  /* HBVL - Het Belang van Limburg */
  {
    authentication: 'ciam',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'QZQRRE2TzAgxXbiKX2E9J5lhpwuM609P',
    defaultLocale: 'nl-be',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Het Belang van Limburg',
    siteUrl: 'https://test.hbvl.be',
    theme: 'hbvl'
  },
  {
    authentication: 'default',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'YpBmx62m4t03aGYcP0Y6IUYrkIX9NOyH',
    defaultLocale: 'nl-be',
    env: 'preview',
    logo: 'square',
    paymentProvider: '',
    siteName: 'Het Belang van Limburg',
    siteUrl: 'https://preview.hbvl.be',
    theme: 'hbvl'
  },
  {
    authentication: 'default',
    brand: 'hbvl',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'n8wOhAzVa1Z0FxJz8QF5NTNoZaaPkzWb',
    defaultLocale: 'nl-be',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Het Belang van Limburg',
    siteUrl: 'https://www.hbvl.be',
    theme: 'hbvl'
  },
  /* NB - Nieuwsblad */
  {
    authentication: 'ciam',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'orAlhLDvDRp25jK1HZCcHv9xpclEDa1h',
    defaultLocale: 'nl-be',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'het Nieuwsblad',
    siteUrl: 'https://test.nieuwsblad.be',
    theme: 'nb'
  },
  {
    authentication: 'default',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: 'T9n4NLK2JMFR0XnhboXHXNzXrEJ3PDwq',
    defaultLocale: 'nl-be',
    env: 'preview',
    logo: 'square',
    paymentProvider: '',
    siteName: 'het Nieuwsblad',
    siteUrl: 'https://preview.nieuwsblad.be',
    theme: 'nb'
  },
  {
    authentication: 'default',
    brand: 'nb',
    ciamBrandGroup: 'mhbe',
    ciamClientId: '09rJaDjVJaabbFak7WgYSSLSZXHMHTOD',
    defaultLocale: 'nl-be',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'het Nieuwsblad',
    siteUrl: 'https://www.nieuwsblad.be',
    theme: 'nb'
  },
  /* --- DE --- */
  /* AZ - Aachener Zeitung */
  {
    authentication: 'none',
    brand: 'az',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Aachener Zeitung',
    siteUrl: 'https://test.aachener-zeitung.de',
    theme: 'az'
  },
  {
    authentication: 'none',
    brand: 'az',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Aachener Zeitung',
    siteUrl: 'https://preview.aachener-zeitung.de',
    theme: 'az'
  },
  {
    authentication: 'none',
    brand: 'az',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'Aachener Zeitung',
    siteUrl: 'https://www.aachener-zeitung.de',
    theme: 'az'
  },
  /* --- NL --- */
  /* DL - De Limburger */
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mhli',
    ciamClientId: '8HlgXnmSlF1y2E2ShHqUZ2snBypD5ma8',
    defaultLocale: 'nl-nl',
    env: 'test',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Limburger',
    siteUrl: 'https://test.limburger.nl',
    theme: 'dl'
  },
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mhli',
    ciamClientId: '6jAa0DSD2n7064M5RfNWCqKjoJCg46Sv',
    defaultLocale: 'nl-nl',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Limburger',
    siteUrl: 'https://preview.limburger.nl',
    theme: 'dl'
  },
  {
    authentication: 'default',
    brand: 'dl',
    ciamBrandGroup: 'mhli',
    ciamClientId: '2io3xpVUwNaHVAcSdNnOulHn2WG6N6K3',
    defaultLocale: 'nl-nl',
    env: 'production',
    logo: 'square',
    paymentProvider: 'mollie',
    siteName: 'De Limburger',
    siteUrl: 'https://www.limburger.nl',
    theme: 'dl'
  },
  /* --- LUX --- */
  /* CO - Contacto */
  {
    authentication: 'lux',
    brand: 'co',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'pt-pt',
    env: 'test',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Contacto',
    siteUrl: 'https://test.contacto.lu',
    theme: 'co'
  },
  {
    authentication: 'lux',
    brand: 'co',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'pt-pt',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Contacto',
    siteUrl: 'https://preview.contacto.lu',
    theme: 'co'
  },
  {
    authentication: 'lux',
    brand: 'co',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'pt-pt',
    env: 'production',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Contacto',
    siteUrl: 'https://www.contacto.lu',
    theme: 'co'
  },
  /* LT - Luxembourg Times */
  {
    authentication: 'lux',
    brand: 'lt',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'en-gb',
    env: 'test',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Luxembourg Times',
    siteUrl: 'https://test.luxtimes.lu',
    theme: 'lt'
  },
  {
    authentication: 'lux',
    brand: 'lt',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'en-gb',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Luxembourg Times',
    siteUrl: 'https://preview.luxtimes.lu',
    theme: 'lt'
  },
  {
    authentication: 'lux',
    brand: 'lt',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'en-gb',
    env: 'production',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Luxembourg Times',
    siteUrl: 'https://www.luxtimes.lu',
    theme: 'lt'
  },
  /* LW - Luxemburger Wort */
  {
    authentication: 'lux',
    brand: 'lw',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'test',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Luxemburger Wort',
    siteUrl: 'https://test.wort.lu',
    theme: 'lw'
  },
  {
    authentication: 'lux',
    brand: 'lw',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'preview',
    logo: 'square',
    paymentProvider: 'datatrans',
    siteName: 'Luxemburger Wort',
    siteUrl: 'https://preview.wort.lu',
    theme: 'lw'
  },
  {
    authentication: 'lux',
    brand: 'lw',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'production',
    logo: 'square',
    paymentProvider: '',
    siteName: 'Luxemburger Wort',
    siteUrl: 'https://www.wort.lu',
    theme: 'lw'
  },
  /* TC - Télécran */
  {
    authentication: 'lux',
    brand: 'tc',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'test',
    logo: 'default',
    paymentProvider: 'datatrans',
    siteName: 'Télécran',
    siteUrl: 'https://test.telecran.lu',
    theme: 'tc'
  },
  {
    authentication: 'lux',
    brand: 'tc',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'preview',
    logo: 'default',
    paymentProvider: 'datatrans',
    siteName: 'Télécran',
    siteUrl: 'https://preview.telecran.lu',
    theme: 'tc'
  },
  {
    authentication: 'lux',
    brand: 'tc',
    ciamBrandGroup: '',
    ciamClientId: '',
    defaultLocale: 'de-de',
    env: 'production',
    logo: 'default',
    paymentProvider: 'datatrans',
    siteName: 'Télécran',
    siteUrl: 'https://www.telecran.lu',
    theme: 'tc'
  }
];

if (process.env.MH_BRAND) {
  variants = variants.filter(
    variant => variant.brand === process.env.MH_BRAND && variant.env === 'test'
  );
}

module.exports = variants;
