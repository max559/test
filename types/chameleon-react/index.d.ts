/* eslint-disable @typescript-eslint/ban-types */
declare module '@mediahuis/chameleon-react' {
  import type {
    AnchorHTMLAttributes,
    ButtonHTMLAttributes,
    ChangeEventHandler,
    ChangeEvent,
    DetailedHTMLProps,
    FocusEvent,
    HTMLAttributes,
    InputHTMLAttributes,
    LabelHTMLAttributes,
    PropsWithChildren,
    ReactElement,
    ReactNode
  } from 'react';

  /* COMPONENTS */

  /* Accordion */
  interface AccordionProps {
    collapsible?: boolean;
    className?: string;
    id: string;
    initialExpandedIndex?: number;
    onChange?: () => void;
  }
  export const Accordion: (props: PropsWithChildren<AccordionProps>) => ReactElement;

  /* AccordionItem */
  interface AccordionItemProps {
    expanded?: boolean;
    focussed?: boolean;
    id?: string;
    index?: number;
    title?: string;
    onClick?: () => void;
    onFocus?: () => void;
    onKeyDown?: () => void;
  }
  export const AccordionItem: (props: PropsWithChildren<AccordionItemProps>) => ReactElement;

  /* AutoComplete */
  interface AutoCompleteProps {
    accessibilityLabelClearButton?: string;
    action?: ReactNode;
    autoComplete?: string;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    forceSelection?: boolean;
    iconLeft?: ReactNode;
    id: string;
    label: string;
    labelHidden?: boolean;
    loading?: boolean;
    markMatches?: boolean;
    message?: string;
    name?: string;
    optionalLabel?: string;
    placeholder?: string;
    placement?: 'bottom' | 'top';
    readOnly?: boolean;
    success?: boolean;
    suggestions: Array<string>;
    type?: string;
    value?: string;
    onBlur?: Function;
    onChange: (value: string) => void;
    onFocus?: () => void;
    onInputChange?: (value: string) => void;
    onSelect?: (value: string | null) => void;
  }
  export const AutoComplete: (props: AutoCompleteProps) => ReactElement;

  /* Banner */
  interface BannerProps {
    accessibilityLabelCloseButton?: string;
    appearance?: 'error' | 'info' | 'success' | 'warning';
    closeHidden?: boolean;
    show?: boolean;
    onClose?: () => void;
  }
  export const Banner: (props: PropsWithChildren<BannerProps>) => ReactElement;

  /* BrandedHeading */
  interface BrandedHeadingProps {
    actionLabel?: string;
    actionUrl?: string;
    as?: string;
    size?: 'sm' | 'lg';
  }
  export const BrandedHeading: (props: PropsWithChildren<BrandedHeadingProps>) => ReactElement;

  /* Button */
  type ButtonDefaultProps = DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >;
  interface ButtonProps extends ButtonDefaultProps {
    appearance?: 'default' | 'primary' | 'secondary' | 'tertiary';
    as?: string | ReactNode | ReactElement;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    iconLeft?: string | ReactNode | ReactElement;
    iconRight?: string | ReactNode | ReactElement;
    loading?: boolean;
    size?: 'sm' | 'lg';
    width?: 'auto' | 'full';
  }
  export const Button: (props: PropsWithChildren<ButtonProps>) => ReactElement;

  /* Caption */
  interface CaptionProps extends React.HtmlHTMLAttributes<HTMLElement> {
    as?: string | ReactNode | object;
    children?: ReactNode;
    className?: string;
    color?: string;
    font?: 'default' | 'alt';
    size?: 'sm' | 'lg';
    weight?: 'default' | 'strong';
  }
  export const Caption: (props: PropsWithChildren<CaptionProps>) => ReactElement;

  /* Checkbox */
  type CheckboxDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  interface CheckboxProps extends CheckboxDefaultProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
    // onChange?: (event) => void;
  }
  export const Checkbox: (props: CheckboxProps) => ReactElement;

  /* Choice */
  type ChoiceDefaultProps = DetailedHTMLProps<
    LabelHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  >;
  interface ChoiceProps extends Omit<ChoiceDefaultProps, 'title'> {
    checked?: boolean;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    imageAlt?: string;
    imageSrc?: string;
    info?: string | ReactNode;
    labelProps?: object;
    message?: string | ReactNode;
    name: string;
    title: string | ReactNode;
    type?: 'radio' | 'checkbox';
    value: string;
    // onChange?: (event) => void;
  }
  export const Choice: (props: PropsWithChildren<ChoiceProps>) => ReactElement;

  /* DatePicker  */
  interface DatePickerProps {
    action?: ReactNode;
    className?: string;
    defaultMonth?: Date;
    disabled?: boolean;
    disabledDays?: Function | Array<any> | { after: Date; before: Date };
    error?: boolean;
    format?: string;
    id: string;
    inline?: boolean;
    label: ReactNode;
    labelHidden?: boolean;
    mask?: string;
    message?: string;
    name?: string;
    optionalLabel?: string;
    pickerFooter?: string;
    pickerInputOnly?: boolean;
    placement?: 'bottom-start' | 'bottom-end' | 'top-start' | 'top-end';
    readOnly?: boolean;
    showOnFocus?: boolean;
    success?: boolean;
    type?: string;
    value?: object | Date | boolean;
    onBlur?: () => void;
    onChange: (date: Date | false, other: { disabled: boolean; formattedDate: string }) => void;
    onClick?: () => void;
    onFocus?: () => void;
    onMouseDown?: () => void;
  }
  export const DatePicker: (props: DatePickerProps) => ReactElement;

  /* Dialog */
  interface DialogProps {
    accessibilityLabelCloseButton?: string;
    children?: ReactNode;
    closeOnBackdropClick?: boolean;
    closeOnEscape?: boolean;
    footer?: ReactNode;
    headerHidden?: boolean;
    lockBodyOnScroll?: boolean;
    show?: boolean;
    onClose?: () => void;
  }
  export const Dialog: (props: DialogProps) => ReactElement;

  /* Divider */
  type DividerDefaultProps = DetailedHTMLProps<HTMLAttributes<HTMLHRElement>, HTMLHRElement>;
  interface DividerProps extends DividerDefaultProps {
    className?: string;
  }
  export const Divider: (props: DividerProps) => ReactElement;

  /* Fieldset */
  interface FieldsetProps {
    action?: ReactNode;
    children: ReactNode;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    hidden?: boolean;
    htmlFor?: string;
    id?: string;
    label?: ReactNode;
    message?: string;
    name?: string;
    optionalLabel?: string;
  }
  export const Fieldset: (props: FieldsetProps) => ReactElement;

  /* Heading */
  interface HeadingProps extends React.HtmlHTMLAttributes<HTMLHeadingElement> {
    as?: ReactNode;
    children: ReactNode;
    className?: string;
    color?: string;
    level?: 1 | 2 | 3 | 4 | 5 | 6;
    size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | '2xl' | 'display';
  }
  export const Heading: (props: PropsWithChildren<HeadingProps>) => ReactElement;

  /* Icon */
  interface IconProps {
    as?: Function | ReactNode | object;
    className?: string;
    color?: string;
    size?: 'sm' | 'md' | 'lg' | 'xl';
  }
  export const Icon: (props: IconProps) => ReactElement;

  /* IconButton */
  interface IconButtonProps {
    accessibilityLabel?: string;
    appearance?: 'default' | 'primary' | 'secondary' | 'tertiary';
    as?: () => void | string | ReactNode | object;
    circular?: boolean;
    className?: string;
    disabled?: boolean;
    icon?: Function | ReactNode | object;
    loading?: boolean;
    size?: 'sm' | 'lg';
    onClick?: () => void;
  }
  export const IconButton: (props: IconButtonProps) => ReactElement;

  /* LinkText */
  type LinkTextDefaultProps = DetailedHTMLProps<
    AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  >;
  interface LinkTextProps extends LinkTextDefaultProps {
    as?: Function | string | ReactNode | object;
    block?: boolean;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    iconRight?: Function | string | ReactNode | object;
  }
  export const LinkText: (props: LinkTextProps) => ReactElement;

  /* List */
  interface ListProps {
    as?: 'ol' | 'ul';
    className?: string;
    children?: ReactNode;
  }
  export const List: (props: PropsWithChildren<ListProps>) => ReactElement;

  /* ListItem */
  interface ListItemProps {
    accessibilityLabel?: string;
    children?: ReactNode;
    className?: string;
    icon?: () => void | ReactNode | object;
  }
  export const ListItem: (props: PropsWithChildren<ListItemProps>) => ReactElement;

  /* Loader */
  interface LoaderProps {
    accessibilityLabel?: string;
    className?: string;
    size?: 'sm' | 'md' | 'lg' | 'xl' | '2xl';
  }
  export const Loader: (props: LoaderProps) => ReactElement;

  /* Logo */
  interface LogoProps extends React.HTMLAttributes<HTMLImageElement> {
    alt?: string;
    className?: string;
    extension?: 'png' | 'svg';
    name?: string;
  }
  export const Logo: (props: LogoProps) => ReactElement;

  /* Paper */
  interface PaperProps extends React.HTMLAttributes<HTMLDivElement> {
    as?: Function | string | ReactNode | object;
    className?: string;
    elevation?: -1 | 0 | 1 | 2 | 3;
    hoverable?: boolean;
  }
  export const Paper: (props: PropsWithChildren<PaperProps>) => ReactElement;

  /* Paragraph */
  interface ParagraphProps extends React.HTMLAttributes<HTMLElement> {
    as?: Function | string | ReactNode | object;
    className?: string;
    color?: string;
    font?: 'default' | 'alt';
    size?: 'sm' | 'lg';
    weight?: 'default' | 'strong';
  }
  export const Paragraph: (props: PropsWithChildren<ParagraphProps>) => ReactElement;

  /* Placeholder */
  interface PlaceholderProps {
    appearance?: 'placeholder' | 'plain';
    aspectRatio: string;
    children?: ReactNode;
    className?: string;
  }
  export const Placeholder: (props: PropsWithChildren<PlaceholderProps>) => ReactElement;

  /* Radio */
  interface RadioProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  }
  export const Radio: (props: RadioProps) => ReactElement;

  /* Select */
  interface SelectProps {
    action?: ReactNode;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    iconLeft?: Function | ReactNode | object;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    message?: string;
    name?: string;
    optionalLabel?: string;
    placeholder?: string;
    required?: boolean;
    success?: boolean;
    value?: string;
    onBlur?: Function;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
    onClick?: Function;
    onFocus?: Function;
  }
  export const Select: (props: PropsWithChildren<SelectProps>) => ReactElement;

  /* Stack */
  interface StackProps extends React.HTMLAttributes<HTMLDivElement> {
    alignContent?:
      | 'center'
      | 'start'
      | 'end'
      | 'flex-start'
      | 'flex-end'
      | 'normal'
      | 'baseline'
      | 'first baseline'
      | 'last baseline'
      | 'space-between'
      | 'space-around'
      | 'space-evenly'
      | 'stretch'
      | 'safe center'
      | 'unsafe center'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    alignItems?:
      | 'normal'
      | 'stretch'
      | 'center'
      | 'start'
      | 'end'
      | 'flex-start'
      | 'flex-end'
      | 'baseline'
      | 'first baseline'
      | 'last baseline'
      | 'safe center'
      | 'unsafe center'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    alignSelf?:
      | 'auto'
      | 'normal'
      | 'center'
      | 'start'
      | 'end'
      | 'self-start'
      | 'self-end'
      | 'flex-start'
      | 'flex-end'
      | 'baseline'
      | 'first baseline'
      | 'last baseline'
      | 'stretch'
      | 'safe center'
      | 'unsafe center'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    bordered?: boolean;
    children?: ReactNode;
    className?: string;
    flexBasis?:
      | 'auto'
      | 'initial'
      | 'none'
      | 'max-content'
      | 'min-content'
      | 'fit-content'
      | 'content'
      | 'inherit'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | string
      | MediaQueryValues;
    flexDirection?:
      | 'row'
      | 'row-reverse'
      | 'column'
      | 'column-reverse'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    flexFlow?:
      | 'row'
      | 'row-reverse'
      | 'column'
      | 'column-reverse'
      | 'nowrap'
      | 'wrap'
      | 'wrap-reverse'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | string
      | MediaQueryValues;
    flexGrow?:
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | string
      | MediaQueryValues;
    flexShrink?:
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | string
      | MediaQueryValues;
    flexWrap?:
      | 'nowrap'
      | 'wrap'
      | 'wrap-reverse'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    gap?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13;
    justifyContent?:
      | 'center'
      | 'start'
      | 'end'
      | 'flex-start'
      | 'flex-end'
      | 'left'
      | 'right'
      | 'normal'
      | 'space-between'
      | 'space-around'
      | 'space-evenly'
      | 'stretch'
      | 'safe center'
      | 'unsafe center'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
    justifySelf?:
      | 'auto'
      | 'normal'
      | 'stretch'
      | 'center'
      | 'start'
      | 'end'
      | 'flex-start'
      | 'flex-end'
      | 'self-start'
      | 'self-end'
      | 'left'
      | 'right'
      | 'baseline'
      | 'first baseline'
      | 'last baseline'
      | 'safe center'
      | 'unsafe center'
      | 'inherit'
      | 'initial'
      | 'revert'
      | 'revert-layer'
      | 'unset'
      | MediaQueryValues;
  }
  export const Stack: (props: StackProps) => ReactElement;

  /* Stepper */
  interface StepperProps {
    accessibilityLabelActive?: string;
    accessibilityLabelCompleted?: string;
    activeStepIndex?: number;
    steps: Array<string>;
  }
  export const Stepper: (props: StepperProps) => ReactElement;

  /* Switch */
  type SwitchDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  interface SwitchProps extends SwitchDefaultProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
  }
  export const Switch: (props: SwitchProps) => ReactElement;

  /* TextField */
  type TextFieldDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  interface TextFieldProps extends TextFieldDefaultProps {
    accessibilityLabelClearButton?: string;
    action?: ReactNode;
    className?: string;
    clearButton?: boolean;
    disabled?: boolean;
    error?: boolean;
    iconLeft?: Function | string | ReactNode | object;
    iconLeftComponent?: ReactNode;
    iconRight?: Function | string | ReactNode | object;
    iconRightComponent?: ReactNode;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelProps?: object;
    mask?: string;
    message?: string;
    name?: string;
    optionalLabel?: string;
    success?: boolean;
    type?: string;
    value?: string | number;
    onBlur?: (e: FocusEvent<HTMLInputElement>) => void;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
    onFocus?: (e: FocusEvent<HTMLInputElement>) => void;
    onIconLeftClick?: Function;
    onIconRightClick?: Function;
  }
  export const TextField: (props: TextFieldProps) => ReactElement;

  /* Tooltip  */
  interface TooltipProps {
    children?: ReactNode;
    defaultVisible?: boolean;
    id: string;
    label?: string;
    placement?:
      | 'auto'
      | 'auto-start'
      | 'auto-end'
      | 'top'
      | 'top-start'
      | 'top-end'
      | 'bottom'
      | 'bottom-start'
      | 'bottom-end'
      | 'right'
      | 'right-start'
      | 'right-end'
      | 'left'
      | 'left-start'
      | 'left-end';
    usePortal?: boolean;
  }
  export const Tooltip: (props: PropsWithChildren<TooltipProps>) => ReactElement;

  /* WideList */
  interface WideListProps {
    children?: ReactNode;
  }
  export const WideList: (props: PropsWithChildren<WideListProps>) => ReactElement;

  /* WideListItem */
  interface WideListItemProps {
    action?: ReactNode;
    avatar?: string;
    clickable?: boolean;
    description?: ReactNode;
    iconLeft?: () => void | ReactNode | object;
    iconRight?: () => void | ReactNode | object;
    loading?: boolean;
    title?: ReactNode;
    onClick?: () => void;
  }
  export const WideListItem: (props: WideListItemProps) => ReactElement;

  /* useMediaQuery */
  interface MediaQueryValues {
    xs?: any;
    sm?: any;
    md?: any;
    lg?: any;
    xl?: any;
  }

  export const useMediaQuery: (values: MediaQueryValues | Array<any>, defaultValue?: any) => any;
}
