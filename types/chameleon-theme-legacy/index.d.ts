declare module '@mediahuis/chameleon-theme-wl/legacy/icons' {
  import type { ReactNode } from 'react';

  export const Add: ReactNode;
  export const ArrowBack: ReactNode;
  export const ArrowDown: ReactNode;
  export const ArrowForward: ReactNode;
  export const ArrowUp: ReactNode;
  export const Brand: ReactNode;
  export const Checkmark: ReactNode;
  export const ChevronBack: ReactNode;
  export const ChevronDoubleBack: ReactNode;
  export const ChevronDoubleForward: ReactNode;
  export const ChevronDown: ReactNode;
  export const ChevronForward: ReactNode;
  export const ChevronUp: ReactNode;
  export const Clipboard: ReactNode;
  export const Close: ReactNode;
  export const Edit: ReactNode;
  export const Error: ReactNode;
  export const Hide: ReactNode;
  export const Home: ReactNode;
  export const Layout: ReactNode;
  export const Phone: ReactNode;
  export const PhoneFill: ReactNode;
  export const Pin: ReactNode;
  export const RecentlyViewed: ReactNode;
  export const Settings: ReactNode;
  export const ShieldFill: ReactNode;
}
