declare module '@mediahuis/chameleon-react-legacy' {
  import type {
    AnchorHTMLAttributes,
    ButtonHTMLAttributes,
    ChangeEventHandler,
    CSSProperties,
    DetailedHTMLProps,
    HTMLAttributes,
    MouseEventHandler,
    InputHTMLAttributes,
    LabelHTMLAttributes,
    PropsWithChildren,
    ReactElement,
    ReactNode
  } from 'react';

  /* SYSTEM PROPS */

  interface backgroundProps {
    bgColor?: string;
  }

  interface borderProps {
    borderBottomColor?: string;
    borderBottomStyle?:
      | 'none'
      | 'hidden'
      | 'dotted'
      | 'dashed'
      | 'solid'
      | 'double'
      | 'groove'
      | 'ridge'
      | 'inset'
      | 'outset'
      | 'initial'
      | 'inherit';
    borderBottomWidth?: 0 | 1 | 2 | 3 | 4;
    borderColor?: string;
    borderLeftColor?: string;
    borderLeftStyle?:
      | 'none'
      | 'hidden'
      | 'dotted'
      | 'dashed'
      | 'solid'
      | 'double'
      | 'groove'
      | 'ridge'
      | 'inset'
      | 'outset'
      | 'initial'
      | 'inherit';
    borderLeftWidth?: 0 | 1 | 2 | 3 | 4;
    borderRadius?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 'fill' | 'sides';
    borderRightColor?: string;
    borderRightStyle?:
      | 'none'
      | 'hidden'
      | 'dotted'
      | 'dashed'
      | 'solid'
      | 'double'
      | 'groove'
      | 'ridge'
      | 'inset'
      | 'outset'
      | 'initial'
      | 'inherit';
    borderRightWidth?: 0 | 1 | 2 | 3 | 4;
    borderStyle?:
      | 'none'
      | 'hidden'
      | 'dotted'
      | 'dashed'
      | 'solid'
      | 'double'
      | 'groove'
      | 'ridge'
      | 'inset'
      | 'outset'
      | 'initial'
      | 'inherit';
    borderTopColor?: string;
    borderTopStyle?:
      | 'none'
      | 'hidden'
      | 'dotted'
      | 'dashed'
      | 'solid'
      | 'double'
      | 'groove'
      | 'ridge'
      | 'inset'
      | 'outset'
      | 'initial'
      | 'inherit';
    borderTopWidth?: 0 | 1 | 2 | 3 | 4;
    borderWidth?: 0 | 1 | 2 | 3 | 4;
  }

  interface borderRadiusProps {
    borderRadius?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 'fill' | 'sides';
  }

  interface colorProps {
    color?: string;
  }

  interface displayProps {
    display?:
      | 'inline'
      | 'block'
      | 'flex'
      | 'grid'
      | 'inline-block'
      | 'inline-flex'
      | 'inline-grid'
      | 'inline-table'
      | 'list-item'
      | 'run-in'
      | 'table'
      | 'table-caption'
      | 'table-column-group'
      | 'table-header-group'
      | 'table-footer-group'
      | 'table-row-group'
      | 'table-cell'
      | 'table-column'
      | 'table-row'
      | 'none'
      | 'initial'
      | 'inherit';
  }

  interface flexProps {
    alignSelf?: 'auto' | 'flex-start' | 'flex-end' | 'center' | 'baseline' | 'stretch';
    flex?: string | number;
    flexBasis?: string | number;
    flexGrow?: string | number;
    flexShrink?: string | number;
    order?: string | number;
  }

  interface flexParentProps {
    alignContent?:
      | 'center'
      | 'flex-end'
      | 'flex-start'
      | 'space-around'
      | 'space-between'
      | 'space-evenly'
      | 'stretch';
    alignItems?: 'baseline' | 'center' | 'flex-end' | 'flex-start' | 'stretch';
    flexDirection?: 'column-reverse' | 'column' | 'row-reverse' | 'row';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    justifyContent?:
      | 'center'
      | 'flex-end'
      | 'flex-start'
      | 'space-around'
      | 'space-between'
      | 'stretch'
      | 'space-evenly';
  }

  interface floatProps {
    float?: 'left' | 'right' | 'none';
  }

  interface heightProps {
    height?: 'full' | 'auto' | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | string;
    maxHeight?: 'full' | 'auto' | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | string;
  }

  interface marginProps {
    m?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    mb?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    ml?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    mr?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    mt?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    mx?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
    my?:
      | -11
      | -10
      | -9
      | -8
      | -7
      | -6
      | -5
      | -4
      | -3
      | -2
      | -1
      | 0
      | 1
      | 2
      | 3
      | 4
      | 5
      | 6
      | 7
      | 8
      | 9
      | 10
      | 11
      | 'auto';
  }

  interface paddingProps {
    p?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    pb?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    pl?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    pr?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    pt?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    px?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    py?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
  }

  interface positioningProps {
    bottom?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    left?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    position?: 'absolute' | 'fixed' | 'relative' | 'sticky';
    right?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    top?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    zIndex?: 0 | 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900;
  }

  interface textProps {
    decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
    fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
    fontStyle?: 'inherit' | 'normal' | 'italic' | 'oblique';
    fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
    textAlign?: 'inherit' | 'left' | 'right' | 'center' | 'justify';
    textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
  }

  interface widthProps {
    maxWidth?: 'full' | 'auto' | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | string;
    width?: 'full' | 'auto' | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | string;
  }

  /* COMPONENTS */

  /* Accordion */
  type AccordionSystemProps = flexProps & floatProps & marginProps;
  interface AccordionProps extends AccordionSystemProps {
    collapsible?: boolean;
    id: string;
    initialExpandedIndex?: number;
  }
  export const Accordion: (props: PropsWithChildren<AccordionProps>) => ReactElement;

  /* AccordionItem */
  interface AccordionItemProps {
    expanded?: boolean;
    focussed?: boolean;
    id?: string;
    index?: number;
    title?: string;
    onClick?: () => void;
    onFocus?: () => void;
    onKeyDown?: () => void;
  }
  export const AccordionItem: (props: PropsWithChildren<AccordionItemProps>) => ReactElement;

  /* AutoComplete */
  type AutoCompleteSystemProps = floatProps & marginProps;
  interface AutoCompleteProps extends AutoCompleteSystemProps {
    accessibilityLabelClearButton?: string;
    action?: ReactNode;
    autoComplete?: string;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    forceSelection?: boolean;
    iconLeft?: ReactNode;
    id: string;
    label: string;
    labelHidden?: boolean;
    loading?: boolean;
    message?: string;
    name?: string;
    optionalLabel?: string;
    placeholder?: string;
    placement?: 'bottom' | 'top';
    readOnly?: boolean;
    success?: boolean;
    suggestions: Array<string>;
    type?: string;
    value?: string;
    onBlur?: Function;
    onChange: ChangeEventHandler<HTMLInputElement>;
    onFocus?: Function;
    onInputChange?: (value: string) => void;
    onSelect?: (value: string | null) => void;
  }
  export const AutoComplete: (props: AutoCompleteProps) => ReactElement;

  /* Banner */
  type BannerSystemProps = flexProps & floatProps & marginProps & positioningProps;
  interface BannerProps extends BannerSystemProps {
    accessibilityLabelCloseButton?: string;
    appearance?: 'error' | 'info' | 'success';
    children?: ReactNode;
    closeHidden?: boolean;
    show?: boolean;
    onClose?: Function;
  }
  export const Banner: (props: BannerProps) => ReactElement;

  /* Box */
  type BoxSystemProps = backgroundProps &
    borderProps &
    displayProps &
    flexProps &
    flexParentProps &
    heightProps &
    marginProps &
    paddingProps &
    widthProps;
  interface BoxProps extends BoxSystemProps {
    alignContent?:
      | 'center'
      | 'flex-end'
      | 'flex-start'
      | 'space-around'
      | 'space-between'
      | 'space-evenly'
      | 'stretch';
    alignItems?: 'flex-start' | 'center' | 'flex-end' | 'stretch' | 'baseline';
    bottom?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    children?: ReactNode;
    className?: string;
    flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    order?: string | number;
    position?: 'absolute' | 'fixed' | 'relative' | 'sticky';
    right?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    style?: CSSProperties;
    top?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
    onClick?: MouseEventHandler<HTMLDivElement>;
  }
  export const Box: (props: PropsWithChildren<BoxProps>) => ReactElement;

  /* BrandedHeading */
  type BrandedHeadingSytemProps = flexProps & floatProps & marginProps;
  interface BrandedHeadingProps extends BrandedHeadingSytemProps {
    appearance?: 'primary' | 'secondary';
    as?: string;
    size?: 'small' | 'large';
  }
  export const BrandedHeading: (props: PropsWithChildren<BrandedHeadingProps>) => ReactElement;

  /* Button */
  type ButtonDefaultProps = DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >;
  type ButtonSystemProps = ButtonDefaultProps & flexProps & floatProps & marginProps;
  interface ButtonProps extends ButtonSystemProps {
    appearance?: 'primary' | 'secondary' | 'plain' | 'outline';
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    iconLeft?: string | ReactNode | ReactElement;
    iconRight?: string | ReactNode | ReactElement;
    loading?: boolean;
    size?: 'small' | 'large';
    width?: 'auto' | 'full';
  }
  export const Button: (props: PropsWithChildren<ButtonProps>) => ReactElement;

  /* Caption */
  type CaptionSystemProps = colorProps &
    displayProps &
    flexProps &
    floatProps &
    marginProps &
    textProps;
  interface CaptionProps extends CaptionSystemProps {
    as?: string | ReactNode;
    className?: string;
    fontStyle?: 'inherit' | 'normal' | 'italic' | 'oblique';
    level?: 1 | 2;
    textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
  }
  export const Caption: (props: PropsWithChildren<CaptionProps>) => ReactElement;

  /* Checkbox */
  type CheckboxDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  type CheckboxSystemProps = CheckboxDefaultProps & floatProps & marginProps;
  interface CheckboxProps extends CheckboxSystemProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
  }
  export const Checkbox: (props: CheckboxProps) => ReactElement;

  /* Choice */
  type ChoiceDefaultProps = DetailedHTMLProps<
    LabelHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  >;
  type ChoiceSystemProps = ChoiceDefaultProps & floatProps & marginProps;
  interface ChoiceProps extends Omit<ChoiceSystemProps, 'title'> {
    message?: string | ReactNode;
    checked?: boolean;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
    id: string;
    imageAlt?: string;
    imageSrc?: string;
    info?: ReactNode;
    labelProps?: object;
    name: string;
    title: ReactNode;
    type?: 'radio' | 'checkbox';
    value: string;
    // onChange?: (event) => void;
  }
  export const Choice: (props: PropsWithChildren<ChoiceProps>) => ReactElement;

  /* DatePicker  */
  type DatePickerSystemProps = marginProps;
  interface DatePickerProps extends DatePickerSystemProps {
    action?: ReactNode;
    className?: string;
    disabled?: boolean;
    disabledDays?: Array<any> | Function | { after: Date; before: Date };
    error?: boolean;
    format?: string;
    id: string;
    inline?: boolean;
    label: ReactNode;
    labelHidden?: boolean;
    mask?: string;
    message?: string;
    name?: string;
    optionalLabel?: string;
    placement?: 'bottom-start' | 'bottom-end' | 'top-start' | 'top-end';
    readOnly?: boolean;
    success?: boolean;
    type?: string;
    value?: object | Date | false;
    onBlur?: () => void;
    onChange: (date: Date | false, other: { disabled: boolean; formattedDate: string }) => void;
    onFocus?: () => void;
    onMouseDown?: () => void;
  }
  export const DatePicker: (props: DatePickerProps) => ReactElement;

  /* Dialog */
  interface DialogProps {
    accessibilityLabelCloseButton?: string;
    children?: ReactNode;
    closeOnBackdropClick?: boolean;
    closeOnEscape?: boolean;
    footer?: ReactNode;
    headerHidden?: boolean;
    lockBodyOnScroll?: boolean;
    show?: boolean;
    onClose?: () => void;
  }
  export const Dialog: (props: DialogProps) => ReactElement;

  /* Divider */
  type DividerDefaultProps = DetailedHTMLProps<HTMLAttributes<HTMLHRElement>, HTMLHRElement>;
  type DividerSystemProps = DividerDefaultProps & floatProps & marginProps;
  interface DividerProps extends DividerSystemProps {
    className?: string;
    color?: string;
  }
  export const Divider: (props: DividerProps) => ReactElement;

  /* Fieldset */
  type FieldsetSystemProps = marginProps;
  interface FieldsetProps extends FieldsetSystemProps {
    action?: ReactNode;
    children: ReactNode;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id?: string;
    label?: ReactNode;
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    name?: string;
    optionalLabel?: string;
  }
  export const Fieldset: (props: FieldsetProps) => ReactElement;

  /* Flex */
  type FlexSystemProps = backgroundProps &
    borderProps &
    borderRadiusProps &
    colorProps &
    flexProps &
    flexParentProps &
    floatProps &
    heightProps &
    marginProps &
    paddingProps &
    positioningProps &
    textProps &
    widthProps;
  interface FlexProps extends FlexSystemProps {
    alignContent?:
      | 'center'
      | 'flex-end'
      | 'flex-start'
      | 'space-around'
      | 'space-between'
      | 'space-evenly'
      | 'stretch';
    alignItems?: 'baseline' | 'center' | 'flex-end' | 'flex-start' | 'stretch';
    className?: string;
    display?: 'flex' | 'inline-flex';
    flexDirection?: 'column-reverse' | 'column' | 'row-reverse' | 'row';
    flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
    style?: CSSProperties;
    onClick?: MouseEventHandler<HTMLDivElement>;
  }
  export const Flex: (props: PropsWithChildren<FlexProps>) => ReactElement;

  /* Heading */
  type HeadingSytemProps = colorProps &
    displayProps &
    flexProps &
    floatProps &
    marginProps &
    textProps;
  interface HeadingProps extends HeadingSytemProps {
    as?: Function | string | ReactNode | object;
    className?: string;
    color?: string;
    decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
    fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
    fontStyle?: 'inherit' | 'normal' | 'italic' | 'oblique';
    fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
    level?: 1 | 2 | 3 | 4 | 5 | 6;
    style?: CSSProperties;
    textAlign?: 'inherit' | 'left' | 'right' | 'center' | 'justify';
    textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
  }
  export const Heading: (props: PropsWithChildren<HeadingProps>) => ReactElement;

  /* Icon */
  type IconSystemProps = colorProps & flexProps & floatProps & marginProps & positioningProps;
  interface IconProps extends IconSystemProps {
    as?: ReactNode;
    className?: string;
    size?: 'small' | 'medium' | 'large' | 'xLarge';
  }
  export const Icon: (props: IconProps) => ReactElement;

  /* IconButton */
  type IconButtonSystemProps = flexProps & floatProps & marginProps & positioningProps;
  interface IconButtonProps extends IconButtonSystemProps {
    accessibilityLabel?: string;
    appearance?: 'default' | 'primary' | 'secondary' | 'plain' | 'outline';
    as?: string | ReactElement | ReactNode;
    circular?: boolean;
    className?: string;
    color?: string;
    disabled?: boolean;
    icon?: ReactNode;
    name?: string;
    size?: 'small' | 'large';
    onClick?: () => void;
  }
  export const IconButton: (props: IconButtonProps) => ReactElement;

  /* LinkText */
  type LinkTextDefaultProps = DetailedHTMLProps<
    AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  >;
  type LinkTextSystemProps = LinkTextDefaultProps &
    flexProps &
    floatProps &
    marginProps &
    positioningProps &
    textProps &
    widthProps;
  interface LinkTextProps extends LinkTextSystemProps {
    as?: string | ReactElement | ReactNode;
    branded?: boolean;
    children?: ReactNode;
    className?: string;
    disabled?: boolean;
  }
  export const LinkText: (props: LinkTextProps) => ReactElement;

  /* List */
  type ListSytemProps = flexProps & marginProps & floatProps & positioningProps & widthProps;
  interface ListProps extends ListSytemProps {
    as?: 'ol' | 'ul';
    className?: string;
    style?: CSSProperties;
  }
  export const List: (props: PropsWithChildren<ListProps>) => ReactElement;

  /* ListItem */
  interface ListItemProps {
    accessibilityLabel?: string;
    className?: string;
    icon?: ReactNode;
    iconColor?: string;
    style?: CSSProperties;
  }
  export const ListItem: (props: PropsWithChildren<ListItemProps>) => ReactElement;

  /* Loader */
  type LoaderSystemProps = flexProps & floatProps & marginProps & positioningProps;
  interface LoaderProps extends LoaderSystemProps {
    accessibilityLabel?: string;
    className?: string;
    size?: 'small' | 'medium' | 'large' | 'xLarge' | 'xxLarge';
  }
  export const Loader: (props: LoaderProps) => ReactElement;

  /* Logo */
  type LogoSystemProps = borderRadiusProps &
    flexProps &
    floatProps &
    heightProps &
    marginProps &
    positioningProps &
    widthProps;
  interface LogoProps extends LogoSystemProps {
    alt?: string;
    className?: string;
    extension?: 'png' | 'svg';
    height?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 'auto' | 'full' | string;
    maxHeight?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 'auto' | 'full' | string;
    name?: string;
    verticalAlign?:
      | 'baseline'
      | 'bottom'
      | 'inherit'
      | 'initial'
      | 'middle'
      | 'sub'
      | 'super'
      | 'text-bottom'
      | 'text-top'
      | 'top'
      | 'unset';
  }
  export const Logo: (props: LogoProps) => ReactElement;

  /* Paper */
  type PaperSytemProps = backgroundProps &
    borderProps &
    borderRadiusProps &
    displayProps &
    flexProps &
    flexParentProps &
    floatProps &
    heightProps &
    marginProps &
    paddingProps &
    positioningProps &
    textProps &
    widthProps;
  interface PaperProps extends PaperSytemProps {
    as?: Function | string | ReactNode | object;
    className?: string;
    elevation?: -1 | 0 | 1 | 2 | 3;
    hoverable?: boolean;
    style?: CSSProperties;
    onClick?: () => void;
  }
  export const Paper: (props: PropsWithChildren<PaperProps>) => ReactElement;

  /* Paragraph */
  type ParagraphSystemProps = colorProps &
    displayProps &
    flexProps &
    floatProps &
    marginProps &
    paddingProps &
    textProps &
    widthProps;
  interface ParagraphProps extends ParagraphSystemProps {
    as?: Function | string | ReactNode | object;
    className?: string;
    style?: CSSProperties;
  }
  export const Paragraph: (props: PropsWithChildren<ParagraphProps>) => ReactElement;

  /* Placeholder */
  type PlaceholderSystemProps = flexProps &
    floatProps &
    positioningProps &
    marginProps &
    paddingProps &
    widthProps;
  interface PlaceholderProps extends PlaceholderSystemProps {
    appearance?: 'placeholder' | 'plain';
    aspectRatio: string;
    className?: string;
  }
  export const Placeholder: (props: PropsWithChildren<PlaceholderProps>) => ReactElement;

  /* Radio */
  type RadioSystemProps = marginProps;
  interface RadioProps extends RadioSystemProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
    onChange?: Function;
  }
  export const Radio: (props: RadioProps) => ReactElement;

  /* Select */
  type SelectSystemProps = marginProps;
  interface SelectProps extends SelectSystemProps {
    action?: ReactNode;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    iconLeft?: ReactNode;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    message?: string;
    name?: string;
    optionalLabel?: string;
    placeholder?: string;
    required?: boolean;
    value?: string;
    onBlur?: Function;
    onChange?: Function;
    onClick?: Function;
    onFocus?: Function;
  }
  export const Select: (props: PropsWithChildren<SelectProps>) => ReactElement;

  /* Stepper */
  type StepperSytemProps = marginProps;
  interface StepperProps extends StepperSytemProps {
    accessibilityLabelActive?: string;
    accessibilityLabelCompleted?: string;
    activeStepIndex?: number;
    steps: Array<string>;
  }
  export const Stepper: (props: StepperProps) => ReactElement;

  /* Switch */
  type SwitchDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  type SwitchSytemProps = SwitchDefaultProps & marginProps;
  interface SwitchProps extends SwitchSytemProps {
    action?: ReactNode;
    checked?: boolean;
    className?: string;
    disabled?: boolean;
    error?: boolean;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    labelPlacement?: 'left' | 'right';
    labelProps?: {
      color?: string;
      decoration?: 'inherit' | 'none' | 'underline' | 'line-through';
      fontFamily?: 'inherit' | 'primary' | 'secondary' | 'system';
      fontWeight?: 'inherit' | 'regular' | 'medium' | 'bold';
      size?:
        | 'Heading1'
        | 'Heading2'
        | 'Heading3'
        | 'Heading4'
        | 'Heading5'
        | 'Heading6'
        | 'Caption1'
        | 'Caption2';
      textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
    };
    message?: string;
    optionalLabel?: string;
    required?: boolean;
    value?: string;
  }
  export const Switch: (props: SwitchProps) => ReactElement;

  /* Text */
  type TextSystemProps = colorProps &
    displayProps &
    flexProps &
    floatProps &
    marginProps &
    textProps;
  interface TextProps extends TextSystemProps {
    as?: ReactNode;
    className?: string;
    fontStyle?: 'inherit' | 'normal' | 'italic' | 'oblique';
    size?:
      | 'Heading1'
      | 'Heading2'
      | 'Heading3'
      | 'Heading4'
      | 'Heading5'
      | 'Heading6'
      | 'Paragraph'
      | 'Caption1'
      | 'Caption2';
    style?: CSSProperties;
    textTransform?: 'inherit' | 'none' | 'capitalize' | 'uppercase' | 'lowercase';
  }
  export const Text: (props: PropsWithChildren<TextProps>) => ReactElement;

  /* TextField */
  type TextFieldDefaultProps = DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
  type TextFieldSystemProps = TextFieldDefaultProps &
    flexProps &
    floatProps &
    marginProps &
    positioningProps &
    widthProps;
  interface TextFieldProps extends TextFieldSystemProps {
    accessibilityLabelClearButton?: string;
    action?: ReactNode;
    className?: string;
    clearButton?: boolean;
    disabled?: boolean;
    error?: boolean;
    iconLeft?: Function | string | ReactNode | object;
    iconLeftComponent?: ReactNode;
    iconRight?: Function | string | ReactNode | object;
    iconRightComponent?: ReactNode;
    id: string;
    label: ReactNode;
    labelHidden?: boolean;
    mask?: string;
    message?: string;
    name?: string;
    optionalLabel?: string;
    success?: boolean;
    type?: string;
    value?: string | number;
    onIconLeftClick?: () => void;
    onIconRightClick?: () => void;
  }
  export const TextField: (props: TextFieldProps) => ReactElement;

  /* Tooltip  */
  interface TooltipProps {
    defaultVisible?: boolean;
    id: string;
    label?: string;
    placement?:
      | 'auto'
      | 'auto-start'
      | 'auto-end'
      | 'top'
      | 'top-start'
      | 'top-end'
      | 'bottom'
      | 'bottom-start'
      | 'bottom-end'
      | 'right'
      | 'right-start'
      | 'right-end'
      | 'left'
      | 'left-start'
      | 'left-end';
    usePortal?: boolean;
  }
  export const Tooltip: (props: PropsWithChildren<TooltipProps>) => ReactElement;

  /* WideList */
  type WideListSystemProps = flexProps & floatProps & marginProps;
  interface WideListProps extends WideListSystemProps {
    elevation?: 0 | 1 | 2 | 3 | 4;
  }
  export const WideList: (props: PropsWithChildren<WideListProps>) => ReactElement;

  /* WideListItem */
  interface WideListItemProps {
    action?: ReactNode;
    avatar?: string;
    avatarCircular?: boolean;
    clickable?: boolean;
    description?: ReactNode;
    iconLeft?: void | ReactNode | object;
    iconRight?: void | ReactNode | object;
    loading?: boolean;
    title?: ReactNode;
    onClick?: () => void;
  }
  export const WideListItem: (props: WideListItemProps) => ReactElement;

  /* useMediaQuery */
  interface MediaQueryValues {
    xs?: any;
    sm?: any;
    md?: any;
    lg?: any;
    xl?: any;
  }

  export const useMediaQuery: (values: MediaQueryValues | Array<any>, defaultValue?: any) => any;
}
