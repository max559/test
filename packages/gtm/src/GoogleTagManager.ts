import type { Brand, Environment } from '@subscriber/globals';
import type { GdprConsents } from '@subscriber/didomi';
import type { ApplicationName, EcommerceEventName, OfferItemCombination } from 'src/gtm.types';

export const version = '1.0.251';

declare global {
  interface Window {
    gdprConsents?: GdprConsents;
    dataLayer: Record<string, any>[]
  }
}

class GoogleTagManager {
  private _brand: Brand;
  private _environment: Environment;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._environment = environment;
  }

  public registerPageView(accountId?: string, pageTitle?: string, pageUrl?: string) {
    this.registerEvent('internal_pageview', {accountId: accountId ?? ''});
    
    /* Disabled till we know if we actually want to send pageviews to GTM, as GA4 already collects pageviews by themselfs (we can override it with the page_view event)
    const isClientSide = this.isClientSide();

    this.registerEvent("page_view", {
      page_title: pageTitle ?? isClientSide ? window.document.title : undefined,
      page_location: pageUrl ?? isClientSide ? window.location.href : undefined,
      send_page_view: true
    });
    */
  }

  public registerEvent(eventName: string, eventData?: any) {
    if (this.isClientSide()) {
      window.dataLayer = window.dataLayer || [];
      
      window.dataLayer.push({
        event: eventName,
        brand: this._brand,
        environment: this._environment,
        consentcookies: window.gdprConsents?.cookies || false,
        consentcreateadsprofile: window.gdprConsents?.create_ads_profile || false,
        consentcreatecontentprofile: window.gdprConsents?.create_content_profile || false,
        consentimproveproducts: window.gdprConsents?.improve_products || false,
        consentmarketresearch: window.gdprConsents?.market_research || false,
        consentmeasureadperformance: window.gdprConsents?.measure_ad_performance || false,
        consentmeasurecontentperformance: window.gdprConsents?.measure_content_performance || false,
        consentselectpersonalizedads: window.gdprConsents?.select_personalized_ads || false,
        consentselectpersonalizedcontent: window.gdprConsents?.select_personalized_content || false,
        consentselectbasicads: window.gdprConsents?.select_basic_ads || false,
        ...eventData
      });
    }
  }

  public registerEcommerceEvent(applicationName: ApplicationName, eventName: EcommerceEventName, items: OfferItemCombination[]) {
    this.resetEcommerce();

    const eventData = {
      'ecommerce': {
        'items': items.map((i, index) => ({
          item_id: i.type?.externalReference ?? i.type?.name,
          item_name: i.type?.title?.replaceAll('&nbsp;', '').replaceAll('\n', '') ?? '',
          index: index,
          item_brand: i.type?.brand ?? i.formula?.brand ?? '',
          item_category: applicationName,
          item_list_id: i.offer.id.toString(),
          item_list_name: i.offer.reference ?? i.offer.slug ?? i.offer.id.toString(),
          item_variant: i.formula?.externalReference ?? i.formula?.name ?? i.formula?.title?.replaceAll('&nbsp;', '').replaceAll('\n', '') ?? '',
          price: i.formula?.projectedLifetimeValue ?? this.calculatePrice(i),
          discount: undefined, // https://jira.mediahuis.nl/browse/ABOSHOP-2403
          item_quantity: 1
        }))
      }
    }

    this.registerEvent(eventName, eventData);
  }

  private calculatePrice = (item: OfferItemCombination) => {
    const paymentRequirement = item.type?.paymentRequirement || item.formula?.paymentRequirement;
  
    if (!paymentRequirement) {
      return 0;
    }
  
    // We presume the amount will be withdrawn every month for recurring formulas with durationtype months
    if (paymentRequirement.recurringPaymentOptions && paymentRequirement.recurringPaymentOptions.length !== 0 && item.formula?.durationType === 'Months') {
      return paymentRequirement.amount * (item.formula?.duration ?? 1);
    }
  
    // In other cases, such as years, weeks or days, we cannot make this assumption
    // Even if we could calculate the exact revenue for complex formulas e.g. linkedsale products, not all information is available to make the calculation correctly
    // Thus we will simply return the payment amount until a solution has been found
  
    return paymentRequirement.amount;
  }

  private resetEcommerce() {
    if (this.isClientSide()) {
      window.dataLayer = window.dataLayer || [];
      window.dataLayer.push({
        ecommerce: null
      });
    }
  }

  private isClientSide = (): boolean => {
    return Boolean(typeof window !== 'undefined' && window.document && window.location);
  };
}

export default GoogleTagManager;