import type { Offer, OfferItem } from '@subscriber/services-core';

export type EcommerceEventName = 'add_to_cart' | 'remove_from_cart' |'view_item' |  'view_item_list';

export type ApplicationName = 'Aboshop' | 'Fast-checkout';

export interface OfferItemCombination {
    offer: Offer,
    formula?: OfferItem,
    type?: OfferItem
}