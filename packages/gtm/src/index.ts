export { GtmScript } from './components/GtmScript';
export { type EcommerceEventName, type ApplicationName , type OfferItemCombination } from './gtm.types';
export { default as GoogleTagManager } from './GoogleTagManager';