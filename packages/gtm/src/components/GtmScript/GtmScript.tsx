import type { Brand, Environment } from '@subscriber/globals';
import type { ReactElement } from 'react';

import Script from 'next/script';

import createGtmScriptConfig from './GtmScript.config';

interface GtmScriptProps {
  brand: Brand;
  enviroment: Environment;
}

declare global {
  interface Window {
    dataLayer: Record<string, any>[]
  }
}

const GtmScript = ({ brand, enviroment }: GtmScriptProps): ReactElement => {
  const config = createGtmScriptConfig(brand, enviroment);

  // Before deploying the nextjs aboshop to the 'real' production enviroment, ensure isLive is set to true!!!
  const scriptContent = `
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({brand: '${brand}'});
  window.dataLayer.push({environment: '${enviroment}'});
  window.dataLayer.push({isLive: false});
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','${config.id}');`;

  return (
    <Script id="google-tag-manager" strategy="afterInteractive">
      {scriptContent}
    </Script>
  );
};

export default GtmScript;