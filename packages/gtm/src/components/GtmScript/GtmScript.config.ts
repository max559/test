import type { Brand, Environment } from '@subscriber/globals';

import type { GtmScriptConfig } from './GtmScript.types';

const idConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  co: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  dl: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  ds: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  gva: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  hbvl: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  lt: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  lw: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  nb: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  },
  tc: {
    test: 'GTM-K5CX4PZ',
    preview: 'GTM-K5CX4PZ',
    production: 'GTM-KF3VN8LK'
  }
};

const createGtmScriptConfig = (brand: Brand, environment: Environment): GtmScriptConfig => {
  return {
    id: idConfig[brand][environment]
  };
};

export default createGtmScriptConfig;
