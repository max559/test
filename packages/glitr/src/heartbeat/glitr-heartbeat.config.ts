import type { Brand, Environment } from '../glitr.types';
import type { GlitrHeartbeatConfig } from './glitr-heartbeat.types';

const heartbeatTimeoutConfig: Record<Brand, Record<Environment, number>> = {
  az: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  co: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  dl: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  ds: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  gva: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  hbvl: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  lt: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  lw: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  nb: {
    test: 5000,
    preview: 5000,
    production: 5000
  },
  tc: {
    test: 5000,
    preview: 5000,
    production: 5000
  }
};

const lookAroundTimeoutConfig: Record<Brand, Record<Environment, number>> = {
  az: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  co: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  dl: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  ds: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  gva: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  hbvl: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  lt: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  lw: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  nb: {
    test: 1000,
    preview: 1000,
    production: 1000
  },
  tc: {
    test: 1000,
    preview: 1000,
    production: 1000
  }
};

const urlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://previewh-mhaa.mhtr.be/next/h',
    preview: 'https://previewh-mhaa.mhtr.be/next/h',
    production: 'https://prodh-mhaa.mhtr.be/next/h'
  },
  co: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  dl: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  ds: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  gva: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  hbvl: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  lt: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  lw: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  nb: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  },
  tc: {
    test: 'https://testh.mhtr.be/h',
    preview: 'https://previewh.mhtr.be/h',
    production: 'https://prodh.mhtr.be/h'
  }
};

export const createGlitrHeartbeatConfig = (
  brand: Brand,
  environment: Environment
): GlitrHeartbeatConfig => {
  return {
    heartbeatTimeout: heartbeatTimeoutConfig[brand][environment],
    lookAroundTimeout: lookAroundTimeoutConfig[brand][environment],
    url: urlConfig[brand][environment]
  };
};
