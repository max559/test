/* eslint-disable @typescript-eslint/no-this-alias */

import type { Brand, Environment } from '../glitr.types';
import type {
  GlitrHeartbeatConfig,
  GlitrHeartbeatData,
  GlitrHeartbeatEvent
} from './glitr-heartbeat.types';

import throttle from 'lodash.throttle';

import { version } from '../glitr';
import { getCookiesInfo } from '../glitr.util';
import { createGlitrHeartbeatConfig } from './glitr-heartbeat.config';
import { hasPulse } from './glitr-heartbeat.util';

class GlitrHeartbeat {
  private _brand: Brand;
  private _config: GlitrHeartbeatConfig;
  private _heartbeatTimer: NodeJS.Timer | undefined;
  private _isActive: boolean;
  private _isPageVisible: boolean;
  private _lastActiveTime: number;
  private _lastTotalPageAttentionSpan: number;
  private _lookAroundTimer: NodeJS.Timer | undefined;
  private _totalPageAttentionSpanInMilliseconds: number;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._config = createGlitrHeartbeatConfig(brand, environment);
    this._heartbeatTimer = undefined;
    this._isActive = false;
    this._isPageVisible = !document.hidden;
    this._lastActiveTime = new Date().getTime();
    this._lastTotalPageAttentionSpan = 0;
    this._lookAroundTimer = undefined;
    this._totalPageAttentionSpanInMilliseconds = 0;

    this.createEventListeners();
  }

  public start() {
    this._isActive = true;
    this._lastActiveTime = new Date().getTime();

    this.clearTimers();
    this.resetCounters();

    this._lookAroundTimer = setInterval(() => {
      this._totalPageAttentionSpanInMilliseconds += hasPulse(this._isPageVisible, this._lastActiveTime)
        ? this._config.lookAroundTimeout
        : 0;
    }, this._config.lookAroundTimeout);
    this._heartbeatTimer = setInterval(() => {
      const checkAttentionSpan =
        this._totalPageAttentionSpanInMilliseconds - this._lastTotalPageAttentionSpan > 0;

      if (checkAttentionSpan) {
        this.trigger();
        this._lastTotalPageAttentionSpan = this._totalPageAttentionSpanInMilliseconds;
      }
    }, this._config.heartbeatTimeout);
  }

  public stop() {
    this._isActive = false;

    this.clearTimers();
    this.resetCounters();

    this.trigger();
  }

  public trigger() {
    const heartbeatEvent = this.createHeartbeatEvent({});

    navigator.sendBeacon(this._config.url, new URLSearchParams({
      ...heartbeatEvent,
      data: JSON.stringify(heartbeatEvent.data)
    } as unknown as URLSearchParams));
  }

  private clearTimers() {
    clearInterval(this._lookAroundTimer);
    clearInterval(this._heartbeatTimer);
  }

  private createEventListeners() {
    const that = this;

    document.addEventListener('visibilitychange', () => that.handleVisibilityChange());
    document.addEventListener('click', () => that.handleEvent());
    document.addEventListener(
      'mousemove',
      throttle(() => that.handleEvent(), 1000)
    );
    document.addEventListener(
      'scroll',
      throttle(() => that.handleEvent(), 1000)
    );
    document.addEventListener('focus', () => that.handleEvent());
    document.addEventListener('blur', () => that.handleEvent());
    document.addEventListener('keydown', () => that.handleEvent());
    document.addEventListener(
      'touchmove',
      throttle(() => that.handleEvent(), 1000)
    );
    window.addEventListener('beforeunload', () => that.handleBeforeUnload(), false);
  }

  private createHeartbeatEvent(heartbeatData: Partial<GlitrHeartbeatData>): GlitrHeartbeatEvent {
    const { viewId, testmode } = getCookiesInfo('heartbeat');

    return {
      clienttimestamp: new Date().getTime(),
      data: {
        brandcode: this._brand,
        isnewdataformat: true,
        totalpageattentionspan: Math.round(this._totalPageAttentionSpanInMilliseconds / 1000),
        type: 'heartbeat',
        viewid: viewId,
        testmode: testmode,
        ...heartbeatData
      },
      version
    };
  }

  private handleBeforeUnload() {
    if (this._isActive) {
      this.trigger();
    }
  }

  private handleEvent() {
    this._lastActiveTime = new Date().getTime();
  }

  private handleVisibilityChange() {
    if (document.hidden) {
      this._isPageVisible = false;
    } else {
      this._isPageVisible = true;
    }
  }

  private resetCounters() {
    this._isPageVisible = !document.hidden;
    this._lastActiveTime = new Date().getTime();
    this._lastTotalPageAttentionSpan = 0;
    this._totalPageAttentionSpanInMilliseconds = 0;
  }
}

export default GlitrHeartbeat;
