import type { Brand } from '../glitr.types';

export interface GlitrHeartbeatConfig {
  heartbeatTimeout: number;
  lookAroundTimeout: number;
  url: string;
}

export interface GlitrHeartbeatEvent {
  clienttimestamp: number;
  data: GlitrHeartbeatData;
  version: string;
}

export interface GlitrHeartbeatData {
  brandcode: Brand;
  isnewdataformat: boolean;
  totalpageattentionspan: number;
  type: 'heartbeat';
  viewid: string;
  testmode: boolean;
}
