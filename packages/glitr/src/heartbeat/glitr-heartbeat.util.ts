export const hasPulse = (isPageVisible: boolean, lastActiveTime: number): boolean => {
  const currentTime = new Date().getTime();
  const seconds = 10;

  if (isPageVisible) {
    if (currentTime - lastActiveTime < seconds * 1000) {
      return true;
    }
  }

  return false;
};
