import type { GdprConsents } from '@subscriber/didomi';

import type { Brand, Environment } from './glitr.types';

import { isClientSide } from './glitr.util';
import { GlitrHeartbeat } from './heartbeat';
import { GlitrViewer } from './viewer';
import { GlitrEvent } from './event';

export const version = '1.0.251';

declare global {
  interface Window {
    gdprConsents?: GdprConsents;
  }
}

class GlitrTracker {
  private _brand: Brand;
  private _environment: Environment;
  private _heartbeatInstance: GlitrHeartbeat | null;
  private _viewerInstance: GlitrViewer;
  private _eventInstance: GlitrEvent;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._environment = environment;
    this._heartbeatInstance = isClientSide() ? new GlitrHeartbeat(brand, environment) : null;
    this._viewerInstance = new GlitrViewer(brand, environment);
    this._eventInstance = new GlitrEvent(brand, environment);
  }

  public registerPageView(pageUrl: string, accountId?: string, utmData?: object) {
    if (this._brand === 'az' && this._environment === 'production') {
      return;
    }

    if (!this._heartbeatInstance && isClientSide()) {
      this._heartbeatInstance = new GlitrHeartbeat(this._brand, this._environment);
    }

    this._heartbeatInstance?.start();
    this._viewerInstance.trigger(pageUrl, accountId, utmData);
  }

  public registerEvent(
    eventType: string,
    eventCategory: string,
    eventAction: string,
    eventLabel: string,
    accountId?: string,
    utmData?: object
  ) {
    if (this._brand === 'az' && this._environment === 'production') {
      return;
    }
    this._eventInstance.trigger(
      eventAction,
      eventCategory,
      eventLabel,
      eventType,
      accountId,
      utmData
    );
  }
}

export default GlitrTracker;
