import type { Brand } from '../glitr.types';

export interface GlitrViewerConfig {
  url: string;
}

export interface GlitrViewerEvent {
  clienttimestamp: number;
  data: GlitrViewerData;
  version: string;
}

export interface GlitrViewerData {
  accessloginshown: boolean;
  accesswalltype: 'none';
  application: 'www';
  brandcode: Brand;
  clienttimestamp: number;
  consentcookies: boolean;
  consentcreateadsprofile: boolean;
  consentcreatecontentprofile: boolean;
  consentimproveproducts: boolean;
  consentmarketresearch: boolean;
  consentmeasurecontentperformance: boolean;
  consentmeasureadperformance: boolean;
  consentselectpersonalizedads: boolean;
  consentselectpersonalizedcontent: boolean;
  consentselectbasicads: boolean;
  cookieid: string;
  dynamicFields: Record<string, unknown>;
  environment: string;
  identityaccountid: string | null;
  isnewdataformat: boolean;
  pagesecure: boolean;
  pagesecuretype: 'open';
  pagetype: 'subscriptionshop';
  pageurl?: string;
  platform: string | null;
  previouspagetypes: Array<any>;
  previousurl: string | null;
  referrer: string | null;
  sessionid: string;
  subscriptionstatus: string | null;
  subscriptiontype: string | null;
  testmode: boolean;
  type: 'pageview';
  viewid: string;
  viewsequence: number;

  utmartid?: string;
  utmcampaign?: string;
  utmcontent?: string;
  utminternal?: string;
  utmmedium?: string;
  utmsource?: string;
  utmterm?: string;
}
