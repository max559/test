import type { Brand, Environment } from '../glitr.types';
import type { GlitrViewerConfig, GlitrViewerData, GlitrViewerEvent } from './glitr-viewer.types';

import { version } from '../glitr';
import { getCookiesInfo } from '../glitr.util';
import { createGlitrViewerConfig } from './glitr-viewer.config';

class GlitrViewer {
  private _brand: Brand;
  private _config: GlitrViewerConfig;
  private _environment: Environment;
  private _previousUrl: string | null;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._config = createGlitrViewerConfig(brand, environment);
    this._environment = environment;
    this._previousUrl = null;
  }

  public trigger(pageUrl: string, accountId?: string, utmData?: object) {
    const hasAccountIdConsent =
      (window.gdprConsents?.cookies || false) &&
      (window.gdprConsents?.measure_ad_performance || false) &&
      (window.gdprConsents?.measure_content_performance || false);
    const viewerEvent = this.createViewerEvent({
      identityaccountid: hasAccountIdConsent ? accountId ?? null : 'no-consent',
      pageurl: pageUrl,
      ...utmData
    });

    navigator.sendBeacon(this._config.url, new URLSearchParams({
      ...viewerEvent,
      data: JSON.stringify(viewerEvent.data)
    } as unknown as URLSearchParams));

    this._previousUrl = window.location.href;
  }

  private createViewerEvent(viewerData: Partial<GlitrViewerData>): GlitrViewerEvent {
    const gdprConsents = window.gdprConsents;

    const { cookieId, sessionId, viewId, viewSequence, testmode } = getCookiesInfo('pageview');

    return {
      clienttimestamp: new Date().getTime(),
      data: {
        accessloginshown: false,
        accesswalltype: 'none',
        application: 'www',
        brandcode: this._brand,
        clienttimestamp: new Date().getTime(),
        consentcookies: gdprConsents?.cookies || false,
        consentcreateadsprofile: gdprConsents?.create_ads_profile || false,
        consentcreatecontentprofile: gdprConsents?.create_content_profile || false,
        consentimproveproducts: gdprConsents?.improve_products || false,
        consentmarketresearch: gdprConsents?.market_research || false,
        consentmeasureadperformance: gdprConsents?.measure_ad_performance || false,
        consentmeasurecontentperformance: gdprConsents?.measure_content_performance || false,
        consentselectpersonalizedads: gdprConsents?.select_personalized_ads || false,
        consentselectpersonalizedcontent: gdprConsents?.select_personalized_content || false,
        consentselectbasicads: gdprConsents?.select_basic_ads || false,
        cookieid: cookieId,
        dynamicFields:
          Object.assign({}, viewerData.utmartid && { utmartid: viewerData.utmartid }) || {},
        environment: this._environment,
        identityaccountid: '',
        isnewdataformat: true,
        pagesecure: false,
        pagesecuretype: 'open',
        pagetype: 'subscriptionshop',
        platform: null,
        previouspagetypes: [],
        previousurl:
          viewSequence === 1 ? document.referrer || this._previousUrl : this._previousUrl,
        referrer: document.referrer || this._previousUrl,
        sessionid: sessionId,
        subscriptionstatus: null, // Won't do, in the old aboshop it is implemented as user is logged in which is wrong. The actual subscription status is not something the aboshop requires (precondition validator contains subscription check)
        subscriptiontype: null, // Won't do, in the old aboshop it is hardcoded to NULL. The actual subscription type is not something the aboshop requires (precondition validator might know)
        testmode: testmode,
        type: 'pageview',
        viewid: viewId,
        viewsequence: viewSequence,
        ...viewerData
      },
      version
    };
  }
}

export default GlitrViewer;
