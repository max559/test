import type { Brand, Environment } from '../glitr.types';
import type { GlitrViewerConfig } from './glitr-viewer.types';

const urlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://preview-mhaa.mhtr.be/next/v',
    preview: 'https://preview-mhaa.mhtr.be/next/v',
    production: 'https://prod-mhaa.mhtr.be/next/v'
  },
  co: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  dl: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  ds: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  gva: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  hbvl: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  lt: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  lw: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  nb: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  },
  tc: {
    test: 'https://test.mhtr.be/v',
    preview: 'https://preview.mhtr.be/v',
    production: 'https://prod.mhtr.be/v'
  }
};

export const createGlitrViewerConfig = (
  brand: Brand,
  environment: Environment
): GlitrViewerConfig => {
  return {
    url: urlConfig[brand][environment]
  };
};
