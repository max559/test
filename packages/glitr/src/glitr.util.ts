import Cookies from 'js-cookie';

import { COOKIE_ID } from './glitr.types';

export const generateCookieUID = () => {
  let time = new Date().getTime();

  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, match => {
    const random = (time + Math.random() * 16) % 16 | 0;

    time = Math.floor(time / 16);

    return (match === 'x' ? random : (random & 0x3) | 0x8).toString(16);
  });
};

export const getCookiesInfo = (
  eventType: 'event' | 'heartbeat' | 'pageview'
): {
  cookieId: string;
  sessionId: string;
  viewId: string;
  viewSequence: number;
  testmode: boolean;
} => {
  let cookieIdValue = Cookies.get(COOKIE_ID.cookieId);
  let sessionIdValue = Cookies.get(COOKIE_ID.sessionId);
  let viewIdValue = Cookies.get(COOKIE_ID.viewId);
  let testmode = Cookies.get(COOKIE_ID.testmode) !== undefined;
  let viewSequenceValue = 1;

  if (!cookieIdValue) {
    const cookieUID = generateCookieUID();

    Cookies.set(COOKIE_ID.cookieId, cookieUID, { expires: 31 });
    cookieIdValue = cookieUID;
  }
  if (sessionIdValue) {
    const [sessionUID, viewSequence] = sessionIdValue.split('.');

    viewSequenceValue = Number(viewSequence) + 1;
    if (eventType === 'pageview') {
      Cookies.set(COOKIE_ID.sessionId, `${sessionUID}.${viewSequenceValue}`, {
        expires: 1 / 48
      }); // Expires in 30 minutes
    }
    sessionIdValue = sessionUID;
  } else {
    const sessionUID = generateCookieUID();

    Cookies.set(COOKIE_ID.sessionId, `${sessionUID}.1`, { expires: 1 / 48 }); // Expires in 30 minutes
    sessionIdValue = sessionUID;
  }
  if (eventType === 'pageview') {
    const viewUID = generateCookieUID();

    Cookies.set(COOKIE_ID.viewId, viewUID);
    viewIdValue = viewUID;
  }

  return {
    cookieId: cookieIdValue,
    sessionId: sessionIdValue || '',
    viewId: viewIdValue || '',
    viewSequence: viewSequenceValue,
    testmode: testmode,
  };
};

export const isClientSide = (): boolean => {
  return Boolean(typeof window !== 'undefined' && window.document);
};
