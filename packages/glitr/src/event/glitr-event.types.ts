import type { Brand } from '../glitr.types';

export interface GlitrEventConfig {
  url: string;
}

export interface GlitrBaseEvent {
  clienttimestamp: number;
  data: GlitrBaseEventData;
  version: string;
}

export interface GlitrBaseEventData {
  accessloginshown: boolean;
  accesswalltype: 'none';
  application: 'www';
  brandcode: Brand;
  clienttimestamp: number;
  consentcookies: boolean;
  consentcreateadsprofile: boolean;
  consentcreatecontentprofile: boolean;
  consentimproveproducts: boolean;
  consentmarketresearch: boolean;
  consentmeasurecontentperformance: boolean;
  consentmeasureadperformance: boolean;
  consentselectpersonalizedads: boolean;
  consentselectpersonalizedcontent: boolean;
  consentselectbasicads: boolean;
  cookieid: string;
  dynamicFields: Record<string, unknown>;
  eventaction: string;
  eventcategory: string;
  eventlabel: string;
  eventtype: string;
  environment: string;
  identityaccountid: string | null;
  isnewdataformat: boolean;
  pagesecure: boolean;
  pagesecuretype: 'open';
  pagetype: 'subscriptionshop';
  platform: string | null;
  previouspagetypes: Array<any>;
  sessionid: string;
  subscriptiontype: string | null;
  testmode: boolean;
  viewid: string;
  type: 'event';

  utmartid?: string;
  utmcampaign?: string;
  utmcontent?: string;
  utminternal?: string;
  utmmedium?: string;
  utmsource?: string;
  utmterm?: string;
}
