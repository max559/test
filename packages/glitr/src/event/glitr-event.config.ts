import type { Brand, Environment } from '../glitr.types';
import type { GlitrEventConfig } from './glitr-event.types';

const urlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://preview-mhaa.mhtr.be/next/e',
    preview: 'https://preview-mhaa.mhtr.be/next/e',
    production: 'https://prod-mhaa.mhtr.be/next/e'
  },
  co: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  dl: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  ds: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  gva: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  hbvl: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  lt: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  lw: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  nb: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  },
  tc: {
    test: 'https://test.mhtr.be/e',
    preview: 'https://preview.mhtr.be/e',
    production: 'https://prod.mhtr.be/e'
  }
};

export const createGlitrEventConfig = (
  brand: Brand,
  environment: Environment
): GlitrEventConfig => {
  return {
    url: urlConfig[brand][environment]
  };
};
