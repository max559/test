import type { Brand, Environment } from '../glitr.types';
import type { GlitrBaseEvent, GlitrBaseEventData, GlitrEventConfig } from './glitr-event.types';

import { version } from '../glitr';
import { getCookiesInfo } from '../glitr.util';
import { createGlitrEventConfig } from './glitr-event.config';

class GlitrEvent {
  private _brand: Brand;
  private _config: GlitrEventConfig;
  private _environment: Environment;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._config = createGlitrEventConfig(brand, environment);
    this._environment = environment;
  }

  public trigger(
    eventAction: string,
    eventCategory: string,
    eventLabel: string,
    eventType: string,
    accountId?: string,
    utmData?: object
  ) {

    const hasAccountIdConsent = (window.gdprConsents?.cookies || false) && (window.gdprConsents?.measure_ad_performance || false) && (window.gdprConsents?.measure_content_performance || false);

    const event = this.createEvent({
      eventaction: eventAction,
      eventcategory: eventCategory,
      eventlabel: eventLabel,
      eventtype: eventType,
      identityaccountid: hasAccountIdConsent ? (accountId ?? null) : 'no-consent',
      ...utmData
    });

    navigator.sendBeacon(this._config.url, new URLSearchParams({
      ...event,
      data: JSON.stringify(event.data)
    } as unknown as URLSearchParams));
  }

  private createEvent(eventData: Partial<GlitrBaseEventData>): GlitrBaseEvent {
    const gdprConsents = typeof window !== 'undefined' ? window.gdprConsents : undefined;

    const { cookieId, sessionId, viewId, testmode } = getCookiesInfo('event');

    return {
      clienttimestamp: new Date().getTime(),
      data: {
        accessloginshown: false,
        accesswalltype: 'none',
        application: 'www',
        brandcode: this._brand,
        clienttimestamp: new Date().getTime(),
        consentcookies: gdprConsents?.cookies || false,
        consentcreateadsprofile: gdprConsents?.create_ads_profile || false,
        consentcreatecontentprofile: gdprConsents?.create_content_profile || false,
        consentimproveproducts: gdprConsents?.improve_products || false,
        consentmarketresearch: gdprConsents?.market_research || false,
        consentmeasureadperformance: gdprConsents?.measure_ad_performance || false,
        consentmeasurecontentperformance: gdprConsents?.measure_content_performance || false,
        consentselectpersonalizedads: gdprConsents?.select_personalized_ads || false,
        consentselectpersonalizedcontent: gdprConsents?.select_personalized_content || false,
        consentselectbasicads: gdprConsents?.select_basic_ads || false,
        cookieid: cookieId,
        dynamicFields:
          Object.assign({}, eventData.utmartid && { utmartid: eventData.utmartid }) || {},
        eventaction: '',
        eventcategory: '',
        eventlabel: '',
        eventtype: '',
        environment: this._environment,
        identityaccountid: '',
        isnewdataformat: true,
        pagesecure: false,
        pagesecuretype: 'open',
        pagetype: 'subscriptionshop',
        platform: null,
        previouspagetypes: [],
        sessionid: sessionId,
        subscriptiontype: null, // Won't do, in the old aboshop it is hardcoded to NULL. The actual subscription type is not something the aboshop requires (precondition validator might know)
        testmode: testmode,
        type: 'event',
        viewid: viewId,
        ...eventData
      },
      version
    };
  }
}

export default GlitrEvent;
