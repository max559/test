export type Brand = 'az' | 'co' | 'dl' | 'ds' | 'gva' | 'hbvl' | 'lt' | 'lw' | 'nb' | 'tc';
export type Environment = 'test' | 'preview' | 'production';

export enum COOKIE_ID {
  cookieId = '_mhtc_cId',
  sessionId = '_mhtc_sId',
  viewId = '_mhtc_vId',
  testmode = '_mhtc_tm'
}
