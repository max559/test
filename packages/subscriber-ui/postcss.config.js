/* eslint-disable turbo/no-undeclared-env-vars */

module.exports = {
  plugins: {
    '@mediahuis/chameleon-postcss-plugin': {
      theme: process.env.MH_BRAND
    },
    autoprefixer: {}
  }
};
