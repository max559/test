import type { PropsWithChildren, ReactElement } from 'react';

import SidebarAside from './SidebarAside';
import SidebarMain from './SidebarMain';

export interface SidebarProps {
  gap?: string | number;
  reverse?: boolean;
}

const Sidebar = ({ children, reverse, gap }: PropsWithChildren<SidebarProps>): ReactElement => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: reverse ? 'row-reverse' : 'row',
        flexWrap: 'wrap',
        gap
      }}
    >
      {children}
    </div>
  );
};

Sidebar.Aside = SidebarAside;
Sidebar.Main = SidebarMain;

export default Sidebar;
