import type { PropsWithChildren, ReactElement } from 'react';

export interface SidebarMainProps {
  minWidthPercentage?: string;
}

const SidebarMain = ({
  children,
  minWidthPercentage = '50%'
}: PropsWithChildren<SidebarMainProps>): ReactElement => {
  return (
    <div style={{ minInlineSize: minWidthPercentage, flexBasis: '0', flexGrow: '999' }}>
      {children}
    </div>
  );
};

export default SidebarMain;
