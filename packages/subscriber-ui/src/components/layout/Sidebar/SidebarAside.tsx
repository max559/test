import type { PropsWithChildren, ReactElement } from 'react';

export interface SidebarAsideProps {
  width?: string;
}

const SidebarAside = ({ children, width }: PropsWithChildren<SidebarAsideProps>): ReactElement => {
  return <div style={{ flexBasis: width, flexGrow: 1 }}>{children}</div>;
};

export default SidebarAside;
