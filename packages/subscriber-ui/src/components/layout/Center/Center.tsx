import type { PropsWithChildren, ReactElement } from 'react';

export interface CenterProps extends React.HTMLAttributes<HTMLDivElement> {
  maxInlineSize: string;
}

export const Center = ({
  children,
  maxInlineSize,
  ...props
}: PropsWithChildren<CenterProps>): ReactElement => (
  <div
    {...props}
    style={{ ...props.style, boxSizing: 'content-box', marginInline: 'auto', maxInlineSize }}
  >
    {children}
  </div>
);

export default Center;
