import type { SPACING_TYPE } from '@subscriber/globals';
import type { PropsWithChildren, ReactElement } from 'react';

import { SPACING } from '@subscriber/globals';

export interface FlexProps
  extends Pick<
    React.CSSProperties,
    | 'alignContent'
    | 'alignItems'
    | 'alignSelf'
    | 'flexBasis'
    | 'flexDirection'
    | 'flexGrow'
    | 'flexShrink'
    | 'flexWrap'
    | 'justifyContent'
    | 'justifySelf'
  > {
  className?: string;
  gap?: SPACING_TYPE;
  style?: React.CSSProperties;
  onClick?: (e: any) => any;
}

const Flex = ({
  alignContent,
  alignItems,
  alignSelf,
  className,
  flexBasis,
  flexDirection,
  flexGrow,
  flexShrink,
  flexWrap,
  gap,
  justifyContent,
  justifySelf,
  children,
  style
}: PropsWithChildren<FlexProps>): ReactElement => {
  return (
    <div
      className={className}
      style={{
        alignContent,
        alignItems,
        alignSelf,
        display: 'flex',
        flexBasis,
        flexDirection,
        flexGrow,
        flexShrink,
        flexWrap,
        gap: gap && SPACING[gap],
        justifyContent,
        justifySelf,
        ...style
      }}
    >
      {children}
    </div>
  );
};

export default Flex;
