import type { ReactElement } from 'react';

import { Logo } from '@mediahuis/chameleon-react';
import { SPACING, SPACING_TYPE } from '@subscriber/globals';
import { Fragment } from 'react';

import { Flex, FlexProps } from '../../../components/layout';

export interface MultibrandLogosProps extends FlexProps {
  brands: Array<'ds' | 'gva' | 'hbvl' | 'nb' | 'dg'>;
  gap?: SPACING_TYPE;
  iconHeight: string;
  iconWidth: string;
  logoHeight: string;
  logoWidth: string;
}

const MultibrandLogos = ({
  brands,
  gap = 1,
  iconHeight,
  iconWidth,
  logoHeight,
  logoWidth,
  ...props
}: MultibrandLogosProps): ReactElement => {
  return (
    <Flex
      {...props}
      alignItems="center"
      style={{ gap: SPACING[gap], minWidth: 'fit-content', ...props.style }}
    >
      {brands.map((brand: string, brandIndex: number) => {
        if (brandIndex === brands.length - 1) {
          return (
            <Logo
              key={brand}
              name={`subscriptions.plus-brand-square-${brand}`}
              style={{ height: logoHeight, width: logoWidth }}
            />
          );
        }

        return (
          <Fragment key={brand}>
            <Logo
              name={`subscriptions.plus-brand-square-${brand}`}
              style={{ height: logoHeight, width: logoWidth }}
            />
            <Logo name="subscriptions.plus-icon" style={{ height: iconHeight, width: iconWidth }} />
          </Fragment>
        );
      })}
    </Flex>
  );
};

export default MultibrandLogos;
