import type { ReactElement } from 'react';

import { Logo, Paragraph } from '@mediahuis/chameleon-react';
import { BORDER_RADIUS, SPACING } from '@subscriber/globals';

import { Center, Flex } from '../../layout';
import { MultibrandLogos } from '../MultibrandLogos';

const multibrandBlue = '#000C7E';
const sortedBrands: Array<'ds' | 'gva' | 'hbvl' | 'nb' | 'dg'> = ['nb', 'ds', 'gva', 'hbvl', 'dg'];

export interface MultibrandBannerProps extends React.HTMLAttributes<HTMLDivElement> {
  activeBrand: 'ds' | 'gva' | 'hbvl' | 'nb';
  text: string;
}

const MultibrandBanner = ({ activeBrand, text, ...props }: MultibrandBannerProps): ReactElement => {
  return (
    <Flex
      {...props}
      flexDirection="column"
      style={{
        backgroundColor: 'var(--color-neutral-10)',
        borderBottomRightRadius: BORDER_RADIUS.lg,
        borderTopRightRadius: BORDER_RADIUS.lg,
        marginBottom: SPACING[6],
        marginRight: SPACING[6],
        paddingBottom: SPACING[10],
        paddingLeft: '10%',
        paddingRight: '10%',
        paddingTop: SPACING[4],
        position: 'relative',
        ...props.style
      }}
    >
      <Center
        maxInlineSize={SPACING[80]}
        style={{ alignItems: 'center', display: 'flex', gap: SPACING[4] }}
      >
        <Logo name="subscriptions.plus-icon" style={{ height: SPACING[4], width: SPACING[4] }} />
        <Paragraph color={multibrandBlue} weight="strong" style={{ textAlign: 'left' }}>
          {text}
        </Paragraph>
      </Center>

      <MultibrandLogos
        alignItems="center"
        alignSelf="center"
        brands={sortedBrands.filter((brand: string) => brand !== activeBrand)}
        gap={1}
        iconHeight={SPACING[2]}
        iconWidth={SPACING[2]}
        logoHeight={SPACING[10]}
        logoWidth={SPACING[10]}
        style={{ bottom: '-20px', position: 'absolute' }}
      />
    </Flex>
  );
};

export default MultibrandBanner;
