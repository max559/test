import type { ReactElement } from 'react';
import type { FlexProps } from 'src/components/layout';

import { Caption, Logo } from '@mediahuis/chameleon-react';
import { SPACING } from '@subscriber/globals';

import { Flex } from '../../layout';
import { MultibrandLogos } from '../MultibrandLogos';

const multibrandBlue = '#000C7E';
const defaultBlack = '#000';
const sortedBrands: Array<'ds' | 'gva' | 'hbvl' | 'nb' | 'dg'> = ['nb', 'ds', 'gva', 'hbvl', 'dg'];

export interface MultibrandBannerInlineProps extends FlexProps {
  activeBrand: 'ds' | 'gva' | 'hbvl' | 'nb' | 'dl';
  text: string;
}

const MultibrandBannerInline = ({
  activeBrand,
  text,
  ...props
}: MultibrandBannerInlineProps): ReactElement => {
  return (
    <Flex
      {...props}
      alignItems="center"
      justifyContent="space-between"
      style={{
        backgroundColor: 'var(--color-neutral-10)',
        borderRadius: 'inherit',
        color: activeBrand === 'dl' ? defaultBlack : multibrandBlue,
        padding: `${SPACING[4]} ${SPACING[2]}`,
        ...props.style
      }}
    >
      <Flex alignItems="center" style={{ gap: SPACING[1] }}>
        <Logo name="subscriptions.plus-icon" style={{ height: SPACING[4], width: SPACING[4] }} />
        <Caption size="sm" weight="strong">
          {text}
        </Caption>
      </Flex>

      <MultibrandLogos
        brands={sortedBrands.filter(
          (brand: 'ds' | 'gva' | 'hbvl' | 'nb' | 'dg') => brand !== activeBrand
        )}
        iconHeight={SPACING[2]}
        iconWidth={SPACING[2]}
        logoHeight={SPACING[8]}
        logoWidth={SPACING[8]}
      />
    </Flex>
  );
};

export default MultibrandBannerInline;
