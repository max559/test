import type { DetailedHTMLProps, ImgHTMLAttributes, ReactElement } from 'react';

import type { OverrideableConfig } from '../../../Overrideable';

import { Heading, Paragraph } from '@mediahuis/chameleon-react';

import { Overrideable } from '../../../Overrideable';

export interface DefaultCardHeaderProps {
  description: OverrideableConfig<string>;
  imageProps?: DetailedHTMLProps<ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;
  title: OverrideableConfig<string>;
}

export const DefaultCardHeader = ({
  description,
  imageProps,
  title
}: DefaultCardHeaderProps): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
      <Overrideable
        config={title}
        defaultWrapper={({ children }) => (
          <Heading
            level={3}
            size="display"
            style={{
              fontSize: '1.5rem',
              textAlign: 'center',
              textTransform: 'capitalize',
              whiteSpace: 'pre-wrap'
            }}
          >
            {children}
          </Heading>
        )}
      />

      <Overrideable
        config={description}
        defaultWrapper={({ children }) => (
          <Paragraph
            style={{
              textAlign: 'center',
              whiteSpace: 'pre-wrap'
            }}
          >
            {children}
          </Paragraph>
        )}
      />

      {imageProps && (
        <div style={{ height: '100px' }}>
          <img
            {...imageProps}
            alt={imageProps.alt || title.value}
            height="100%"
            style={{
              objectFit: 'contain'
            }}
            width="100%"
          />
        </div>
      )}
    </div>
  );
};

export default DefaultCardHeader;
