import type { ReactElement } from 'react';

import type { OverrideableConfig } from '../../../Overrideable';

import { Button, Caption, Paragraph } from '@mediahuis/chameleon-react';
import { ChevronForward } from '@mediahuis/chameleon-theme-wl/icons';

import { Overrideable } from '../../../Overrideable';

export interface DefaultCardFooterProps {
  buttonCaption?: OverrideableConfig<string>;
  buttonLoading?: boolean;
  buttonText: string;
  priceCaption?: OverrideableConfig<string>;
  priceText: OverrideableConfig<string>;
  onButtonClick: () => void;
}

export const DefaultCardFooter = ({
  buttonCaption,
  buttonLoading = false,
  buttonText,
  priceCaption,
  priceText,
  onButtonClick
}: DefaultCardFooterProps): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '0.75rem' }}>
      {priceCaption && (
        <Overrideable
          config={priceCaption}
          defaultWrapper={({ children }) => (
            <Caption style={{ textAlign: 'center' }}>{children}</Caption>
          )}
        />
      )}

      <Overrideable
        config={priceText}
        defaultWrapper={({ children }) => (
          <Paragraph
            style={{
              textAlign: 'center'
            }}
          >
            {children}
          </Paragraph>
        )}
      />

      <div style={{ display: 'flex', flexDirection: 'column', gap: '0.25rem' }}>
        <Button
          appearance="primary"
          iconRight={ChevronForward}
          loading={buttonLoading}
          width="full"
          onClick={event => {
            event.stopPropagation();
            onButtonClick();
          }}
        >
          {buttonText}
        </Button>

        {buttonCaption && (
          <Overrideable
            config={buttonCaption}
            defaultWrapper={({ children }) => (
              <Caption color="var(--color-neutral-60)" style={{ textAlign: 'center' }}>
                {children}
              </Caption>
            )}
          />
        )}
      </div>
    </div>
  );
};

export default DefaultCardFooter;
