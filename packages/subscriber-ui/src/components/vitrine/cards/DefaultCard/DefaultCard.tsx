import type { ReactElement } from 'react';

import type { FeaturesListItem } from '../../../FeaturesList';
import type { MultibrandBannerProps } from '../../../multibrand';
import type { DefaultCardFooterProps } from './DefaultCardFooter';
import type { DefaultCardHeaderProps } from './DefaultCardHeader';

import { FeaturesList } from '../../../FeaturesList';
import { DefaultTemplate } from '../../templates';

import { DefaultCardFooter } from './DefaultCardFooter';
import { DefaultCardHeader } from './DefaultCardHeader';

type WithSlotsProps<T> = T & DefaultCardFooterProps & DefaultCardHeaderProps;

interface DefaultCardProps {
  featuresListItems: Array<FeaturesListItem>;
  highlight?: boolean;
  label?: string;
  loading?: boolean;
  multibrandProps?: MultibrandBannerProps;
  testId?: string;
  onPaperClick: () => void;
}

const DefaultCard = ({
  buttonCaption,
  buttonText,
  description,
  featuresListItems,
  highlight = false,
  imageProps,
  label,
  loading = false,
  multibrandProps,
  priceCaption,
  priceText,
  testId,
  title,
  onButtonClick,
  onPaperClick
}: WithSlotsProps<DefaultCardProps>): ReactElement => {
  return (
    <DefaultTemplate
      footer={
        <DefaultCardFooter
          buttonCaption={buttonCaption}
          buttonLoading={loading}
          buttonText={buttonText}
          priceCaption={priceCaption}
          priceText={priceText}
          onButtonClick={onButtonClick}
        />
      }
      header={<DefaultCardHeader description={description} imageProps={imageProps} title={title} />}
      highlight={highlight}
      label={label}
      main={<FeaturesList gap={4} hideUnavailable items={featuresListItems} />}
      multibrandProps={multibrandProps}
      testId={testId}
      onPaperClick={onPaperClick}
    />
  );
};

export default DefaultCard;
