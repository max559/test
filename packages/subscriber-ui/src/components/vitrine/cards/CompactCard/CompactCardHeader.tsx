import type { PropsWithChildren, ReactElement } from 'react';

import { Heading } from '@mediahuis/chameleon-react';

const CompactCardHeader = ({ children }: PropsWithChildren<Record<never, never>>): ReactElement => {
  return (
    <Heading
      level={4}
      size="md"
      style={{
        textTransform: 'capitalize'
      }}
    >
      {children}
    </Heading>
  );
};

export default CompactCardHeader;
