import type { ReactElement } from 'react';

import type { FeaturesListItem } from '../../../FeaturesList';
import type { OverrideableConfig } from '../../../Overrideable';
import type { MultibrandBannerInlineProps } from '../../../multibrand';

import { Paragraph } from '@mediahuis/chameleon-react';

import { FeaturesList } from '../../../FeaturesList';
import { Overrideable } from '../../../Overrideable';
import { CompactMultibrandTemplate } from '../../templates/CompactMultibrandTemplate';
import { CompactTemplate } from '../../templates/CompactTemplate';

import CompactCardHeader from './CompactCardHeader';

interface CompactCardProps {
  featuresListItems: Array<FeaturesListItem>;
  highlight?: boolean;
  label?: string;
  multibrandProps?: MultibrandBannerInlineProps;
  price: OverrideableConfig<string>;
  testId?: string;
  title: OverrideableConfig<string>;
  onPaperClick: () => void;
}

const CompactCard = ({
  featuresListItems,
  highlight,
  label,
  multibrandProps,
  price,
  testId,
  title,
  onPaperClick
}: CompactCardProps): ReactElement => {
  const templateAside = <Overrideable config={price} defaultWrapper={Paragraph} />;
  const templateHeader = <Overrideable config={title} defaultWrapper={CompactCardHeader} />;
  const templateMain = <FeaturesList items={featuresListItems} />;

  if (multibrandProps) {
    return (
      <CompactMultibrandTemplate
        aside={templateAside}
        header={templateHeader}
        highlight={highlight}
        label={label}
        main={templateMain}
        multibrandProps={multibrandProps}
        testId={testId}
        onPaperClick={onPaperClick}
      />
    );
  }

  return (
    <CompactTemplate
      aside={templateAside}
      header={templateHeader}
      highlight={highlight}
      label={label}
      main={templateMain}
      testId={testId}
      onPaperClick={onPaperClick}
    />
  );
};

export default CompactCard;
