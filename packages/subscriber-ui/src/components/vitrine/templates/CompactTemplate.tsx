import type { ReactElement, ReactNode } from 'react';

import { Caption, Icon, Paper } from '@mediahuis/chameleon-react';
import { ChevronForward } from '@mediahuis/chameleon-theme-wl/icons';
import { BORDER_RADIUS, SPACING } from '@subscriber/globals';

export interface CompactTemplateProps {
  aside: ReactNode;
  header: ReactNode;
  highlight?: boolean;
  label?: string;
  main: ReactNode;
  testId?: string;
  onPaperClick: () => void;
}

export const CompactTemplate = ({
  aside,
  header,
  highlight = false,
  label,
  main,
  testId,
  onPaperClick
}: CompactTemplateProps): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div
        style={{
          backgroundColor: 'var(--semantic-background-brand-static-default-fill)',
          borderTopLeftRadius: BORDER_RADIUS.default,
          borderTopRightRadius: BORDER_RADIUS.default,
          display: label ? 'block' : 'none',
          height: SPACING[6],
          margin: '0 15px',
          maxWidth: '90%',
          padding: `${SPACING[1]} ${SPACING[8]}`,
          visibility: label ? 'visible' : 'hidden',
          width: 'fit-content'
        }}
      >
        {label && (
          <Caption
            color="var(--semantic-foreground-on-brand-static-default-fill)"
            size="sm"
            style={{ textAlign: 'center' }}
            weight="strong"
          >
            {label}
          </Caption>
        )}
      </div>

      <Paper
        data-testid={testId}
        elevation={2}
        hoverable
        style={{
          border: highlight
            ? '2px solid var(--semantic-background-brand-static-default-fill)'
            : '2px solid transparent',
          borderRadius: BORDER_RADIUS.default,
          display: 'grid',
          gridTemplateColumns: '3fr 1fr 30px',
          gridTemplateRows: 'min-content min-content'
        }}
        onClick={onPaperClick}
      >
        <div
          style={{
            gridColumnStart: 1,
            gridColumnEnd: 1,
            gridRowStart: 1,
            gridRowEnd: 1,
            padding: '1rem 1rem 0.75rem 1rem'
          }}
        >
          {header}
        </div>

        <div
          style={{
            gridColumnStart: 1,
            gridColumnEnd: 1,
            gridRowStart: 2,
            gridRowEnd: 2,
            padding: '0.75rem 1rem 1rem 1rem'
          }}
        >
          {main}
        </div>

        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            gridColumnStart: 2,
            gridColumnEnd: 2,
            gridRowStart: 1,
            gridRowEnd: 3,
            padding: '0 0.5rem',
            justifyContent: 'center'
          }}
        >
          {aside}
        </div>

        <div
          style={{
            alignItems: 'center',
            backgroundColor: 'var(--semantic-background-brand-static-default-fill)',
            borderBottomRightRadius: 'inherit',
            borderTopRightRadius: 'inherit',
            color: 'var(--semantic-foreground-on-brand-static-default-fill)',
            display: 'flex',
            flexDirection: 'column',
            gridColumnStart: 3,
            gridColumnEnd: 3,
            gridRowStart: 1,
            gridRowEnd: 3,
            justifyContent: 'center',
            margin: '-2px'
          }}
        >
          <Icon as={ChevronForward} />
        </div>
      </Paper>
    </div>
  );
};

export default CompactTemplate;
