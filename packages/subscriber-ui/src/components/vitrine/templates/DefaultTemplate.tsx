import type { ReactElement, ReactNode } from 'react';

import type { MultibrandBannerProps } from '../../multibrand';

import { Caption, Divider, Paper } from '@mediahuis/chameleon-react';
import { BORDER_RADIUS, SPACING } from '@subscriber/globals';

import { MultibrandBanner } from '../../multibrand';

export interface DefaultTemplateProps {
  footer: ReactNode;
  header: ReactNode;
  highlight?: boolean;
  label?: string;
  main: ReactNode;
  multibrandProps?: MultibrandBannerProps;
  testId?: string;
  onPaperClick: () => void;
}

export const DefaultTemplate = ({
  footer,
  header,
  highlight,
  label,
  main,
  multibrandProps,
  testId,
  onPaperClick
}: DefaultTemplateProps): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div
        style={{
          backgroundColor: 'var(--semantic-background-brand-static-default-fill)',
          borderTopLeftRadius: BORDER_RADIUS.default,
          borderTopRightRadius: BORDER_RADIUS.default,
          height: SPACING[6],
          margin: '0 auto',
          maxWidth: '85%',
          padding: `${SPACING[1]} ${SPACING[8]}`,
          visibility: label ? 'visible' : 'hidden',
          width: 'fit-content'
        }}
      >
        {label && (
          <Caption
            color="var(--semantic-foreground-on-brand-static-default-fill)"
            size="sm"
            style={{ textAlign: 'center' }}
            weight="strong"
          >
            {label}
          </Caption>
        )}
      </div>

      <Paper
        data-testid={testId}
        elevation={2}
        hoverable
        style={{
          border: highlight
            ? '2px solid var(--semantic-background-brand-static-default-fill)'
            : '2px solid transparent',
          borderRadius: BORDER_RADIUS.default,
          display: 'grid',
          gap: SPACING[4],
          margin: '-2px',
          paddingBottom: '5%',
          paddingTop: '5%'
        }}
        onClick={onPaperClick}
      >
        <div style={{ justifySelf: 'center', width: '85%' }}>{header}</div>

        <Divider style={{ justifySelf: 'center', width: '85%' }} />

        <div style={{ justifySelf: 'center', width: '85%' }}>{main}</div>

        {multibrandProps && <MultibrandBanner {...multibrandProps} />}

        <Divider style={{ justifySelf: 'center', width: '85%' }} />

        <div style={{ justifySelf: 'center', width: '85%' }}>{footer}</div>
      </Paper>
    </div>
  );
};

export default DefaultTemplate;
