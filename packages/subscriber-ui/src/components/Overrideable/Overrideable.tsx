/* eslint-disable @typescript-eslint/no-explicit-any */

import type { PropsWithChildren, ReactElement } from 'react';

import type { OverrideableConfig } from './Overrideable.types';

interface OverrideableProps {
  config: OverrideableConfig<any>;
  defaultWrapper: ({ children }: PropsWithChildren<object>) => ReactElement;
}

const Overrideable = ({
  config,
  defaultWrapper: DefaultWrapper
}: OverrideableProps): ReactElement => {
  if (config.wrapper) {
    const ConfigWrapper = config.wrapper;

    if (config.render) {
      return <ConfigWrapper>{config.render(config.value)}</ConfigWrapper>;
    } else {
      return <ConfigWrapper>{config.value}</ConfigWrapper>;
    }
  }

  if (config.render) {
    if (config.wrapper === null) {
      return config.render(config.value);
    } else {
      return <DefaultWrapper>{config.render(config.value)}</DefaultWrapper>;
    }
  }

  return <DefaultWrapper>{config.value}</DefaultWrapper>;
};

export default Overrideable;
