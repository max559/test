import type { PropsWithChildren, ReactElement } from 'react';

export interface OverrideableConfig<T> {
  render?: (value: T) => ReactElement;
  value: T;
  wrapper?: ({ children }: PropsWithChildren<object>) => ReactElement | null;
}
