import type { ReactElement } from 'react';

import { Flex } from '../layout';

export interface PopoverBackdropProps {
  closeOnClick?: boolean;
  onClick?: () => void | undefined;
}

export const PopoverBackdrop = ({
  closeOnClick = true,
  onClick
}: PopoverBackdropProps): ReactElement => {
  return (
    <Flex
      alignItems="flex-end"
      justifyContent="center"
      style={{
        background: 'rgba(0,0,0,0.75)',
        height: '100vh',
        left: 0,
        position: 'fixed',
        top: 0,
        width: '100vw',
        zIndex: 1250
      }}
      onClick={closeOnClick ? onClick : undefined}
    ></Flex>
  );
};
