import type { PropsWithChildren, ReactElement } from 'react';

import { IconButton, useMediaQuery } from '@mediahuis/chameleon-react';
import { Close } from '@mediahuis/chameleon-theme-wl/icons';
import { SPACING } from '@subscriber/globals';
import { createPortal } from 'react-dom';

import { Flex } from '../layout';
import { PopoverBackdrop } from './PopoverBackdrop';

export interface PopoverProps {
  closeOnOutsideClick?: boolean;
  hideBackdrop?: boolean;
  show?: boolean;
  showClose?: boolean;
  onClose: () => void;
}

export const Popover = ({
  children,
  closeOnOutsideClick = false,
  hideBackdrop = false,
  show = false,
  showClose = true,
  onClose
}: PropsWithChildren<PopoverProps>): ReactElement | null => {
  const popoverStyle = useMediaQuery({
    xs: {
      background: '#fff',
      borderTop: '1px solid #e0dad1',
      bottom: 0,
      boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
      maxHeight: '80%',
      position: 'fixed',
      width: '100%',
      zIndex: 1251
    }
  });

  return show
    ? createPortal(
        <>
          {!hideBackdrop && (
            <PopoverBackdrop closeOnClick={closeOnOutsideClick} onClick={onClose} />
          )}
          <Flex flexDirection="column" style={{ ...popoverStyle }}>
            {showClose && (
              <div style={{ position: 'absolute', right: SPACING[2], top: SPACING[2] }}>
                <IconButton circular icon={Close} size="sm" onClick={onClose} />
              </div>
            )}
            <div style={{ overflowX: 'hidden', overflowY: 'auto' }}>{children}</div>
          </Flex>
        </>,
        document.body
      )
    : null;
};
