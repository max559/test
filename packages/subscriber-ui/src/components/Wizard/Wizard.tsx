import type { ComponentType, PropsWithChildren, ReactElement } from 'react';

import type { WizardStep, WizardStepRenderParams } from './Wizard.types';

import { useState } from 'react';

interface WizardProps<T> {
  RootElement?: ComponentType<PropsWithChildren<Partial<WizardStepRenderParams<T>>>>;
  startId: string;
  steps: Array<WizardStep<T>>;
  onComplete: (data: T) => void;
}

const Wizard = <T,>({
  RootElement = ({ children }) => <>{children}</>,
  startId,
  steps,
  onComplete
}: WizardProps<T>): ReactElement => {
  const [wizardStack, setWizardStack] = useState<Array<string>>(
    steps.reduce((acc: Array<string>, step) => {
      if (step.id === startId) {
        return acc.concat(step.id);
      }

      return acc;
    }, [])
  );

  function goToNextStep(id: string) {
    const nextStepId = steps.find(step => step.id === id)?.id;

    if (nextStepId) {
      setWizardStack(prevWizardStack => [...prevWizardStack, nextStepId]);
    }
  }

  function goToPreviousStep() {
    if (wizardStack.length > 0) {
      setWizardStack(prevWizardStack => prevWizardStack.slice(0, -1));
    }
  }

  const currentStep = steps.find(step => step.id === wizardStack.at(-1));

  return (
    <RootElement>
      {currentStep?.render({
        completeWizard: data => onComplete(data),
        goToNextStep,
        goToPreviousStep
      })}
    </RootElement>
  );
};

export default Wizard;
