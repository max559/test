import type { ReactElement } from 'react';

export interface WizardStep<T> {
  id: string;
  render: (params: WizardStepRenderParams<T>) => ReactElement;
}

export interface WizardStepRenderParams<T> {
  completeWizard: (data: T) => void;
  goToNextStep: (id: string) => void;
  goToPreviousStep: () => void;
}
