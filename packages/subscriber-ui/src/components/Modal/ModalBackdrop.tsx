import type { PropsWithChildren, ReactElement } from 'react';

import { Flex } from '../layout';

export interface ModalBackdropProps {
  closeOnClick?: boolean;
  onClick?: () => void | undefined;
}

export const ModalBackdrop = ({
  children,
  closeOnClick = true,
  onClick
}: PropsWithChildren<ModalBackdropProps>): ReactElement => {
  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      style={{
        background: 'rgba(0,0,0,0.75)',
        height: '100vh',
        left: 0,
        position: 'fixed',
        top: 0,
        width: '100vw',
        zIndex: 1250
      }}
      onClick={closeOnClick ? onClick : undefined}
    >
      {children}
    </Flex>
  );
};
