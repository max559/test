import type { PropsWithChildren, ReactElement } from 'react';

import { IconButton, useMediaQuery } from '@mediahuis/chameleon-react';
import { Close } from '@mediahuis/chameleon-theme-wl/icons';
import { BORDER_RADIUS, SPACING } from '@subscriber/globals';
import { createPortal } from 'react-dom';

import { Flex } from '../layout';
import { ModalBackdrop } from './ModalBackdrop';

export interface ModalProps {
  closeOnOutsideClick?: boolean;
  show?: boolean;
  showClose?: boolean;
  onClose: () => void;
}

export const Modal = ({
  children,
  closeOnOutsideClick = false,
  show = false,
  showClose = true,
  onClose
}: PropsWithChildren<ModalProps>): ReactElement | null => {
  const modalStyle = useMediaQuery({
    xs: { background: '#fff', height: '100%', width: '100%', zIndex: 1251 },
    md: {
      background: '#fff',
      boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
      borderRadius: BORDER_RADIUS.default,
      maxHeight: '95%',
      zIndex: 1251
    }
  });

  return show
    ? createPortal(
        <ModalBackdrop closeOnClick={closeOnOutsideClick} onClick={onClose}>
          <Flex
            gap={0}
            style={{ ...modalStyle }}
            flexDirection="column"
            onClick={e => e.stopPropagation()}
          >
            <Flex
              alignItems="center"
              flexDirection="row"
              justifyContent="flex-end"
              style={{
                backgroundColor: '#f9f9f9',
                borderTopLeftRadius: BORDER_RADIUS.default,
                borderTopRightRadius: BORDER_RADIUS.default,
                height: SPACING[12],
                padding: SPACING[2]
              }}
            >
              {showClose && <IconButton circular icon={Close} size="sm" onClick={onClose} />}
            </Flex>
            <div style={{ overflowX: 'hidden', overflowY: 'auto' }}>{children}</div>
          </Flex>
        </ModalBackdrop>,
        document.body
      )
    : null;
};
