export type BannerVariant = 'error' | 'info' | 'primary' | 'secondary' | 'success' | 'warning';
