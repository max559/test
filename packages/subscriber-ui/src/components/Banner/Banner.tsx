import type { PropsWithChildren, ReactElement } from 'react';

import { IconButton } from '@mediahuis/chameleon-react';
import { Close } from '@mediahuis/chameleon-theme-wl/icons';
import { BORDER_RADIUS, SPACING } from '@subscriber/globals';

import { BannerVariant } from './Banner.types';

export interface BannerProps {
  show?: boolean;
  showClose?: boolean;
  variant?: BannerVariant;
  onClose?: () => void;
}

const getVariantStyle = (variant: BannerVariant) => {
  switch (variant) {
    case 'error':
      return {
        bgColor: 'var(--color-red-10)',
        borderColor: 'var(--color-red-70)',
        textColor: 'var(--color-text-red-10)'
      };
    case 'info':
      return {
        bgColor: 'var(--color-blue-10)',
        borderColor: 'var(--color-blue-70)',
        textColor: 'var(--color-text-blue-10)'
      };
    case 'primary':
      return {
        bgColor: 'var(--color-primary-10)',
        borderColor: 'var(--color-primary-70)',
        textColor: 'var(--color-text-primary-10)'
      };
    case 'secondary':
      return {
        bgColor: 'var(--color-secondary-10)',
        borderColor: 'var(--color-secondary-70)',
        textColor: 'var(--color-text-secondary-10)'
      };
    case 'success':
      return {
        bgColor: 'var(--color-green-10)',
        borderColor: 'var(--color-green-70)',
        textColor: 'var(--color-text-green-10)'
      };
    case 'warning':
      return {
        bgColor: 'var(--color-orange-10)',
        borderColor: 'var(--color-orange-70)',
        textColor: 'var(--color-text-orange-10)'
      };
  }
};

export const Banner = ({
  children,
  show = true,
  showClose = false,
  variant = 'info',
  onClose
}: PropsWithChildren<BannerProps>): ReactElement | null => {
  const variantStyle = getVariantStyle(variant);

  return show ? (
    <div
      style={{
        background: variantStyle.bgColor,
        border: `1px solid ${variantStyle.borderColor}`,
        borderRadius: BORDER_RADIUS.default,
        color: variantStyle.textColor,
        padding: SPACING[4],
        position: 'relative'
      }}
    >
      {showClose && (
        <div style={{ position: 'absolute', right: SPACING[2], top: SPACING[2] }}>
          <IconButton circular icon={Close} size="sm" onClick={onClose} />
        </div>
      )}
      <div>{children}</div>
    </div>
  ) : null;
};
