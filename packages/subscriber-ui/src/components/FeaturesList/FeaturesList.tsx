import type { SPACING_TYPE } from '@subscriber/globals';
import type { ReactElement } from 'react';

import type { FeaturesListItem } from '../FeaturesList';

import { Icon, Paragraph } from '@mediahuis/chameleon-react';
import { Checkmark, Close } from '@mediahuis/chameleon-theme-wl/icons';
import { SPACING } from '@subscriber/globals';

import { Overrideable } from '../Overrideable';

export interface FeaturesListProps {
  availableOnly?: boolean;
  className?: string;
  gap?: SPACING_TYPE;
  hideUnavailable?: boolean;
  iconSize?: 'sm' | 'md' | 'lg' | 'xl';
  items: Array<FeaturesListItem>;
}

const FeaturesList = ({
  availableOnly = false,
  className,
  gap = 2,
  hideUnavailable = false,
  iconSize = 'md',
  items
}: FeaturesListProps): ReactElement => {
  const sortedItems: Array<FeaturesListItem> = [...items].sort(
    (a, b) => Number(b.isAvailable) - Number(a.isAvailable)
  );

  return (
    <ul
      className={className}
      style={{ display: 'flex', flexDirection: 'column', gap: SPACING[gap] }}
    >
      {(hideUnavailable ? sortedItems : items).map((item: FeaturesListItem) => {
        if ((availableOnly && item.isAvailable) || !availableOnly || hideUnavailable) {
          return (
            <li
              key={item.key}
              style={{
                alignItems: 'center',
                color: item.isAvailable ? 'inherit' : 'var(--color-neutral-40)',
                display: 'flex',
                gap: SPACING[1],
                visibility: hideUnavailable && !item.isAvailable ? 'hidden' : undefined
              }}
            >
              <Icon
                as={item.isAvailable ? Checkmark : Close}
                color={item.isAvailable ? 'var(--color-green-50)' : 'var(--color-neutral-40)'}
                size={iconSize}
              />
              <Overrideable config={item.text} defaultWrapper={Paragraph} />
            </li>
          );
        }

        return null;
      })}
    </ul>
  );
};

export default FeaturesList;
