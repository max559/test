import type { OverrideableConfig } from '../Overrideable';

export interface FeaturesListItem {
  isAvailable: boolean;
  key: number | string;
  text: OverrideableConfig<string>;
}
