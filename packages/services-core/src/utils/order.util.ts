import {
  AddressAreaValidation,
  BaseValidation,
  BooleanValidation,
  ChoiceValidation,
  DateValidation,
  EmailValidation,
  FileValidation,
  InvoiceDetailsValidation,
  LoginValidation,
  ORDER_STATE,
  Order,
  PaymentValidation,
  PreconditionsValidation,
  TextfieldValidation,
  VALIDATION_STATE,
  VoucherValidation
} from '../services/order/order.types';

import {
  AddressAreaRequirement,
  BaseRequirement,
  BooleanRequirement,
  ChoiceRequirement,
  DateRequirement,
  EmailRequirement,
  FileRequirement,
  InvoiceDetailsRequirement,
  LoginRequirement,
  OfferItem,
  PaymentRequirement,
  PreconditionsRequirement,
  TextfieldRequirement,
  VoucherRequirement
} from '../services/offer/offer.types';

export class OrderUtil {
  public generateOrder(
    checkedFormula: OfferItem,
    type?: string,
    formulaOfferId?: number,
    trackerData: object = {}
  ): Order {
    return {
      brand: checkedFormula.brand,
      creationDate: '',
      formulaOfferId: formulaOfferId,
      id: 0,
      language: 'nl',
      offerHash: checkedFormula.hash,
      state: ORDER_STATE.Pending,
      subscriptionFormulaId: checkedFormula.id,
      trackerData: trackerData,
      type: type,
      updateDate: '',
      addressAreaValidations: checkedFormula.addressAreaRequirements?.map(addressAreaRequirement =>
        this.generateAddressAreaValidation(addressAreaRequirement)
      ),
      booleanValidations: checkedFormula.booleanRequirements?.map(booleanRequirement =>
        this.generateBooleanValidation(booleanRequirement)
      ),
      choiceValidations: checkedFormula.choiceRequirements
        ? checkedFormula.choiceRequirements.map(
            x => this.generateChoiceValidation(x) as ChoiceValidation
          )
        : undefined,
      dateValidations: checkedFormula.dateRequirements?.map(dateRequirement =>
        this.generateDateValidation(dateRequirement)
      ),
      emailValidation: this.generateEmailValidation(checkedFormula.emailRequirement),
      fileValidations: checkedFormula.fileRequirements?.map(fileRequirement =>
        this.generateFileValidation(fileRequirement)
      ),
      invoiceDetailsValidation: this.generateInvoiceDetailsValidation(
        checkedFormula.invoiceDetailsRequirement
      ),
      loginValidation: this.generateLoginValidation(checkedFormula.loginRequirement),
      paymentValidation: this.generatePaymentValidation(checkedFormula.paymentRequirement),
      preconditionsValidation: this.generatePreconditionsValidation(
        checkedFormula.preconditionsRequirement
      ),
      textfieldValidations: checkedFormula.textfieldRequirements
        ? checkedFormula.textfieldRequirements.map(
            x => this.generateTextfieldValidation(x) as TextfieldValidation
          )
        : undefined,
      voucherValidation: this.generateVoucherValidation(checkedFormula.voucherRequirement)
    } as Order;
  }

  private generateValidationBaseFields<T extends BaseValidation>(requirement: BaseRequirement): T {
    return {
      id: 0,
      orderId: 0,
      state: VALIDATION_STATE.Pending,
      description: requirement.description,
      name: requirement.name
    } as T;
  }

  private generateAddressAreaValidation(
    requirement: AddressAreaRequirement | undefined
  ): AddressAreaValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<AddressAreaValidation>(requirement);
    validation.hasDeliveryArea = requirement.hasDeliveryArea;
    validation.isDeliveryAddress = requirement.isDeliveryAddress;
    validation.pickUpAllowed = requirement.pickUpAllowed;
    validation.countries = requirement.countries;
    validation.brand = requirement.brand;
    return validation;
  }

  private generateBooleanValidation(
    requirement: BooleanRequirement | undefined
  ): BooleanValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<BooleanValidation>(requirement);
    validation.errorMessage = requirement.errorMessage;
    validation.required = requirement.required;
    validation.data = { value: false };
    return validation;
  }

  private generateChoiceValidation(
    requirement: ChoiceRequirement | undefined
  ): ChoiceValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<ChoiceValidation>(requirement);
    validation.key = requirement.key;
    validation.choices = requirement.choices;
    return validation;
  }

  private generateDateValidation(
    requirement: DateRequirement | undefined
  ): DateValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<DateValidation>(requirement);
    validation.timeFrameStart = requirement.timeFrameStartCalculated;
    validation.timeFrameEnd = requirement.timeFrameEndCalculated;
    return validation;
  }

  private generateEmailValidation(
    requirement: EmailRequirement | undefined
  ): EmailValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<EmailValidation>(requirement);
    return validation;
  }

  private generateFileValidation(
    requirement: FileRequirement | undefined
  ): FileValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<FileValidation>(requirement);
    return validation;
  }

  private generateInvoiceDetailsValidation(
    requirement: InvoiceDetailsRequirement | undefined
  ): InvoiceDetailsValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<InvoiceDetailsValidation>(requirement);
    return validation;
  }

  private generatePaymentValidation(
    requirement: PaymentRequirement | undefined
  ): PaymentValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<PaymentValidation>(requirement);
    validation.amount = requirement.amount;
    validation.oneShotPaymentOptions = requirement.oneShotPaymentOptions;
    validation.recurringPaymentOptions = requirement.recurringPaymentOptions;
    return validation;
  }

  private generateLoginValidation(
    requirement: LoginRequirement | undefined
  ): LoginValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<LoginValidation>(requirement);
    return validation;
  }

  private generatePreconditionsValidation(
    requirement: PreconditionsRequirement | undefined
  ): PreconditionsValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<PreconditionsValidation>(requirement);
    return validation;
  }

  private generateTextfieldValidation(
    requirement: TextfieldRequirement | undefined
  ): TextfieldValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<TextfieldValidation>(requirement);
    validation.key = requirement.key;
    return validation;
  }

  private generateVoucherValidation(
    requirement: VoucherRequirement | undefined
  ): VoucherValidation | undefined {
    if (!requirement) {
      return undefined;
    }
    const validation = this.generateValidationBaseFields<VoucherValidation>(requirement);
    return validation;
  }
}

export default OrderUtil;
