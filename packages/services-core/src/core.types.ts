export type Brand = 'az' | 'co' | 'dl' | 'ds' | 'gva' | 'hbvl' | 'lt' | 'lw' | 'nb' | 'tc';
export type Environment = 'test' | 'preview' | 'production';
