import type { Brand, Environment } from './core.types';

import { AccountService } from './services/account';
import { AccountManagementService } from './services/account-management';
import { AddressService } from './services/address';
import { AuthService } from './services/auth';
import { CbeService } from './services/cbe';
import { ContentService } from './services/content';
import { DefaultValidatorService } from './services/default-validator';
import { FileValidatorService } from './services/file-validator';
import { FileshareService } from './services/fileshare';
import { LocationValidatorService } from './services/location-validator';
import { LoginValidatorService } from './services/login-validator';
import { MeteringService } from './services/metering';
import { OfferService } from './services/offer';
import { OptinService } from './services/optin';
import { OrderService } from './services/order';
import { OrderSettleService } from './services/order-settle';
import { PaymentService } from './services/payment';
import { PickupLocationService } from './services/pickup-location';
import { PreconditionValidatorService } from './services/precondition-validator';
import { SubscriptionService } from './services/subscription';
import { TextfieldValidatorService } from './services/textfield-validator';
import { VoucherService } from './services/voucher';
import { OrderUtil } from './utils/order.util';

class ServicesCore {
  protected _brand: Brand;
  protected _environment: Environment;

  private _accountService!: AccountService;
  private _accountManagementService!: AccountManagementService;
  private _addressService!: AddressService;
  private _authService!: AuthService;
  private _cbeService!: CbeService;
  private _contentService!: ContentService;
  private _defaultValidatorService!: DefaultValidatorService;
  private _fileshareService!: FileshareService;
  private _fileValidatorService!: FileValidatorService;
  private _locationValidatorService!: LocationValidatorService;
  private _loginValidatorService!: LoginValidatorService;
  private _meteringService!: MeteringService;
  private _offerService!: OfferService;
  private _optinService!: OptinService;
  private _orderService!: OrderService;
  private _orderSettleService!: OrderSettleService;
  private _paymentService!: PaymentService;
  private _pickupLocationService!: PickupLocationService;
  private _preconditionValidatorService!: PreconditionValidatorService;
  private _subscriptionService!: SubscriptionService;
  private _textfieldValidatorService!: TextfieldValidatorService;
  private _voucherService!: VoucherService;

  private _orderUtil: OrderUtil;

  constructor(brand: Brand, environment: Environment) {
    this._brand = brand;
    this._environment = environment;

    this._orderUtil = new OrderUtil();
    this.setServices();
  }

  private setServices() {
    this._accountService = new AccountService(this._brand, this._environment);
    this._accountManagementService = new AccountManagementService(this._brand, this._environment);
    this._addressService = new AddressService(this._brand, this._environment);
    this._authService = new AuthService(this._brand, this._environment);
    this._cbeService = new CbeService(this._brand, this._environment);
    this._contentService = new ContentService(this._brand, this._environment);
    this._defaultValidatorService = new DefaultValidatorService(this._brand, this._environment);
    this._fileshareService = new FileshareService(this._brand, this._environment);
    this._fileValidatorService = new FileValidatorService(this._brand, this._environment);
    this._locationValidatorService = new LocationValidatorService(this._brand, this._environment);
    this._loginValidatorService = new LoginValidatorService(this._brand, this._environment);
    this._meteringService = new MeteringService(this._brand, this._environment);
    this._offerService = new OfferService(this._brand, this._environment);
    this._optinService = new OptinService(this._brand, this._environment);
    this._orderService = new OrderService(this._brand, this._environment);
    this._orderSettleService = new OrderSettleService(this._brand, this._environment);
    this._paymentService = new PaymentService(this._brand, this._environment);
    this._pickupLocationService = new PickupLocationService(this._brand, this._environment);
    this._preconditionValidatorService = new PreconditionValidatorService(
      this._brand,
      this._environment
    );
    this._subscriptionService = new SubscriptionService(this._brand, this._environment);
    this._textfieldValidatorService = new TextfieldValidatorService(this._brand, this._environment);
    this._voucherService = new VoucherService(this._brand, this._environment);
  }

  get brand() {
    return this._brand;
  }
  set brand(value: Brand) {
    this._brand = value;
    this.setServices();
  }

  get environment() {
    return this._environment;
  }
  set environment(value: Environment) {
    this._environment = value;
    this.setServices();
  }

  get accountService() {
    return this._accountService;
  }
  get accountManagementService() {
    return this._accountManagementService;
  }
  get addressService() {
    return this._addressService;
  }
  get authService() {
    return this._authService;
  }
  get cbeService() {
    return this._cbeService;
  }
  get contentService() {
    return this._contentService;
  }
  get defaultValidatorService() {
    return this._defaultValidatorService;
  }
  get fileshareService() {
    return this._fileshareService;
  }
  get fileValidatorService() {
    return this._fileValidatorService;
  }
  get locationValidatorService() {
    return this._locationValidatorService;
  }
  get loginValidatorService() {
    return this._loginValidatorService;
  }
  get meteringService() {
    return this._meteringService;
  }
  get offerService() {
    return this._offerService;
  }
  get optinService() {
    return this._optinService;
  }
  get orderService() {
    return this._orderService;
  }
  get orderSettleService() {
    return this._orderSettleService;
  }
  get paymentService() {
    return this._paymentService;
  }
  get pickupLocationService() {
    return this._pickupLocationService;
  }
  get preconditionValidatorService() {
    return this._preconditionValidatorService;
  }
  get subscriptionService() {
    return this._subscriptionService;
  }
  get textfieldValidatorService() {
    return this._textfieldValidatorService;
  }
  get voucherService() {
    return this._voucherService;
  }

  get orderUtil() {
    return this._orderUtil;
  }
}

export default ServicesCore;
