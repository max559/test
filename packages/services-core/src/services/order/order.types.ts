import type { BoolData, ChoiceData, DateData, EmailData } from '../default-validator';
import type { FileValidationData } from '../file-validator';
import type { DeliveryAddressData, InvoiceAddressData } from '../location-validator';
import type { LoginData } from '../login-validator';
import type { PaymentValidationData } from '../payment';
import type { PreconditionValidationData } from '../precondition-validator';
import type { TextfieldData, TextfieldKey } from '../textfield-validator';
import type { VoucherValidationData } from '../voucher';

import { REQUIREMENT_TYPE } from '../offer';

export enum ORDER_STATE {
  Pending = 'Pending',
  Confirmed = 'Confirmed',
  Revoked = 'Revoked'
}

export enum PAYMENT_OPTION {
  Bancontact = 'Bancontact',
  Belfius = 'Belfius',
  CreditCard = 'CreditCard',
  DirectDebit = 'DirectDebit',
  Ideal = 'Ideal',
  KBC = 'KBC',
  PayPal = 'PayPal',
  WireTransfer = 'WireTransfer'
}

export enum VALIDATION_STATE {
  Compliant = 'Compliant',
  Pending = 'Pending',
  NotValid = 'NotValid'
}

export interface Order {
  addressAreaValidation?: AddressAreaValidation;
  addressAreaValidations?: Array<AddressAreaValidation>;
  booleanValidation?: BooleanValidation;
  booleanValidations?: Array<BooleanValidation>;
  brand?: string;
  choiceValidations?: Array<ChoiceValidation>;
  creationDate: string; //Date
  dateValidation?: DateValidation;
  dateValidations?: Array<DateValidation>;
  emailValidation?: EmailValidation;
  emailValidations?: Array<EmailValidation>;
  fileValidation?: FileValidation;
  fileValidations?: Array<FileValidation>;
  formulaOfferId?: number;
  id: number;
  invoiceDetailsValidation?: InvoiceDetailsValidation;
  language?: string;
  lastModifiedBy?: string;
  loginValidation?: LoginValidation;
  offerHash?: string;
  orderHash?: string;
  paymentValidation?: PaymentValidation;
  preconditionsValidation?: PreconditionsValidation;
  state: ORDER_STATE;
  subscriptionFormulaId?: number;
  textfieldValidations?: Array<TextfieldValidation>;
  trackerData: object;
  type?:
    | 'acquisition'
    | 'renewal'
    | 'settlement'
    | 'voucherpurchase'
    | 'voucherredemption'
    | 'winback';
  updateDate: string; //Date
  voucherValidation?: VoucherValidation;
}

export interface BaseValidation {
  brand?: string;
  creationDate: string; //Date
  description?: string;
  id: number;
  lastModifiedBy?: string;
  name?: string;
  orderId: number;
  state: VALIDATION_STATE;
  updateDate: string; //Date
}

export interface AddressAreaValidation extends BaseValidation {
  countries?: Array<string>;
  data: DeliveryAddressData;
  hasDeliveryArea: boolean;
  isDeliveryAddress: boolean;
  pickUpAllowed: boolean;
  type: REQUIREMENT_TYPE.AddressArea;
}

export interface BooleanValidation extends BaseValidation {
  data: BoolData;
  errorMessage?: string;
  isPrechecked: boolean;
  newsletter?: string;
  required: boolean;
  type: REQUIREMENT_TYPE.Boolean;
}

export interface ChoiceValidation extends BaseValidation {
  choices?: Array<string>;
  data: ChoiceData;
  key: string;
  type: REQUIREMENT_TYPE.Choice;
}

export interface DateValidation extends BaseValidation {
  data: DateData;
  timeFrameEnd?: string;
  timeFrameStart?: string;
  type: REQUIREMENT_TYPE.Date;
}

export interface EmailValidation extends BaseValidation {
  data: EmailData;
  type: REQUIREMENT_TYPE.Email;
}

export interface FileValidation extends BaseValidation {
  data: FileValidationData;
  type: REQUIREMENT_TYPE.File;
}

export interface InvoiceDetailsValidation extends BaseValidation {
  data?: InvoiceAddressData;
  type: REQUIREMENT_TYPE.InvoiceDetails;
}

export interface LoginValidation extends BaseValidation {
  data: LoginData;
  type: REQUIREMENT_TYPE.Login;
}

export interface PaymentValidation extends BaseValidation {
  amount: number;
  data: PaymentValidationData;
  oneShotPaymentOptions?: Array<PAYMENT_OPTION>;
  recurringPaymentOptions?: Array<PAYMENT_OPTION>;
  type: REQUIREMENT_TYPE.Payment;
  version?: number;
}

export interface PreconditionsValidation extends BaseValidation {
  data: PreconditionValidationData;
  type: REQUIREMENT_TYPE.Preconditions;
}

export interface VoucherValidation extends BaseValidation {
  data: VoucherValidationData;
  type: REQUIREMENT_TYPE.Voucher;
}

export interface TextfieldValidation extends BaseValidation {
  data: TextfieldData;
  key: TextfieldKey;
  type: REQUIREMENT_TYPE.Textfield;
}
