import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { BaseValidation, Order } from './order.types';

import { coreFetch } from '../../core-fetch';
import { REQUIREMENT_TYPE } from '../offer';
import baseUrlConfig from './order.config';

export interface getOrderParams {
  id?: string | number;
  orderHash?: string;
}

export interface getValidationParams {
  id?: string | number;
  orderHash?: string;
  type?: REQUIREMENT_TYPE;
}

export class OrderService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public createOrder(order: Order): Promise<CoreResponse<Order>> {
    return coreFetch(new URL(`${this._baseUrl}/api/orders`), {
      body: JSON.stringify(order),
      method: 'POST',
      params: {
        lang: 'nl',
        'api-version': '2.0'
      }
    });
  }

  public getOrder(params: getOrderParams): Promise<CoreResponse<Order>> {
    return coreFetch(new URL(`${this._baseUrl}/api/orders/${params.id}`), {
      method: 'GET',
      params: {
        orderHash: params.orderHash,
        'api-version': '2.0'
      }
    });
  }

  public getValidation(params: getValidationParams): Promise<CoreResponse<BaseValidation>> {
    return coreFetch(
      new URL(`${this._baseUrl}/api/validations/${params.type}/hashed/${params.id}`),
      {
        method: 'GET',
        params: {
          orderHash: params.orderHash,
          'api-version': '2.0'
        }
      }
    );
  }
}

export default OrderService;
