import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testorderservice.medienhausaachen.de',
    preview: 'https://previeworderservice.medienhausaachen.de',
    production: 'https://orderservice.medienhausaachen.de'
  },
  co: {
    test: 'https://testorderservice.mediahuis.lu',
    preview: 'https://previeworderservice.mediahuis.lu',
    production: 'https://orderservice.mediahuis.lu'
  },
  dl: {
    test: 'https://testorderservice.limburger.nl',
    preview: 'https://previeworderservice.limburger.nl',
    production: 'https://orderservice.limburger.nl'
  },
  ds: {
    test: 'https://testorderservice.mediahuis.be',
    preview: 'https://previeworderservice.mediahuis.be',
    production: 'https://orderservice.mediahuis.be'
  },
  gva: {
    test: 'https://testorderservice.mediahuis.be',
    preview: 'https://previeworderservice.mediahuis.be',
    production: 'https://orderservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testorderservice.mediahuis.be',
    preview: 'https://previeworderservice.mediahuis.be',
    production: 'https://orderservice.mediahuis.be'
  },
  lt: {
    test: 'https://testorderservice.mediahuis.lu',
    preview: 'https://previeworderservice.mediahuis.lu',
    production: 'https://orderservice.mediahuis.lu'
  },
  lw: {
    test: 'https://testorderservice.mediahuis.lu',
    preview: 'https://previeworderservice.mediahuis.lu',
    production: 'https://orderservice.mediahuis.lu'
  },
  nb: {
    test: 'https://testorderservice.mediahuis.be',
    preview: 'https://previeworderservice.mediahuis.be',
    production: 'https://orderservice.mediahuis.be'
  },
  tc: {
    test: 'https://testorderservice.mediahuis.lu',
    preview: 'https://previeworderservice.mediahuis.lu',
    production: 'https://orderservice.mediahuis.lu'
  }
};

export default baseUrlConfig;
