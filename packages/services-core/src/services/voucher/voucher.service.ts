import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { Voucher } from './voucher.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './voucher.config';
import { VoucherValidation } from '../order';

export class VoucherService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getVoucherByCode(code: string): Promise<CoreResponse<Voucher>> {
    return coreFetch(new URL(`${this._baseUrl}/api/vouchers/code/${code}`), {
      method: 'GET'
    });
  }

  public getVoucherHashed(orderId: number, orderHash: string): Promise<CoreResponse<Voucher>> {
    return coreFetch(new URL(`${this._baseUrl}/api/vouchers/hashed`), {
      method: 'GET',
      params: {
        orderId,
        orderHash
      }
    });
  }

  public postVoucherValidation(body: VoucherValidation): Promise<CoreResponse<VoucherValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/vouchervalidation`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }
}

export default VoucherService;
