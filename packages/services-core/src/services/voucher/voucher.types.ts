export enum VOUCHER_STATE {
  New = 'New',
  Revoked = 'Revoked',
  Sold = 'Sold',
  Used = 'Used'
}

export interface Voucher {
  brand?: string;
  code?: string;
  expirationDate: string; // Date
  generatedDate: string; // Date
  id: number;
  purchaseFormulaId?: number;
  purchaseOrderId?: string;
  redeemOrderId?: number;
  redeemFormulaId: string;
  state: VOUCHER_STATE;
  usedDate?: string; // Date
}

export interface VoucherValidationData {
  voucherCode?: string;
}
