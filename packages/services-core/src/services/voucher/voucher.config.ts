import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  co: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  dl: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  ds: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  gva: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  lt: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  lw: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  nb: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  },
  tc: {
    test: 'https://testvoucherservice.mediahuis.be',
    preview: 'https://previewvoucherservice.mediahuis.be',
    production: 'https://voucherservice.mediahuis.be'
  }
};

export default baseUrlConfig;
