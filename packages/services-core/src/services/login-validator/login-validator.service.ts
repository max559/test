import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import { LoginValidation } from '../order';
import baseUrlConfig from './login-validator.config';

export class LoginValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postLoginValidation(
    body: LoginValidation,
    idToken?: string
  ): Promise<CoreResponse<LoginValidation>> {
    const baseUrl = this._baseUrl;
    const headers: HeadersInit = { 'x-api-version': '2' };

    if (idToken) {
      headers['Authorization'] = idToken;
    }

    return coreFetch(new URL(`${baseUrl}/api/loginvalidation`), {
      body: JSON.stringify(body),
      credentials: 'include',
      headers,
      method: 'POST'
    });
  }
}

export default LoginValidatorService;
