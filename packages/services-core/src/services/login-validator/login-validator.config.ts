import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testloginvalidator.aachener-zeitung.de',
    preview: 'https://previewloginvalidator.aachener-zeitung.de',
    production: 'https://loginvalidator.aachener-zeitung.de'
  },
  co: {
    test: 'https://testloginvalidator.contacto.lu',
    preview: 'https://previewloginvalidator.contacto.lu',
    production: 'https://loginvalidator.contacto.lu'
  },
  dl: {
    test: 'https://testloginvalidator.limburger.nl',
    preview: 'https://previewloginvalidator.limburger.nl',
    production: 'https://loginvalidator.limburger.nl'
  },
  ds: {
    test: 'https://testloginvalidator.standaard.be',
    preview: 'https://previewloginvalidator.standaard.be',
    production: 'https://loginvalidator.standaard.be'
  },
  gva: {
    test: 'https://testloginvalidator.gva.be',
    preview: 'https://previewloginvalidator.gva.be',
    production: 'https://loginvalidator.gva.be'
  },
  hbvl: {
    test: 'https://testloginvalidator.hbvl.be',
    preview: 'https://previewloginvalidator.hbvl.be',
    production: 'https://loginvalidator.hbvl.be'
  },
  lt: {
    test: 'https://testloginvalidator.luxtimes.lu',
    preview: 'https://previewloginvalidator.luxtimes.lu',
    production: 'https://loginvalidator.luxtimes.lu'
  },
  lw: {
    test: 'https://testloginvalidator.wort.lu',
    preview: 'https://previewloginvalidator.wort.lu',
    production: 'https://loginvalidator.wort.lu'
  },
  nb: {
    test: 'https://testloginvalidator.nieuwsblad.be',
    preview: 'https://previewloginvalidator.nieuwsblad.be',
    production: 'https://loginvalidator.nieuwsblad.be'
  },
  tc: {
    test: 'https://testloginvalidator.telecran.lu',
    preview: 'https://previewloginvalidator.telecran.lu',
    production: 'https://loginvalidator.telecran.lu'
  }
};

export default baseUrlConfig;
