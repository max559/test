export interface LoginRequirementParameters {
  brand?: string;
}

export interface LoginData {
  account?: string;
  brand?: string;
  firstName?: string;
  gender?: string;
  lastName?: string;
}
