import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './fileshare.config';
import { FileshareDto } from './fileshare.types';

export class FileshareService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public uploadImage(file: File): Promise<CoreResponse<FileshareDto>> {
    const formDataImage = new FormData();
    formDataImage.append('file', file);

    return coreFetch(new URL(`${this._baseUrl}/api/Files/aboshop-validation`), {
      body: formDataImage,
      isFormData: true,
      method: 'POST'
    });
  }
}

export default FileshareService;
