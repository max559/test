import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testsubscriberfileshare.medienhausaachen.de',
    preview: 'https://previewsubscriberfileshare.medienhausaachen.de',
    production: 'https://subscriberfileshare.medienhausaachen.de'
  },
  co: {
    test: 'https://testsubscriberfileshare.mediahuis.lu',
    preview: 'https://previewsubscriberfileshare.mediahuis.lu',
    production: 'https://subscriberfileshare.mediahuis.lu'
  },
  dl: {
    test: 'https://testsubscriberfileshare.limburger.nl',
    preview: 'https://previewsubscriberfileshare.limburger.nl',
    production: 'https://subscriberfileshare.limburger.nl'
  },
  ds: {
    test: 'https://testsubscriberfileshare.mediahuis.be',
    preview: 'https://previewsubscriberfileshare.mediahuis.be',
    production: 'https://subscriberfileshare.mediahuis.be'
  },
  gva: {
    test: 'https://testsubscriberfileshare.mediahuis.be',
    preview: 'https://previewsubscriberfileshare.mediahuis.be',
    production: 'https://subscriberfileshare.mediahuis.be'
  },
  hbvl: {
    test: 'https://testsubscriberfileshare.mediahuis.be',
    preview: 'https://previewsubscriberfileshare.mediahuis.be',
    production: 'https://subscriberfileshare.mediahuis.be'
  },
  lt: {
    test: 'https://testsubscriberfileshare.mediahuis.lu',
    preview: 'https://previewsubscriberfileshare.mediahuis.lu',
    production: 'https://subscriberfileshare.mediahuis.lu'
  },
  lw: {
    test: 'https://testsubscriberfileshare.mediahuis.lu',
    preview: 'https://previewsubscriberfileshare.mediahuis.lu',
    production: 'https://subscriberfileshare.mediahuis.lu'
  },
  nb: {
    test: 'https://testsubscriberfileshare.mediahuis.be',
    preview: 'https://previewsubscriberfileshare.mediahuis.be',
    production: 'https://subscriberfileshare.mediahuis.be'
  },
  tc: {
    test: 'https://testsubscriberfileshare.mediahuis.lu',
    preview: 'https://previewsubscriberfileshare.mediahuis.lu',
    production: 'https://subscriberfileshare.mediahuis.lu'
  }
};

export default baseUrlConfig;
