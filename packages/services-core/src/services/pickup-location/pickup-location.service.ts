import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { Shop } from './pickup-location.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './pickup-location.config';

export class PickupLocationService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getShops(brand: string, postalCode: string): Promise<CoreResponse<Array<Shop>>> {
    return coreFetch(new URL(`${this._baseUrl}/api/shop`), {
      method: 'GET',
      params: {
        brand,
        postalCode
      }
    });
  }
}

export default PickupLocationService;
