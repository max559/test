import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  co: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  dl: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  ds: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  gva: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  lt: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  lw: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  nb: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  },
  tc: {
    test: 'https://testpickuplocationservice.mediahuis.be',
    preview: 'https://previewpickuplocationservice.mediahuis.be',
    production: 'https://pickuplocationservice.mediahuis.be'
  }
};

export default baseUrlConfig;
