export interface Shop {
  bus?: string;
  city?: string;
  countryCode?: string;
  deliveryStore?: string;
  houseNumber?: string;
  postalCode?: string;
  shopId: number;
  street?: string;
}
