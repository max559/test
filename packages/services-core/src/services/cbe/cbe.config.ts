import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  co: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  dl: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  ds: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  gva: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  lt: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  lw: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  nb: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  },
  tc: {
    test: 'https://testcbeservice.mediahuis.be',
    preview: 'https://previewcbeservice.mediahuis.be',
    production: 'https://cbeservice.mediahuis.be'
  }
};

export default baseUrlConfig;
