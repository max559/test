import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { Enterprise } from './cbe.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './cbe.config';

export class CbeService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getEnterprise(vatNumber: string): Promise<CoreResponse<Enterprise>> {
    return coreFetch(new URL(`${this._baseUrl}/api/enterprise/${vatNumber}`), {
      method: 'GET'
    });
  }
}

export default CbeService;
