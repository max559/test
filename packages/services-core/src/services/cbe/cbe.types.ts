export interface Enterprise {
  box?: string;
  city?: string;
  companyName?: string;
  country?: string;
  entityNumber?: string;
  houseNumber?: string;
  postalCode?: string;
  street?: string;
  vatNumber?: string;
}
