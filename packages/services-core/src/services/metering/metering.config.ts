import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: '',
    preview: '',
    production: ''
  },
  co: {
    test: '',
    preview: '',
    production: ''
  },
  dl: {
    test: '',
    preview: '',
    production: ''
  },
  ds: {
    test: 'https://testmeteringservice.mediahuis.be',
    preview: 'https://previewmeteringservice.mediahuis.be',
    production: 'https://meteringservice.mediahuis.be'
  },
  gva: {
    test: 'https://testmeteringservice.mediahuis.be',
    preview: 'https://previewmeteringservice.mediahuis.be',
    production: 'https://meteringservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testmeteringservice.mediahuis.be',
    preview: 'https://previewmeteringservice.mediahuis.be',
    production: 'https://meteringservice.mediahuis.be'
  },
  lt: {
    test: '',
    preview: '',
    production: ''
  },
  lw: {
    test: '',
    preview: '',
    production: ''
  },
  nb: {
    test: 'https://testmeteringservice.mediahuis.be',
    preview: 'https://previewmeteringservice.mediahuis.be',
    production: 'https://meteringservice.mediahuis.be'
  },
  tc: {
    test: '',
    preview: '',
    production: ''
  }
};

export default baseUrlConfig;
