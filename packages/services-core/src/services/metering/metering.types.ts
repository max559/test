export interface UnlockRequest {
  accountId: string;
  articleId: string;
  brand: string;
  isConfirmed?: boolean;
}

export interface UnlockResponse {
  allowedState: 'Allowed' | 'NotAllowed';
  rsa: string;
}
