import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { UnlockRequest, UnlockResponse } from './metering.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './metering.config';

export class MeteringService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postUnlock(body: UnlockRequest): Promise<CoreResponse<UnlockResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/unlock`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }
}

export default MeteringService;
