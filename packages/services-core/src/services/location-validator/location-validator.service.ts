import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './location-validator.config';
import { AddressAreaValidation, InvoiceDetailsValidation } from '../order';

export class LocationValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postAddressAreaValidation(
    body: AddressAreaValidation
  ): Promise<CoreResponse<AddressAreaValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/addressareavalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }

  public postInvoiceDetailsValidation(
    body: InvoiceDetailsValidation
  ): Promise<CoreResponse<InvoiceDetailsValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/invoiceaddressvalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }
}

export default LocationValidatorService;
