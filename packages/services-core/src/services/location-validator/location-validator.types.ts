export interface DeliveryAddressData {
  addressee?: string;
  addressKey?: string;
  busNumber?: string;
  city?: string;
  countryCode?: string;
  deliveryStore?: string;
  houseNumber?: string;
  postalCode?: string;
  shopId?: number;
  street?: string;
}

export interface InvoiceAddressData {
  addressee?: string;
  addressKey?: string;
  busNumber?: string;
  city?: string;
  companyName?: string;
  countryCode?: string;
  houseNumber?: string;
  postalCode?: string;
  street?: string;
  vatNumber?: string;
}
