import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testlocationvalidator.medienhausaachen.de',
    preview: 'https://previewlocationvalidator.medienhausaachen.de',
    production: 'https://locationvalidator.medienhausaachen.de'
  },
  co: {
    test: 'https://testlocationvalidator.mediahuis.lu',
    preview: 'https://previewlocationvalidator.mediahuis.lu',
    production: 'https://locationvalidator.mediahuis.lu'
  },
  dl: {
    test: 'https://testlocationvalidator.limburger.nl',
    preview: 'https://previewlocationvalidator.limburger.nl',
    production: 'https://locationvalidator.limburger.nl'
  },
  ds: {
    test: 'https://testlocationvalidator.mediahuis.be',
    preview: 'https://previewlocationvalidator.mediahuis.be',
    production: 'https://locationvalidator.mediahuis.be'
  },
  gva: {
    test: 'https://testlocationvalidator.mediahuis.be',
    preview: 'https://previewlocationvalidator.mediahuis.be',
    production: 'https://locationvalidator.mediahuis.be'
  },
  hbvl: {
    test: 'https://testlocationvalidator.mediahuis.be',
    preview: 'https://previewlocationvalidator.mediahuis.be',
    production: 'https://locationvalidator.mediahuis.be'
  },
  lt: {
    test: 'https://testlocationvalidator.mediahuis.lu',
    preview: 'https://previewlocationvalidator.mediahuis.lu',
    production: 'https://locationvalidator.mediahuis.lu'
  },
  lw: {
    test: 'https://testlocationvalidator.mediahuis.lu',
    preview: 'https://previewlocationvalidator.mediahuis.lu',
    production: 'https://locationvalidator.mediahuis.lu'
  },
  nb: {
    test: 'https://testlocationvalidator.mediahuis.be',
    preview: 'https://previewlocationvalidator.mediahuis.be',
    production: 'https://locationvalidator.mediahuis.be'
  },
  tc: {
    test: 'https://testlocationvalidator.mediahuis.lu',
    preview: 'https://previewlocationvalidator.mediahuis.lu',
    production: 'https://locationvalidator.mediahuis.lu'
  }
};

export default baseUrlConfig;
