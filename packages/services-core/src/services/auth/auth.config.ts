import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testauth.aachener-zeitung.de',
    preview: 'https://previewauth.aachener-zeitung.de',
    production: 'https://auth.aachener-zeitung.de'
  },
  co: {
    test: 'https://testauth.contacto.lu',
    preview: 'https://previewauth.contacto.lu',
    production: 'https://auth.contacto.lu'
  },
  dl: {
    test: 'https://testauth.limburger.nl',
    preview: 'https://previewauth.limburger.nl',
    production: 'https://auth.limburger.nl'
  },
  ds: {
    test: 'https://testauth.standaard.be',
    preview: 'https://previewauth.standaard.be',
    production: 'https://auth.standaard.be'
  },
  gva: {
    test: 'https://testauth.gva.be',
    preview: 'https://previewauth.gva.be',
    production: 'https://auth.gva.be'
  },
  hbvl: {
    test: 'https://testauth.hbvl.be',
    preview: 'https://previewauth.hbvl.be',
    production: 'https://auth.hbvl.be'
  },
  lt: {
    test: 'https://testauth.luxtimes.lu',
    preview: 'https://previewauth.luxtimes.lu',
    production: 'https://auth.luxtimes.lu'
  },
  lw: {
    test: 'https://testauth.wort.lu',
    preview: 'https://previewauth.wort.lu',
    production: 'https://auth.wort.lu'
  },
  nb: {
    test: 'https://testauth.nieuwsblad.be',
    preview: 'https://previewauth.nieuwsblad.be',
    production: 'https://auth.nieuwsblad.be'
  },
  tc: {
    test: 'https://testauth.telecran.lu',
    preview: 'https://previewauth.telecran.lu',
    production: 'https://auth.telecran.lu'
  }
};

export default baseUrlConfig;
