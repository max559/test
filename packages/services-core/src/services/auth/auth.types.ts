export interface AuthenticationData {
  password?: string;
  username?: string;
}

export interface AuthenticationResponse {
  authenticated: boolean;
  cookies: Array<{
    cookieDomain: string;
    cookieName: string;
    cookiePath: string | null;
    cookieValue: string;
  }>;
}
