import type { Brand, Environment } from '../../core.types';
import type { CoreResponse } from '../../core-fetch';
import type { AuthenticationData, AuthenticationResponse } from './auth.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './auth.config';

export class AuthService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postAuthentication(
    body: AuthenticationData
  ): Promise<CoreResponse<AuthenticationResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/authentication`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }
}

export default AuthService;
