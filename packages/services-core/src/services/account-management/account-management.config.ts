import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testaccountmanagement.aachener-zeitung.de',
    preview: 'https://previewaccountmanagement.aachener-zeitung.de',
    production: 'https://accountmanagement.aachener-zeitung.de'
  },
  co: {
    test: 'https://testaccountmanagement.contacto.lu',
    preview: 'https://previewaccountmanagement.contacto.lu',
    production: 'https://accountmanagement.contacto.lu'
  },
  dl: {
    test: 'https://testaccountmanagement.limburger.nl',
    preview: 'https://previewaccountmanagement.limburger.nl',
    production: 'https://accountmanagement.limburger.nl'
  },
  ds: {
    test: 'https://testaccountmanagement.standaard.be',
    preview: 'https://previewaccountmanagement.standaard.be',
    production: 'https://accountmanagement.standaard.be'
  },
  gva: {
    test: 'https://testaccountmanagement.gva.be',
    preview: 'https://previewaccountmanagement.gva.be',
    production: 'https://accountmanagement.gva.be'
  },
  hbvl: {
    test: 'https://testaccountmanagement.hbvl.be',
    preview: 'https://previewaccountmanagement.hbvl.be',
    production: 'https://accountmanagement.hbvl.be'
  },
  lt: {
    test: 'https://testaccountmanagement.luxtimes.lu',
    preview: 'https://previewaccountmanagement.luxtimes.lu',
    production: 'https://accountmanagement.luxtimes.lu'
  },
  lw: {
    test: 'https://testaccountmanagement.wort.lu',
    preview: 'https://previewaccountmanagement.wort.lu',
    production: 'https://accountmanagement.wort.lu'
  },
  nb: {
    test: 'https://testaccountmanagement.nieuwsblad.be',
    preview: 'https://previewaccountmanagement.nieuwsblad.be',
    production: 'https://accountmanagement.nieuwsblad.be'
  },
  tc: {
    test: 'https://testaccountmanagement.telecran.lu',
    preview: 'https://previewaccountmanagement.telecran.lu',
    production: 'https://accountmanagement.telecran.lu'
  }
};

export default baseUrlConfig;
