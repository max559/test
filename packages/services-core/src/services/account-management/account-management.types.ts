export enum ACCOUNT_FLOW {
  CONFIRM_EMAIL_AFTER_REGISTRATION = 'ConfirmEmailAfterRegistration',
  CONFIRM_EXISTING_ACCOUNT = 'ConfirmationExistingAccount',
  CONFIRM_NEW_ACCOUNT = 'ConfirmationNewAccount',
  MODIFY_PASSWORD = 'ModifyPassword',
  MODIFY_PASSWORD_MULTIBRAND = 'ModifyPasswordMultibrand',
  MODIFY_PASSWORD_STANDALONE = 'ModifyPasswordStandalone',
  STOP_CONFIRM_EMAIL_AFTER_REGISTRATION = 'StopConfirmEmailAfterRegistration'
}

export enum GENDER {
  Unknown,
  Male,
  Female,
  Company,
  MaleFemale,
  PublicService,
  NatuurlijkPersoon
}

export interface AccountInfoRequest {
  brand?: string;
  email?: string;
}

export interface AccountInfoResponse {
  AccountGuid: string;
  BirthDate?: string;
  Box?: string;
  City?: string;
  Country?: string;
  EmailAddress?: string;
  FirstName?: string;
  Gender?: GENDER;
  HouseNumber?: string;
  IsConfirmed?: boolean;
  MobileNumber?: string;
  Name?: string;
  PhoneNumber?: string;
  Street?: string;
  Zipcode?: string;
}

export interface AccountStatus {
  accountStatus?: 'Full' | 'Light' | 'Medium' | 'NotFound';
  accountType?: 'Natural' | 'Unknown' | 'Unnatural';
}

export interface CookieSuccess {
  Cookies: Array<{
    CookieDomain: string;
    CookieName: string;
    CookieValue: string;
  }>;
  Success: boolean;
}

export interface CreateAccountRequest {
  birthDate?: string;
  box?: string;
  brand?: string;
  city?: string;
  country?: string;
  emailAddress: string;
  firstName?: string;
  gender: GENDER;
  houseNumber?: string;
  name?: string;
  password: string;
  passwordRepeat: string;
  phoneNumber?: string;
  registrationCampaignCode?: string;
  street?: string;
  zipcode?: string;
}

export interface CreateAccountResponse {
  accountGuid: string;
}

export interface MailRequest {
  brand?: string;
  copyData?: Record<string, unknown>;
  goto?: string;
  mail?: string;
  source?: string;
  step: number;
}

export interface PasswordResetWithEmailCommand {
  brand: string;
  email: string;
  newPassword: string;
  newPasswordRepeat: string;
}

export interface ValidHashResponse {
  flow: ACCOUNT_FLOW;
  mail: string;
  status: 'Expired' | 'Valid';
  step: number;
}
