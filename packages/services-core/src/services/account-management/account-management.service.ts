import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type {
  AccountInfoRequest,
  AccountInfoResponse,
  AccountStatus,
  CookieSuccess,
  CreateAccountRequest,
  CreateAccountResponse,
  MailRequest,
  PasswordResetWithEmailCommand,
  ValidHashResponse
} from './account-management.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './account-management.config';

export class AccountManagementService {
  private _baseUrl: string;
  private _brand: Brand;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
    this._brand = brand;
  }

  public getAccountInfo(body?: AccountInfoRequest): Promise<CoreResponse<AccountInfoResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/account/info`), {
      body: JSON.stringify(body || {}),
      credentials: 'include',
      method: 'POST'
    });
  }

  public getAccountStatus(emailAddress?: string): Promise<CoreResponse<AccountStatus>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/accountstatus`), {
      credentials: 'include',
      method: 'GET',
      params: {
        brand: this._brand,
        emailAddress
      }
    });
  }

  public getHashValidation(hash: string): Promise<CoreResponse<ValidHashResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/hash/validate/${hash}`), {
      method: 'GET'
    });
  }

  public postAccount(body: CreateAccountRequest): Promise<CoreResponse<CreateAccountResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v2/account`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postMailModifyPassword(body: MailRequest): Promise<CoreResponse<null>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/mail/ModifyPassword`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postMailModifyPasswordStandalone(body: MailRequest): Promise<CoreResponse<null>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/mail/ModifyPasswordStandalone`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postResetPasswordWithEmail(
    body: PasswordResetWithEmailCommand,
    mailHash: string
  ): Promise<CoreResponse<CookieSuccess>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v1.1/account/ResetPasswordWithEmail`), {
      body: JSON.stringify(body),
      headers: {
        email: body.email,
        mailhash: mailHash
      },
      method: 'POST'
    });
  }
}

export default AccountManagementService;
