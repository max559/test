import { PAYMENT_OPTION } from '../order';

export enum PAYMENT_STATUS {
  Authenticated = 'authenticated',
  Authorized = 'authorized',
  Canceled = 'canceled',
  Chargedback = 'chargedback',
  Expired = 'expired',
  Failed = 'failed',
  NotCreated = 'notCreated',
  Open = 'open',
  Paid = 'paid',
  PartiallyChargedback = 'partiallyChargedback',
  PartiallyRefunded = 'partiallyRefunded',
  Pending = 'pending',
  Refunded = 'refunded'
}

export enum PAYMENT_TYPE {
  Charge = 'Charge',
  FirstRecurring = 'FirstRecurring',
  OneShot = 'OneShot'
}

export interface Payment {
  amount: number;
  currency?: string;
  customerId?: string;
  description?: string;
  email?: string;
  id?: string;
  mandateId?: string;
  methods: Array<PAYMENT_OPTION>;
  name?: string;
  paymentInfo?: string;
  paymentReceiver?: string;
  paymentType: PAYMENT_TYPE;
  redirectUrl?: string;
  source?: string;
  sourceReference?: string;
}

export interface PaymentRequest {
  amount: number;
  currency?: string;
  customerId?: string;
  description?: string;
  email?: string;
  id?: string;
  mandateId?: string;
  methods: Array<string>;
  name?: string;
  paymentInfo?: string;
  paymentReceiver?: string;
  paymentType: PAYMENT_TYPE;
  redirectUrl?: string;
  source?: string;
  sourceReference?: string;
}

export interface PaymentStatus {
  paymentId?: string;
  status: PAYMENT_STATUS;
  source?: string;
  sourceReference?: string;
}

export interface PaymentValidationData {
  paymentId: string;
  state: string;
  price: number;
  customerId: string;
  mandateId: string;
  method: string;
  interval: string;
  accountNumber: string;
  accountOwner: string;
  cardLabel: string;
  cardExpiration: string;
  cardNumber: string;
  creationDate: string; // Date
}
