import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { Payment, PaymentRequest, PaymentStatus } from './payment.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './payment.config';

export class PaymentService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public createPayment(body: PaymentRequest): Promise<CoreResponse<Payment>> {
    return coreFetch(new URL(`${this._baseUrl}/api/payments`), {
      body: JSON.stringify(body),
      method: 'POST',
      params: {
        'api-version': '2'
      }
    });
  }

  public createPaymentCallback(paymentId: string): Promise<CoreResponse<void>> {
    const formData = new FormData();

    formData.append('id', paymentId);

    return coreFetch(new URL(`${this._baseUrl}/api/callback`), {
      body: formData,
      isFormData: true,
      method: 'POST',
      params: {
        'api-version': '2'
      }
    });
  }

  public getPaymentStatus(paymentReference: string): Promise<CoreResponse<PaymentStatus>> {
    return coreFetch(new URL(`${this._baseUrl}/api/paymentstatusses/${paymentReference}`), {
      method: 'GET',
      params: {
        'api-version': '2'
      }
    });
  }
}

export default PaymentService;
