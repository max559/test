import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testpaymentservice.medienhausaachen.de',
    preview: 'https://previewpaymentservice.medienhausaachen.de',
    production: 'https://paymentservice.medienhausaachen.de'
  },
  co: {
    test: 'https://testpaymentservice.mediahuis.lu',
    preview: 'https://previewpaymentservice.mediahuis.lu',
    production: 'https://paymentservice.mediahuis.lu'
  },
  dl: {
    test: 'https://testpaymentservice.limburger.nl',
    preview: 'https://previewpaymentservice.limburger.nl',
    production: 'https://paymentservice.limburger.nl'
  },
  ds: {
    test: 'https://testpaymentservice.mediahuis.be',
    preview: 'https://previewpaymentservice.mediahuis.be',
    production: 'https://paymentservice.mediahuis.be'
  },
  gva: {
    test: 'https://testpaymentservice.mediahuis.be',
    preview: 'https://previewpaymentservice.mediahuis.be',
    production: 'https://paymentservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testpaymentservice.mediahuis.be',
    preview: 'https://previewpaymentservice.mediahuis.be',
    production: 'https://paymentservice.mediahuis.be'
  },
  lt: {
    test: 'https://testpaymentservice.mediahuis.lu',
    preview: 'https://previewpaymentservice.mediahuis.lu',
    production: 'https://paymentservice.mediahuis.lu'
  },
  lw: {
    test: 'https://testpaymentservice.mediahuis.lu',
    preview: 'https://previewpaymentservice.mediahuis.lu',
    production: 'https://paymentservice.mediahuis.lu'
  },
  nb: {
    test: 'https://testpaymentservice.mediahuis.be',
    preview: 'https://previewpaymentservice.mediahuis.be',
    production: 'https://paymentservice.mediahuis.be'
  },
  tc: {
    test: 'https://testpaymentservice.mediahuis.lu',
    preview: 'https://previewpaymentservice.mediahuis.lu',
    production: 'https://paymentservice.mediahuis.lu'
  }
};

export default baseUrlConfig;
