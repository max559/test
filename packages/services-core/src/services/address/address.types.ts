export interface Address {
  BoxNumber?: string;
  City: City;
  Country?: string;
  CountryISOCode?: string;
  HouseNumber?: string;
  Latitude?: number;
  Locality?: string;
  Longitude?: number;
  NiS9Code?: string;
  QualityCode?: string;
  Street: Street;
}

export interface AddressValidationRequest {
  AddressToValidate: Address;
}

export interface AddressValidationResponse {
  Information: InformationMessages;
  ValidationResult: AddressValidationResult;
}

export interface AddressValidationResult {
  ErrorMessage?: string;
  Information: InformationMessages;
  IsValid: boolean;
  ValidatedAddress: Address;
  ValidatedAddressKey?: string;
  ValidationLevel: 881850000 | 881850001 | 881850002 | 881850003;
  ValidationPerField?: Record<string, boolean>;
}

export interface CitiesResponse {
  Cities: Array<City>;
  Information: {
    EncounteredError: boolean;
    Messages: Array<{
      Code?: string;
      ErrorType: 0 | 1 | 2 | 3;
      MessageText?: string;
    }>;
  };
}

export interface City {
  CityId: number;
  Id: number;
  Name?: string;
  PostalCode?: string;
}

export interface CityRequest {
  Limit?: number;
  Key?: string;
  Language?: string;
}

export interface Country {
  IsoCode: string;
  Name: string;
}

export interface InformationMessage {
  Code?: string;
  ErrorType: 0 | 1 | 2 | 3;
  MessageText?: string;
}

export interface InformationMessages {
  EncounteredError: boolean;
  Messages?: Array<InformationMessage>;
}

export interface ResolveDutchAddressRequest {
  PostalCode: string;
  HouseNumber: number;
}

export interface ResolveDutchAddressResponse {
  Information: InformationMessages;
  Address: Address;
}

export interface Street {
  Name?: string;
  StreetId: number;
}

export interface StreetRequest {
  CityId?: number;
  Key?: string;
  Language?: string;
  Limit?: number;

  PostalCode?: string;
}

export interface StreetsResponse {
  Information: {
    EncounteredError: boolean;
    Messages: Array<{
      Code?: string;
      ErrorType: 0 | 1 | 2 | 3;
      MessageText?: string;
    }>;
  };
  Streets: Array<Street>;
}
