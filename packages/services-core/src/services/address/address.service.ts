import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type {
  AddressValidationRequest,
  AddressValidationResponse,
  CitiesResponse,
  CityRequest,
  Country,
  ResolveDutchAddressRequest,
  ResolveDutchAddressResponse,
  StreetRequest,
  StreetsResponse
} from './address.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './address.config';

export class AddressService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getCountriesByCode(
    countryCode: string,
    languageCode?: string
  ): Promise<CoreResponse<Array<Country>>> {
    return coreFetch(new URL(`${this._baseUrl}/countries/${countryCode}`), {
      method: 'GET',
      params: {
        languageCode
      }
    });
  }

  public getCountriesByLanguage(language: string): Promise<CoreResponse<Array<Country>>> {
    return coreFetch(new URL(`${this._baseUrl}/countries/language/${language}`), {
      method: 'GET'
    });
  }

  public postAddressValidate(
    body: AddressValidationRequest
  ): Promise<CoreResponse<AddressValidationResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/address/validate`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postCitiesAutocomplete(
    body: CityRequest,
    countryIsoCode: string
  ): Promise<CoreResponse<CitiesResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/address/autocomplete/cities/${countryIsoCode}`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postResolveDutchAddress(
    body: ResolveDutchAddressRequest
  ): Promise<CoreResponse<ResolveDutchAddressResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/address/resolveDutchAddress`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }

  public postStreetsAutocomplete(
    body: StreetRequest,
    countryIsoCode: string
  ): Promise<CoreResponse<StreetsResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/address/autocomplete/streets/${countryIsoCode}`), {
      body: JSON.stringify(body),
      method: 'POST'
    });
  }
}

export default AddressService;
