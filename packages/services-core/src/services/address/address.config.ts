import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  co: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  dl: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  ds: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  gva: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  lt: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  lw: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  nb: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  },
  tc: {
    test: 'https://testaddressservice.mediahuis.be',
    preview: 'https://previewaddressservice.mediahuis.be',
    production: 'https://addressservice.mediahuis.be'
  }
};

export default baseUrlConfig;
