import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: '',
    preview: '',
    production: ''
  },
  co: {
    test: '',
    preview: '',
    production: ''
  },
  dl: {
    test: 'https://testsubscriptionservice.limburger.nl',
    preview: 'https://previewsubscriptionservice.limburger.nl',
    production: 'https://subscriptionservice.limburger.nl'
  },
  ds: {
    test: 'https://testsubscriptionservice.standaard.be',
    preview: 'https://previewsubscriptionservice.standaard.be',
    production: 'https://subscriptionservice.standaard.be'
  },
  gva: {
    test: 'https://testsubscriptionservice.gva.be',
    preview: 'https://previewsubscriptionservice.gva.be',
    production: 'https://subscriptionservice.gva.be'
  },
  hbvl: {
    test: 'https://testsubscriptionservice.hbvl.be',
    preview: 'https://previewsubscriptionservice.hbvl.be',
    production: 'https://subscriptionservice.hbvl.be'
  },
  lt: {
    test: 'https://testsubscriptionservice.luxtimes.lu',
    preview: 'https://previewsubscriptionservice.luxtimes.lu',
    production: 'https://subscriptionservice.luxtimes.lu'
  },
  lw: {
    test: 'https://testsubscriptionservice.wort.lu',
    preview: 'https://previewsubscriptionservice.wort.lu',
    production: 'https://subscriptionservice.wort.lu'
  },
  nb: {
    test: 'https://testsubscriptionservice.nieuwsblad.be',
    preview: 'https://previewsubscriptionservice.nieuwsblad.be',
    production: 'https://subscriptionservice.nieuwsblad.be'
  },
  tc: {
    test: '',
    preview: '',
    production: ''
  }
};

export default baseUrlConfig;
