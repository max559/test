import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type {
  SubscriptionAddressRequest,
  SubscriptionInfo,
  SubscriptionLinkResponse,
  SubscriptionStatus
} from './subscription.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './subscription.config';

export class SubscriptionService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postAccount(accountId: string, idToken?: string): Promise<CoreResponse<SubscriptionInfo>> {
    const headers: HeadersInit = {};
    if (idToken) {
      headers['Authorization'] = idToken;
    }

    return coreFetch(new URL(`${this._baseUrl}/api/account/${accountId}`), {
      credentials: 'include',
      headers,
      method: 'POST'
    });
  }

  public postLinkAccountToSubscription(
    body: SubscriptionAddressRequest,
    idToken?: string
  ): Promise<CoreResponse<SubscriptionLinkResponse>> {
    const headers: HeadersInit = {};
    if (idToken) {
      headers['Authorization'] = idToken;
    }
    return coreFetch(
      new URL(`${this._baseUrl}/api/subscription/linkaccounttosubscriptionwithaddress`),
      {
        body: JSON.stringify(body),
        credentials: 'include',
        headers,
        method: 'POST'
      }
    );
  }

  public postSubscriptionsByAddressAndNumber(
    body: SubscriptionAddressRequest,
    idToken?: string
  ): Promise<CoreResponse<SubscriptionStatus>> {
    const headers: HeadersInit = {};
    if (idToken) {
      headers['Authorization'] = idToken;
    }

    return coreFetch(
      new URL(`${this._baseUrl}/api/subscription/getsubscriptionsbyaddressandsubscriptionnumber`),
      {
        body: JSON.stringify(body),
        credentials: 'include',
        headers,
        method: 'POST'
      }
    );
  }
}

export default SubscriptionService;
