export enum SUBSCRIPTION_STATUS {
  FoundButLinked = 'FoundButLinked',
  FoundNoAccess = 'FoundNoAccess',
  FoundNotLinked = 'FoundNotLinked',
  NotFound = 'NotFound'
}

export interface SubscriptionAddressRequest {
  accountGuid?: string;
  address: {
    boxNumber?: string;
    city?: string;
    countryCode?: string;
    houseNumber?: string;
    street?: string;
    zipCode?: string;
  };
  subscriptionNumber: string;
}

export interface SubscriptionInfo {
  accountGuid: string;
  subscription?: {
    type: string;
  };
}

export interface SubscriptionLinkResponse {
  isLinked: boolean;
}

export interface SubscriptionStatus {
  status: SUBSCRIPTION_STATUS;
  subscriptions?: Array<{
    name: string;
    subscriptionNumber: string;
    type: string;
  }>;
}
