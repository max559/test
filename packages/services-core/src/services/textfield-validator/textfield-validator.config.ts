import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testtextfieldvalidator.medienhausaachen.de',
    preview: 'https://previewtextfieldvalidator.medienhausaachen.de',
    production: 'https://textfieldvalidator.medienhausaachen.de'
  },
  co: {
    test: 'https://testtextfieldvalidator.mediahuis.lu',
    preview: 'https://previewtextfieldvalidator.mediahuis.lu',
    production: 'https://textfieldvalidator.mediahuis.lu'
  },
  dl: {
    test: 'https://testtextfieldvalidator.limburger.nl',
    preview: 'https://previewtextfieldvalidator.limburger.nl',
    production: 'https://textfieldvalidator.limburger.nl'
  },
  ds: {
    test: 'https://testtextfieldvalidator.mediahuis.be',
    preview: 'https://previewtextfieldvalidator.mediahuis.be',
    production: 'https://textfieldvalidator.mediahuis.be'
  },
  gva: {
    test: 'https://testtextfieldvalidator.mediahuis.be',
    preview: 'https://previewtextfieldvalidator.mediahuis.be',
    production: 'https://textfieldvalidator.mediahuis.be'
  },
  hbvl: {
    test: 'https://testtextfieldvalidator.mediahuis.be',
    preview: 'https://previewtextfieldvalidator.mediahuis.be',
    production: 'https://textfieldvalidator.mediahuis.be'
  },
  lt: {
    test: 'https://testtextfieldvalidator.mediahuis.lu',
    preview: 'https://previewtextfieldvalidator.mediahuis.lu',
    production: 'https://textfieldvalidator.mediahuis.lu'
  },
  lw: {
    test: 'https://testtextfieldvalidator.mediahuis.lu',
    preview: 'https://previewtextfieldvalidator.mediahuis.lu',
    production: 'https://textfieldvalidator.mediahuis.lu'
  },
  nb: {
    test: 'https://testtextfieldvalidator.mediahuis.be',
    preview: 'https://previewtextfieldvalidator.mediahuis.be',
    production: 'https://textfieldvalidator.mediahuis.be'
  },
  tc: {
    test: 'https://testtextfieldvalidator.mediahuis.lu',
    preview: 'https://previewtextfieldvalidator.mediahuis.lu',
    production: 'https://textfieldvalidator.mediahuis.lu'
  }
};

export default baseUrlConfig;
