import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import { TextfieldValidation } from '../order';
import baseUrlConfig from './textfield-validator.config';

export class TextfieldValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postTextfieldValidation(
    body: TextfieldValidation
  ): Promise<CoreResponse<TextfieldValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/textfieldvalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }
}

export default TextfieldValidatorService;
