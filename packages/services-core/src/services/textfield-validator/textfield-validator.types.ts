export type TextfieldKey = 'incentivePhoneNumber' | 'other' | 'phoneNumber' | 'primaryAgreementId';

export interface TextfieldData {
  value?: string;
}
