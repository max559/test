import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import { FileValidation } from '../order';
import baseUrlConfig from './file-validator.config';

export class FileValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postFileValidation(body: FileValidation): Promise<CoreResponse<FileValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/filevalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }
}

export default FileValidatorService;
