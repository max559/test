import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testfilevalidator.medienhausaachen.de',
    preview: 'https://previewfilevalidator.medienhausaachen.de',
    production: 'https://filevalidator.medienhausaachen.de'
  },
  co: {
    test: 'https://testfilevalidator.mediahuis.lu',
    preview: 'https://previewfilevalidator.mediahuis.lu',
    production: 'https://filevalidator.mediahuis.lu'
  },
  dl: {
    test: 'https://testfilevalidator.limburger.nl',
    preview: 'https://previewfilevalidator.limburger.nl',
    production: 'https://filevalidator.limburger.nl'
  },
  ds: {
    test: 'https://testfilevalidator.mediahuis.be',
    preview: 'https://previewfilevalidator.mediahuis.be',
    production: 'https://filevalidator.mediahuis.be'
  },
  gva: {
    test: 'https://testfilevalidator.mediahuis.be',
    preview: 'https://previewfilevalidator.mediahuis.be',
    production: 'https://filevalidator.mediahuis.be'
  },
  hbvl: {
    test: 'https://testfilevalidator.mediahuis.be',
    preview: 'https://previewfilevalidator.mediahuis.be',
    production: 'https://filevalidator.mediahuis.be'
  },
  lt: {
    test: 'https://testfilevalidator.mediahuis.lu',
    preview: 'https://previewfilevalidator.mediahuis.lu',
    production: 'https://filevalidator.mediahuis.lu'
  },
  lw: {
    test: 'https://testfilevalidator.mediahuis.lu',
    preview: 'https://previewfilevalidator.mediahuis.lu',
    production: 'https://filevalidator.mediahuis.lu'
  },
  nb: {
    test: 'https://testfilevalidator.mediahuis.be',
    preview: 'https://previewfilevalidator.mediahuis.be',
    production: 'https://filevalidator.mediahuis.be'
  },
  tc: {
    test: 'https://testfilevalidator.mediahuis.lu',
    preview: 'https://previewfilevalidator.mediahuis.lu',
    production: 'https://filevalidator.mediahuis.lu'
  }
};

export default baseUrlConfig;
