import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testoptin.aachener-zeitung.de',
    preview: 'https://previewoptin.aachener-zeitung.de',
    production: 'https://optin.aachener-zeitung.de'
  },
  co: {
    test: 'https://testoptin.contacto.lu',
    preview: 'https://previewoptin.contacto.lu',
    production: 'https://optin.contacto.lu'
  },
  dl: {
    test: 'https://testoptin.limburger.nl',
    preview: 'https://previewoptin.limburger.nl',
    production: 'https://optin.limburger.nl'
  },
  ds: {
    test: 'https://testoptin.standaard.be',
    preview: 'https://previewoptin.standaard.be',
    production: 'https://optin.standaard.be'
  },
  gva: {
    test: 'https://testoptin.gva.be',
    preview: 'https://previewoptin.gva.be',
    production: 'https://optin.gva.be'
  },
  hbvl: {
    test: 'https://testoptin.hbvl.be',
    preview: 'https://previewoptin.hbvl.be',
    production: 'https://optin.hbvl.be'
  },
  lt: {
    test: 'https://testoptin.luxtimes.lu',
    preview: 'https://previewoptin.luxtimes.lu',
    production: 'https://optin.luxtimes.lu'
  },
  lw: {
    test: 'https://testoptin.wort.lu',
    preview: 'https://previewoptin.wort.lu',
    production: 'https://optin.wort.lu'
  },
  nb: {
    test: 'https://testoptin.nieuwsblad.be',
    preview: 'https://previewoptin.nieuwsblad.be',
    production: 'https://optin.nieuwsblad.be'
  },
  tc: {
    test: 'https://testoptin.telecran.lu',
    preview: 'https://previewoptin.telecran.lu',
    production: 'https://optin.telecran.lu'
  }
};

export default baseUrlConfig;
