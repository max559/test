export interface NewsletterOptin {
  NewsletterCode?: string;
  Status: -1 | 0 | 1;
}

export interface OptinAmSaveRequest {
  CampaignCode?: string;
  Newsletters?: Array<NewsletterOptin>;
}

export interface OptinAmSaveResponse {
  Value: Array<NewsletterOptin>;
}

export interface OptinSaveRequest {
  AccountGuid: string;
  Brand: string;
  CampaignCode?: string;
  Newsletters?: Array<NewsletterOptin>;
}

export interface OptinSaveResponse {
  Value: Array<NewsletterOptin>;
}
