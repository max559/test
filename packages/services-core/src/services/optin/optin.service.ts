import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type {
  OptinAmSaveRequest,
  OptinAmSaveResponse,
  OptinSaveRequest,
  OptinSaveResponse
} from './optin.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './optin.config';

export class OptinService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postOptinSave(body: OptinSaveRequest): Promise<CoreResponse<OptinSaveResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v3/optin/save`), {
      body: JSON.stringify(body),
      credentials: 'include',
      method: 'POST'
    });
  }

  public postOptinAmSave(body: OptinAmSaveRequest): Promise<CoreResponse<OptinAmSaveResponse>> {
    return coreFetch(new URL(`${this._baseUrl}/api/v3/optinopenam/save`), {
      body: JSON.stringify(body),
      credentials: 'include',
      method: 'POST'
    });
  }
}

export default OptinService;
