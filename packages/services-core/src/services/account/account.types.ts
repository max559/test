export interface LoginDto {
  lang?: string;
  redirectUrl?: string;
}

export interface UserInfo {
  user: {
    user: {
      contactDetails: {
        firstName?: string;
        gender?: string;
        lastName?: string;
        preferredLanguage?: string;
      };
      email?: string;
      id?: string;
    };
  };
}
