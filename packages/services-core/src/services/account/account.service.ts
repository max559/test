import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { LoginDto, UserInfo } from './account.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './account.config';

export class AccountService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getAccountInfo(): Promise<CoreResponse<UserInfo>> {
    return coreFetch(new URL(`${this._baseUrl}/api/account/info`), {
      credentials: 'include',
      method: 'GET',
      params: {
        'api-version': '2'
      }
    });
  }

  public postLogin(body: LoginDto): Promise<CoreResponse<LoginDto>> {
    return coreFetch(new URL(`${this._baseUrl}/api/auth/login`), {
      credentials: 'include',
      body: JSON.stringify(body),
      method: 'POST',
      params: {
        'api-version': '2'
      }
    });
  }
}

export default AccountService;
