import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testaccountservice.aachener-zeitung.de',
    preview: 'https://previewaccountservice.aachener-zeitung.de',
    production: 'https://accountservice.aachener-zeitung.de'
  },
  co: {
    test: 'https://testaccountservice.contacto.lu',
    preview: 'https://previewaccountservice.contacto.lu',
    production: 'https://accountservice.contacto.lu'
  },
  dl: {
    test: 'https://testaccountservice.limburger.nl',
    preview: 'https://previewaccountservice.limburger.nl',
    production: 'https://accountservice.limburger.nl'
  },
  ds: {
    test: 'https://testaccountservice.standaard.be',
    preview: 'https://previewaccountservice.standaard.be',
    production: 'https://accountservice.standaard.be'
  },
  gva: {
    test: 'https://testaccountservice.gva.be',
    preview: 'https://previewaccountservice.gva.be',
    production: 'https://accountservice.gva.be'
  },
  hbvl: {
    test: 'https://testaccountservice.hbvl.be',
    preview: 'https://previewaccountservice.hbvl.be',
    production: 'https://accountservice.hbvl.be'
  },
  lt: {
    test: 'https://testaccountservice.luxtimes.lu',
    preview: 'https://previewaccountservice.luxtimes.lu',
    production: 'https://accountservice.luxtimes.lu'
  },
  lw: {
    test: 'https://testaccountservice.wort.lu',
    preview: 'https://previewaccountservice.wort.lu',
    production: 'https://accountservice.wort.lu'
  },
  nb: {
    test: 'https://testaccountservice.nieuwsblad.be',
    preview: 'https://previewaccountservice.nieuwsblad.be',
    production: 'https://accountservice.nieuwsblad.be'
  },
  tc: {
    test: 'https://testaccountservice.telecran.lu',
    preview: 'https://previewaccountservice.telecran.lu',
    production: 'https://accountservice.telecran.lu'
  }
};

export default baseUrlConfig;
