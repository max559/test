import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import { PreconditionsValidation } from '../order';
import baseUrlConfig from './precondition-validator.config';

export class PreconditionValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postPreconditionValidation(
    body: PreconditionsValidation,
    idToken?: string
  ): Promise<CoreResponse<PreconditionsValidation>> {
    const baseUrl = this._baseUrl;
    const headers: HeadersInit = { 'x-api-version': '2' };

    if (idToken) {
      headers['Authorization'] = idToken;
    }

    return coreFetch(new URL(`${baseUrl}/api/preconditionvalidation`), {
      body: JSON.stringify(body),
      credentials: 'include',
      headers,
      method: 'POST'
    });
  }
}

export default PreconditionValidatorService;
