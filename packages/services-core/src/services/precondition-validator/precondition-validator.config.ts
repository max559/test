import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testpreconditionvalidator.aachener-zeitung.de',
    preview: 'https://previewpreconditionvalidator.aachener-zeitung.de',
    production: 'https://preconditionvalidator.aachener-zeitung.de'
  },
  co: {
    test: 'https://testpreconditionvalidator.contacto.lu',
    preview: 'https://previewpreconditionvalidator.contacto.lu',
    production: 'https://preconditionvalidator.contacto.lu'
  },
  dl: {
    test: 'https://testpreconditionvalidator.limburger.nl',
    preview: 'https://previewpreconditionvalidator.limburger.nl',
    production: 'https://preconditionvalidator.limburger.nl'
  },
  ds: {
    test: 'https://testpreconditionvalidator.standaard.be',
    preview: 'https://previewpreconditionvalidator.standaard.be',
    production: 'https://preconditionvalidator.standaard.be'
  },
  gva: {
    test: 'https://testpreconditionvalidator.gva.be',
    preview: 'https://previewpreconditionvalidator.gva.be',
    production: 'https://preconditionvalidator.gva.be'
  },
  hbvl: {
    test: 'https://testpreconditionvalidator.hbvl.be',
    preview: 'https://previewpreconditionvalidator.hbvl.be',
    production: 'https://preconditionvalidator.hbvl.be'
  },
  lt: {
    test: 'https://testpreconditionvalidator.luxtimes.lu',
    preview: 'https://previewpreconditionvalidator.luxtimes.lu',
    production: 'https://preconditionvalidator.luxtimes.lu'
  },
  lw: {
    test: 'https://testpreconditionvalidator.wort.lu',
    preview: 'https://previewpreconditionvalidator.wort.lu',
    production: 'https://preconditionvalidator.wort.lu'
  },
  nb: {
    test: 'https://testpreconditionvalidator.nieuwsblad.be',
    preview: 'https://previewpreconditionvalidator.nieuwsblad.be',
    production: 'https://preconditionvalidator.nieuwsblad.be'
  },
  tc: {
    test: 'https://testpreconditionvalidator.telecran.lu',
    preview: 'https://previewpreconditionvalidator.telecran.lu',
    production: 'https://preconditionvalidator.telecran.lu'
  }
};

export default baseUrlConfig;
