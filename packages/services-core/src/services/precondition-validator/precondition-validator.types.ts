export interface PreconditionValidationData {
  accountId?: string;
  boxNumber?: string;
  email?: string;
  errorMessage?: string;
  houseNumber?: string;
  postalCode?: string;
  ruleName?: string;
  street?: string;
  subscriptionFormulaId?: number;
}

export interface PreconditionValidationError {
  errors?: Array<string>;
  fallbackOfferId?: number;
}
