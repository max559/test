import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { ContentLinkResult } from './content.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './content.config';

export class ContentService {
  private _baseUrl: string;
  private _brand: Brand;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
    this._brand = brand;
  }

  public getContentLinks(key: string): Promise<CoreResponse<ContentLinkResult>> {
    return coreFetch(new URL(`${this._baseUrl}/api/contentlinks`), {
      method: 'GET',
      params: {
        brand: this._brand,
        key
      }
    });
  }
}

export default ContentService;
