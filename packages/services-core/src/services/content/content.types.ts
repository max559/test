export interface Content {
  brand?: string;
  contentLinks?: Array<ContentLink>;
  id: number;
  name?: string;
  value?: string;
}

export interface ContentLink {
  brand?: string;
  content: Content;
  contentId: number;
  id: number;
  key?: string;
  location?: string;
  position: number;
}

export interface ContentLinkResult {
  contentLinks?: {
    [location: string]: Array<ContentLink>;
  };
  key?: string;
}
