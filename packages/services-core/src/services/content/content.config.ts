import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testcontentservice.medienhausaachen.de',
    preview: 'https://previewcontentservice.medienhausaachen.de',
    production: 'https://contentservice.medienhausaachen.de'
  },
  co: {
    test: 'https://testcontentservice.mediahuis.lu',
    preview: 'https://previewcontentservice.mediahuis.lu',
    production: 'https://contentservice.mediahuis.lu'
  },
  dl: {
    test: 'https://testcontentservice.limburger.nl',
    preview: 'https://previewcontentservice.limburger.nl',
    production: 'https://contentservice.limburger.nl'
  },
  ds: {
    test: 'https://testcontentservice.mediahuis.be',
    preview: 'https://previewcontentservice.mediahuis.be',
    production: 'https://contentservice.mediahuis.be'
  },
  gva: {
    test: 'https://testcontentservice.mediahuis.be',
    preview: 'https://previewcontentservice.mediahuis.be',
    production: 'https://contentservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testcontentservice.mediahuis.be',
    preview: 'https://previewcontentservice.mediahuis.be',
    production: 'https://contentservice.mediahuis.be'
  },
  lt: {
    test: 'https://testcontentservice.mediahuis.lu',
    preview: 'https://previewcontentservice.mediahuis.lu',
    production: 'https://contentservice.mediahuis.lu'
  },
  lw: {
    test: 'https://testcontentservice.mediahuis.lu',
    preview: 'https://previewcontentservice.mediahuis.lu',
    production: 'https://contentservice.mediahuis.lu'
  },
  nb: {
    test: 'https://testcontentservice.mediahuis.be',
    preview: 'https://previewcontentservice.mediahuis.be',
    production: 'https://contentservice.mediahuis.be'
  },
  tc: {
    test: 'https://testcontentservice.mediahuis.lu',
    preview: 'https://previewcontentservice.mediahuis.lu',
    production: 'https://contentservice.mediahuis.lu'
  }
};

export default baseUrlConfig;
