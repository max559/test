import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testofferservice.medienhausaachen.de',
    preview: 'https://previewofferservice.medienhausaachen.de',
    production: 'https://offerservice.medienhausaachen.de'
  },
  co: {
    test: 'https://testofferservice.mediahuis.lu',
    preview: 'https://previewofferservice.mediahuis.lu',
    production: 'https://offerservice.mediahuis.lu'
  },
  dl: {
    test: 'https://testofferservice.limburger.nl',
    preview: 'https://previewofferservice.limburger.nl',
    production: 'https://offerservice.limburger.nl'
  },
  ds: {
    test: 'https://testofferservice.mediahuis.be',
    preview: 'https://previewofferservice.mediahuis.be',
    production: 'https://offerservice.mediahuis.be'
  },
  gva: {
    test: 'https://testofferservice.mediahuis.be',
    preview: 'https://previewofferservice.mediahuis.be',
    production: 'https://offerservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testofferservice.mediahuis.be',
    preview: 'https://previewofferservice.mediahuis.be',
    production: 'https://offerservice.mediahuis.be'
  },
  lt: {
    test: 'https://testofferservice.mediahuis.lu',
    preview: 'https://previewofferservice.mediahuis.lu',
    production: 'https://offerservice.mediahuis.lu'
  },
  lw: {
    test: 'https://testofferservice.mediahuis.lu',
    preview: 'https://previewofferservice.mediahuis.lu',
    production: 'https://offerservice.mediahuis.lu'
  },
  nb: {
    test: 'https://testofferservice.mediahuis.be',
    preview: 'https://previewofferservice.mediahuis.be',
    production: 'https://offerservice.mediahuis.be'
  },
  tc: {
    test: 'https://testofferservice.mediahuis.lu',
    preview: 'https://previewofferservice.mediahuis.lu',
    production: 'https://offerservice.mediahuis.lu'
  }
};

export default baseUrlConfig;
