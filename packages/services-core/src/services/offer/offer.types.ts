import type { TextfieldKey } from '../textfield-validator';

import { PAYMENT_OPTION } from '../order';

export enum OFFER_ITEM_TYPE {
  SubscriptionType = 'SubscriptionType',
  SubscriptionFormula = 'SubscriptionFormula'
}

export enum PRICE_TYPE {
  Free = 'Free',
  Incentive = 'Incentive',
  LinkedSale = 'LinkedSale',
  NotSpecified = 'NotSpecified',
  Promo = 'Promo',
  Regular = 'Regular',
  Stunt = 'Stunt',
  VoucherPurchase = 'VoucherPurchase',
  VoucherRedemption = 'VoucherRedemption'
}

export enum REQUIREMENT_TYPE {
  AddressArea = 'AddressArea',
  Boolean = 'Boolean',
  Choice = 'Choice',
  Date = 'Date',
  Email = 'Email',
  File = 'File',
  InvoiceDetails = 'InvoiceDetails',
  Login = 'Login',
  Payment = 'Payment',
  Preconditions = 'Preconditions',
  Textfield = 'Textfield',
  Voucher = 'Voucher'
}

export interface Feature {
  brand?: string;
  description?: string;
  id: number;
  name?: string;
  negativeDescription?: string;
}

export interface Offer {
  active: boolean;
  brand?: string;
  description?: string;
  endDate?: string; // Date
  extra?: string;
  htmlContentBlocks?: Array<OfferContentBlock>;
  id: number;
  items?: Array<OfferItem>;
  language?: string;
  metaDescription?: string;
  metaTitle?: string;
  published: boolean;
  reference?: string;
  slug?: string;
  startDate?: string; // Date
  title?: string;
  type?: 'acquisition' | 'renewal' | 'winback';
}

export interface OfferContentBlock {
  block?: string;
  htmlContentId: number;
  id: number;
  offerId: number;
  sequenceIndex: number;
}

export interface OfferItem extends RequirementsContainer {
  brand?: string;
  buttonLabel?: string;
  costCenter?: string;
  description?: string;
  duration?: number;
  durationType?: string;
  endDate?: string; // Date
  externalReference?: string;
  extra?: string;
  features?: Array<SortableFeature>;
  hash?: string;
  id: number;
  image?: string;
  incentiveDescription?: string;
  isFixedTerm?: boolean;
  isHighlighted: boolean;
  isMultiBrand: boolean;
  label?: string;
  lastModifiedBy?: string;
  logo?: string;
  lumpSum?: number;
  name?: string;
  nextStep?: string;
  parent?: string;
  priceExtra?: string;
  priceSentence?: string;
  priceType: PRICE_TYPE;
  projectedLifetimeValue?: number;
  sequenceIndex: number;
  startDate?: string; // Date
  title?: string;
  type?: OFFER_ITEM_TYPE;
}

export interface SortableFeature extends Feature {
  sequenceIndex: number;
}

export interface SubscriptionFormula extends RequirementsContainer {
  brand?: string;
  costCenter?: string;
  description?: string;
  duration: number;
  durationType: number; // Enum 0 | 1 | 2 | 3 | 4
  endDate?: string; // Date
  externalReference?: string;
  extra?: string;
  id: number;
  incentiveDescription?: string;
  isFixedTerm?: boolean;
  lastModifiedBy?: string;
  lumpSum?: number;
  parent?: string;
  priceExtra?: string;
  priceSentence?: string;
  priceType: number; // Enum  0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
  reference?: string;
  startDate?: string; // Date
  subscriptionTypes?: Array<SubscriptionType>;
  title?: string;
}

export interface SubscriptionType extends RequirementsContainer {
  brand?: string;
  description?: string;
  extra?: string;
  features?: Array<SortableFeature>;
  id: number;
  image?: string;
  lastModifiedBy?: string;
  logo?: string;
  priceExtra?: string;
  priceSentence?: string;
  reference?: string;
  title?: string;
}

export interface RequirementsContainer {
  addressAreaRequirement?: AddressAreaRequirement;
  addressAreaRequirements?: Array<AddressAreaRequirement>;
  booleanRequirement?: BooleanRequirement;
  booleanRequirements?: Array<BooleanRequirement>;
  choiceRequirements?: Array<ChoiceRequirement>;
  dateRequirement?: DateRequirement;
  dateRequirements?: Array<DateRequirement>;
  emailRequirement?: EmailRequirement;
  fileRequirement?: FileRequirement;
  fileRequirements?: Array<FileRequirement>;
  invoiceDetailsRequirement?: InvoiceDetailsRequirement;
  loginRequirement?: LoginRequirement;
  paymentRequirement?: PaymentRequirement;
  preconditionsRequirement?: PreconditionsRequirement;
  textfieldRequirements?: Array<TextfieldRequirement>;
  voucherRequirement?: VoucherRequirement;
}

export interface BaseRequirement {
  brand: string;
  description: string;
  id: number;
  name: string;
  subscriptionFormulaId?: number;
  subscriptionTypeId: number;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface EmailRequirement extends BaseRequirement {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface LoginRequirement extends BaseRequirement {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InvoiceDetailsRequirement extends BaseRequirement {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface PreconditionsRequirement extends BaseRequirement {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FileRequirement extends BaseRequirement {}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface VoucherRequirement extends BaseRequirement {}

export interface DateRequirement extends BaseRequirement {
  timeFrameStartCalculated: string; // Date
  timeFrameEndCalculated: string; // Date
}

export interface AddressAreaRequirement extends BaseRequirement {
  pickUpAllowed: boolean;
  hasDeliveryArea: boolean;
  isDeliveryAddress: boolean;
  countries?: Array<string>;
}

export interface PaymentRequirement extends BaseRequirement {
  amount: number;
  oneShotPaymentOptions: Array<PAYMENT_OPTION>;
  recurringPaymentOptions: Array<PAYMENT_OPTION>;
}

export interface BooleanRequirement extends BaseRequirement {
  required: boolean;
  errorMessage?: string;
}

export interface TextfieldRequirement extends BaseRequirement {
  key: TextfieldKey;
}

export interface ChoiceRequirement extends BaseRequirement {
  key: string;
  choices: Array<string>;
}
