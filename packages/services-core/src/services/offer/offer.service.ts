import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { Offer, OfferItem } from './offer.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './offer.config';
import { OFFER_ITEM_TYPE } from './offer.types';

export interface getOfferBySlugParams {
  slug: string;
}

export interface getOfferItemParams {
  id?: string | number;
  type?: OFFER_ITEM_TYPE;
}

const getLanguage = (brand: Brand): string => {
  if (brand === 'az') {
    return 'de';
  }
  if (brand === 'co') {
    return 'pt';
  }
  if (brand === 'lt') {
    return 'en';
  }
  if (brand === 'lw') {
    return 'de';
  }
  if (brand === 'tc') {
    return 'de';
  }
  return 'nl';
};

export class OfferService {
  private _baseUrl: string;
  private _brand: Brand;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
    this._brand = brand;
  }

  public getOffer(id: number): Promise<CoreResponse<Offer>> {
    return coreFetch(new URL(`${this._baseUrl}/api/offers/${id}`), {
      method: 'GET',
      params: {
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getOfferBySlug(params: getOfferBySlugParams): Promise<CoreResponse<Offer>> {
    return coreFetch(new URL(`${this._baseUrl}/api/offers/${params.slug}`), {
      method: 'GET',
      params: {
        brand: this._brand,
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getOfferItem(params: getOfferItemParams): Promise<CoreResponse<OfferItem>> {
    return coreFetch(new URL(`${this._baseUrl}/api/offeritems/${params.type}/${params.id}`), {
      method: 'GET',
      params: {
        brand: this._brand,
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getOfferItemBySlug(slug: string): Promise<CoreResponse<OfferItem>> {
    return coreFetch(new URL(`${this._baseUrl}${slug}`), {
      method: 'GET',
      params: {
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getVitrineOffer(): Promise<CoreResponse<Offer>> {
    return coreFetch<Offer>(new URL(`${this._baseUrl}/api/offers/aboshop-vitrine`), {
      method: 'GET',
      params: {
        brand: this._brand,
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getOfferItemBySubscriptionFormulaId(
    id: string | number
  ): Promise<CoreResponse<OfferItem>> {
    return coreFetch(new URL(`${this._baseUrl}/api/offeritems/SubscriptionFormula/${id}`), {
      method: 'GET',
      params: {
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }

  public getOfferItemBySubscriptionTypeId(id: string | number): Promise<CoreResponse<OfferItem>> {
    return coreFetch(new URL(`${this._baseUrl}/api/offeritems/SubscriptionType/${id}`), {
      method: 'GET',
      params: {
        lang: getLanguage(this._brand),
        'api-version': '2.0'
      }
    });
  }
}

export default OfferService;
