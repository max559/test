import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testdefaultvalidator.medienhausaachen.de',
    preview: 'https://previewdefaultvalidator.medienhausaachen.de',
    production: 'https://defaultvalidator.medienhausaachen.de'
  },
  co: {
    test: 'https://testdefaultvalidator.mediahuis.lu',
    preview: 'https://previewdefaultvalidator.mediahuis.lu',
    production: 'https://defaultvalidator.mediahuis.lu'
  },
  dl: {
    test: 'https://testdefaultvalidator.limburger.nl',
    preview: 'https://previewdefaultvalidator.limburger.nl',
    production: 'https://defaultvalidator.limburger.nl'
  },
  ds: {
    test: 'https://testdefaultvalidator.mediahuis.be',
    preview: 'https://previewdefaultvalidator.mediahuis.be',
    production: 'https://defaultvalidator.mediahuis.be'
  },
  gva: {
    test: 'https://testdefaultvalidator.mediahuis.be',
    preview: 'https://previewdefaultvalidator.mediahuis.be',
    production: 'https://defaultvalidator.mediahuis.be'
  },
  hbvl: {
    test: 'https://testdefaultvalidator.mediahuis.be',
    preview: 'https://previewdefaultvalidator.mediahuis.be',
    production: 'https://defaultvalidator.mediahuis.be'
  },
  lt: {
    test: 'https://testdefaultvalidator.mediahuis.lu',
    preview: 'https://previewdefaultvalidator.mediahuis.lu',
    production: 'https://defaultvalidator.mediahuis.lu'
  },
  lw: {
    test: 'https://testdefaultvalidator.mediahuis.lu',
    preview: 'https://previewdefaultvalidator.mediahuis.lu',
    production: 'https://defaultvalidator.mediahuis.lu'
  },
  nb: {
    test: 'https://testdefaultvalidator.mediahuis.be',
    preview: 'https://previewdefaultvalidator.mediahuis.be',
    production: 'https://defaultvalidator.mediahuis.be'
  },
  tc: {
    test: 'https://testdefaultvalidator.mediahuis.lu',
    preview: 'https://previewdefaultvalidator.mediahuis.lu',
    production: 'https://defaultvalidator.mediahuis.lu'
  }
};

export default baseUrlConfig;
