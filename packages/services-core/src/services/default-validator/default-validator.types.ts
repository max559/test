export interface BoolData {
  value: boolean;
}

export interface DateData {
  value?: string;
}

export interface EmailData {
  value?: string;
}

export interface ChoiceData {
  value?: string;
}