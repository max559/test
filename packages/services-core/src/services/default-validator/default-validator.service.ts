import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './default-validator.config';
import { BooleanValidation, ChoiceValidation, DateValidation, EmailValidation } from '../order';

export class DefaultValidatorService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public postBoolValidation(body: BooleanValidation): Promise<CoreResponse<BooleanValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/boolvalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }

  public postChoiceValidation(body: ChoiceValidation): Promise<CoreResponse<ChoiceValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/choicevalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }

  public postDateValidation(body: DateValidation): Promise<CoreResponse<DateValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/datevalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }

  public postEmailValidation(body: EmailValidation): Promise<CoreResponse<EmailValidation>> {
    return coreFetch(new URL(`${this._baseUrl}/api/emailvalidation`), {
      body: JSON.stringify(body),
      headers: {
        'x-api-version': '2'
      },
      method: 'POST'
    });
  }
}

export default DefaultValidatorService;
