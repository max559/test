import type { Brand, Environment } from '../../core.types';

const baseUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testordersettleservice.medienhausaachen.de',
    preview: 'https://previewordersettleservice.medienhausaachen.de',
    production: 'https://ordersettleservice.medienhausaachen.de'
  },
  co: {
    test: 'https://testordersettleservice.mediahuis.lu',
    preview: 'https://previewordersettleservice.mediahuis.lu',
    production: 'https://ordersettleservice.mediahuis.lu'
  },
  dl: {
    test: 'https://testordersettleservice.limburger.nl',
    preview: 'https://previewordersettleservice.limburger.nl',
    production: 'https://ordersettleservice.limburger.nl'
  },
  ds: {
    test: 'https://testordersettleservice.mediahuis.be',
    preview: 'https://previewordersettleservice.mediahuis.be',
    production: 'https://ordersettleservice.mediahuis.be'
  },
  gva: {
    test: 'https://testordersettleservice.mediahuis.be',
    preview: 'https://previewordersettleservice.mediahuis.be',
    production: 'https://ordersettleservice.mediahuis.be'
  },
  hbvl: {
    test: 'https://testordersettleservice.mediahuis.be',
    preview: 'https://previewordersettleservice.mediahuis.be',
    production: 'https://ordersettleservice.mediahuis.be'
  },
  lt: {
    test: 'https://testordersettleservice.mediahuis.lu',
    preview: 'https://previewordersettleservice.mediahuis.lu',
    production: 'https://ordersettleservice.mediahuis.lu'
  },
  lw: {
    test: 'https://testordersettleservice.mediahuis.lu',
    preview: 'https://previewordersettleservice.mediahuis.lu',
    production: 'https://ordersettleservice.mediahuis.lu'
  },
  nb: {
    test: 'https://testordersettleservice.mediahuis.be',
    preview: 'https://previewordersettleservice.mediahuis.be',
    production: 'https://ordersettleservice.mediahuis.be'
  },
  tc: {
    test: 'https://testordersettleservice.mediahuis.lu',
    preview: 'https://previewordersettleservice.mediahuis.lu',
    production: 'https://ordersettleservice.mediahuis.lu'
  }
};

export default baseUrlConfig;
