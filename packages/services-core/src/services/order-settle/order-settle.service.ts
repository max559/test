import type { CoreResponse } from '../../core-fetch';
import type { Brand, Environment } from '../../core.types';
import type { OrderSettlement } from './order-settle.types';

import { coreFetch } from '../../core-fetch';
import baseUrlConfig from './order-settle.config';

export interface getOrderSettlementParams {
  agreementId?: string;
  orderId?: string | number;
  paymentReference?: string;
}

export class OrderSettleService {
  private _baseUrl: string;

  constructor(brand: Brand, environment: Environment) {
    this._baseUrl = baseUrlConfig[brand][environment];
  }

  public getOrderSettlement(
    params: getOrderSettlementParams
  ): Promise<CoreResponse<OrderSettlement>> {
    return coreFetch(new URL(`${this._baseUrl}/api/OrderSettlements`), {
      method: 'GET',
      params: params as Record<string, string>
    });
  }
}

export default OrderSettleService;
