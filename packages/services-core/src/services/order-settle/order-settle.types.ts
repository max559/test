export interface OrderSettlement {
  agreementId?: string;
  brandBankAccount?: string;
  orderId: number;
  pageNumber?: string;
  paymentReference?: string;
  structuredBankMessage?: string;
  subscriptionTypeId?: number;
}
