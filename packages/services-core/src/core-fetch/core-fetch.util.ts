export const convertToSearchParams = (params: Record<string, unknown>): Record<string, string> => {
  return Object.entries(params).reduce((acc: Record<string, string>, [key, value]) => {
    if (value) {
      acc[key] = String(value);
    }

    return acc;
  }, {});
};
