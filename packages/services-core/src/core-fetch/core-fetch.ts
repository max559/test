import type { CoreError, CoreFetchConfig, CoreResponse } from './core-fetch.types';

import { convertToSearchParams } from './core-fetch.util';

export const coreFetch = <T>(url: URL, config: CoreFetchConfig = {}): Promise<CoreResponse<T>> => {
  if (!config.isFormData) {
    config.headers = {
      'Content-Type': 'application/json',
      ...config.headers
    };
  }

  if (config.params) {
    url.search = new URLSearchParams(convertToSearchParams(config.params)).toString();
  }

  return fetch(url.toString(), config).then(async response => {
    let responseData;

    const contentTypeHeader = response.headers.get('content-type');

    if (!response.ok) {
      const coreError: CoreError = {
        status: response.status,
        statusText: response.statusText
      };

      throw coreError;
    }
    if (contentTypeHeader && contentTypeHeader.indexOf('application/json') !== -1) {
      responseData = await response.json();
    }

    return {
      data: responseData,
      headers: Object.fromEntries(response.headers.entries()),
      status: response.status,
      statusText: response.statusText
    };
  });
};

export default coreFetch;
