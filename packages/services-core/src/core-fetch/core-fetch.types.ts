export interface CoreError {
  status: number;
  statusText: string;
}

export interface CoreFetchConfig extends RequestInit {
  params?: Record<string, unknown>;
  isFormData?: boolean;
}

export interface CoreResponse<T> {
  data: T;
  headers: Record<string, string>;
  status: number;
  statusText: string;
}
