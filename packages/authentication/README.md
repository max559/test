# @subscriber/authentication

The authentication package of team subscriber.

## Installation

Use your package manager of choice:

`npm install @subscriber/authentication`

`yarn add @subscriber/authentication`

`pnpm add @subscriber/authentication`

## Provider

Add the provider component to your application:

```
  import { AuthProvider } from '@subscriber/authentication';

  <AuthProvider brand="ds" environment="preview">
    ...
  </AuthProvider>
```

#### API

| Property           | Description                                                                       | Type                                          | Required | Default |
|--------------------|-----------------------------------------------------------------------------------|-----------------------------------------------|----------|---------|
| autoAuthentication | Set to false if you don't want the package to automatically authenticate on mount | boolean                                       | false    | true    |
| brand              | The MH brand you are working with                                                 | dl &#124; ds &#124; gva &#124; hbvl &#124; nb | true     |         |
| environment        | The environment you are working with                                              | test &#124; preview &#124; production         | true     |         |

## Hook

Use the useAuthentication hook to access authentication data:

```
  import { useAuthentication } from '@subscriber/authentication';

  const SomeComponent = () => {
    const { userInfo } = useAuthentication();

    return (
      ...
    );
  }
```

#### API

| Property             | Description                                          | Type                                                                    |
| -------------------- | ---------------------------------------------------- | ----------------------------------------------------------------------- |
| authenticationStatus | Current status of the authentication process         | AUTHENTICATION_STATUS                                                   |
| userInfo             | Contains user data when logged in, null otherwise    | AccountInfoResponse &#124; null                                         |
| getUserInfo          | Helper function to retrieve the user data manually   | () => Promise<AccountInfoResponse &#124; null>                          |
| getUserStatus        | Helper function to retrieve the user status manually | (username: string) => Promise<AccountStatus>                            |
| login                | Helper function to manually do the login call        | (username: string, password: string) => Promise<AuthenticationResponse> |
| redirectToSignOut    | Redirect to the logout URL                           | () => void                                                              |

###### AUTHENTICATION_STATUS

`authenticated | error | loading | unauthenticated | unknown`
