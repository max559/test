import type { AccountInfoResponse } from '@subscriber/services-core';
import type { ComponentType, PropsWithChildren, ReactElement } from 'react';

import type { WizardStep, WizardStepRenderParams } from './components/Wizard';
import type {
  ContactDataFormValues,
  EmailFormValues,
  PasswordFormValues,
  PersonalDataFormValues,
  PreferencesFormValues
} from './forms';

import { GENDER } from '@subscriber/services-core';
import { useState } from 'react';

import { useAuthStore } from '../context';
import { Wizard } from './components/Wizard';
import {
  ContactInfoRegistration,
  EmailVerification,
  EmailVerificationWall,
  PasswordReset,
  PasswordResetConfirmation,
  PasswordResetMailConfirmation,
  PasswordVerification,
  PasswordlessLoginMailConfirmation,
  PersonalInfoRegistration,
  PreferencesRegistration
} from './steps';

interface LoginWizardDefaultConfig {
  type: 'default';
}

interface LoginWizardWallConfig {
  articleTitle: string;
  type: 'wall';
}

enum LOGIN_WIZARD_STEP {
  ContactInfoRegistration = 'ContactInfoRegistration',
  EmailVerification = 'EmailVerification',
  EmailVerificationWall = 'EmailVerificationWall',
  PasswordlessLoginMailConfirmation = 'PasswordlessLoginMailConfirmation',
  PasswordReset = 'PasswordReset',
  PasswordResetConfirmation = 'PasswordResetConfirmation',
  PasswordResetMailConfirmation = 'PasswordResetMailConfirmation',
  PasswordVerification = 'PasswordVerification',
  PersonalInfoRegistration = 'PersonalInfoRegistration',
  PreferencesRegistration = 'PreferencesRegistration'
}

const getStartId = (wizardType: 'default' | 'wall', authHash?: string): LOGIN_WIZARD_STEP => {
  let startId: LOGIN_WIZARD_STEP = LOGIN_WIZARD_STEP.EmailVerification;

  if (wizardType === 'wall') {
    startId = LOGIN_WIZARD_STEP.EmailVerificationWall;
  }
  if (authHash) {
    startId = LOGIN_WIZARD_STEP.PasswordReset;
  }

  return startId;
};

interface LoginWizardProps {
  authHash?: string;
  config?: LoginWizardDefaultConfig | LoginWizardWallConfig;
  RootElement?: ComponentType<
    PropsWithChildren<Partial<WizardStepRenderParams<LOGIN_WIZARD_STEP>>>
  >;
  onCompleteLogin?: (userInfo?: AccountInfoResponse | null) => void;
  onCompleteRegistration?: (userInfo?: AccountInfoResponse | null) => void;
}

const LoginWizard = ({
  authHash,
  config = { type: 'default' },
  RootElement,
  onCompleteLogin,
  onCompleteRegistration
}: LoginWizardProps): ReactElement => {
  const brand = useAuthStore(state => state.config.brand);
  const getUserInfo = useAuthStore(state => state.getUserInfo);

  const [contactDataFormValues, setContactDataFormValues] = useState<ContactDataFormValues>({
    box: '',
    city: '',
    country: brand === 'dl' ? 'NL' : 'BE',
    houseNumber: '',
    postalCode: '',
    street: '',
    telephone: ''
  });
  const [emailFormValues, setEmailFormValues] = useState<EmailFormValues>({
    email: ''
  });
  const [passwordFormValues, setPasswordFormValues] = useState<PasswordFormValues>({
    password: ''
  });
  const [personalDataFormValues, setPersonalDataFormvalues] = useState<PersonalDataFormValues>({
    birthDate: undefined,
    emailConfirmation: '',
    firstName: '',
    gender: 'male',
    lastName: '',
    password: ''
  });
  const [preferencesFormValues, setPreferencesFormValues] = useState<PreferencesFormValues>();

  const wizardSteps: Array<WizardStep<LOGIN_WIZARD_STEP>> = [
    /* Login flow */
    {
      id: LOGIN_WIZARD_STEP.EmailVerification,
      render: ({ goToNextStep }) => (
        <EmailVerification
          formValues={emailFormValues}
          onNotFound={formValues => {
            setEmailFormValues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.PersonalInfoRegistration);
          }}
          onSuccess={formValues => {
            setEmailFormValues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.PasswordVerification);
          }}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.EmailVerificationWall,
      render: ({ goToNextStep }) => (
        <EmailVerificationWall
          articleTitle={config.type === 'wall' ? config.articleTitle : ''}
          formValues={emailFormValues}
          onLogin={() => {
            goToNextStep(LOGIN_WIZARD_STEP.EmailVerification);
          }}
          onNotFound={formValues => {
            setEmailFormValues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.PersonalInfoRegistration);
          }}
          onRegister={() => {
            goToNextStep(LOGIN_WIZARD_STEP.EmailVerification);
          }}
          onSuccess={formValues => {
            setEmailFormValues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.PasswordVerification);
          }}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.PasswordVerification,
      render: ({ completeWizard, goToNextStep }) => (
        <PasswordVerification
          email={emailFormValues.email}
          formValues={passwordFormValues}
          onForgotPassword={() => goToNextStep(LOGIN_WIZARD_STEP.PasswordResetMailConfirmation)}
          onPasswordlessLogin={() =>
            goToNextStep(LOGIN_WIZARD_STEP.PasswordlessLoginMailConfirmation)
          }
          onSuccess={formValues => {
            setPasswordFormValues(formValues);
            completeWizard(LOGIN_WIZARD_STEP.PasswordVerification);
          }}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.PasswordReset,
      render: ({ completeWizard, goToNextStep }) => (
        <PasswordReset
          authHash={authHash as string}
          onComplete={() => {
            goToNextStep(LOGIN_WIZARD_STEP.PasswordResetConfirmation);
            completeWizard(LOGIN_WIZARD_STEP.PasswordReset);
          }}
          onError={() => goToNextStep(getStartId(config.type))}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.PasswordResetConfirmation,
      render: () => <PasswordResetConfirmation />
    },
    {
      id: LOGIN_WIZARD_STEP.PasswordResetMailConfirmation,
      render: () => <PasswordResetMailConfirmation email={emailFormValues.email} />
    },
    {
      id: LOGIN_WIZARD_STEP.PasswordlessLoginMailConfirmation,
      render: () => <PasswordlessLoginMailConfirmation email={emailFormValues.email} />
    },
    /* Registration flow */
    {
      id: LOGIN_WIZARD_STEP.PersonalInfoRegistration,
      render: ({ goToNextStep, goToPreviousStep }) => (
        <PersonalInfoRegistration
          accountEmail={emailFormValues.email}
          formValues={personalDataFormValues}
          isStepperVariant={Boolean(RootElement)}
          stepIndex={1}
          onBack={goToPreviousStep}
          onNext={formValues => {
            setPersonalDataFormvalues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.ContactInfoRegistration);
          }}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.ContactInfoRegistration,
      render: ({ goToNextStep, goToPreviousStep }) => (
        <ContactInfoRegistration
          formValues={contactDataFormValues}
          isStepperVariant={Boolean(RootElement)}
          stepIndex={2}
          onBack={goToPreviousStep}
          onNext={formValues => {
            setContactDataFormValues(formValues);
            goToNextStep(LOGIN_WIZARD_STEP.PreferencesRegistration);
          }}
        />
      )
    },
    {
      id: LOGIN_WIZARD_STEP.PreferencesRegistration,
      render: ({ completeWizard, goToPreviousStep }) => (
        <PreferencesRegistration
          createAccountRequest={{
            birthDate: personalDataFormValues.birthDate
              ? personalDataFormValues.birthDate.toDateString()
              : undefined,
            box: contactDataFormValues.box,
            brand,
            city: contactDataFormValues.city,
            country: contactDataFormValues.country,
            emailAddress: personalDataFormValues.emailConfirmation,
            firstName: personalDataFormValues.firstName,
            gender: personalDataFormValues.gender === 'male' ? GENDER.Male : GENDER.Female,
            houseNumber: contactDataFormValues.houseNumber,
            name: personalDataFormValues.lastName,
            password: personalDataFormValues.password,
            passwordRepeat: personalDataFormValues.password,
            phoneNumber: contactDataFormValues.telephone,
            street: contactDataFormValues.street,
            zipcode: contactDataFormValues.postalCode
          }}
          formValues={preferencesFormValues}
          isStepperVariant={Boolean(RootElement)}
          stepIndex={3}
          onBack={goToPreviousStep}
          onConfirm={formValues => {
            setPreferencesFormValues(formValues);
            completeWizard(LOGIN_WIZARD_STEP.PreferencesRegistration);
          }}
        />
      )
    }
  ];

  return (
    <Wizard
      RootElement={RootElement}
      steps={wizardSteps}
      startId={getStartId(config.type, authHash)}
      onComplete={(lastCompletedStep: LOGIN_WIZARD_STEP) => {
        getUserInfo()
          .then((userInfo: AccountInfoResponse | null) => {
            if (onCompleteLogin && lastCompletedStep === LOGIN_WIZARD_STEP.PasswordVerification) {
              onCompleteLogin(userInfo);
            }
            if (onCompleteLogin && lastCompletedStep === LOGIN_WIZARD_STEP.PasswordReset) {
              onCompleteLogin(userInfo);
            }
            if (
              onCompleteRegistration &&
              lastCompletedStep === LOGIN_WIZARD_STEP.PreferencesRegistration
            ) {
              onCompleteRegistration(userInfo);
            }
          })
          .catch(() => {
            if (onCompleteLogin && lastCompletedStep === LOGIN_WIZARD_STEP.PasswordVerification) {
              onCompleteLogin();
            }
            if (onCompleteLogin && lastCompletedStep === LOGIN_WIZARD_STEP.PasswordReset) {
              onCompleteLogin();
            }
            if (
              onCompleteRegistration &&
              lastCompletedStep === LOGIN_WIZARD_STEP.PreferencesRegistration
            ) {
              onCompleteRegistration();
            }
          });
      }}
    />
  );
};

export default LoginWizard;
