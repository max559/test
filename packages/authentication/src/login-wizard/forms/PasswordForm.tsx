import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { Hide, RecentlyViewed } from '@mediahuis/chameleon-theme-wl/icons';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';

const PasswordFormSchema = Yup.object().shape({
  password: Yup.string().required('Vul je wachtwoord in om verder te gaan')
});

export interface PasswordFormValues {
  password: string;
}

interface PasswordFormProps {
  email?: string;
  formRef: RefObject<FormikProps<PasswordFormValues>>;
  initialValues: PasswordFormValues;
  onSubmit: (values: PasswordFormValues) => void;
}

export const PasswordForm = ({
  email,
  formRef,
  initialValues,
  onSubmit
}: PasswordFormProps): ReactElement => {
  const [isPasswordVisible, setIsPasswordVisible] = useState<boolean>(false);

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={PasswordFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
          {Boolean(email) && (
            <TextField
              disabled
              id="EmailInput"
              label=""
              labelHidden
              name="email"
              placeholder="Vul je e-mailadres in"
              value={email}
            />
          )}

          <Field name="password">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                error={Boolean(meta.touched) && Boolean(meta.error)}
                iconRight={isPasswordVisible ? Hide : RecentlyViewed}
                id="PasswordInput"
                label=""
                labelHidden
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                placeholder="Wachtwoord"
                type={isPasswordVisible ? 'text' : 'password'}
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onIconRightClick={() => setIsPasswordVisible(!isPasswordVisible)}
              />
            )}
          </Field>
        </div>
      </Form>
    </Formik>
  );
};
