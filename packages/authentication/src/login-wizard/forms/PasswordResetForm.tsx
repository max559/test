import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { Hide, RecentlyViewed } from '@mediahuis/chameleon-theme-wl/icons';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';

const PasswordResetFormSchema = Yup.object().shape({
  password: Yup.string().required('Vul je nieuwe wachtwoord in '),
  passwordConfirmation: Yup.string().required('Herhaal je nieuwe wachtwoord')
});

export interface PasswordResetFormValues {
  password: string;
  passwordConfirmation: string;
}

interface PasswordResetFormProps {
  email: string;
  formRef: RefObject<FormikProps<PasswordResetFormValues>>;
  initialValues: PasswordResetFormValues;
  onSubmit: (values: PasswordResetFormValues) => void;
}

export const PasswordResetForm = ({
  email,
  formRef,
  initialValues,
  onSubmit
}: PasswordResetFormProps): ReactElement => {
  const [isPasswordVisible, setIsPasswordVisible] = useState<boolean>(false);
  const [isPasswordConfirmationVisible, setIsPasswordConfirmationVisible] =
    useState<boolean>(false);

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={PasswordResetFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
          <TextField disabled id="EmailInput" label="" labelHidden name="email" value={email} />

          <Field name="password">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                error={Boolean(meta.touched) && Boolean(meta.error)}
                iconRight={isPasswordVisible ? Hide : RecentlyViewed}
                id="PasswordInput"
                label=""
                labelHidden
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                placeholder="Wachtwoord"
                type={isPasswordVisible ? 'text' : 'password'}
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onIconRightClick={() => setIsPasswordVisible(!isPasswordVisible)}
              />
            )}
          </Field>

          <Field name="passwordConfirmation">
            {({ field, meta }: FieldProps<string>) => (
              <TextField
                error={Boolean(meta.touched) && Boolean(meta.error)}
                iconRight={isPasswordConfirmationVisible ? Hide : RecentlyViewed}
                id="PasswordConfirmationInput"
                label=""
                labelHidden
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                placeholder="Wachtwoord herhalen"
                type={isPasswordConfirmationVisible ? 'text' : 'password'}
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onIconRightClick={() =>
                  setIsPasswordConfirmationVisible(!isPasswordConfirmationVisible)
                }
              />
            )}
          </Field>
        </div>
      </Form>
    </Formik>
  );
};
