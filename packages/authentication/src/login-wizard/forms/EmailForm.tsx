import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { TextField } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

const EmailFormSchema = Yup.object().shape({
  email: Yup.string()
    .required('Gelieve een geldig e-mailadres in te geven')
    .email('Gelieve een geldig e-mailadres in te geven')
});

export interface EmailFormValues {
  email: string;
}

interface EmailFormProps {
  formRef: RefObject<FormikProps<EmailFormValues>>;
  initialValues: EmailFormValues;
  onSubmit: (values: EmailFormValues) => void;
}

export const EmailForm = ({ formRef, initialValues, onSubmit }: EmailFormProps): ReactElement => {
  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={EmailFormSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <Field name="email">
          {({ field, meta }: FieldProps<string>) => (
            <TextField
              error={Boolean(meta.touched) && Boolean(meta.error)}
              id="EmailInput"
              label=""
              labelHidden
              message={meta.touched && meta.error ? meta.error : undefined}
              name={field.name}
              placeholder="Vul je e-mailadres in"
              value={field.value}
              onBlur={field.onBlur}
              onChange={field.onChange}
            />
          )}
        </Field>
      </Form>
    </Formik>
  );
};
