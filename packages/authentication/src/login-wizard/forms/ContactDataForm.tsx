import type { City, Country, Street } from '@subscriber/services-core';
import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { AutoComplete, Select, TextField } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import { useEffect, useState } from 'react';
import * as Yup from 'yup';

import { useAuthStore } from '../../context';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const debounce = <F extends (...args: any[]) => any>(func: F, waitFor: number) => {
  let timeout: number;

  return (...args: Parameters<F>): Promise<ReturnType<F>> =>
    new Promise(resolve => {
      if (timeout) {
        clearTimeout(timeout);
      }

      timeout = setTimeout(() => resolve(func(...args)), waitFor);
    });
};

const mapCitySuggestions = (cities: Array<City>): Array<string> => {
  return cities.reduce((acc: Array<string>, city: City) => {
    if (city.Name && city.PostalCode) {
      return acc.concat(`${city.Name} (${city.PostalCode})`);
    }

    return acc;
  }, []);
};

const mapStreetSuggestions = (streets: Array<Street>): Array<string> => {
  return streets.reduce((acc: Array<string>, street: Street) => {
    if (street.Name) {
      return acc.concat(street.Name);
    }

    return acc;
  }, []);
};

const ContactDataFormSchema = Yup.object().shape({
  box: Yup.string(),
  country: Yup.string().required('Gelieve een land te kiezen'),
  houseNumber: Yup.string().required('Gelieve je huisnummer in te vullen'),
  postalCode: Yup.string().required('Gelieve je postcode in te vullen'),
  street: Yup.string().required('Gelieve je straat in te vullen'),
  telephone: Yup.string()
});

export interface ContactDataFormValues {
  box: string;
  city: string;
  country: string;
  houseNumber: string;
  postalCode: string;
  street: string;
  telephone: string;
}

interface ContactDataFormProps {
  countries: Array<Country>;
  formRef: RefObject<FormikProps<ContactDataFormValues>>;
  initialValues: ContactDataFormValues;
  onSubmit: (values: ContactDataFormValues) => void;
}

export const ContactDataForm = ({
  countries,
  formRef,
  initialValues,
  onSubmit
}: ContactDataFormProps): ReactElement => {
  const coreInstance = useAuthStore(state => state.coreInstance);

  const [cityInput, setCityInput] = useState<string>('');
  const [streetInput, setStreetInput] = useState<string>('');
  const [citySuggestions, setCitySuggestions] = useState<Array<string>>([]);
  const [streetSuggestions, setStreetSuggestions] = useState<Array<string>>([]);

  useEffect(() => {
    let active = true;

    if (cityInput) {
      if (formRef.current?.values.country) {
        coreInstance.addressService
          .postCitiesAutocomplete({ Key: cityInput }, formRef.current.values.country)
          .then(response => {
            if (active) {
              setCitySuggestions(mapCitySuggestions(response.data.Cities));
            }
          });
      }
    } else {
      setCitySuggestions([]);
    }

    return () => {
      active = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cityInput]);

  useEffect(() => {
    let active = true;

    if (streetInput) {
      if (formRef.current?.values.country && formRef.current?.values.postalCode) {
        coreInstance.addressService
          .postStreetsAutocomplete(
            { Key: streetInput, PostalCode: formRef.current.values.postalCode },
            formRef.current.values.country
          )
          .then(response => {
            if (active) {
              setStreetSuggestions(mapStreetSuggestions(response.data.Streets));
            }
          });
      }
    } else {
      setStreetSuggestions([]);
    }

    return () => {
      active = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [streetInput]);

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={ContactDataFormSchema}
      onSubmit={onSubmit}
    >
      {({ setFieldValue, setFieldTouched }) => (
        <Form>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
            <Field name="country">
              {({ field, meta }: FieldProps<string>) => (
                <Select
                  disabled={countries.length <= 1}
                  error={!!meta.touched && !!meta.error}
                  id="CountrySelect"
                  label="Land"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                >
                  {countries.map((country: Country) => (
                    <option key={country.Name} value={country.IsoCode}>
                      {country.Name}
                    </option>
                  ))}
                </Select>
              )}
            </Field>

            <div style={{ display: 'flex', gap: '0.5rem' }}>
              <div style={{ flexGrow: 1 }}>
                <Field name="postalCode">
                  {({ field, form, meta }: FieldProps<string>) => (
                    <AutoComplete
                      disabled={!form.values.country}
                      error={!!meta.touched && !!meta.error}
                      id="PostalCodeAutoComplete"
                      label="Postcode"
                      message={meta.touched && meta.error ? meta.error : undefined}
                      name={field.name}
                      suggestions={citySuggestions}
                      value={field.value}
                      onBlur={field.onBlur}
                      onChange={field.onChange}
                      onInputChange={debounce(value => setCityInput(value), 50)}
                      onSelect={value => {
                        if (value) {
                          const splitValues = value.split(' ');

                          setFieldValue('city', splitValues[0]);
                          setFieldValue('postalCode', (splitValues[1] as string).slice(1, -1));
                        } else {
                          setFieldValue('postalCode', '');
                          setFieldValue('city', '');
                          setFieldTouched('city', false, false);
                          setFieldValue('street', '');
                          setFieldTouched('street', false, false);
                          setFieldValue('houseNumber', '');
                          setFieldTouched('houseNumber', false, false);
                          setFieldValue('box', '');
                        }
                      }}
                    />
                  )}
                </Field>
              </div>

              <Field name="city">
                {({ field }: FieldProps<string>) => (
                  <TextField
                    disabled
                    id="CityInput"
                    label="Gemeente"
                    name={field.name}
                    value={field.value}
                    style={{ flexGrow: 1 }}
                  />
                )}
              </Field>
            </div>

            <Field name="street">
              {({ field, form, meta }: FieldProps<string>) => (
                <AutoComplete
                  disabled={!form.values.city || !form.values.postalCode}
                  error={!!meta.touched && !!meta.error}
                  id="StreetAutoComplete"
                  label="Straat"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  suggestions={streetSuggestions}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                  onInputChange={debounce(value => setStreetInput(value), 150)}
                  onSelect={value => {
                    if (value) {
                      setFieldValue('street', value);
                    } else {
                      setFieldValue('street', '');
                      setFieldValue('houseNumber', '');
                      setFieldTouched('houseNumber', false, false);
                      setFieldValue('box', '');
                    }
                  }}
                />
              )}
            </Field>

            <div style={{ display: 'flex', gap: '0.5rem' }}>
              <Field name="houseNumber">
                {({ field, form, meta }: FieldProps<string>) => (
                  <TextField
                    disabled={!form.values.street}
                    error={Boolean(meta.touched) && Boolean(meta.error)}
                    id="HouseNumberInput"
                    label="Huisnummer"
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    style={{ flexGrow: 1 }}
                  />
                )}
              </Field>

              <Field name="box">
                {({ field, form, meta }: FieldProps<string>) => (
                  <TextField
                    disabled={!form.values.street}
                    error={Boolean(meta.touched) && Boolean(meta.error)}
                    id="BoxInput"
                    label="Bus"
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    optionalLabel="Optioneel"
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    style={{ flexGrow: 1 }}
                  />
                )}
              </Field>
            </div>

            <Field name="telephone">
              {({ field, meta }: FieldProps<string>) => (
                <TextField
                  error={Boolean(meta.touched) && Boolean(meta.error)}
                  id="TelephoneInput"
                  label="Telefoonnummer"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  optionalLabel="Optioneel"
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>
          </div>
        </Form>
      )}
    </Formik>
  );
};
