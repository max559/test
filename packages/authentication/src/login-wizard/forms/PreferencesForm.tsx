import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import type { Brand } from '../../types';

import { Divider, Switch } from '@mediahuis/chameleon-react';
import { Field, Form, Formik } from 'formik';
import { Fragment } from 'react';

interface PreferencesFieldConfig {
  code: string;
  defaultValue: boolean;
  event: string;
  label: string;
  message: string;
}

const fieldsConfig: Record<Brand, Array<PreferencesFieldConfig>> = {
  dl: [
    {
      code: 'SERV_AC_LIMNL_OCHTEND',
      defaultValue: false,
      event: 'morningnews',
      label: 'Ochtend nieuwsbrief 7.00 uur',
      message: 'Ik ontvang graag iedere ochtend om 7:00 de ochtend nieuwsbrief.'
    },
    {
      code: 'SERV_AC_LIMNL_MIDDAG',
      defaultValue: true,
      event: 'middaynews',
      label: 'Middag nieuwsbrief 12.00 uur',
      message:
        'Iedere middag het belangrijkste nieuws uit Limburg, Nederland en de rest van de wereld.'
    },
    {
      code: 'SERV_AC_LIMNL_REGIO',
      defaultValue: true,
      event: 'regionnews',
      label: 'Regio nieuwsbrief 16.00 uur',
      message: 'Ontvang het laatste nieuws uit jouw regio.'
    },
    {
      code: 'SERV_AC_LIMNL_AVOND',
      defaultValue: true,
      event: 'eveningnews',
      label: 'Avond nieuwsbrief 20.00 uur',
      message:
        'Iedere avond een voorproefje van diverse Plus-artikelen die de volgende dag in de krant staan.'
    },
    {
      code: 'SERV_AC_LIMNL_WEBSHOP',
      defaultValue: false,
      event: 'weeklyshopactions',
      label: 'Webshop nieuwsbrief',
      message: 'Ontvang twee keer per week de beste aanbiedingen uit onze webshop.'
    }
  ],
  ds: [
    {
      code: 'SERV_AC_DS_BREAKING_NEWS',
      defaultValue: true,
      event: 'breaking',
      label: 'Breaking News',
      message: 'Ik ben als eerste op de hoogte bij breaking news'
    },
    {
      code: 'SERV_AC_DS_MIDDAY_UPDATE',
      defaultValue: true,
      event: 'middaynews',
      label: 'Middagupdate',
      message: 'Ik ontvang graag dagelijks de middagupdate'
    },
    {
      code: 'SERV_WOPPIES_OWN_BRAND',
      defaultValue: true,
      event: 'offers',
      label: 'Acties en voordelen',
      message: 'Ik ben graag op de hoogte van de acties en voordelen van De Standaard'
    },
    {
      code: 'SERV_AC_DS_PARTNER',
      defaultValue: false,
      event: 'partneroffers',
      label: 'Promoties van partners',
      message: 'Ik ontvang graag per e-mail promoties en acties van partners van De Standaard'
    },
    {
      code: 'SERV_AC_DS_WEBSHOP_BIS',
      defaultValue: false,
      event: 'weeklyshopactions',
      label: 'De Standaard Shop',
      message: 'Ik ontvang elke week promoties op tickets, elektro, boeken,…'
    }
  ],
  gva: [
    {
      code: 'SERV_AC_GVA_BREAKING',
      defaultValue: true,
      event: 'breaking',
      label: 'Breaking news',
      message: 'Onmiddellijk op de hoogte bij belangrijk nieuws'
    },
    {
      code: 'SERV_AC_GVA_OCHTEND',
      defaultValue: true,
      event: 'morningnews',
      label: 'Ochtend nieuwsbrief',
      message: 'Elke ochtend het nieuws uit jouw regio en de wereld'
    },
    {
      code: 'SERV_WOPPIES_OWN_BRAND',
      defaultValue: true,
      event: 'offers',
      label: 'Acties en voordelen van GVA',
      message: 'Ontvang nieuws over acties en voordelen van Gazet van Antwerpen'
    },
    {
      code: 'SERV_AC_GVA_PARTNER',
      defaultValue: false,
      event: 'partneroffers',
      label: 'Promoties van partners',
      message: 'Via mail op de hoogte van promoties van onze partners'
    },
    {
      code: 'SERV_AC_GVA_WEBSHOP',
      defaultValue: false,
      event: 'weeklyshopactions',
      label: 'GVA Shop nieuwsbrief',
      message: 'Ontvang elke week acties op tickets, elektro, boeken,…'
    }
  ],
  hbvl: [
    {
      code: 'SERV_AC_HBVL_BREAKING',
      defaultValue: true,
      event: 'breaking',
      label: 'Breaking nieuws',
      message: 'Onmiddellijk op de hoogte bij belangrijk nieuws'
    },
    {
      code: 'SERV_AC_HBVL_OCHTEND',
      defaultValue: true,
      event: 'morningnews',
      label: 'Ochtend nieuws',
      message: 'Elke ochtend de belangrijkste nieuwsfeiten op een rijtje'
    },
    {
      code: 'SERV_WOPPIES_OWN_BRAND',
      defaultValue: true,
      event: 'offers',
      label: 'Acties en voordelen',
      message: 'Ontvang nieuws over acties en voordelen van Het Belang van Limburg'
    },
    {
      code: 'SERV_AC_HBVL_PARTNER',
      defaultValue: false,
      event: 'partneroffers',
      label: 'Promoties van partners',
      message: 'Via e-mail op de hoogte van promoties van onze partners'
    },
    {
      code: 'SERV_AC_HBVL_WEBSHOP',
      defaultValue: false,
      event: 'weeklyshopactions',
      label: 'Het Belang van Limburg Shop nieuwsbrief',
      message: 'Ontvang elke week promoties op tickets, elektro, boeken,…'
    }
  ],
  nb: [
    {
      code: 'SERV_AC_BN_NEWS',
      defaultValue: true,
      event: 'breaking',
      label: 'Breaking News',
      message: 'Onmiddellijk op de hoogte bij belangrijk nieuws'
    },
    {
      code: 'SERV_AC_LOCAL_NEWS',
      defaultValue: true,
      event: 'morningnews',
      label: 'Ochtend nieuwsbrief',
      message: 'Elke ochtend het nieuws uit jouw regio en de wereld'
    },
    {
      code: 'SERV_WOPPIES_OWN_BRAND',
      defaultValue: true,
      event: 'offers',
      label: 'Acties en voordelen',
      message: 'Ontvang nieuws over acties en voordelen van Nieuwsblad'
    },
    {
      code: 'SERV_AC_NB_PARTNER',
      defaultValue: false,
      event: 'partneroffers',
      label: 'Promoties van partners',
      message: 'Via e-mail op de hoogte van promoties van onze partners'
    },
    {
      code: 'SERV_AC_NB_WEBSHOP',
      defaultValue: false,
      event: 'weeklyshopactions',
      label: 'Nieuwsbladshop nieuwsbrief',
      message: 'Ontvang elke week promoties op tickets, elektro, boeken,…'
    }
  ]
};

export type PreferencesFormValues = Record<string, boolean>;

interface PreferencesFormProps {
  brand: Brand;
  formRef: RefObject<FormikProps<PreferencesFormValues>>;
  initialValues?: PreferencesFormValues;
  onSubmit: (values: PreferencesFormValues) => void;
}

export const PreferencesForm = ({
  brand,
  formRef,
  initialValues,
  onSubmit
}: PreferencesFormProps): ReactElement => {
  const defaultValues = fieldsConfig[brand].reduce(
    (acc: PreferencesFormValues, config: PreferencesFieldConfig) => {
      acc[config.code] = config.defaultValue;

      return acc;
    },
    {}
  );

  return (
    <Formik innerRef={formRef} initialValues={initialValues ?? defaultValues} onSubmit={onSubmit}>
      {({ setFieldValue }) => (
        <Form>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
            {fieldsConfig[brand].map((fieldConfig, fieldConfigIndex) => (
              <Fragment key={fieldConfig.code}>
                <Field name={fieldConfig.code}>
                  {({ field }: FieldProps<boolean>) => (
                    <Switch
                      checked={field.value ?? fieldConfig.defaultValue}
                      id={`Switch-${fieldConfig.code}`}
                      label={fieldConfig.label}
                      message={fieldConfig.message}
                      value={fieldConfig.code}
                      onChange={() => setFieldValue(field.name, !field.value)}
                    />
                  )}
                </Field>

                {fieldConfigIndex < fieldsConfig[brand].length - 1 && (
                  <Divider style={{ width: '100%' }} />
                )}
              </Fragment>
            ))}
          </div>
        </Form>
      )}
    </Formik>
  );
};
