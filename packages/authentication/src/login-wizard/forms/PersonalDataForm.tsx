import type { FieldProps, FormikProps } from 'formik';
import type { ReactElement, RefObject } from 'react';

import { DatePicker, Fieldset, Radio, TextField } from '@mediahuis/chameleon-react';
import { Hide, RecentlyViewed } from '@mediahuis/chameleon-theme-wl/icons';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import * as Yup from 'yup';

export interface PersonalDataFormValues {
  birthDate?: Date;
  emailConfirmation: string;
  firstName: string;
  gender: 'female' | 'male';
  lastName: string;
  password: string;
}

interface PersonalDataFormProps {
  email: string;
  formRef: RefObject<FormikProps<PersonalDataFormValues>>;
  initialValues: PersonalDataFormValues;
  onSubmit: (values: PersonalDataFormValues) => void;
}

export const PersonalDataForm = ({
  email,
  formRef,
  initialValues,
  onSubmit
}: PersonalDataFormProps): ReactElement => {
  const [isPasswordVisible, setIsPasswordVisible] = useState<boolean>(false);

  const PersonalDataFormSchema = Yup.object().shape({
    birthDate: Yup.date().required('Gelieve je geboortedatum in te geven'),
    emailConfirmation: Yup.string()
      .matches(new RegExp(`${email}`), 'Deze waarde komt niet overeen met het e-mailadres')
      .required('Gelieve je e-mailadres in te geven'),
    firstName: Yup.string()
      .min(2, 'Je voornaam moet minimaal 2 karakters lang zijn')
      .required('Gelieve je voornaam in te geven'),
    lastName: Yup.string().required('Gelieve je achternaam in te geven'),
    password: Yup.string()
      .min(6, 'Je wachtwoord moet minimaal 6 karakters lang zijn')
      .max(16, 'Je wachtwoord mag maximum 16 karakters lang zijn')
      .required('Gelieve je wachtwoord in te geven')
  });

  return (
    <Formik
      innerRef={formRef}
      initialValues={initialValues}
      validationSchema={PersonalDataFormSchema}
      onSubmit={onSubmit}
    >
      {({ setFieldValue }) => (
        <Form>
          <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
            <TextField disabled id="EmailInput" label="E-mailadres" name="email" value={email} />

            <Field name="emailConfirmation">
              {({ field, meta }: FieldProps<string>) => (
                <TextField
                  error={Boolean(meta.touched) && Boolean(meta.error)}
                  id="EmailConfirmationInput"
                  label="Bevestig e-mailadres"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>

            <Field name="password">
              {({ field, meta }: FieldProps<string>) => (
                <TextField
                  error={Boolean(meta.touched) && Boolean(meta.error)}
                  iconRight={isPasswordVisible ? Hide : RecentlyViewed}
                  id="PasswordInput"
                  label="Wachtwoord"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  type={isPasswordVisible ? 'text' : 'password'}
                  value={field.value}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                  onIconRightClick={() => setIsPasswordVisible(!isPasswordVisible)}
                />
              )}
            </Field>

            <div style={{ display: 'flex', gap: '0.5rem' }}>
              <Field name="firstName">
                {({ field, meta }: FieldProps<string>) => (
                  <TextField
                    error={Boolean(meta.touched) && Boolean(meta.error)}
                    id="FisrtNameInput"
                    label="Voornaam"
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    style={{ flexGrow: 1 }}
                  />
                )}
              </Field>

              <Field name="lastName">
                {({ field, meta }: FieldProps<string>) => (
                  <TextField
                    error={Boolean(meta.touched) && Boolean(meta.error)}
                    id="LastNameInput"
                    label="Achternaam"
                    message={meta.touched && meta.error ? meta.error : undefined}
                    name={field.name}
                    value={field.value}
                    onBlur={field.onBlur}
                    onChange={field.onChange}
                    style={{ flexGrow: 1 }}
                  />
                )}
              </Field>
            </div>

            <Field name="birthDate">
              {({ field, meta }: FieldProps<Date>) => (
                <DatePicker
                  error={Boolean(meta.touched) && Boolean(meta.error)}
                  id="BirtDatePicker"
                  label="Geboortedatum"
                  message={meta.touched && meta.error ? meta.error : undefined}
                  value={field.value}
                  onChange={val => {
                    if (val) {
                      setFieldValue('birthDate', val);
                    }
                  }}
                />
              )}
            </Field>

            <Field name="gender">
              {({ field }: FieldProps<'female' | 'male'>) => (
                <Fieldset label="Geslacht">
                  <div style={{ display: 'flex', flexWrap: 'wrap', gap: '1rem' }}>
                    <Radio
                      checked={field.value === 'male'}
                      id="GenderMaleRadio"
                      label="Man"
                      value="male"
                      onChange={() => setFieldValue('gender', 'male')}
                    />
                    <Radio
                      checked={field.value === 'female'}
                      id="GenderFemaleRadio"
                      label="Vrouw"
                      value="female"
                      onChange={() => setFieldValue('gender', 'female')}
                    />
                  </div>
                </Fieldset>
              )}
            </Field>
          </div>
        </Form>
      )}
    </Formik>
  );
};
