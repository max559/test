import type { ButtonProps } from '@mediahuis/chameleon-react';
import type { PropsWithChildren, ReactElement } from 'react';

import { Button, Heading, Logo } from '@mediahuis/chameleon-react';

interface LoginLayoutProps {
  buttons?: Array<ButtonProps>;
  headingText: string;
}

const LoginLayout = ({
  buttons,
  children,
  headingText
}: PropsWithChildren<LoginLayoutProps>): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
      <div style={{ alignItems: 'center', display: 'flex', gap: '1rem' }}>
        <Logo name="brand-square-main" style={{ height: '4.5rem', width: '4.5rem' }} />
        <Heading level={4} size="md">
          {headingText}
        </Heading>
      </div>

      {children}

      {buttons && (
        <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
          {buttons.map((buttonProps, buttonIndex) => (
            <Button key={buttonProps.id || buttonIndex} {...buttonProps} />
          ))}
        </div>
      )}
    </div>
  );
};

export default LoginLayout;
