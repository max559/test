import type { ButtonProps } from '@mediahuis/chameleon-react';
import type { PropsWithChildren, ReactElement } from 'react';

import {
  Button,
  Caption,
  Heading,
  IconButton,
  LinkText,
  Paragraph,
  Stepper
} from '@mediahuis/chameleon-react';
import { ArrowBack } from '@mediahuis/chameleon-theme-wl/icons';

interface RegistrationLayoutProps {
  activeStepIndex?: number;
  buttons: Array<ButtonProps>;
  title: string;
  variant?: 'default' | 'stepper';
  onBack: () => void;
}

const RegistrationLayout = ({
  activeStepIndex,
  buttons,
  children,
  title,
  variant = 'default',
  onBack
}: PropsWithChildren<RegistrationLayoutProps>): ReactElement => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
      {variant === 'default' ? (
        <div style={{ alignItems: 'center', display: 'flex', gap: '1rem' }}>
          <IconButton appearance="secondary" circular icon={ArrowBack} onClick={onBack} />
          <Heading level={4} size="md">
            {title}
          </Heading>
        </div>
      ) : (
        <>
          <Heading level={3} size="lg" style={{ textAlign: 'center' }}>
            {title}
          </Heading>
          <Stepper activeStepIndex={activeStepIndex} steps={['', '', '', '', '']} />
        </>
      )}

      {children}

      <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
        {buttons.map((buttonProps, buttonIndex) => (
          <Button key={buttonProps.id || buttonIndex} {...buttonProps} />
        ))}
      </div>

      <Paragraph style={{ textAlign: 'center' }}>
        Door te registreren ga je akkoord met de{' '}
        <LinkText href="https://www.mediahuis.be/nl/gebruiksvoorwaarden/">
          gebruiksvoorwaarden
        </LinkText>
        .
      </Paragraph>

      <Caption size="lg" style={{ textAlign: 'center' }}>
        Ons privacybeleid kan je steeds{' '}
        <LinkText href="https://www.mediahuis.be/privacy-policy/">hier</LinkText> nalezen.
      </Caption>
    </div>
  );
};

export default RegistrationLayout;
