import type { ReactElement } from 'react';

import { Paragraph } from '@mediahuis/chameleon-react';

import { LoginLayout } from '../layouts';

const PasswordResetConfirmation = (): ReactElement => {
  return (
    <LoginLayout headingText="Nieuw wachtwoord ingesteld">
      <Paragraph>Het wachtwoord is succesvol gewijzigd. Veel leesplezier!</Paragraph>
    </LoginLayout>
  );
};

export default PasswordResetConfirmation;
