import type { ReactElement } from 'react';

import { Heading, LinkText, Paragraph } from '@mediahuis/chameleon-react';

import { Mail } from '../../visuals';
import { LoginLayout } from '../layouts';

interface PasswordlessLoginMailConfirmationProps {
  email: string;
}

const PasswordlessLoginMailConfirmation = ({
  email
}: PasswordlessLoginMailConfirmationProps): ReactElement => {
  return (
    <LoginLayout headingText="Ga naar je inbox en open je e-mail">
      <div style={{ display: 'flex', flexDirection: 'column', gap: '2.5rem' }}>
        <Paragraph>
          We hebben een e-mail verstuurd naar {email}. Klik op de link in de e-mail. Zo word je
          automatisch aangemeld en kan je meteen verder.
        </Paragraph>

        <Mail style={{ alignSelf: 'center', maxHeight: '25%', maxWidth: '25%' }} />

        <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
          <Heading level={6} size="xs">
            Geen e-mail ontvangen?
          </Heading>
          <Paragraph>
            Bekijk je spamfolder of <LinkText>ontvang een nieuwe e-mail</LinkText>.
          </Paragraph>
        </div>
      </div>
    </LoginLayout>
  );
};

export default PasswordlessLoginMailConfirmation;
