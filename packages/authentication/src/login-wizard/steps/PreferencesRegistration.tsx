import type { CreateAccountRequest, OptinAmSaveRequest } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { PreferencesFormValues } from '../forms';

import { Banner, Paragraph } from '@mediahuis/chameleon-react';
import { useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { PreferencesForm } from '../forms';
import { RegistrationLayout } from '../layouts';

interface PreferencesRegistrationProps {
  createAccountRequest: CreateAccountRequest;
  formValues?: PreferencesFormValues;
  isStepperVariant: boolean;
  stepIndex: number;
  onBack: () => void;
  onConfirm: (formValues: PreferencesFormValues) => void;
}

const PreferencesRegistration = ({
  createAccountRequest,
  formValues,
  isStepperVariant,
  stepIndex,
  onBack,
  onConfirm
}: PreferencesRegistrationProps): ReactElement => {
  const { brand, coreInstance, login } = useAuthStore(state => ({
    brand: state.config.brand,
    coreInstance: state.coreInstance,
    login: state.login
  }));

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isCreatingAccount, setIsCreatingAccount] = useState<boolean>(false);

  const preferencesFormRef = useRef<FormikProps<PreferencesFormValues>>(null);

  function handleSubmit(formValues: PreferencesFormValues) {
    const campaignCodeSuffix = brand === 'dl' ? 'LIMNL' : brand;

    setIsCreatingAccount(true);

    coreInstance.accountManagementService
      .postAccount(createAccountRequest)
      .then(async () => {
        const optinSaveRequest: OptinAmSaveRequest = {
          CampaignCode: `CAMP_${campaignCodeSuffix}`,
          Newsletters: Object.entries(formValues).map(([key, value]) => ({
            NewsletterCode: key,
            Status: value === true ? 1 : 0
          }))
        };

        await login(createAccountRequest.emailAddress, createAccountRequest.password);
        await coreInstance.optinService.postOptinAmSave(optinSaveRequest);

        setErrorMessage(null);
        setIsCreatingAccount(false);
        onConfirm(formValues);
      })
      .catch(() => {
        setErrorMessage('Er ging iets mis bij het aanmaken van je account');
        setIsCreatingAccount(false);
      });
  }

  return (
    <RegistrationLayout
      activeStepIndex={stepIndex}
      buttons={[
        {
          appearance: 'primary',
          children: 'Bevestigen',
          loading: isCreatingAccount,
          width: 'full',
          onClick: () => preferencesFormRef.current?.submitForm()
        }
      ]}
      title={
        isStepperVariant ? 'Maak een account en lees verder zonder abonnement' : 'Nieuwsbrieven'
      }
      variant={isStepperVariant ? 'stepper' : 'default'}
      onBack={onBack}
    >
      <Banner appearance="error" show={Boolean(errorMessage)}>
        {errorMessage}
      </Banner>

      <Paragraph>
        Kies hier welke nieuwsbrieven en aanbiedingen je wil ontvangen. Je kan op elk moment je
        voorkeuren aanpassen.
      </Paragraph>

      <PreferencesForm
        brand={brand}
        formRef={preferencesFormRef}
        initialValues={formValues}
        onSubmit={handleSubmit}
      />
    </RegistrationLayout>
  );
};

export default PreferencesRegistration;
