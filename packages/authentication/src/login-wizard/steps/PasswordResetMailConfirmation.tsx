import type { ReactElement } from 'react';

import { Banner, Heading, LinkText, Paragraph } from '@mediahuis/chameleon-react';
import { useState } from 'react';

import { useAuthStore } from '../../context';
import { Mail } from '../../visuals';
import { LoginLayout } from '../layouts';

interface PasswordResetMailConfirmationProps {
  email: string;
}

const PasswordResetMailConfirmation = ({
  email
}: PasswordResetMailConfirmationProps): ReactElement => {
  const { brand, coreInstance } = useAuthStore(state => ({
    brand: state.config.brand,
    coreInstance: state.coreInstance
  }));

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  function handleResendClick() {
    coreInstance.accountManagementService
      .postMailModifyPasswordStandalone({
        brand,
        goto: typeof window !== 'undefined' ? window.location.href : '',
        mail: email,
        step: 1
      })
      .then(() => {
        setErrorMessage(null);
        setSuccessMessage(`Er is een nieuwe email verstuurd naar ${email}`);

        setTimeout(() => {
          setSuccessMessage(null);
        }, 2500);
      })
      .catch(() => {
        setErrorMessage('Er ging iets mis bij het versturen van de email');
        setSuccessMessage(null);
      });
  }

  return (
    <LoginLayout headingText="Ga naar je inbox en open je e-mail">
      <Banner appearance="error" show={Boolean(errorMessage)} onClose={() => setErrorMessage(null)}>
        {errorMessage}
      </Banner>
      <Banner
        appearance="success"
        show={Boolean(successMessage)}
        onClose={() => setSuccessMessage(null)}
      >
        {successMessage}
      </Banner>

      <div style={{ display: 'flex', flexDirection: 'column', gap: '2.5rem' }}>
        <div>
          <Paragraph>
            We hebben een e-mail verstuurd naar {email}. Klik op de link in de e-mail om een nieuw
            wachtwoord te kiezen.
          </Paragraph>
          <Paragraph>let op: de link blijft slechts 24u geldig.</Paragraph>
        </div>

        <Mail style={{ alignSelf: 'center', maxHeight: '25%', maxWidth: '25%' }} />

        <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
          <Heading level={6} size="xs">
            Geen e-mail ontvangen?
          </Heading>
          <Paragraph>
            Bekijk je spamfolder of{' '}
            <LinkText as="span" onClick={handleResendClick}>
              ontvang een nieuwe e-mail
            </LinkText>
            .
          </Paragraph>
        </div>
      </div>
    </LoginLayout>
  );
};

export default PasswordResetMailConfirmation;
