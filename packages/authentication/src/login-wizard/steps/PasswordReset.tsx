import type { ValidHashResponse } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { PasswordResetFormValues } from '../forms';

import { Banner, Loader } from '@mediahuis/chameleon-react';
import { ACCOUNT_FLOW } from '@subscriber/services-core';
import Cookies from 'js-cookie';
import { useEffect, useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { PasswordResetForm } from '../forms';
import { LoginLayout } from '../layouts';

interface PasswordResetProps {
  authHash: string;
  onComplete: () => void;
  onError: () => void;
}

const PasswordReset = ({ authHash, onComplete, onError }: PasswordResetProps): ReactElement => {
  const config = useAuthStore(state => state.config);
  const coreInstance = useAuthStore(state => state.coreInstance);

  const [hashData, setHashData] = useState<ValidHashResponse | null>(null);
  const [isResetting, setIsResetting] = useState<boolean>(false);

  const passwordResetFormRef = useRef<FormikProps<PasswordResetFormValues>>(null);

  const isExpiredHash = hashData?.status === 'Expired';
  const isValidHash = hashData?.status === 'Valid';

  useEffect(() => {
    coreInstance.accountManagementService
      .getHashValidation(authHash)
      .then(response => {
        const { flow } = response.data;

        if (
          flow === ACCOUNT_FLOW.MODIFY_PASSWORD ||
          flow === ACCOUNT_FLOW.MODIFY_PASSWORD_MULTIBRAND ||
          flow === ACCOUNT_FLOW.MODIFY_PASSWORD_STANDALONE
        ) {
          setHashData(response.data);
        } else {
          onError();
        }
      })
      .catch(() => onError());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleSubmit(formValues: PasswordResetFormValues, email: string) {
    setIsResetting(true);

    coreInstance.accountManagementService
      .postResetPasswordWithEmail(
        {
          brand: config.brand,
          email,
          newPassword: formValues.password,
          newPasswordRepeat: formValues.passwordConfirmation
        },
        authHash
      )
      .then(response => {
        response.data.Cookies.forEach(cookie => {
          Cookies.set(cookie.CookieName, cookie.CookieValue, {
            domain: cookie.CookieDomain
          });
        });

        onComplete();
      })
      .catch(() => onError())
      .finally(() => setIsResetting(false));
  }

  return (
    <LoginLayout
      buttons={[
        {
          appearance: 'primary',
          children: 'bevestig nieuw wachtwoord',
          disabled: !hashData,
          loading: isResetting,
          onClick: () => passwordResetFormRef.current?.submitForm()
        }
      ]}
      headingText="Kies een nieuw wachtwoord"
    >
      <Banner appearance="warning" show={Boolean(isExpiredHash)}>
        Sorry, de link om een nieuw wachtwoord in te stellen is verlopen. Om veiligheidsredenen is
        deze slechts 24 uur geldig.
      </Banner>

      {!hashData && (
        <div style={{ margin: '0 auto' }}>
          <Loader size="2xl" />
        </div>
      )}

      {hashData && isValidHash && (
        <PasswordResetForm
          email={hashData.mail}
          formRef={passwordResetFormRef}
          initialValues={{
            password: '',
            passwordConfirmation: ''
          }}
          onSubmit={formValues => handleSubmit(formValues, hashData.mail)}
        />
      )}
    </LoginLayout>
  );
};

export default PasswordReset;
