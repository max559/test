import type { CoreError } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { PasswordFormValues } from '../forms';

import { Banner, LinkText } from '@mediahuis/chameleon-react';
import { useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { PasswordForm } from '../forms';
import { LoginLayout } from '../layouts';

interface PasswordVerificationProps {
  email: string;
  formValues: PasswordFormValues;
  onForgotPassword: () => void;
  onPasswordlessLogin: () => void;
  onSuccess: (formValues: PasswordFormValues) => void;
}

const PasswordVerification = ({
  email,
  formValues,
  onForgotPassword,
  onPasswordlessLogin,
  onSuccess
}: PasswordVerificationProps): ReactElement => {
  const { brand, coreInstance, login } = useAuthStore(state => ({
    brand: state.config.brand,
    coreInstance: state.coreInstance,
    login: state.login
  }));

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isCreatingLoginLink, setIsCreatingLoginLink] = useState<boolean>(false);
  const [isVerifyingPassword, setIsVerifyingPassword] = useState<boolean>(false);

  const emailPasswordFormRef = useRef<FormikProps<PasswordFormValues>>(null);

  function handleForgotPassword() {
    coreInstance.accountManagementService
      .postMailModifyPasswordStandalone({
        brand,
        goto: typeof window !== 'undefined' ? window.location.href : '',
        mail: email,
        // source: '', // 'article',
        step: 1
      })
      .then(() => {
        setErrorMessage(null);
        onForgotPassword();
      })
      .catch(() => {
        setErrorMessage('Er ging iets mis bij het genereren van de email');
      });
  }

  function handlePasswordlessLogin() {
    setIsCreatingLoginLink(true);

    coreInstance.accountManagementService
      .postMailModifyPassword({
        brand,
        goto: typeof window !== 'undefined' ? window.location.href : '',
        mail: email,
        // source: '', // 'article',
        step: 1
      })
      .then(() => {
        setErrorMessage(null);
        setIsCreatingLoginLink(false);
        onPasswordlessLogin();
      })
      .catch(() => {
        setErrorMessage('Er ging iets mis bij het genereren van de email');
        setIsCreatingLoginLink(false);
      });
  }

  function handleSubmit(formValues: PasswordFormValues) {
    setIsVerifyingPassword(true);

    login(email, formValues.password)
      .then(() => {
        setErrorMessage(null);
        setIsVerifyingPassword(false);
        onSuccess(formValues);
      })
      .catch((error: CoreError) => {
        setIsVerifyingPassword(false);

        if (error.status === 403) {
          setErrorMessage('Dit wachtwoord is niet correct');
        } else {
          setErrorMessage('Er ging iets mis bij het authenticeren van je account');
        }
      });
  }

  return (
    <LoginLayout
      buttons={[
        {
          appearance: 'primary',
          children: 'Meld je aan',
          loading: isVerifyingPassword,
          onClick: () => emailPasswordFormRef.current?.submitForm()
        },
        {
          appearance: 'secondary',
          children: 'Meld je aan zonder wachtwoord',
          loading: isCreatingLoginLink,
          onClick: handlePasswordlessLogin
        }
      ]}
      headingText="Hoe meld je je liefst aan?"
    >
      <Banner appearance="error" show={Boolean(errorMessage)}>
        {errorMessage}
      </Banner>

      <div style={{ display: 'flex', flexDirection: 'column', gap: '0.25rem' }}>
        <PasswordForm
          email={email}
          formRef={emailPasswordFormRef}
          initialValues={formValues}
          onSubmit={handleSubmit}
        />
        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
          <LinkText as="span" style={{ fontSize: '14px' }} onClick={handleForgotPassword}>
            Wachtwoord vergeten
          </LinkText>
        </div>
      </div>
    </LoginLayout>
  );
};

export default PasswordVerification;
