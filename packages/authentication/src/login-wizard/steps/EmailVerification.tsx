import type { AccountStatus, CoreError } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { EmailFormValues } from '../forms';

import { Banner } from '@mediahuis/chameleon-react';
import { useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { EmailForm } from '../forms';
import { LoginLayout } from '../layouts';

interface EmailVerificationProps {
  formValues: EmailFormValues;
  onNotFound: (formValues: EmailFormValues) => void;
  onSuccess: (formValues: EmailFormValues, accountStatus: AccountStatus) => void;
}

const EmailVerification = ({
  formValues,
  onNotFound,
  onSuccess
}: EmailVerificationProps): ReactElement => {
  const { brandName, getUserStatus } = useAuthStore(state => ({
    brandName: state.config.brandName,
    getUserStatus: state.getUserStatus
  }));

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isVerifyingAccount, setIsVerifyingAccount] = useState<boolean>(false);

  const emailFormRef = useRef<FormikProps<EmailFormValues>>(null);

  function handleSubmit(formValues: EmailFormValues) {
    setIsVerifyingAccount(true);

    getUserStatus(formValues.email)
      .then((accountStatus: AccountStatus) => {
        setErrorMessage(null);
        setIsVerifyingAccount(false);
        onSuccess(formValues, accountStatus);
      })
      .catch((error: CoreError) => {
        setIsVerifyingAccount(false);

        if (error.status === 404) {
          setErrorMessage(null);
          onNotFound(formValues);
        } else {
          setErrorMessage(
            'Er ging iets mis bij het ophalen van je account. Controleer of je e-mailadres correct is.'
          );
        }
      });
  }

  return (
    <LoginLayout
      buttons={[
        {
          appearance: 'primary',
          children: 'Ga verder',
          loading: isVerifyingAccount,
          onClick: () => emailFormRef.current?.submitForm()
        }
      ]}
      headingText={`Welkom bij ${brandName}`}
    >
      <Banner appearance="error" show={Boolean(errorMessage)}>
        {errorMessage}
      </Banner>

      <EmailForm formRef={emailFormRef} initialValues={formValues} onSubmit={handleSubmit} />
    </LoginLayout>
  );
};

export default EmailVerification;
