import type { Country } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { ContactDataFormValues } from '../forms';

import { useEffect, useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { ContactDataForm } from '../forms';
import { RegistrationLayout } from '../layouts';

interface ContactInfoRegistrationProps {
  formValues: ContactDataFormValues;
  isStepperVariant: boolean;
  stepIndex: number;
  onBack: () => void;
  onNext: (formValues: ContactDataFormValues) => void;
}

const ContactInfoRegistration = ({
  formValues,
  isStepperVariant,
  stepIndex,
  onBack,
  onNext
}: ContactInfoRegistrationProps): ReactElement => {
  const coreInstance = useAuthStore(state => state.coreInstance);

  const [countries, setCountries] = useState<Array<Country>>([]);

  const contactDataFormRef = useRef<FormikProps<ContactDataFormValues>>(null);

  useEffect(() => {
    coreInstance.addressService.getCountriesByLanguage('nl').then(response => {
      setCountries(response.data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <RegistrationLayout
      activeStepIndex={stepIndex}
      buttons={[
        {
          appearance: 'primary',
          children: 'Ga verder',
          width: 'full',
          onClick: () => contactDataFormRef.current?.submitForm()
        }
      ]}
      title={
        isStepperVariant ? 'Maak een account en lees verder zonder abonnement' : 'Contactgegevens'
      }
      variant={isStepperVariant ? 'stepper' : 'default'}
      onBack={onBack}
    >
      <ContactDataForm
        countries={countries}
        formRef={contactDataFormRef}
        initialValues={formValues}
        onSubmit={onNext}
      />
    </RegistrationLayout>
  );
};

export default ContactInfoRegistration;
