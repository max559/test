import type { AccountStatus, CoreError } from '@subscriber/services-core';
import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { EmailFormValues } from '../forms';

import {
  Banner,
  Button,
  Caption,
  Heading,
  LinkText,
  Logo,
  Paragraph
} from '@mediahuis/chameleon-react';
import { useRef, useState } from 'react';

import { useAuthStore } from '../../context';
import { EmailForm } from '../forms';

interface EmailVerificationWallProps {
  articleTitle: string;
  formValues: EmailFormValues;
  onLogin: () => void;
  onNotFound: (formValues: EmailFormValues) => void;
  onRegister: () => void;
  onSuccess: (formValues: EmailFormValues, accountStatus: AccountStatus) => void;
}

const EmailVerificationWall = ({
  articleTitle,
  formValues,
  onLogin,
  onNotFound,
  onRegister,
  onSuccess
}: EmailVerificationWallProps): ReactElement => {
  const getUserStatus = useAuthStore(state => state.getUserStatus);

  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isVerifyingAccount, setIsVerifyingAccount] = useState<boolean>(false);

  const emailFormRef = useRef<FormikProps<EmailFormValues>>(null);

  function handleSubmit(formValues: EmailFormValues) {
    setIsVerifyingAccount(true);

    getUserStatus(formValues.email)
      .then((accountStatus: AccountStatus) => {
        setErrorMessage(null);
        setIsVerifyingAccount(false);
        onSuccess(formValues, accountStatus);
      })
      .catch((error: CoreError) => {
        setIsVerifyingAccount(false);

        if (error.status === 404) {
          setErrorMessage(null);
          onNotFound(formValues);
        } else {
          setErrorMessage(
            'Er ging iets mis bij het ophalen van je account. Controleer of je e-mailadres correct is.'
          );
        }
      });
  }

  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '1.5rem' }}>
      <div style={{ alignItems: 'center', display: 'flex', gap: '1rem' }}>
        <Logo name="brand-square-main" style={{ height: '4.5rem', width: '4.5rem' }} />
        <Heading level={4} size="md">
          Lezen zonder abonnement?
        </Heading>
      </div>

      <Paragraph>
        Meld je aan en lees <b style={{ fontWeight: 'bold' }}>{`'${articleTitle}'`}</b>.
      </Paragraph>

      <div style={{ display: 'flex', flexDirection: 'column', gap: '0.5rem' }}>
        <Banner appearance="error" show={Boolean(errorMessage)}>
          {errorMessage}
        </Banner>

        <EmailForm formRef={emailFormRef} initialValues={formValues} onSubmit={handleSubmit} />

        <Button
          appearance="primary"
          loading={isVerifyingAccount}
          onClick={() => emailFormRef.current?.submitForm()}
        >
          Ga verder
        </Button>

        <Caption style={{ alignSelf: 'center' }}>
          Nog geen account?{' '}
          <LinkText as="span" onClick={onRegister}>
            Registreer je
          </LinkText>
        </Caption>
      </div>

      <Caption style={{ alignSelf: 'center' }}>
        Al abonnee?{' '}
        <LinkText as="span" onClick={onLogin}>
          Klik hier om je aan te melden
        </LinkText>
      </Caption>
    </div>
  );
};

export default EmailVerificationWall;
