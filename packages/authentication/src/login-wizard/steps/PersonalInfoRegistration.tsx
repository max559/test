import type { FormikProps } from 'formik';
import type { ReactElement } from 'react';

import type { PersonalDataFormValues } from '../forms';

import { useRef } from 'react';

import { PersonalDataForm } from '../forms';
import { RegistrationLayout } from '../layouts';

interface PersonalInfoRegistrationProps {
  accountEmail: string;
  formValues: PersonalDataFormValues;
  isStepperVariant: boolean;
  stepIndex: number;
  onBack: () => void;
  onNext: (formValues: PersonalDataFormValues) => void;
}

const PersonalInfoRegistration = ({
  accountEmail,
  formValues,
  isStepperVariant,
  stepIndex,
  onBack,
  onNext
}: PersonalInfoRegistrationProps): ReactElement => {
  const personalDataFormRef = useRef<FormikProps<PersonalDataFormValues>>(null);

  return (
    <RegistrationLayout
      activeStepIndex={stepIndex}
      buttons={[
        {
          appearance: 'primary',
          children: 'Ga verder',
          width: 'full',
          onClick: () => personalDataFormRef.current?.submitForm()
        }
      ]}
      title={
        isStepperVariant ? 'Maak een account en lees verder zonder abonnement' : 'Persoonsgegevens'
      }
      variant={isStepperVariant ? 'stepper' : 'default'}
      onBack={onBack}
    >
      <PersonalDataForm
        email={accountEmail}
        formRef={personalDataFormRef}
        initialValues={formValues}
        onSubmit={onNext}
      />
    </RegistrationLayout>
  );
};

export default PersonalInfoRegistration;
