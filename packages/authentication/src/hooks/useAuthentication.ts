import { useAuthStore } from '../context';

export const useAuthentication = () => {
  const authenticationLuxState = useAuthStore(state => ({
    authenticationStatus: state.authenticationStatus,
    userInfo: state.userInfo,
    getUserInfo: state.getUserInfo,
    getUserStatus: state.getUserStatus,
    login: state.login,
    redirectToSignOut: state.redirectToSignOut
  }));

  return authenticationLuxState;
};
