import type {
  AccountInfoResponse,
  AccountStatus,
  AuthenticationResponse,
  CoreError
} from '@subscriber/services-core';
import type { StoreApi } from 'zustand';

import type { AuthStoreConfig } from '../types';

import { ServicesCore } from '@subscriber/services-core';
import Cookies from 'js-cookie';
import { createContext } from 'react';
import { createStore } from 'zustand';

import { AUTHENTICATION_STATUS } from '../types';
import { getSignOutUrl } from '../utils';

export interface AuthStore {
  authenticationStatus: AUTHENTICATION_STATUS;
  config: AuthStoreConfig;
  coreInstance: ServicesCore;
  userInfo: AccountInfoResponse | null;
  getUserInfo: () => Promise<AccountInfoResponse | null>;
  getUserStatus: (username: string) => Promise<AccountStatus>;
  login: (username: string, password: string) => Promise<AuthenticationResponse>;
  redirectToSignOut: (goto?: string) => void;
}

export const AuthContext = createContext<StoreApi<AuthStore> | null>(null);

export const createAuthStore = (config: AuthStoreConfig): StoreApi<AuthStore> => {
  return createStore<AuthStore>()((set, get) => ({
    authenticationStatus: AUTHENTICATION_STATUS.UNKNOWN,
    config,
    coreInstance: new ServicesCore(config.brand, config.environment),
    userInfo: null,
    getUserInfo: () =>
      new Promise((resolve, reject) => {
        set({ authenticationStatus: AUTHENTICATION_STATUS.LOADING });

        get()
          .coreInstance.accountManagementService.getAccountInfo()
          .then(accountInfoResponse => {
            const userInfo = accountInfoResponse.data;

            set({
              authenticationStatus: AUTHENTICATION_STATUS.AUTHENTICATED,
              userInfo
            });

            resolve(userInfo);
          })
          .catch(({ status }) => {
            if (status === 401) {
              set({
                authenticationStatus: AUTHENTICATION_STATUS.UNAUTHENTICATED,
                userInfo: null
              });

              resolve(null);
            } else {
              set({
                authenticationStatus: AUTHENTICATION_STATUS.ERROR,
                userInfo: null
              });

              reject();
            }
          });
      }),
    getUserStatus: username =>
      get()
        .coreInstance.accountManagementService.getAccountStatus(username)
        .then(response => response.data),
    login: (username, password) =>
      new Promise((resolve, reject) => {
        get()
          .coreInstance.authService.postAuthentication({
            password,
            username
          })
          .then(response => {
            response.data.cookies.forEach(cookie => {
              Cookies.set(cookie.cookieName, cookie.cookieValue, {
                domain: cookie.cookieDomain,
                path: cookie.cookiePath ? cookie.cookiePath : '/'
              });
            });

            resolve(response.data);
          })
          .catch((error: CoreError) => reject(error));
      }),
    redirectToSignOut: goto => {
      const signOutUrl = new URL(getSignOutUrl(get().config.brand, get().config.environment));

      if (goto) {
        signOutUrl.searchParams.set('goto', goto);
      }

      window.location.assign(signOutUrl);
    }
  }));
};
