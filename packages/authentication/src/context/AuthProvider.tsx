import type { Environment } from '@subscriber/globals';
import type { PropsWithChildren, ReactElement } from 'react';

import { useRef } from 'react';

import { Authentication } from '../components/Authentication';
import { getBrandName } from '../utils';
import { AuthContext, createAuthStore } from './AuthContext';

interface AuthProviderProps {
  autoAuthentication?: boolean;
  brand: 'dl' | 'ds' | 'gva' | 'hbvl' | 'nb';
  environment: Environment;
}

const AuthProvider = ({
  autoAuthentication = true,
  brand,
  children,
  environment
}: PropsWithChildren<AuthProviderProps>): ReactElement => {
  const store = useRef(
    createAuthStore({
      autoAuthentication,
      brand,
      brandName: getBrandName(brand),
      environment
    })
  ).current;

  return (
    <AuthContext.Provider value={store}>
      {autoAuthentication ? <Authentication silent>{children}</Authentication> : children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
