import type { AuthStore } from './AuthContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { AuthContext } from './AuthContext';

const useAuthStore = <T>(selector: (state: AuthStore) => T): T => {
  const authContext = useContext(AuthContext);

  if (!authContext) {
    throw new Error('The useAuthStore hook can only be used in AuthProvider');
  }

  return useStore(authContext, selector);
};

export default useAuthStore;
