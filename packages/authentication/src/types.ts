import type { Environment } from '@subscriber/globals';

export type Brand = 'dl' | 'ds' | 'gva' | 'hbvl' | 'nb';

export enum AUTHENTICATION_STATUS {
  AUTHENTICATED = 'authenticated',
  ERROR = 'error',
  LOADING = 'loading',
  UNAUTHENTICATED = 'unauthenticated',
  UNKNOWN = 'unknown'
}

export interface AuthStoreConfig {
  autoAuthentication: boolean;
  brand: Brand;
  brandName: string;
  environment: Environment;
}
