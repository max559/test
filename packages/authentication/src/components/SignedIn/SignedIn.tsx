import type { PropsWithChildren, ReactElement } from 'react';

import { useAuthStore } from '../../context';

import { AUTHENTICATION_STATUS } from '../../types';

const SignedIn = ({ children }: PropsWithChildren): ReactElement | null => {
  const authenticationStatus = useAuthStore(state => state.authenticationStatus);

  if (authenticationStatus === AUTHENTICATION_STATUS.AUTHENTICATED) {
    return <>{children}</>;
  }

  return null;
};

export default SignedIn;
