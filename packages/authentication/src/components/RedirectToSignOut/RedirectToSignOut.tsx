import { useEffect } from 'react';

import { useAuthStore } from '../../context';

interface RedirectToSignOutProps {
  goto?: string;
}

const RedirectToSignOut = ({ goto }: RedirectToSignOutProps): null => {
  const redirectToSignOut = useAuthStore(state => state.redirectToSignOut);

  useEffect(() => {
    redirectToSignOut(goto);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default RedirectToSignOut;
