import type { Environment } from '@subscriber/globals';

import type { Brand } from './types';

export const getBrandName = (brand: Brand): string => {
  const brandNames: Record<Brand, string> = {
    dl: 'De Limburger',
    ds: 'De Standaard',
    gva: 'Gazet van Antwerpen',
    hbvl: 'Het Belang van Limburg',
    nb: 'Nieuwsblad'
  };

  return brandNames[brand];
};

export const getSignOutUrl = (brand: Brand, environment: Environment): string => {
  const logoffUrls: Record<Brand, Record<Environment, string>> = {
    dl: {
      test: 'http://test.limburger.nl/account/logoff',
      preview: 'http://preview.limburger.nl/account/logoff',
      production: 'http://limburger.nl/account/logoff'
    },
    ds: {
      test: 'http://test.standaard.be/account/logoff',
      preview: 'http://preview.standaard.be/account/logoff',
      production: 'http://standaard.be/account/logoff'
    },
    gva: {
      test: 'http://test.gva.be/account/logoff',
      preview: 'http://preview.gva.be/account/logoff',
      production: 'http://gva.be/account/logoff'
    },
    hbvl: {
      test: 'http://test.hbvl.be/account/logoff',
      preview: 'http://preview.hbvl.be/account/logoff',
      production: 'http://hbvl.be/account/logoff'
    },
    nb: {
      test: 'http://test.nieuwsblad.be/account/logoff',
      preview: 'http://preview.nieuwsblad.be/account/logoff',
      production: 'http://nieuwsblad.be/account/logoff'
    }
  };

  return logoffUrls[brand][environment];
};
