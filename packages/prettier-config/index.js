const defaultMhConfig = require('@mediahuis/prettier-config');

module.exports = {
  ...defaultMhConfig,
  printWidth: 100,
  trailingComma: 'none'
};
