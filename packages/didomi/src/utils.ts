import type { IDidomiObject } from '@didomi/react';

import type { GdprConsents } from './types';

import { DIDOMI_PURPOSE } from './types';

export const createGdprConsents = (didomiObject: IDidomiObject): GdprConsents => {
  return didomiObject
    .getRequiredPurposes()
    .reduce((acc: GdprConsents, purpose: { id: DIDOMI_PURPOSE }) => {
      acc[purpose.id] = Boolean(didomiObject.getUserConsentStatusForPurpose(purpose.id));

      return acc;
    }, {});
};
