import type { IDidomiObject } from '@didomi/react';
import type { ReactElement } from 'react';

import type { GdprConsents } from './types';

import { DidomiSDK } from '@didomi/react';

import { createGdprConsents } from './utils';

declare global {
  interface Window {
    Didomi?: IDidomiObject;
    gdprConsents?: GdprConsents;
  }
}

const didomiConsentEvent = new Event('didomi-consent');
const didomiReadyEvent = new Event('didomi-ready');

interface DidomiProps {
  language: 'de' | 'en' | 'nl';
  onConsentChanged?: () => void;
  onReady?: () => void;
}

const Didomi = ({ language, onConsentChanged, onReady }: DidomiProps): ReactElement => {
  return (
    <DidomiSDK
      apiKey="11ef8ac9-6270-4d5e-8b99-8d6a5bd60059"
      config={{
        user: {
          bots: {
            consentRequired: false,
            types: ['crawlers', 'performance'],
            extraUserAgents: ['mh_monitoring']
          }
        },
        languages: {
          default: language,
          enabled: [language]
        }
      }}
      gdprAppliesGlobally
      iabVersion={2}
      onConsentChanged={() => {
        window.dispatchEvent(didomiConsentEvent);

        if (window.Didomi) {
          window.gdprConsents = createGdprConsents(window.Didomi);
        }
        if (onConsentChanged) {
          onConsentChanged();
        }
      }}
      onReady={(didomi: IDidomiObject) => {
        window.gdprConsents = createGdprConsents(didomi);
        window.dispatchEvent(didomiReadyEvent);

        if (onReady) {
          onReady();
        }
      }}
    />
  );
};

export default Didomi;
