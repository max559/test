export type { IDidomiObject } from '@didomi/react';

export { default as Didomi } from './Didomi';
export * from './types';
