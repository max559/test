export enum DIDOMI_PURPOSE {
  cookies = 'cookies',
  createAdsProfile = 'create_ads_profile',
  createContentProfile = 'create_content_profile',
  improveProducts = 'improve_products',
  marketResearch = 'market_research',
  measureAdPerformance = 'measure_ad_performance',
  measureContentPerformance = 'measure_content_performance',
  selectBasicAds = 'select_basic_ads',
  selectPersonalizedAds = 'select_personalized_ads',
  selectPersonalizedContent = 'select_personalized_content'
}

export type GdprConsents = Record<DIDOMI_PURPOSE, boolean>;
