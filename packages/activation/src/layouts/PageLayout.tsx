import { BORDER_RADIUS } from '@subscriber/globals';
import type { PropsWithChildren, ReactElement } from 'react';

const PageLayout = ({ children }: PropsWithChildren<Record<string, unknown>>): ReactElement => {
  return (
    <div
      style={{
        alignItems: 'center',
        border: '1px solid var(--color-neutral-20)',
        borderRadius: BORDER_RADIUS.default,
        display: 'flex',
        flexDirection: 'column',
        gap: '2rem',
        justifyContent: 'center',
        margin: '3rem auto',
        maxWidth: '750px',
        padding: '3rem'
      }}
    >
      {children}
    </div>
  );
};

export default PageLayout;
