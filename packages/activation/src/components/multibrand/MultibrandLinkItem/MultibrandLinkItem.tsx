import { ReactElement } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { Icon } from '@mediahuis/chameleon-react';
import { Checkmark, ChevronForward } from '@mediahuis/chameleon-theme-wl/icons';
import { BORDER_RADIUS } from '@subscriber/globals';
import { Flex } from '@subscriber/subscriber-ui';

import { LOGO_URL } from '../Multibrand.constants';
import './MultibrandLinkItem.css';

export type MultiBrandItem = { brand: string; brandName: string; url: string };

interface MultibrandLinkItemProps {
  item: MultiBrandItem;
  active?: boolean;
}

const MultibrandLinkItem = ({ item, active = false }: MultibrandLinkItemProps): ReactElement => {
  const { t } = useTranslation('Components');

  return (
    <a href={item.url} rel="noreferrer" target="_blank" className="multibrand-link-item">
      <Flex
        alignItems="center"
        gap={4}
        justifyContent="space-between"
        style={{
          border: '1px solid var(--color-neutral-20)',
          borderRadius: BORDER_RADIUS.default,
          color:
            'var( --ch-paragraph-color,var(--semantic-foreground-base-adaptive-default-fill) )',
          padding: '1rem'
        }}
      >
        <Flex alignItems="center" gap={8}>
          <div style={{ flexShrink: 0, position: 'relative', width: '3rem' }}>
            <img
              alt={`${item.brand}-logo`}
              src={`${LOGO_URL}/plus-brand-square-${item.brand}-color.svg`}
              style={{ height: '3rem', verticalAlign: 'middle' }}
            />
            <span
              style={{
                alignItems: 'center',
                background: 'var(--color-green-60)',
                borderRadius: '50%',
                display: 'flex',
                justifyContent: 'center',
                position: 'absolute',
                right: '-0.5rem',
                top: '-0.5rem'
              }}
            >
              <Icon as={Checkmark} size="sm" color="var(--color-neutral-10)" />
            </span>
          </div>
          <span>
            <Trans
              i18nKey="multibrand.multibrandLinkItem.text"
              t={t}
              values={{
                brandName: item.brandName
              }}
            />
          </span>
        </Flex>
        <Icon as={ChevronForward} />
      </Flex>
    </a>
  );
};

export default MultibrandLinkItem;
