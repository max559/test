import { MultiBrandItem } from './MultibrandLinkItem';

export const LOGO_URL = 'https://static.hbvl.be/Assets/Subscriber/aboshop/';

export const MULTIBRANDS: MultiBrandItem[] = [
  { brand: 'nb', brandName: 'het Nieuwsblad', url: 'https://urlgeni.us/nbonieuws/+plus' },
  { brand: 'ds', brandName: 'De Standaard', url: 'https://urlgeni.us/dsnieuws/activatie-plus' },
  { brand: 'gva', brandName: 'Gazet van Antwerpen', url: 'https://urlgeni.us/+plus' },
  { brand: 'hbvl', brandName: 'Het Belang van Limburg', url: 'https://urlgeni.us/hbvl/+plus' },
  { brand: 'dg', brandName: 'De Gentenaar', url: 'https://www.gentenaar.be' }
];
