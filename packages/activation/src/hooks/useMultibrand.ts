export type SubscriptionType =
  | 'C'
  | 'T_C'
  | 'CW'
  | 'T_CW'
  | 'D'
  | 'T_D'
  | 'M'
  | 'MH'
  | 'T_MH'
  | 'P'
  | 'PW'
  | 'S'
  | 'W'
  | 'T_W'
  | 'Z';

const useMultibrand = () => {
  return {
    isMultibrandSubscriptionType: (subscriptionType: SubscriptionType): boolean => {
      switch (subscriptionType) {
        case 'C':
        case 'T_C':
        case 'CW':
        case 'T_CW':
        case 'D':
        case 'T_D':
        case 'MH':
        case 'T_MH':
        case 'W':
        case 'T_W':
          return true;
        default:
          return false;
      }
    }
  };
};

export default useMultibrand;
