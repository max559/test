import type { SubscriptionAddressRequest, SubscriptionStatus } from '@subscriber/services-core';
import type { SubscriptionType } from './useMultibrand';

import { SUBSCRIPTION_STATUS } from '@subscriber/services-core';
import {
  useActivateSubscription,
  useLinkSubscription,
  useSubscriptionInfo
} from '@subscriber/services-hooks';
import { useEffect, useState } from 'react';

import { useActivationContext } from '../context/Activation.context';
import useMultibrand from './useMultibrand';

export enum ACTIVATION_STATUS {
  Activate = 'Activate',
  AlreadyLinked = 'AlreadyLinked',
  Error = 'Error',
  Linked = 'Linked',
  NoAccess = 'NoAccess',
  NotFound = 'NotFound',
  NotLinked = 'NotLinked'
}

const useActivationStatus = (accountGuid?: string, idToken?: string) => {
  const [isMultibrand, setIsMultibrand] = useState<boolean>(false);
  const [status, setStatus] = useState<ACTIVATION_STATUS | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { brand } = useActivationContext();

  const activateSubscription = useActivateSubscription(idToken);
  const linkSubscription = useLinkSubscription(idToken);
  const { isMultibrandSubscriptionType } = useMultibrand();
  const subscriptionInfoQuery = useSubscriptionInfo(encodeURI(accountGuid as string), idToken);

  useEffect(() => {
    if (linkSubscription.isError || subscriptionInfoQuery.isError) {
      setStatus(ACTIVATION_STATUS.Error);
    }
  }, [linkSubscription.isError, subscriptionInfoQuery.isError]);

  useEffect(() => {
    if (linkSubscription.isSuccess) {
      if (linkSubscription.data.isLinked) {
        setStatus(ACTIVATION_STATUS.Linked);
      } else {
        setStatus(ACTIVATION_STATUS.NotLinked);
      }
      setIsLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [linkSubscription.isSuccess]);

  useEffect(() => {
    if (!subscriptionInfoQuery.isFetching && subscriptionInfoQuery.isSuccess) {
      if (subscriptionInfoQuery.data.subscription) {
        if (
          isMultibrandSubscriptionType(
            subscriptionInfoQuery.data.subscription.type as SubscriptionType
          )
        ) {
          setIsMultibrand(true);
        }
        setStatus(ACTIVATION_STATUS.Linked);
      } else if (!status) {
        setStatus(ACTIVATION_STATUS.Activate);
        setIsLoading(false);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [subscriptionInfoQuery.isFetching, subscriptionInfoQuery.isSuccess]);

  return {
    status,
    isLoading,
    isMultibrand,
    activate: (request: SubscriptionAddressRequest) => {
      setIsLoading(true);
      activateSubscription
        .mutateAsync(request)
        .then((data: SubscriptionStatus) => {
          const isCurrentNumberLinked: boolean | undefined = data.subscriptions?.some(
            subscription => subscription.subscriptionNumber === request.subscriptionNumber
          );

          let linkRequest = request;

          if (brand === 'dl') {
            const subscriptionNumber: string | undefined = data.subscriptions?.find(
              subscription => subscription.subscriptionNumber
            )?.subscriptionNumber;

            linkRequest = {
              ...request,
              subscriptionNumber: subscriptionNumber || ''
            };
          }

          switch (data.status) {
            case SUBSCRIPTION_STATUS.FoundButLinked:
              if (isCurrentNumberLinked) {
                setStatus(ACTIVATION_STATUS.Linked);
              } else {
                setStatus(ACTIVATION_STATUS.AlreadyLinked);
              }
              break;
            case SUBSCRIPTION_STATUS.FoundNoAccess:
              setStatus(ACTIVATION_STATUS.NoAccess);
              break;
            case SUBSCRIPTION_STATUS.FoundNotLinked:
              linkSubscription.mutateAsync(linkRequest);

              break;
            case SUBSCRIPTION_STATUS.NotFound:
              setStatus(ACTIVATION_STATUS.NotFound);
              break;
            default:
              setStatus(ACTIVATION_STATUS.Error);
          }
        })
        .catch(() => {
          setIsLoading(false);
          setStatus(ACTIVATION_STATUS.Error);
        });
    },
    reset: () => {
      setStatus(null);
      subscriptionInfoQuery.remove();
      setIsLoading(false);
    }
  };
};

export default useActivationStatus;
