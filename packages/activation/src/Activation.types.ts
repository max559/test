import { EVENT_TYPE } from '@subscriber/globals';

export enum TRACKING_SHOW_EVENTS {
  ACTIVATION_ERROR = 'ActivationErrorShow',
  ACTIVATION_HAS_ACCESS = 'ActivationHasAccessShow',
  ACTIVATION_NO_ACCESS = 'ActivationNoAccessShow',
  ACTIVATION_START = 'ActivationStartShow',
  ACTIVATION_SUCCESS = 'ActivationSuccessShow',
  ACTIVATION_SUCCESS_MULTIBRAND = 'ActivationSuccessMultibrandShow'
}

export enum TRACKING_CLICK_EVENTS {
  ACTIVATION_ERROR_CONTACT = 'ActivationErrorContact',
  ACTIVATION_HAS_ACCESS_CONTACT = 'ActivationHasAccessContact',
  ACTIVATION_NO_ACCESS_CONTACT = 'ActivationNoAccessContact',
  ACTIVATION_START_CONTACT = 'ActivationStartContact',
  ACTIVATION_SUCCESS_CONTACT = 'ActivationSuccessContact'
}

export type TrackingShowEvent<Key extends TRACKING_SHOW_EVENTS, Data> = {
  type: EVENT_TYPE.SHOW;
  key: Key;
  data?: Data;
};

export type TrackingClickEvent<Key extends TRACKING_CLICK_EVENTS, Data> = {
  type: EVENT_TYPE.CLICK;
  key: Key;
  data?: Data;
};

export type ActivationEvent =
  | ActivationErrorShow
  | ActivationHasAccessShow
  | ActivationNoAccessShow
  | ActivationStartShow
  | ActivationSuccessMultibrandShow
  | ActivationSuccessShow
  | ActivationErrorContact
  | ActivationHasAccessContact
  | ActivationNoAccessContact
  | ActivationStartContact
  | ActivationSuccessContact;

export type ActivationErrorShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_ERROR,
  undefined
>;

export type ActivationHasAccessShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_HAS_ACCESS,
  undefined
>;

export type ActivationNoAccessShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_NO_ACCESS,
  undefined
>;

export type ActivationStartShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_START,
  undefined
>;

export type ActivationSuccessShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS,
  undefined
>;

export type ActivationSuccessMultibrandShow = TrackingShowEvent<
  TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS_MULTIBRAND,
  undefined
>;

export type ActivationErrorContact = TrackingClickEvent<
  TRACKING_CLICK_EVENTS.ACTIVATION_ERROR_CONTACT,
  undefined
>;

export type ActivationHasAccessContact = TrackingClickEvent<
  TRACKING_CLICK_EVENTS.ACTIVATION_HAS_ACCESS_CONTACT,
  undefined
>;

export type ActivationNoAccessContact = TrackingClickEvent<
  TRACKING_CLICK_EVENTS.ACTIVATION_NO_ACCESS_CONTACT,
  undefined
>;

export type ActivationStartContact = TrackingClickEvent<
  TRACKING_CLICK_EVENTS.ACTIVATION_START_CONTACT,
  undefined
>;

export type ActivationSuccessContact = TrackingClickEvent<
  TRACKING_CLICK_EVENTS.ACTIVATION_SUCCESS_CONTACT,
  undefined
>;

export interface UserInfo {
  address: {
    box?: string;
    city?: string;
    country?: string;
    houseNumber?: string;
    postalCode?: string;
    street?: string;
  };
  email?: string;
  firstName?: string;
  id: string;
  idToken?: string;
  lastName?: string;
}
