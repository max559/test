import type { MouseEventHandler, ReactElement } from 'react';

import { Heading, LinkText, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { EVENT_TYPE } from '@subscriber/globals';
import { Flex } from '@subscriber/subscriber-ui';
import { useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import {
  ActivationHasAccessContact,
  ActivationHasAccessShow,
  TRACKING_CLICK_EVENTS,
  TRACKING_SHOW_EVENTS
} from '../../Activation.types';
import { useActivationContext } from '../../context';

interface ActivationHasAccessProps {
  onContactCustomerService: MouseEventHandler<HTMLAnchorElement>;
}

const ActivationHasAccess = ({
  onContactCustomerService
}: ActivationHasAccessProps): ReactElement => {
  const { t } = useTranslation('Activation');

  const { userInfo, onEvent } = useActivationContext();

  const showEvent: ActivationHasAccessShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_HAS_ACCESS
  };

  const clickContactEvent: ActivationHasAccessContact = {
    type: EVENT_TYPE.CLICK,
    key: TRACKING_CLICK_EVENTS.ACTIVATION_HAS_ACCESS_CONTACT
  };

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading size="lg">{t('activationHasAccess.title')}</Heading>
      </Flex>

      <Paragraph>
        <Trans
          components={[
            <span key="userEmail" style={{ fontWeight: 'bold' }} />,
            <LinkText
              key="contactCustomerService"
              onClick={event => {
                onEvent && onEvent(clickContactEvent);
                onContactCustomerService(event);
              }}
            />
          ]}
          i18nKey="activationHasAccess.message"
          t={t}
          values={{
            userEmail: userInfo?.email
          }}
        />
      </Paragraph>
    </Flex>
  );
};

export default ActivationHasAccess;
