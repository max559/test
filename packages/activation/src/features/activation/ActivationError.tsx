import type { MouseEventHandler, ReactElement } from 'react';

import { Heading, LinkText, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { EVENT_TYPE } from '@subscriber/globals';
import { Flex } from '@subscriber/subscriber-ui';
import { useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import {
  ActivationErrorContact,
  ActivationErrorShow,
  TRACKING_CLICK_EVENTS,
  TRACKING_SHOW_EVENTS
} from '../../Activation.types';
import { useActivationContext } from '../../context';

interface ActivationErrorProps {
  onContactCustomerService: MouseEventHandler<HTMLAnchorElement>;
}

const ActivationError = ({ onContactCustomerService }: ActivationErrorProps): ReactElement => {
  const { t } = useTranslation('Activation');

  const { onEvent } = useActivationContext();

  const showEvent: ActivationErrorShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_ERROR
  };

  const clickContactEvent: ActivationErrorContact = {
    type: EVENT_TYPE.CLICK,
    key: TRACKING_CLICK_EVENTS.ACTIVATION_ERROR_CONTACT
  };

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading size="lg">{t('activationError.title')}</Heading>
      </Flex>

      <Paragraph>
        <Trans
          components={[
            <LinkText
              key="contactCustomerService"
              onClick={event => {
                onEvent && onEvent(clickContactEvent);
                onContactCustomerService(event);
              }}
            />
          ]}
          i18nKey="activationError.message"
          t={t}
        />
      </Paragraph>
    </Flex>
  );
};

export default ActivationError;
