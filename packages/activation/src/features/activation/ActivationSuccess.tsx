import type { MouseEventHandler, ReactElement } from 'react';

import { Heading, LinkText, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import { EVENT_TYPE, getConfiguration } from '@subscriber/globals';
import { useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import {
  ActivationSuccessContact,
  ActivationSuccessShow,
  TRACKING_CLICK_EVENTS,
  TRACKING_SHOW_EVENTS
} from '../../Activation.types';
import { useActivationContext } from '../../context';
import { MULTIBRANDS, MultibrandLinkItem } from '../../components/multibrand';

interface ActivationSuccessProps {
  onContactCustomerService: MouseEventHandler<HTMLAnchorElement>;
}

const ActivationSuccess = ({ onContactCustomerService }: ActivationSuccessProps): ReactElement => {
  const { t } = useTranslation('Activation');
  const { userInfo, brand, environment, onEvent } = useActivationContext();
  const { brandName } = getConfiguration(brand, environment);

  const showEvent: ActivationSuccessShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS
  };

  const clickContactEvent: ActivationSuccessContact = {
    type: EVENT_TYPE.CLICK,
    key: TRACKING_CLICK_EVENTS.ACTIVATION_SUCCESS_CONTACT
  };

  const activeBrandItem = MULTIBRANDS.find(item => item.brand === brand);
  const otherBrandItems = MULTIBRANDS.filter(item => item.brand !== brand);
  const filteredBrandNames = otherBrandItems
    .filter(item => item.brand !== 'dg')
    .map(item => item.brandName);

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading key="title" size="lg">
          {t('activationSuccess.title')}
        </Heading>
      </Flex>

      <Paragraph>
        <Trans
          components={[<span key="userEmail" style={{ fontWeight: 'bold' }} />]}
          i18nKey="activationSuccess.message"
          t={t}
          values={{
            brandName: brandName,
            userEmail: userInfo?.email
          }}
        />
      </Paragraph>

      <Flex gap={4}>{activeBrandItem && <MultibrandLinkItem item={activeBrandItem} />}</Flex>

      <Flex alignItems="center" gap={2}>
        <Logo name="subscriptions.plus-icon-name" style={{ height: '1rem' }} />
        <Paragraph style={{ margin: 0 }}>{t('activationSuccess.plusNotIncluded')}</Paragraph>
      </Flex>

      <Paragraph>
        <Trans
          components={[
            <LinkText
              onClick={event => {
                onEvent && onEvent(clickContactEvent);
                onContactCustomerService(event);
              }}
              key="contactCustomerService"
            />
          ]}
          i18nKey="activationSuccess.ctaPlus"
          t={t}
          values={{
            brandName: brandName,
            otherBrandNames: filteredBrandNames.join(', ')
          }}
        />
      </Paragraph>
    </Flex>
  );
};

export default ActivationSuccess;
