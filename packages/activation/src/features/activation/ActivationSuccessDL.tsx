import type { ReactElement } from 'react';

import { Button, Heading, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { Flex } from '@subscriber/subscriber-ui';
import { EVENT_TYPE, getConfiguration } from '@subscriber/globals';
import { useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import { ActivationSuccessShow, TRACKING_SHOW_EVENTS } from '../../Activation.types';
import { useActivationContext } from '../../context';

const ActivationSuccessDL = (): ReactElement => {
  const { t } = useTranslation('Activation');
  const { userInfo, brand, environment, onEvent } = useActivationContext();
  const { brandName, siteUrl } = getConfiguration(brand, environment);

  const showEvent: ActivationSuccessShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS
  };

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading key="title" size="lg">
          {t('activationSuccess.title')}
        </Heading>
      </Flex>

      <div>
        <Paragraph>
          <Trans
            components={[<span key="userEmail" style={{ fontWeight: 'bold' }} />]}
            i18nKey="activationSuccess.message"
            t={t}
            values={{
              brandName: brandName,
              userEmail: userInfo?.email
            }}
          />
        </Paragraph>

        <Paragraph>{t('activationSuccess.startReading')}</Paragraph>
      </div>

      <Button appearance="primary" onClick={() => (window.location.href = siteUrl)}>
        {t('activationSuccess.startReadingButton')}
      </Button>
    </Flex>
  );
};

export default ActivationSuccessDL;
