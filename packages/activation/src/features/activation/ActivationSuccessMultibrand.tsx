import type { ReactElement } from 'react';

import { Button, Heading, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { EVENT_TYPE, getConfiguration } from '@subscriber/globals';
import { Flex } from '@subscriber/subscriber-ui';
import { useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import { ActivationSuccessMultibrandShow, TRACKING_SHOW_EVENTS } from '../../Activation.types';
import { MULTIBRANDS, MultibrandLinkItem } from '../../components/multibrand';
import { useActivationContext } from '../../context';

const ActivationSuccessMultibrand = (): ReactElement => {
  const { t } = useTranslation('Activation');
  const { userInfo, brand, environment, onEvent } = useActivationContext();
  const { brandName } = getConfiguration(brand, environment);

  const showEvent: ActivationSuccessMultibrandShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_SUCCESS_MULTIBRAND
  };

  const activeBrandItem = MULTIBRANDS.find(item => item.brand === brand);
  const otherBrandsItems = MULTIBRANDS.filter(item => item.brand !== brand);

  const otherBrandsList = (): string => {
    const otherBrands = MULTIBRANDS.filter(
      item => item.brand !== activeBrandItem?.brand && item.brand !== 'dg'
    ).map(item => item.brandName);

    return `${otherBrands.join(', ')} en De Gentenaar`;
  };

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading key="title" size="lg">
          {t('activationSuccessMultibrand.title')}
        </Heading>
      </Flex>

      <div>
        <Paragraph>{t('activationSuccessMultibrand.startReading')}</Paragraph>

        <Paragraph>
          <Trans
            components={[<span key="userEmail" style={{ fontWeight: 'bold' }} />]}
            i18nKey="activationSuccessMultibrand.message"
            t={t}
            values={{
              brandName: brandName,
              userEmail: userInfo?.email,
              otherBrands: otherBrandsList()
            }}
          />
        </Paragraph>
      </div>

      <Flex gap={4} flexWrap="wrap">
        {activeBrandItem && <MultibrandLinkItem item={activeBrandItem} active />}
        {otherBrandsItems &&
          otherBrandsItems.map(item => <MultibrandLinkItem key={item.brand} item={item} />)}
      </Flex>
    </Flex>
  );
};

export default ActivationSuccessMultibrand;
