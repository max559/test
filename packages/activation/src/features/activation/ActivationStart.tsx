import type { MouseEventHandler, ReactElement } from 'react';
import type { FormikProps } from 'formik';
import type { ActivationFormValues } from '../../forms';

import { Button, Caption, Heading, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { EVENT_TYPE } from '@subscriber/globals';
import { useValidateAddress } from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import { RefObject, useEffect, useRef, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import {
  ActivationStartContact,
  ActivationStartShow,
  TRACKING_CLICK_EVENTS,
  TRACKING_SHOW_EVENTS
} from '../../Activation.types';
import { useActivationContext } from '../../context';
import { ActivationForm } from '../../forms';

interface ActivationStartProps {
  formValues?: ActivationFormValues;
  isLoading: boolean;
  onContactCustomerService: MouseEventHandler<HTMLAnchorElement>;
  onSubmit: (values: ActivationFormValues) => void;
}

const ActivationStart = ({
  formValues,
  isLoading,
  onContactCustomerService,
  onSubmit
}: ActivationStartProps): ReactElement => {
  const { t } = useTranslation('Activation');
  const [isValidationError, setIsValidationError] = useState<boolean>(false);

  const { userInfo, onEvent } = useActivationContext();
  const validateAddress = useValidateAddress();

  const subscriptionFormRef: RefObject<FormikProps<ActivationFormValues>> =
    useRef<FormikProps<ActivationFormValues>>(null);

  const showEvent: ActivationStartShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_START
  };

  const clickContactEvent: ActivationStartContact = {
    type: EVENT_TYPE.CLICK,
    key: TRACKING_CLICK_EVENTS.ACTIVATION_START_CONTACT
  };

  useEffect(() => {
    if (validateAddress.isError) {
      setIsValidationError(true);
    }
  }, [validateAddress.isError]);

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading size="lg">{t('activationStart.title')}</Heading>
      </Flex>

      <Paragraph>
        <Trans
          components={[<span key="userEmail" style={{ fontWeight: 'bold' }} />]}
          i18nKey="activationStart.intro"
          t={t}
          values={{
            userEmail: userInfo?.email
          }}
        />
      </Paragraph>

      <ActivationForm
        formRef={subscriptionFormRef}
        initialValues={
          formValues || {
            box: userInfo?.address.box || '',
            city: userInfo?.address.city
              ? {
                  CityId: 0,
                  Id: 0,
                  Name: userInfo?.address.city,
                  PostalCode: userInfo.address.postalCode
                }
              : undefined,
            country: userInfo?.address.country || 'BE',
            houseNumber: userInfo?.address.houseNumber || '',
            postalCode: userInfo?.address.postalCode || '',
            street: userInfo?.address.street
              ? { StreetId: 0, Name: userInfo?.address.street }
              : undefined,
            subscriptionNumber: ''
          }
        }
        onContactCustomerService={event => {
          onEvent && onEvent(clickContactEvent);
          onContactCustomerService(event);
        }}
        onSubmit={values => {
          if (values.city && values.street) {
            validateAddress
              .mutateAsync({
                AddressToValidate: {
                  BoxNumber: values.box,
                  City: values?.city,
                  Country: values.country,
                  HouseNumber: values.houseNumber,
                  Street: values.street
                }
              })
              .then(data => {
                if (data.ValidationResult.IsValid) {
                  onSubmit(values);
                  setIsValidationError(false);
                } else {
                  setIsValidationError(true);
                }
              });
          }
        }}
      />

      <Flex flexDirection="column" gap={2}>
        <Button
          appearance="primary"
          className="!font-semibold"
          width="full"
          loading={isLoading}
          onClick={() => subscriptionFormRef.current?.submitForm()}
        >
          {t('activationStart.button')}
        </Button>

        {isValidationError && <Caption color="RedBase">{t('activationStart.error')}</Caption>}

        <Caption style={{ textAlign: 'center' }}>{t('activationStart.buttonCaption')}</Caption>
      </Flex>
    </Flex>
  );
};

export default ActivationStart;
