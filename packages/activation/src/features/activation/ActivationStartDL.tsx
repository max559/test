import type { ReactElement } from 'react';
import type { FormikProps } from 'formik';
import type { ActivationFormDLValues } from '../../forms';

import { Button, Caption, Heading, Logo, Paragraph } from '@mediahuis/chameleon-react';
import { EVENT_TYPE } from '@subscriber/globals';
import { useValidateAddress } from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import { RefObject, useEffect, useRef, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';

import { ActivationStartShow, TRACKING_SHOW_EVENTS } from '../../Activation.types';
import { useActivationContext } from '../../context';
import { ActivationFormDL } from '../../forms';

interface ActivationStartDLProps {
  formValues?: ActivationFormDLValues;
  isLoading: boolean;
  onSubmit: (values: ActivationFormDLValues) => void;
}

const ActivationStartDL = ({
  formValues,
  isLoading,
  onSubmit
}: ActivationStartDLProps): ReactElement => {
  const { t } = useTranslation('Activation');
  const [isValidationError, setIsValidationError] = useState<boolean>(false);

  const { userInfo, onEvent } = useActivationContext();
  const validateAddress = useValidateAddress();

  const subscriptionFormRef: RefObject<FormikProps<ActivationFormDLValues>> =
    useRef<FormikProps<ActivationFormDLValues>>(null);

  const showEvent: ActivationStartShow = {
    type: EVENT_TYPE.SHOW,
    key: TRACKING_SHOW_EVENTS.ACTIVATION_START
  };

  useEffect(() => {
    if (validateAddress.isError) {
      setIsValidationError(true);
    }
  }, [validateAddress.isError]);

  useEffect(() => {
    onEvent && onEvent(showEvent);
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" gap={8}>
      <Flex alignItems="center" gap={4}>
        <Logo name="brand-square-main" style={{ height: '2.5rem' }} />
        <Heading size="lg">{t('activationStart.title')}</Heading>
      </Flex>

      <Paragraph>
        <Trans
          components={[<span key="userEmail" style={{ fontWeight: 'bold' }} />]}
          i18nKey="activationStart.intro"
          t={t}
          values={{
            userEmail: userInfo?.email
          }}
        />
      </Paragraph>

      <ActivationFormDL
        formRef={subscriptionFormRef}
        initialValues={
          formValues || {
            box: userInfo?.address.box || '',
            city: userInfo?.address.city
              ? {
                  CityId: 0,
                  Id: 0,
                  Name: userInfo?.address.city,
                  PostalCode: userInfo.address.postalCode
                }
              : undefined,
            country: userInfo?.address.country || 'NL',
            houseNumber: userInfo?.address.houseNumber || '',
            postalCode: userInfo?.address.postalCode || '',
            street: userInfo?.address.street
              ? { StreetId: 0, Name: userInfo?.address.street }
              : undefined
          }
        }
        onSubmit={values => {
          if (values.city && values.street) {
            validateAddress
              .mutateAsync({
                AddressToValidate: {
                  BoxNumber: values.box,
                  City: values?.city,
                  Country: values.country,
                  HouseNumber: values.houseNumber,
                  Street: values.street
                }
              })
              .then(data => {
                if (data.ValidationResult.IsValid) {
                  onSubmit(values);
                  setIsValidationError(false);
                } else {
                  setIsValidationError(true);
                }
              });
          }
        }}
      />

      <Flex flexDirection="column" gap={2}>
        <Button
          appearance="primary"
          className="!font-semibold"
          width="full"
          loading={isLoading}
          onClick={() => subscriptionFormRef.current?.submitForm()}
        >
          {t('activationStart.button')}
        </Button>

        {isValidationError && <Caption color="RedBase">{t('activationStart.error')}</Caption>}

        <Caption style={{ textAlign: 'center' }}>{t('activationStart.buttonCaption')}</Caption>
      </Flex>
    </Flex>
  );
};

export default ActivationStartDL;
