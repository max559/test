import { ReactElement, useEffect, useMemo, useState } from 'react';
import { Loader } from '@mediahuis/chameleon-react';
import { Brand, Environment, getConfiguration } from '@subscriber/globals';
import { ServicesCore } from '@subscriber/services-core';
import { ServicesHooksProvider } from '@subscriber/services-hooks';
import { QueryClient } from '@tanstack/react-query';

import { ActivationEvent, UserInfo } from './Activation.types';
import {
  ActivationContext,
  ActivationContextValue,
  useActivationContext
} from './context/Activation.context';
import {
  ActivationError,
  ActivationHasAccess,
  ActivationNoAccess,
  ActivationStart,
  ActivationStartDL,
  ActivationSuccess,
  ActivationSuccessDL,
  ActivationSuccessMultibrand
} from './features/activation';
import { ActivationFormDLValues, ActivationFormValues } from './forms';
import { ACTIVATION_STATUS, useActivationStatus } from './hooks';
import i18next from './i18n';
import { PageLayout } from './layouts';

const queryClient = new QueryClient();

interface ActivationProps {
  brand: Brand;
  environment: Environment;
  userInfo: UserInfo | null;
  withLayout?: boolean;
  onEvent?: (event: ActivationEvent) => void;
}

const Activation = ({
  brand,
  environment,
  userInfo,
  withLayout = false,
  onEvent
}: ActivationProps): ReactElement => {
  const providerValue: ActivationContextValue = useMemo(() => {
    return {
      userInfo,
      brand,
      coreInstance: new ServicesCore(brand, environment),
      environment,
      onEvent
    };
  }, [userInfo, brand, environment, onEvent]);

  return (
    <ActivationContext.Provider value={providerValue}>
      <ServicesHooksProvider coreInstance={providerValue.coreInstance} queryClient={queryClient}>
        {withLayout ? (
          <PageLayout>
            <ActivationContent />
          </PageLayout>
        ) : (
          <ActivationContent />
        )}
      </ServicesHooksProvider>
    </ActivationContext.Provider>
  );
};

const ActivationContent = (): ReactElement => {
  const { userInfo, brand, environment } = useActivationContext();
  const { isLoading, isMultibrand, status, activate, reset } = useActivationStatus(
    userInfo?.id,
    userInfo?.idToken
  );

  const [formValues, setFormValues] = useState<ActivationFormValues | undefined>();
  const [formDLValues, setFormDLValues] = useState<ActivationFormDLValues | undefined>();
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [language, setLanguage] = useState<string>(
    getConfiguration(brand, environment).defaultLocale
  );

  useEffect(() => {
    i18next.changeLanguage(language);
  }, [language]);

  const onContactCustomerService = () => {
    window.open(`${getConfiguration(brand, environment).customerServiceUrl}`, '_blank');
  };

  switch (status) {
    case ACTIVATION_STATUS.AlreadyLinked:
      return <ActivationHasAccess onContactCustomerService={onContactCustomerService} />;
    case ACTIVATION_STATUS.Error:
    case ACTIVATION_STATUS.NotLinked:
      return <ActivationError onContactCustomerService={onContactCustomerService} />;
    case ACTIVATION_STATUS.Linked:
      // TODO - Check on multibrand?
      if (brand === 'dl') {
        return <ActivationSuccessDL />;
      }
      return <ActivationSuccessMultibrand />;
    // return isMultibrand ? (
    //   <ActivationSuccessMultibrand />
    // ) : (
    //   <ActivationSuccess onContactCustomerService={onContactCustomerService} />
    // );
    case ACTIVATION_STATUS.NoAccess:
    case ACTIVATION_STATUS.NotFound:
      return (
        <ActivationNoAccess
          onCheckData={reset}
          onContactCustomerService={onContactCustomerService}
        />
      );
    case null:
      return <Loader size="lg" />;
    default:
      if (brand === 'dl') {
        return (
          <ActivationStartDL
            formValues={formDLValues}
            isLoading={isLoading}
            onSubmit={(activationFormValues: ActivationFormDLValues) => {
              activate({
                accountGuid: userInfo?.id,
                address: {
                  boxNumber: activationFormValues.box,
                  city: activationFormValues.city?.Name,
                  countryCode: activationFormValues.country,
                  houseNumber: activationFormValues.houseNumber,
                  street: activationFormValues.street?.Name,
                  zipCode: activationFormValues.postalCode
                },
                subscriptionNumber: ''
              });
              setFormDLValues(activationFormValues);
            }}
          />
        );
      }
      return (
        <ActivationStart
          formValues={formValues}
          isLoading={isLoading}
          onContactCustomerService={onContactCustomerService}
          onSubmit={(activationFormValues: ActivationFormValues) => {
            activate({
              accountGuid: userInfo?.id,
              address: {
                boxNumber: activationFormValues.box,
                city: activationFormValues.city?.Name,
                countryCode: activationFormValues.country,
                houseNumber: activationFormValues.houseNumber,
                street: activationFormValues.street?.Name,
                zipCode: activationFormValues.postalCode
              },
              subscriptionNumber: activationFormValues.subscriptionNumber
            });
            setFormValues(activationFormValues);
          }}
        />
      );
  }
};

export default Activation;
