import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

import EN_Activation from './locales/en-gb/features/activation.json';
import EN_Components from './locales/en-gb/components.json';
import EN_Forms from './locales/en-gb/forms.json';

import BE_Activation from './locales/nl-be/features/activation.json';
import BE_Components from './locales/nl-be/components.json';
import BE_Forms from './locales/nl-be/forms.json';

import NL_Activation from './locales/nl-nl/features/activation.json';
import NL_Components from './locales/nl-nl/components.json';
import NL_Forms from './locales/nl-nl/forms.json';

export const resources = {
  Activation: EN_Activation,
  Components: EN_Components,
  Forms: EN_Forms
} as const;

i18next.use(initReactI18next).init({
  debug: false,
  fallbackLng: 'nl-BE',
  supportedLngs: ['en-GB', 'nl-BE', 'nl-NL'],
  load: 'currentOnly',
  cleanCode: true,
  ns: ['Activation', 'Components', 'Forms'],
  react: { useSuspense: false },
  resources: {
    en: {
      Activation: EN_Activation,
      Components: EN_Components,
      Forms: EN_Forms
    },
    'nl-BE': {
      Activation: BE_Activation,
      Components: BE_Components,
      Forms: BE_Forms
    },
    'nl-NL': {
      Activation: NL_Activation,
      Components: NL_Components,
      Forms: NL_Forms
    }
  }
});

export default i18next;
