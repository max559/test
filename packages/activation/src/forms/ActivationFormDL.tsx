import type { City, Country, Street } from '@subscriber/services-core';
import type { FieldProps, FormikProps } from 'formik';
import { type ReactElement, type RefObject } from 'react';

import { AutoComplete, Select, TextField } from '@mediahuis/chameleon-react';
import {
  useAutocompleteCities,
  useAutocompleteStreets,
  useCountriesByLanguage,
  useResolveDutchAddress
} from '@subscriber/services-hooks';
import { Flex } from '@subscriber/subscriber-ui';
import { Field, Form, Formik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';

const mapCitySuggestions = (cities: Array<City>): Array<string> => {
  return cities.reduce((acc: Array<string>, city: City) => {
    if (city.Name && city.PostalCode) {
      return acc.concat(`${city.Name} (${city.PostalCode})`);
    }

    return acc;
  }, []);
};

const mapStreetSuggestions = (streets: Array<Street>): Array<string> => {
  return streets.reduce((acc: Array<string>, street: Street) => {
    if (street.Name) {
      return acc.concat(street.Name);
    }

    return acc;
  }, []);
};

export interface ActivationFormDLValues {
  box: string;
  city?: City;
  country: string;
  houseNumber: string;
  postalCode: string;
  street?: Street;
}

interface ActivationFormDLProps {
  formRef: RefObject<FormikProps<ActivationFormDLValues>>;
  initialValues: ActivationFormDLValues;
  onSubmit: (values: ActivationFormDLValues) => void;
}

const ActivationFormDL = ({
  formRef,
  initialValues,
  onSubmit
}: ActivationFormDLProps): ReactElement => {
  const { t } = useTranslation('Forms');

  const autocompleteCities = useAutocompleteCities();
  const autocompleteStreets = useAutocompleteStreets();
  const countriesByLanguageQuery = useCountriesByLanguage('nl');
  const resolveDutchAddress = useResolveDutchAddress();

  const ActivationFormSchema = Yup.object().shape({
    box: Yup.string(),
    country: Yup.string().required(t('activation.countryError')),
    houseNumber: Yup.string().required(t('activation.houseNumberError')),
    postalCode: Yup.string().required(t('activation.postalCodeError'))
  });

  const resolveAddress = (postalCode: string, houseNumber: number, country: string) => {
    if (postalCode && houseNumber && country === 'NL') {
      return resolveDutchAddress.mutateAsync({ PostalCode: postalCode, HouseNumber: houseNumber });
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      innerRef={formRef}
      validationSchema={ActivationFormSchema}
      onSubmit={onSubmit}
    >
      {({ setFieldValue }) => (
        <Form style={{ display: 'flex', flexDirection: 'column', gap: '1rem' }}>
          <Field name="country">
            {({ field, meta }: FieldProps<string>) => (
              <Select
                disabled={!countriesByLanguageQuery.isSuccess}
                error={!!meta.touched && !!meta.error}
                id="CountrySelect"
                label={t('activation.country')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                value={field.value}
                onBlur={field.onBlur}
                onChange={event => {
                  if (event.target.value !== field.value) {
                    setFieldValue('postalCode', '');
                    setFieldValue('city', '');
                    setFieldValue('street', '');
                    setFieldValue('houseNumber', '');
                    setFieldValue('box', '');
                  }
                  setFieldValue('country', event.target.value);
                }}
              >
                {countriesByLanguageQuery.isSuccess &&
                  countriesByLanguageQuery.data.map((country: Country) => (
                    <option key={country.Name} value={country.IsoCode}>
                      {country.Name}
                    </option>
                  ))}
              </Select>
            )}
          </Field>

          <Field name="postalCode">
            {({ field, form, meta }: FieldProps<string>) => (
              <AutoComplete
                disabled={!form.values.country}
                error={!!meta.touched && !!meta.error}
                id="PostalCodeAutoComplete"
                label={t('activation.postalCode')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                suggestions={
                  autocompleteCities.isSuccess
                    ? mapCitySuggestions(autocompleteCities.data.Cities)
                    : []
                }
                value={field.value}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onInputChange={value => {
                  return autocompleteCities.mutate({
                    body: { Key: value },
                    countryIsoCode: form.values.country
                  });
                }}
                onSelect={value => {
                  if (value) {
                    const splittedValue = value.split('(');
                    const postalCode = splittedValue[1] ? splittedValue[1].slice(0, -1) : value;
                    const cityName = splittedValue[0] ? splittedValue[0].slice(0, -1) : value;

                    resolveAddress(
                      postalCode,
                      Number(form.values.houseNumber),
                      form.values.country
                    )?.then(value =>
                      setFieldValue('street', value?.Address?.Street ? value.Address.Street : '')
                    );

                    setFieldValue(
                      'city',
                      autocompleteCities.data?.Cities.find(
                        city => city.PostalCode === postalCode && city.Name === cityName
                      )
                    );

                    setFieldValue('postalCode', postalCode);
                  } else {
                    setFieldValue('postalCode', '');
                    setFieldValue('city', '');
                    setFieldValue('street', '');
                    setFieldValue('houseNumber', '');
                    setFieldValue('box', '');
                  }
                }}
              />
            )}
          </Field>

          <Flex gap={4}>
            <Field name="houseNumber">
              {({ field, form, meta }: FieldProps<string>) => (
                <TextField
                  className="flex-grow"
                  disabled={!form.values.postalCode}
                  error={!!meta.touched && !!meta.error}
                  id="HouseNumberInput"
                  label={t('activation.houseNumber')}
                  maxLength={5}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  required
                  value={field.value ? field.value : ''}
                  onBlur={field.onBlur}
                  onChange={event => {
                    if (event.target.value) {
                      setFieldValue('houseNumber', event.target.value);
                      resolveAddress(
                        form.values.postalCode,
                        Number(event.target.value),
                        form.values.country
                      )?.then(value =>
                        setFieldValue('street', value?.Address?.Street ? value.Address.Street : '')
                      );
                    } else {
                      setFieldValue('houseNumber', '');
                      setFieldValue('street', '');
                    }
                  }}
                />
              )}
            </Field>
            <Field name="box">
              {({ field, form, meta }: FieldProps<string>) => (
                <TextField
                  className="flex-grow"
                  disabled={!form.values.postalCode}
                  id="BoxInput"
                  label={t('activation.box')}
                  message={meta.touched && meta.error ? meta.error : undefined}
                  name={field.name}
                  required={false}
                  value={field.value ? field.value : ''}
                  onBlur={field.onBlur}
                  onChange={field.onChange}
                />
              )}
            </Field>
          </Flex>

          <Field name="city">
            {({ field }: FieldProps<City>) => (
              <TextField
                disabled
                id="CityInput"
                label={t('activation.city')}
                name={field.name}
                required
                value={field.value ? field.value.Name : ''}
              />
            )}
          </Field>

          <Field name="street">
            {({ field, form, meta }: FieldProps<Street>) => (
              <AutoComplete
                disabled={
                  form.values.country === 'NL' || !form.values.city || !form.values.postalCode
                }
                error={!!meta.touched && !!meta.error}
                id="StreetAutoComplete"
                label={t('activation.street')}
                message={meta.touched && meta.error ? meta.error : undefined}
                name={field.name}
                suggestions={
                  autocompleteStreets.isSuccess
                    ? mapStreetSuggestions(autocompleteStreets.data.Streets)
                    : []
                }
                value={field.value ? field.value?.Name : ''}
                onBlur={field.onBlur}
                onChange={field.onChange}
                onInputChange={value => {
                  autocompleteStreets.mutate({
                    body: { Key: value, PostalCode: form.values.postalCode },
                    countryIsoCode: form.values.country
                  });
                }}
                onSelect={value =>
                  setFieldValue(
                    'street',
                    autocompleteStreets.data?.Streets.find(street => street.Name === value)
                  )
                }
              />
            )}
          </Field>
        </Form>
      )}
    </Formik>
  );
};

export default ActivationFormDL;
