import type { Brand, Environment } from '@subscriber/globals';
import type { ServicesCore } from '@subscriber/services-core';
import type { ActivationEvent, UserInfo } from '../Activation.types';

import { createContext, useContext } from 'react';

export interface ActivationContextValue {
  userInfo: UserInfo | null;
  brand: Brand;
  coreInstance: ServicesCore;
  environment: Environment;
  onEvent?: (event: ActivationEvent) => void;
}

export const ActivationContext = createContext<ActivationContextValue | null>(null);

export const useActivationContext = () => {
  const activationContext = useContext(ActivationContext);

  if (!activationContext) {
    throw new Error(
      'The useActivationContext hook can only be used inside of an ActivationProvider component'
    );
  }

  return activationContext;
};
