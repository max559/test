import { CoreError, getValidationParams, BaseValidation } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const VALIDATION_KEY = 'validation';

const useValidation = (params: getValidationParams, initialData?: BaseValidation) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<BaseValidation, CoreError>(
    [VALIDATION_KEY, params.id, params.type], 
    () => coreInstance.orderService.getValidation(params).then(response => response.data),
    {
      enabled: !!params.id && !!params.type,
      initialData: () => initialData || undefined
    }
  );
};

export default useValidation;
