import { CoreError, Order } from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useCreateOrder = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<Order, CoreError, Order>((order: Order) =>
    coreInstance.orderService.createOrder(order).then(response => response.data)
  );
};

export default useCreateOrder;
