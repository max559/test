import { CoreError, getOrderParams, Order } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ORDER_KEY = 'order';

const useOrder = (params: getOrderParams, initialData?: Order) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Order, CoreError>(
    [ORDER_KEY, params.id],
    () =>
      coreInstance.orderService.getOrder(params).then(response => {
        const orderResponse: Order = {
          ...response.data,
          orderHash: params.orderHash as string
        };

        return orderResponse;
      }),
    {
      enabled: !!params.id,
      initialData: () => initialData || undefined
    }
  );
};

export default useOrder;
