import {
  CoreError,
  Order,
  PreconditionsValidation,
  PreconditionValidationError,
  REQUIREMENT_TYPE
} from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidatePrecondition = (idToken?: string) => {
  const { coreInstance } = useServicesHooksContext();
  const queryClient = useQueryClient();

  return useMutation<
    PreconditionsValidation,
    CoreError | PreconditionValidationError,
    PreconditionsValidation
  >(
    (validation: PreconditionsValidation) =>
      coreInstance.preconditionValidatorService
        .postPreconditionValidation(validation, idToken)
        .then(response => response.data),
    {
      onSuccess: async (responseData: PreconditionsValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<PreconditionsValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Preconditions],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                preconditionsValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidatePrecondition;
