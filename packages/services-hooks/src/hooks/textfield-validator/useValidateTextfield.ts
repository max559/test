import { CoreError, REQUIREMENT_TYPE, TextfieldValidation } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateTextfield = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<TextfieldValidation, CoreError, TextfieldValidation>(
    (validation: TextfieldValidation) =>
      coreInstance.textfieldValidatorService
        .postTextfieldValidation(validation)
        .then(response => response.data),
    {
      onSuccess: (responseData: TextfieldValidation) => {
        queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<TextfieldValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Textfield],
          prevValidation => {
            if (prevValidation) {
              return {
                ...prevValidation,
                ...responseData
              };
            }

            return responseData as TextfieldValidation;
          }
        );
      }
    }
  );
};

export default useValidateTextfield;
