import { CoreError, LoginValidation, Order, REQUIREMENT_TYPE } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateLogin = (idToken?: string) => {
  const { coreInstance } = useServicesHooksContext();
  const queryClient = useQueryClient();

  return useMutation<LoginValidation, CoreError, LoginValidation>(
    (validation: LoginValidation) =>
      coreInstance.loginValidatorService
        .postLoginValidation(validation, idToken)
        .then(response => response.data),
    {
      onSuccess: (responseData: LoginValidation) => {
        queryClient.setQueryData<LoginValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Login],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                loginValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateLogin;
