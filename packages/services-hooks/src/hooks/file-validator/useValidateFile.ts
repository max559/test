import { CoreError, REQUIREMENT_TYPE, FileValidation } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateFile = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<FileValidation, CoreError, FileValidation>(
    (validation: FileValidation) =>
      coreInstance.fileValidatorService
        .postFileValidation(validation)
        .then(response => response.data),
    {
      onSuccess: (responseData: FileValidation) => {
        queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<FileValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.File],
          prevValidation => {
            if (prevValidation) {
              return {
                ...prevValidation,
                ...responseData
              };
            }

            return responseData as FileValidation;
          }
        );
      }
    }
  );
};

export default useValidateFile;
