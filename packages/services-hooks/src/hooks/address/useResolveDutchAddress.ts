import {
  ResolveDutchAddressRequest,
  ResolveDutchAddressResponse,
  CoreError
} from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useResolveDutchAddress = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<ResolveDutchAddressResponse, CoreError, ResolveDutchAddressRequest>(
    (resolveDutchAddressRequest: ResolveDutchAddressRequest) =>
      coreInstance.addressService
        .postResolveDutchAddress(resolveDutchAddressRequest)
        .then(response => response.data)
  );
};

export default useResolveDutchAddress;
