import { CoreError, StreetRequest, StreetsResponse } from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

interface useAutocompleteStreetsMutation {
  body: StreetRequest;
  countryIsoCode: string;
}

const useAutocompleteStreets = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<StreetsResponse, CoreError, useAutocompleteStreetsMutation>(
    ({ body, countryIsoCode }) =>
      coreInstance.addressService
        .postStreetsAutocomplete(body, countryIsoCode)
        .then(response => response.data)
  );
};

export default useAutocompleteStreets;
