import {
  AddressValidationRequest,
  AddressValidationResponse,
  CoreError
} from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useValidateAddress = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<AddressValidationResponse, CoreError, AddressValidationRequest>(
    (validationRequest: AddressValidationRequest) =>
      coreInstance.addressService
        .postAddressValidate(validationRequest)
        .then(response => response.data)
  );
};

export default useValidateAddress;
