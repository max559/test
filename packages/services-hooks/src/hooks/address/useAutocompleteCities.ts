import { CitiesResponse, CityRequest, CoreError } from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

interface useAutocompleteCitiesMutation {
  body: CityRequest;
  countryIsoCode: string;
}

const useAutocompleteCities = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<CitiesResponse, CoreError, useAutocompleteCitiesMutation>(
    ({ body, countryIsoCode }) =>
      coreInstance.addressService
        .postCitiesAutocomplete(body, countryIsoCode)
        .then(response => response.data)
  );
};

export default useAutocompleteCities;
