import { CoreError, Country } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const COUNTRIES_LANG_KEY = 'countries-lang';

const useCountriesByLanguage = (language: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Array<Country>, CoreError>([COUNTRIES_LANG_KEY, language], () =>
    coreInstance.addressService.getCountriesByLanguage(language).then(response => response.data)
  );
};

export default useCountriesByLanguage;
