import { CoreError, Offer, getOfferBySlugParams } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const OFFER_SLUG_KEY = 'offer-slug';

const useOfferBySlug = (params: getOfferBySlugParams, initialData?: Offer) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Offer, CoreError>(
    [OFFER_SLUG_KEY, params],
    () => coreInstance.offerService.getOfferBySlug(params).then(response => response.data),
    {
      enabled: !!params.slug,
      initialData: () => initialData || undefined
    }
  );
};

export default useOfferBySlug;
