import { CoreError, OfferItem } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const OFFER_ITEM_SLUG_KEY = 'offer-item-slug';

const useOfferItemBySlug = (url?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<OfferItem, CoreError>(
    [OFFER_ITEM_SLUG_KEY, url],
    () =>
      coreInstance.offerService.getOfferItemBySlug(url as string).then(response => response.data),
    {
      enabled: !!url
    }
  );
};

export default useOfferItemBySlug;
