import { CoreError, OfferItem } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const SUBSCRIPTION_FORMULA_KEY = 'offer-item-slug';

const useOfferItemBySubscriptionFormulaId = (id?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<OfferItem, CoreError>(
    [SUBSCRIPTION_FORMULA_KEY, id],
    () =>
      coreInstance.offerService
        .getOfferItemBySubscriptionFormulaId(id as string | number)
        .then(response => response.data),
    {
      enabled: !!id
    }
  );
};

export default useOfferItemBySubscriptionFormulaId;
