import { CoreError, Offer } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const OFFER_KEY = 'offer';

const useOffer = (id?: number) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Offer, CoreError>(
    [OFFER_KEY, id],
    () => coreInstance.offerService.getOffer(id as number).then(response => response.data),
    {
      enabled: !!id
    }
  );
};

export default useOffer;
