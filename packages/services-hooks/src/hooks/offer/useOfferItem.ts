import { CoreError, getOfferItemParams, OfferItem } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const OFFER_ITEM_KEY = 'offer-item';

const useOfferItem = (params: getOfferItemParams, initialData?: OfferItem) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<OfferItem, CoreError>(
    [OFFER_ITEM_KEY, params],
    () => coreInstance.offerService.getOfferItem(params).then(response => response.data),
    {
      enabled: !!params.id && !!params.type,
      initialData: () => initialData || undefined
    }
  );
};

export default useOfferItem;
