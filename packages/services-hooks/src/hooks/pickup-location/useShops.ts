import { CoreError, Shop } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const SHOPS_KEY = 'shops';

const useShops = (brand: string, postalCode: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Array<Shop>, CoreError>(
    [SHOPS_KEY, postalCode, brand],
    () =>
      coreInstance.pickupLocationService
        .getShops(brand, postalCode)
        .then(response => response.data),
    { enabled: !!brand && !!postalCode }
  );
};

export default useShops;
