import { FileshareDto, CoreError } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useFileshare = (file: File) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<FileshareDto, CoreError>(
    [file?.name],
    () => coreInstance.fileshareService.uploadImage(file).then(response => response.data),
    { enabled: !!file, retry: false }
  );
};

export default useFileshare;
