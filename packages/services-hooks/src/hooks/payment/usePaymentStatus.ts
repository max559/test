import { CoreError, PaymentStatus } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const PAYMENT_STATUS_KEY = 'payment-status';

const usePaymentStatus = (paymentReference?: string, initialData?: PaymentStatus) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<PaymentStatus | undefined, CoreError>(
    [PAYMENT_STATUS_KEY, paymentReference],
    () =>
      coreInstance.paymentService
        .getPaymentStatus(paymentReference as string)
        .then(response => response.data),
    {
      enabled: !!paymentReference,
      initialData
    }
  );
};

export default usePaymentStatus;
