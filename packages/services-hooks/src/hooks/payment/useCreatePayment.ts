import { CoreError, Payment, PaymentRequest } from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useCreatePayment = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<Payment, CoreError, PaymentRequest>((body: PaymentRequest) =>
    coreInstance.paymentService.createPayment(body).then(response => response.data)
  );
};

export default useCreatePayment;
