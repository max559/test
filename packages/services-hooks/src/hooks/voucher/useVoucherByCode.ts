import { CoreError, Voucher } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const VOUCHER_KEY = 'voucher';

const useVoucherByCode = (code?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Voucher, CoreError>(
    [VOUCHER_KEY, code],
    () =>
      coreInstance.voucherService.getVoucherByCode(code as string).then(response => response.data),
    {
      enabled: !!code,
      retry: false
    }
  );
};

export default useVoucherByCode;
