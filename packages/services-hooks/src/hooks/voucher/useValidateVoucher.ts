import {
  CoreError,
  REQUIREMENT_TYPE,
  VoucherValidation
} from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateVoucher = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<VoucherValidation, CoreError, VoucherValidation>(
    (validation: VoucherValidation) =>
      coreInstance.voucherService.postVoucherValidation(validation).then(response => response.data),
    {
      onSuccess: (responseData: VoucherValidation) => {
        queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<VoucherValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Email],
          prevValidation => {
            if (prevValidation) {
              return {
                ...prevValidation,
                ...responseData
              };
            }

            return responseData as VoucherValidation;
          }
        );
      }
    }
  );
};

export default useValidateVoucher;
