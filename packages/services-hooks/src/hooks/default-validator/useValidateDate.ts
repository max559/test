import { CoreError, DateValidation, Order, REQUIREMENT_TYPE } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateDate = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<DateValidation, CoreError, DateValidation>(
    (validation: DateValidation) =>
      coreInstance.defaultValidatorService
        .postDateValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: DateValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<DateValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Date],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                dateValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateDate;
