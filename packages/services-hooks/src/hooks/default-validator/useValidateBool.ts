import { BooleanValidation, CoreError, Order, REQUIREMENT_TYPE } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateBool = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<BooleanValidation, CoreError, BooleanValidation>(
    (validation: BooleanValidation) =>
      coreInstance.defaultValidatorService
        .postBoolValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: BooleanValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<BooleanValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Boolean],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                booleanValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateBool;
