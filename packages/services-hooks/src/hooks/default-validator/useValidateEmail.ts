import { CoreError, EmailValidation, Order, REQUIREMENT_TYPE } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateEmail = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<EmailValidation, CoreError, EmailValidation>(
    (validation: EmailValidation) =>
      coreInstance.defaultValidatorService
        .postEmailValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: EmailValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<EmailValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Email],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                emailValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateEmail;
