import { CoreError, REQUIREMENT_TYPE, ChoiceValidation, Order } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateChoice = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<ChoiceValidation, CoreError, ChoiceValidation>(
    (validation: ChoiceValidation) =>
      coreInstance.defaultValidatorService
        .postChoiceValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: ChoiceValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<ChoiceValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.Choice],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                choiceValidations: prevOrder.choiceValidations?.map(choiceValidation => {
                  if (choiceValidation.id === responseData.id) {
                    return responseData;
                  }

                  return choiceValidation;
                })
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateChoice;
