import { CoreError, SubscriptionInfo } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const SUBSCRIPTION_INFO_KEY = 'subscription-info';

const useSubscriptionInfo = (accountId?: string, idToken?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<SubscriptionInfo, CoreError>(
    [SUBSCRIPTION_INFO_KEY, accountId],
    () =>
      coreInstance.subscriptionService
        .postAccount(accountId as string, idToken)
        .then(response => response.data),
    {
      enabled: !!accountId,
      retry: false
    }
  );
};

export default useSubscriptionInfo;
