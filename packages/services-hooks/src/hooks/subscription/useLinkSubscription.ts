import {
  CoreError,
  SubscriptionAddressRequest,
  SubscriptionLinkResponse
} from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { SUBSCRIPTION_INFO_KEY } from './useSubscriptionInfo';

const useLinkSubscription = (idToken?: string) => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<SubscriptionLinkResponse, CoreError, SubscriptionAddressRequest>(
    (request: SubscriptionAddressRequest) =>
      coreInstance.subscriptionService
        .postLinkAccountToSubscription(request, idToken)
        .then(response => response.data),
    {
      onSuccess: (_data, variables) => {
        queryClient.invalidateQueries([SUBSCRIPTION_INFO_KEY, variables.accountGuid]);
      }
    }
  );
};

export default useLinkSubscription;
