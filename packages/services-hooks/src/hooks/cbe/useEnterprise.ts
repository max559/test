import { CoreError, Enterprise } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ENTERPRISE_KEY = 'enterprise';

const useEnterprise = (vatNumber?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<Enterprise, CoreError>(
    [ENTERPRISE_KEY, vatNumber],
    () =>
      coreInstance.cbeService.getEnterprise(vatNumber as string).then(response => response.data),
    { enabled: !!vatNumber, retry: false }
  );
};

export default useEnterprise;
