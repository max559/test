import { CoreError, getOrderSettlementParams, OrderSettlement } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ORDER_SETTLEMENT_KEY = 'order-settlement';

const useOrderSettlement = (params: getOrderSettlementParams, initialData?: OrderSettlement) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<OrderSettlement, CoreError>(
    [ORDER_SETTLEMENT_KEY, params],
    () =>
      coreInstance.orderSettleService.getOrderSettlement(params).then(response => response.data),
    {
      enabled: !!params.agreementId || !!params.orderId || !!params.paymentReference,
      initialData: () => initialData || undefined
    }
  );
};

export default useOrderSettlement;
