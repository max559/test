import { AccountInfoRequest, AccountInfoResponse, CoreError } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ACCOUNT_INFO_KEY = 'account-info';

const useAccountInfo = (body?: AccountInfoRequest) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<AccountInfoResponse, CoreError>(
    [ACCOUNT_INFO_KEY],
    () =>
      coreInstance.accountManagementService.getAccountInfo(body).then(response => response.data),
    {
      refetchOnWindowFocus: false,
      retry: 1,
      staleTime: Infinity
    }
  );
};

export default useAccountInfo;
