import { AccountStatus, CoreError } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ACCOUNT_STATUS_KEY = 'account-status';

const useAccountStatus = (emailAddress?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<AccountStatus, CoreError>([ACCOUNT_STATUS_KEY], () =>
    coreInstance.accountManagementService
      .getAccountStatus(emailAddress)
      .then(response => response.data)
  );
};

export default useAccountStatus;
