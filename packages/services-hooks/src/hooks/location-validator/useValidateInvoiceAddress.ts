import { InvoiceDetailsValidation, REQUIREMENT_TYPE, Order } from '@subscriber/services-core';
import { CoreError } from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateInvoiceAddress = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<InvoiceDetailsValidation, CoreError, InvoiceDetailsValidation>(
    (validation: InvoiceDetailsValidation) =>
      coreInstance.locationValidatorService
        .postInvoiceDetailsValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: InvoiceDetailsValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<InvoiceDetailsValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.InvoiceDetails],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                invoiceDetailsValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
        }
      }
    }
  );
};

export default useValidateInvoiceAddress;
