import {
  AddressAreaValidation,
  CoreError,
  Order,
  REQUIREMENT_TYPE
} from '@subscriber/services-core';
import { useMutation, useQueryClient } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';
import { ORDER_KEY } from '../order/useOrder';
import { VALIDATION_KEY } from '../order/useValidation';

const useValidateAddressArea = () => {
  const queryClient = useQueryClient();
  const { coreInstance } = useServicesHooksContext();

  return useMutation<AddressAreaValidation, CoreError, AddressAreaValidation>(
    (validation: AddressAreaValidation) =>
      coreInstance.locationValidatorService
        .postAddressAreaValidation(validation)
        .then(response => response.data),
    {
      onSuccess: async (responseData: AddressAreaValidation) => {
        await queryClient.cancelQueries({ queryKey: [ORDER_KEY] });
        queryClient.setQueryData<AddressAreaValidation>(
          [VALIDATION_KEY, responseData.id, REQUIREMENT_TYPE.AddressArea],
          responseData
        );

        if (responseData.orderId) {
          queryClient.setQueryData<Order>([ORDER_KEY, responseData.orderId], prevOrder => {
            if (prevOrder) {
              return {
                ...prevOrder,
                addressAreaValidation: responseData
              };
            }

            return prevOrder;
          });
        } else {
          setTimeout(() => {
            queryClient.invalidateQueries({ queryKey: [ORDER_KEY] });
          }, 1000);
        }
      }
    }
  );
};

export default useValidateAddressArea;
