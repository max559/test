import { ContentLinkResult, CoreError } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const CONTENT_LINKS_KEY = 'content-links';

const useContentLinks = (key?: string) => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<ContentLinkResult, CoreError>(
    [CONTENT_LINKS_KEY, key],
    () =>
      coreInstance.contentService.getContentLinks(key as string).then(response => response.data),
    {
      enabled: !!key
    }
  );
};

export default useContentLinks;
