import { CoreError, UserInfo } from '@subscriber/services-core';
import { useQuery } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

export const ACCOUNT_INFO_LUX_KEY = 'account-info-lux';

const useAccountInfoLux = () => {
  const { coreInstance } = useServicesHooksContext();

  return useQuery<UserInfo, CoreError>(
    [ACCOUNT_INFO_LUX_KEY],
    () => coreInstance.accountService.getAccountInfo().then(response => response.data),
    {
      cacheTime: Infinity,
      retry: false
    }
  );
};

export default useAccountInfoLux;
