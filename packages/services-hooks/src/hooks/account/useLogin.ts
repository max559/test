import { CoreError, LoginDto } from '@subscriber/services-core';
import { useMutation } from '@tanstack/react-query';

import { useServicesHooksContext } from '../../context';

const useLogin = () => {
  const { coreInstance } = useServicesHooksContext();

  return useMutation<LoginDto, CoreError, LoginDto>(body =>
    coreInstance.accountService.postLogin(body).then(response => response.data)
  );
};

export default useLogin;
