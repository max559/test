import type { ServicesHooksValues } from './ServicesHooksContext';

import { useContext } from 'react';

import { ServicesHooksContext } from './ServicesHooksContext';

const useServicesHooksContext = () => {
  const servicesHooksContext: ServicesHooksValues | null = useContext(ServicesHooksContext);

  if (!servicesHooksContext) {
    throw new Error(
      'You can only use the useServicesHooksContext hook inside a ServicesHooksProvider component'
    );
  }

  return servicesHooksContext;
};

export default useServicesHooksContext;
