import type { ServicesCore } from '@subscriber/services-core';

import { createContext } from 'react';

export interface ServicesHooksValues {
  coreInstance: ServicesCore;
}

export const ServicesHooksContext = createContext<ServicesHooksValues | null>(null);
