import type { QueryClient } from '@tanstack/react-query';
import type { PropsWithChildren } from 'react';

import { ServicesCore } from '@subscriber/services-core';
import { QueryClientProvider } from '@tanstack/react-query';

import { ServicesHooksContext } from './ServicesHooksContext';

interface ServicesHooksProviderProps {
  coreInstance: ServicesCore;
  queryClient: QueryClient;
}

const ServicesHooksProvider = ({
  children,
  coreInstance,
  queryClient
}: PropsWithChildren<ServicesHooksProviderProps>) => {
  return (
    <QueryClientProvider client={queryClient}>
      <ServicesHooksContext.Provider value={{ coreInstance }}>
        {children}
      </ServicesHooksContext.Provider>
    </QueryClientProvider>
  );
};

export default ServicesHooksProvider;
