import type { PropsWithChildren, ReactElement } from 'react';

import type { Brand, BrandGroup, Environment } from '../types';

import { useRef } from 'react';

import { Authentication } from '../components/Authentication';
import { Script } from '../components/Script';
import { CIAM_REDIRECT_PATH } from '../types';
import { CiamContext, createCiamStore } from './CiamContext';

interface CiamProviderProps {
  autoAuthentication?: boolean;
  brand: Brand;
  brandGroup: BrandGroup;
  clientId: string;
  environment: Environment;
  redirectPaths?: {
    login?: string;
    logout?: string;
    register?: string;
  };
}

const CiamProvider = ({
  autoAuthentication = true,
  brand,
  brandGroup,
  children,
  clientId,
  environment,
  redirectPaths
}: PropsWithChildren<CiamProviderProps>): ReactElement => {
  const store = useRef(
    createCiamStore({
      autoAuthentication,
      brand,
      brandGroup,
      clientId,
      environment,
      redirectPaths: {
        login: redirectPaths?.login || CIAM_REDIRECT_PATH.LOGIN,
        logout: redirectPaths?.logout || CIAM_REDIRECT_PATH.LOGOUT,
        register: redirectPaths?.register || CIAM_REDIRECT_PATH.REGISTER
      }
    })
  ).current;

  return (
    <CiamContext.Provider value={store}>
      <Script />
      {autoAuthentication ? <Authentication silent>{children}</Authentication> : children}
    </CiamContext.Provider>
  );
};

export default CiamProvider;
