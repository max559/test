import type { StoreApi } from 'zustand';

import type { CiamSDK, CiamStoreConfig, CiamUserInfo, RedirectOptions } from '../types';

import { createContext } from 'react';
import { createStore } from 'zustand';

import { CIAM_AUTHENTICATION_STATUS } from '../types';
import { buildRedirectUrl, isClientSide } from '../utils';

declare global {
  interface Window {
    CIAM: CiamSDK | Array<() => void>;
  }
}

export interface CiamStore {
  authenticationStatus: CIAM_AUTHENTICATION_STATUS;
  config: CiamStoreConfig;
  userInfo: CiamUserInfo | null;
  getUserInfo: () => Promise<CiamUserInfo | null>;
  redirectToRegister: (options?: RedirectOptions) => void;
  redirectToSignIn: (options?: RedirectOptions) => void;
  redirectToSignOut: (options?: RedirectOptions) => void;
}

export const CiamContext = createContext<StoreApi<CiamStore> | null>(null);

export const createCiamStore = (config: CiamStoreConfig) => {
  const ciamStore = createStore<CiamStore>()((set, get) => ({
    authenticationStatus: CIAM_AUTHENTICATION_STATUS.UNKNOWN,
    config,
    userInfo: null,
    getUserInfo: () => {
      return new Promise(
        (
          resolve: (userInfo: CiamUserInfo | null) => void,
          reject: (errorMessage: string) => void
        ) => {
          if (isClientSide()) {
            window.CIAM = window.CIAM || [];

            set({ authenticationStatus: CIAM_AUTHENTICATION_STATUS.LOADING });

            window.CIAM.push(() =>
              Promise.all([
                (window.CIAM as CiamSDK).getDecodedAccessToken(get().config.clientId),
                (window.CIAM as CiamSDK).getFullUserProfile(get().config.clientId),
                (window.CIAM as CiamSDK).getUserInfo(get().config.clientId),
                (window.CIAM as CiamSDK).getUserSubscriptions(get().config.clientId)
              ])
                .then(([accessToken, fullIdentity, idToken, subscriptions]) => {
                  if (accessToken && fullIdentity && idToken && subscriptions) {
                    const userInfo: CiamUserInfo = {
                      accessToken,
                      fullIdentity,
                      idToken,
                      subscriptions: subscriptions ?? [],
                      tokens: {
                        access:
                          (window.CIAM as CiamSDK).getAccessToken(get().config.clientId) ?? '',
                        id: (window.CIAM as CiamSDK).getIdToken(get().config.clientId) ?? ''
                      }
                    };

                    set({
                      authenticationStatus: CIAM_AUTHENTICATION_STATUS.AUTHENTICATED,
                      userInfo
                    });
                    resolve(userInfo);
                  } else {
                    set({
                      authenticationStatus: CIAM_AUTHENTICATION_STATUS.UNAUTHENTICATED,
                      userInfo: null
                    });
                    resolve(null);
                  }
                })
                .catch(() => {
                  set({
                    authenticationStatus: CIAM_AUTHENTICATION_STATUS.ERROR,
                    userInfo: null
                  });
                  reject('Error while retrieving user info');
                })
            );
          } else {
            reject('You can only call getUserInfo() from the browser');
          }
        }
      );
    },
    redirectToRegister: options => {
      const redirectUrl = buildRedirectUrl(
        new URL(`${window.location.origin}${get().config.redirectPaths.register}`),
        options
      );

      window.location.assign(redirectUrl);
    },
    redirectToSignIn: options => {
      const redirectUrl = buildRedirectUrl(
        new URL(`${window.location.origin}${get().config.redirectPaths.login}`),
        options
      );

      window.location.assign(redirectUrl);
    },
    redirectToSignOut: options => {
      const redirectUrl = buildRedirectUrl(
        new URL(`${window.location.origin}${get().config.redirectPaths.logout}`),
        options
      );

      window.location.assign(redirectUrl);
    }
  }));

  return ciamStore;
};
