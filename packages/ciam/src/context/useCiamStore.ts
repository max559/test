import type { CiamStore } from './CiamContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { CiamContext } from './CiamContext';

const useCiamStore = <T>(selector: (state: CiamStore) => T): T => {
  const ciamContext = useContext(CiamContext);

  if (!ciamContext) {
    throw new Error('The useCiamStore hook can only be used in CiamProvider');
  }

  return useStore(ciamContext, selector);
};

export default useCiamStore;
