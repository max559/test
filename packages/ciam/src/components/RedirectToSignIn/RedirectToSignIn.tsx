import type { RedirectOptions } from '../../types';

import { useEffect } from 'react';

import { useCiamStore } from '../../context';

interface RedirectToSignInProps {
  options?: RedirectOptions;
}

const RedirectToSignIn = ({ options }: RedirectToSignInProps): null => {
  const redirectToSignIn = useCiamStore(state => state.redirectToSignIn);

  useEffect(() => {
    redirectToSignIn(options);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default RedirectToSignIn;
