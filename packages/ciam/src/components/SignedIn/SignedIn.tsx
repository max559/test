import type { PropsWithChildren, ReactElement } from 'react';

import { useCiamStore } from '../../context';
import { CIAM_AUTHENTICATION_STATUS } from '../../types';

const SignedIn = ({ children }: PropsWithChildren): ReactElement | null => {
  const authenticationStatus = useCiamStore(state => state.authenticationStatus);

  if (authenticationStatus === CIAM_AUTHENTICATION_STATUS.AUTHENTICATED) {
    return <>{children}</>;
  }

  return null;
};

export default SignedIn;
