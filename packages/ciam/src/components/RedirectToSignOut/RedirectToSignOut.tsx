import type { RedirectOptions } from '../../types';

import { useEffect } from 'react';

import { useCiamStore } from '../../context';

interface RedirectToSignOutProps {
  options?: RedirectOptions;
}

const RedirectToSignOut = ({ options }: RedirectToSignOutProps): null => {
  const redirectToSignOut = useCiamStore(state => state.redirectToSignOut);

  useEffect(() => {
    redirectToSignOut(options);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default RedirectToSignOut;
