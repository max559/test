import type { Environment } from '../../types';

import { useEffect, useRef } from 'react';

import { useCiamStore } from '../../context';

const scriptPrefixes: Record<Environment, string> = Object.freeze({
  test: 'testshared',
  preview: 'previewshared',
  production: 'shared'
});

const Script = (): null => {
  const { brand, brandGroup, clientId, environment } = useCiamStore(state => state.config);

  const isScriptAppended = useRef<boolean>(false);

  useEffect(() => {
    if (!isScriptAppended.current) {
      const script = document.querySelector(`script[data-client-id="${clientId}"]`);

      if (!script) {
        const scriptElement = document.createElement('script');

        scriptElement.setAttribute('data-brand', brand);
        scriptElement.setAttribute('data-client-id', clientId);
        scriptElement.setAttribute(
          'src',
          `https://${scriptPrefixes[environment]}.mediahuis.be/extra/ciam/${brandGroup}/auth0-integration.js`
        );

        document.body.appendChild(scriptElement);
      }

      isScriptAppended.current = true;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default Script;
