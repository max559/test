import type { RedirectOptions } from '../../types';

import { useEffect } from 'react';

import { useCiamStore } from '../../context';

interface RedirectToRegisterProps {
  options?: RedirectOptions;
}

const RedirectToRegister = ({ options }: RedirectToRegisterProps): null => {
  const redirectToRegister = useCiamStore(state => state.redirectToRegister);

  useEffect(() => {
    redirectToRegister(options);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default RedirectToRegister;
