import type { PropsWithChildren, ReactElement } from 'react';

import { useEffect } from 'react';

import { useCiamStore } from '../../context';
import { CIAM_AUTHENTICATION_STATUS } from '../../types';

interface AuthenticationProps {
  fallback?: ReactElement;
  silent?: boolean;
}

const Authentication = ({
  children,
  fallback,
  silent = false
}: PropsWithChildren<AuthenticationProps>): ReactElement | null => {
  const authenticationStatus = useCiamStore(state => state.authenticationStatus);
  const getUserInfo = useCiamStore(state => state.getUserInfo);

  useEffect(() => {
    if (authenticationStatus === CIAM_AUTHENTICATION_STATUS.UNKNOWN) {
      getUserInfo();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authenticationStatus]);

  if (
    silent ||
    authenticationStatus === CIAM_AUTHENTICATION_STATUS.AUTHENTICATED ||
    authenticationStatus === CIAM_AUTHENTICATION_STATUS.UNAUTHENTICATED
  ) {
    return <>{children}</>;
  }

  if (authenticationStatus === CIAM_AUTHENTICATION_STATUS.LOADING && fallback) {
    return fallback;
  }

  return null;
};

export default Authentication;
