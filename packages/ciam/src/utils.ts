import type { RedirectOptions } from './types';

const redirectUrlParams: Record<keyof RedirectOptions, string> = Object.freeze({
  extLocation: 'ext-location',
  loginIdentityLevel: 'login_identity_level',
  process: 'process',
  registerIdentityLevel: 'register_identity_level',
  returnTo: 'returnTo',
  skipEmailVerification: 'skip_email_verification'
});

export const buildRedirectUrl = (url: URL, options?: RedirectOptions): URL => {
  if (options) {
    Object.entries(options).forEach(([key, value]) => {
      url.searchParams.set(redirectUrlParams[key as keyof RedirectOptions], value);
    });
  }

  return url;
};

export const isClientSide = (): boolean =>
  Boolean(typeof window !== 'undefined') && Boolean(window.document);
