export type Brand = 'dl' | 'ds' | 'gva' | 'hbvl' | 'nb';
export type BrandGroup = 'mhbe' | 'mhli' | 'mnl' | 'mnr' | 'be' | 'ir' | 'nl';
export type Environment = 'test' | 'preview' | 'production';

export enum CIAM_AUTHENTICATION_STATUS {
  AUTHENTICATED = 'authenticated',
  ERROR = 'error',
  LOADING = 'loading',
  UNAUTHENTICATED = 'unauthenticated',
  UNKNOWN = 'unknown'
}

export enum CIAM_IDENTITY_LEVEL {
  MH1 = 'MH1',
  MH2 = 'MH2',
  MH3 = 'MH3',
  MH4 = 'MH4',
  MH5 = 'MH5',
  MH6 = 'MH6',
  MH7 = 'MH7'
}

export enum CIAM_REDIRECT_PATH {
  LOGIN = '/auth/login',
  LOGOUT = '/auth/logout',
  REGISTER = '/auth/register'
}

export interface CiamSDK {
  getAccessToken: (clientId: string) => string | null;
  getConfig: (clientId: string) => URLSearchParams;
  getDecodedAccessToken: (clientId: string) => Promise<AccessToken | null>;
  getFullUserProfile: (clientId: string) => Promise<FullIdentity | null>;
  getIdToken: (clientId: string) => string | null;
  getUserInfo: (clientId: string) => Promise<IdToken | null>;
  getUserSubscriptions: (clientId: string) => Promise<Array<string> | null>;
  isAuthenticated: (clientId: string) => boolean;
  isEmailVerified: (clientId: string) => Promise<boolean>;
  sendEmailVerification: (clientId: string) => Promise<boolean>;
  showEmailConfirmation: (options: {
    clientId: string;
    el: HTMLElement;
    type: 'paywall';
  }) => Promise<void>;
  showSilentLoginNotification: (options: { brand: string; clientId: string }) => Promise<void>;
  push: (func: () => unknown) => void;
}

export interface CiamStoreConfig {
  autoAuthentication: boolean;
  brand: Brand;
  brandGroup: BrandGroup;
  clientId: string;
  environment: Environment;
  redirectPaths: {
    login: string;
    logout: string;
    register: string;
  };
}

export interface CiamUserInfo {
  accessToken: AccessToken;
  fullIdentity: FullIdentity;
  idToken: IdToken;
  subscriptions: Array<string>;
  tokens: {
    access: string;
    id: string;
  };
}

export interface AccessToken {
  aud: Array<string>;
  azp: string;
  exp: number;
  'https://mediahuis.com/identity-levels'?: Array<string>;
  'https://mediahuis.com/principal-type': 'USER';
  'https: //mediahuis.com/subscribed-accesses'?: Array<string>;
  iat: number;
  iss: string;
  jti?: string;
  nbf?: number;
  scope: string;
  sub: string;
}

export interface FullIdentity {
  address: {
    city?: string;
    countryCode?: string;
    houseNumber?: string;
    houseNumberExtension?: string;
    postalCode?: string;
    qualityLabel?: string;
    street?: string;
  };
  contactpointsPhone: {
    phone?: string;
  };
  demographicsPerson: {
    dateOfBirth: string;
    genderCode: 'u' | 'm' | 'f' | 'o';
    genderCustom?: string;
  };
  email: string;
  emailVerified: boolean;
  identityLevels: Array<string>;
  namesPerson: {
    firstName: string;
    lastName: string;
    nickName: string;
  };
}

export interface IdToken {
  aud?: string | Array<string>;
  email: string;
  email_verified: boolean;
  exp?: number;
  family_name: string;
  gender: {
    type: 'Male' | 'Female' | 'Other';
  };
  given_name: string;
  iat?: number;
  isInFirstTimeLoginBrand: boolean;
  iss?: string;
  jti?: string;
  name: string;
  nbf?: number;
  nickname: string;
  picture: string;
  sid?: string;
  sub?: string;
  updated_at: string;
}

export interface RedirectOptions {
  extLocation?: string;
  loginIdentityLevel?: CIAM_IDENTITY_LEVEL;
  process?: string;
  registerIdentityLevel?: string;
  returnTo?: string;
  skipEmailVerification?: string;
}
