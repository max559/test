import { useCiamStore } from '../context';

export const useCiam = () => {
  const ciamData = useCiamStore(state => ({
    authenticationStatus: state.authenticationStatus,
    userInfo: state.userInfo,
    getUserInfo: state.getUserInfo,
    redirectToRegister: state.redirectToRegister,
    redirectToSignIn: state.redirectToSignIn,
    redirectToSignOut: state.redirectToSignOut
  }));

  return ciamData;
};
