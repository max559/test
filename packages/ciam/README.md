# @subscriber/ciam

The @subscriber/ciam package is a React wrapper for the CIAM SDK. it provides a Provider, hook and some components which will make it easy to implement authentication in your application.

More documentation on the CIAM SDK:
https://gitlab.mediahuisgroup.com/ciam-group/ciam-web-integrations

## Installation

Use your package manager of choice:

`npm install @subscriber/ciam`

`yarn add @subscriber/ciam`

`pnpm add @subscriber/ciam`

## Provider

Add the provider component to your application. It will make the CIAM state available to all nested components that need to access it.

```
  import { CiamProvider } from '@subscriber/ciam';

  <CiamProvider
    brand="..."
    brandGroup="..."
    clientId="..."
  >
    ...
  </CiamProvider>
```

#### API

| Property           | Description                                                                       | Type                                                                 | Required | Default                                                                      |
| ------------------ | --------------------------------------------------------------------------------- | -------------------------------------------------------------------- | -------- | ---------------------------------------------------------------------------- |
| autoAuthentication | Set to false if you don't want the package to automatically authenticate on mount | boolean                                                              | false    | true                                                                         |
| brand              | The MH brand you are working with                                                 | string                                                               | true     |                                                                              |
| brandGroup         | The brand group of the MH brand                                                   | mhbe &#124; mhli &#124; mnl &#124; mnr &#124; be &#124; ir &#124; nl | true     |                                                                              |
| clientId           | CIAM client ID of the brand                                                       | string                                                               | true     |                                                                              |
| redirectPaths      | Overrides for the different redirect URLs                                         | { login?: string; logout?: string; register?: string }               | false    | { login: '/auth/login', logout: '/auth/logout', register: '/auth/register' } |

## Hook

You can use the useCiam hook to interact with the authentication state:

```
  import { useCiam } from '@subscriber/ciam';

  const UserEmail = (): ReactElement => {
    const { userInfo } = useCiam();

    return <p>{userInfo.email}</p>;
  }
```

#### API

| Property             | Description                                        | Type                                    |
| -------------------- | -------------------------------------------------- | --------------------------------------- |
| authenticationStatus | Current status of the authentication process       | CIAM_AUTHENTICATION_STATUS              |
| userInfo             | Contains user data when logged in, null otherwise  | CiamUserInfo &#124; null                |
| getUserInfo          | Helper function to retrieve the user data manually | () => Promise<CiamUserInfo &#124; null> |
| redirectToRegister   | Redirect to the registration URL                   | (options: RedirectOptions) => void      |
| redirectToSignIn     | Redirect to the login URL                          | (options: RedirectOptions) => void      |
| redirectToSignOut    | Redirect to the logout URL                         | () => void                              |

###### CIAM_AUTHENTICATION_STATUS

`authenticated | error | loading | unauthenticated | unknown`

###### CiamUserInfo

| Property      | Description                         | Type                           |
| ------------- | ----------------------------------- | ------------------------------ |
| accessToken   | All access token values of the user | AccessToken                    |
| fullIdentity  | All the identity values of the user | FullIdentity                   |
| idToken       | All id token values of the user     | IdToken                        |
| subscriptions | Subscriptions linked to the user    | Array<string>                  |
| tokens        | Both Access and Id tokens as string | { access: string; id: string } |

###### RedirectOptions

| Property              | Description                                                | Type                | Required |
| --------------------- | ---------------------------------------------------------- | ------------------- | -------- |
| extLocation           | URL for the back button on login/registration screens      | string              | false    |
| loginIdentityLevel    | The required identity necessary for a user on login        | CIAM_IDENTITY_LEVEL | false    |
| process               | The required identity necessary for a user on registration | string              | false    |
| registerIdentityLevel | Should users need to verify their email                    | CIAM_IDENTITY_LEVEL | false    |
| returnTo              | The return url to go back to after user has authenticated  | string              | false    |
| skipEmailVerification | What application context you are in                        | string              | false    |

###### CIAM_IDENTITY_LEVEL

`MH1 | MH2 | MH3 | MH4 | MH5 | MH6 | MH7`

## Components

The package contains a couple of components created to help you implement authentication more quickly.

### Authentication

The <Authenticaion /> component can be used to determine the boundary of where authentication is needed. Make sure you set the autoAuthentication prop on the <CiamProvider /> to false.

#### API

| Property | Description                                                     | Type         | Required |
| -------- | --------------------------------------------------------------- | ------------ | -------- |
| fallback | Fallback element while authenticating                           | ReactElement | false    |
| silent   | Set to true if you want to render children while authenticating | boolean      | false    |

### RedirectToRegister

The <RedirectToRegister /> component will navigate to the registration url. You can set the query parameters via the props of the component.

#### API

| Property | Description                    | Type            | Required |
| -------- | ------------------------------ | --------------- | -------- |
| options  | Configure the redirect options | RedirectOptions | false    |

### RedirectToSignIn

The <RedirectToSignIn /> component will navigate to the login url. You can set the query parameters via the props of the component.

#### API

| Property | Description                    | Type            | Required |
| -------- | ------------------------------ | --------------- | -------- |
| options  | Configure the redirect options | RedirectOptions | false    |

### RedirectToSignOut

The <RedirectToSignOut /> component will navigate to the logout url. You can set the query parameters via the props of the component.

#### API

| Property | Description                    | Type            | Required |
| -------- | ------------------------------ | --------------- | -------- |
| options  | Configure the redirect options | RedirectOptions | false    |

### SignedIn

Any children components wrapped by a <SignedIn> component will be rendered only if there is a user logged in.

```
  import { SignedIn } from '@subscriber/ciam';

  const SomeComponent = (): ReactElement => {
    ...

    return (
      <div>
        <SignedIn>
          <p>Only visible when signed in</p>
        </SignedIn>

        <p>This is always visible</p>
      </div>
    );
  }
```

### SignedOut

Any children components wrapped by a <SignedOut> component will be rendered only if there is no user logged in.

```
  import { RedirectToSignIn, SignedIn, SignedOut } from '@subscriber/ciam';

  const SomeComponent = (): ReactElement => {
    ...

    return (
      <div>
        <SignedIn>
          <p>Only visible when signed in</p>
        </SignedIn>

        <SignedOut>
          <RedirectToSignIn />
        </SignedOut>
      </div>
    );
  }
```
