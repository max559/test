import type { Brand, Environment } from '../index';

const customerServiceUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testhelp.aachener-zeitung.de',
    preview: 'https://previewhelp.aachener-zeitung.de',
    production: 'https://help.aachener-zeitung.de'
  },
  co: {
    test: 'https://testhelp.contacto.lu',
    preview: 'https://previewhelp.contacto.lu',
    production: 'https://help.contacto.lu'
  },
  dl: {
    test: 'https://testhelp.limburger.nl',
    preview: 'https://previewhelp.limburger.nl',
    production: 'https://help.limburger.nl'
  },
  ds: {
    test: 'https://testhelp.standaard.be',
    preview: 'https://previewhelp.standaard.be',
    production: 'https://help.standaard.be'
  },
  gva: {
    test: 'https://testhelp.gva.be',
    preview: 'https://previewhelp.gva.be',
    production: 'https://help.gva.be'
  },
  hbvl: {
    test: 'https://testhelp.hbvl.be',
    preview: 'https://previewhelp.hbvl.be',
    production: 'https://help.hbvl.be'
  },
  lt: {
    test: 'https://testhelp.luxtimes.lu',
    preview: 'https://previewhelp.luxtimes.lu',
    production: 'https://help.luxtimes.lu'
  },
  lw: {
    test: 'https://testhelp.wort.lu',
    preview: 'https://previewhelp.wort.lu',
    production: 'https://help.wort.lu'
  },
  nb: {
    test: 'https://testhelp.nieuwsblad.be',
    preview: 'https://previewhelp.nieuwsblad.be',
    production: 'https://help.nieuwsblad.be'
  },
  tc: {
    test: 'https://testhelp.telecran.lu',
    preview: 'https://previewhelp.telecran.lu',
    production: 'https://help.telecran.lu'
  }
};

export default customerServiceUrlConfig;
