import type { Brand, Environment } from '../index';

const siteUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://test.aachener-zeitung.de',
    preview: 'https://preview.aachener-zeitung.de',
    production: 'https://www.aachener-zeitung.de'
  },
  co: {
    test: 'https://test.contacto.lu',
    preview: 'https://preview.contacto.lu',
    production: 'https://www.contacto.lu'
  },
  dl: {
    test: 'https://test.limburger.nl',
    preview: 'https://preview.limburger.nl',
    production: 'https://www.limburger.nl'
  },
  ds: {
    test: 'https://test.standaard.be',
    preview: 'https://preview.standaard.be',
    production: 'https://www.standaard.be'
  },
  gva: {
    test: 'https://test.gva.be',
    preview: 'https://preview.gva.be',
    production: 'https://www.gva.be'
  },
  hbvl: {
    test: 'https://test.hbvl.be',
    preview: 'https://preview.hbvl.be',
    production: 'https://www.hbvl.be'
  },
  lt: {
    test: 'https://test.luxtimes.lu',
    preview: 'https://preview.luxtimes.lu',
    production: 'https://www.luxtimes.lu'
  },
  lw: {
    test: 'https://test.wort.lu',
    preview: 'https://preview.wort.lu',
    production: 'https://www.wort.lu'
  },
  nb: {
    test: 'https://test.nieuwsblad.be',
    preview: 'https://preview.nieuwsblad.be',
    production: 'https://www.nieuwsblad.be'
  },
  tc: {
    test: 'https://test.telecran.lu',
    preview: 'https://preview.telecran.lu',
    production: 'https://www.telecran.lu'
  }
};

export default siteUrlConfig;
