import type { Brand, Environment } from '../index';

const staticUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://teststatic.aachener-zeitung.de',
    preview: 'https://previewstatic.aachener-zeitung.de',
    production: 'https://static.aachener-zeitung.de'
  },
  co: {
    test: 'https://teststatic.contacto.lu',
    preview: 'https://previewstatic.contacto.lu',
    production: 'https://static.contacto.lu'
  },
  dl: {
    test: 'https://teststatic.limburger.nl',
    preview: 'https://previewstatic.limburger.nl',
    production: 'https://static.limburger.nl'
  },
  ds: {
    test: 'https://teststatic.standaard.be',
    preview: 'https://previewstatic.standaard.be',
    production: 'https://static.standaard.be'
  },
  gva: {
    test: 'https://teststatic.gva.be',
    preview: 'https://previewstatic.gva.be',
    production: 'https://static.gva.be'
  },
  hbvl: {
    test: 'https://teststatic.hbvl.be',
    preview: 'https://previewstatic.hbvl.be',
    production: 'https://static.hbvl.be'
  },
  lt: {
    test: 'https://teststatic.luxtimes.lu',
    preview: 'https://previewstatic.luxtimes.lu',
    production: 'https://static.luxtimes.lu'
  },
  lw: {
    test: 'https://teststatic.wort.lu',
    preview: 'https://previewstatic.wort.lu',
    production: 'https://static.wort.lu'
  },
  nb: {
    test: 'https://teststatic.nieuwsblad.be',
    preview: 'https://previewstatic.nieuwsblad.be',
    production: 'https://static.nieuwsblad.be'
  },
  tc: {
    test: 'https://teststatic.telecran.lu',
    preview: 'https://previewstatic.telecran.lu',
    production: 'https://static.telecran.lu'
  }
};

export default staticUrlConfig;
