import type { Brand, Environment } from '../index';

const defaultLocaleConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'de-DE',
    preview: 'de-DE',
    production: 'de-DE'
  },
  co: {
    test: 'pt-LU',
    preview: 'pt-LU',
    production: 'pt-LU'
  },
  dl: {
    test: 'nl-NL',
    preview: 'nl-NL',
    production: 'nl-NL'
  },
  ds: {
    test: 'nl-BE',
    preview: 'nl-BE',
    production: 'nl-BE'
  },
  gva: {
    test: 'nl-BE',
    preview: 'nl-BE',
    production: 'nl-BE'
  },
  hbvl: {
    test: 'nl-BE',
    preview: 'nl-BE',
    production: 'nl-BE'
  },
  lt: {
    test: 'en-LU',
    preview: 'en-LU',
    production: 'en-LU'
  },
  lw: {
    test: 'de-LU',
    preview: 'de-LU',
    production: 'de-LU'
  },
  nb: {
    test: 'nl-BE',
    preview: 'nl-BE',
    production: 'nl-BE'
  },
  tc: {
    test: 'de-LU',
    preview: 'de-LU',
    production: 'de-LU'
  }
};

export default defaultLocaleConfig;
