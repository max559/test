import type { Brand, Environment } from '../index';

const brandNameConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'Aachener Zeitung',
    preview: 'Aachener Zeitung',
    production: 'Aachener Zeitung'
  },
  co: {
    test: 'Contacto',
    preview: 'Contacto',
    production: 'Contacto'
  },
  dl: {
    test: 'De Limburger',
    preview: 'De Limburger',
    production: 'De Limburger'
  },
  ds: {
    test: 'De Standaard',
    preview: 'De Standaard',
    production: 'De Standaard'
  },
  gva: {
    test: 'Gazet van Antwerpen',
    preview: 'Gazet van Antwerpen',
    production: 'Gazet van Antwerpen'
  },
  hbvl: {
    test: 'Het Belang van Limburg',
    preview: 'Het Belang van Limburg',
    production: 'Het Belang van Limburg'
  },
  lt: {
    test: 'Luxembourg Times',
    preview: 'Luxembourg Times',
    production: 'Luxembourg Times'
  },
  lw: {
    test: 'Luxemburger Wort',
    preview: 'Luxemburger Wort',
    production: 'Luxemburger Wort'
  },
  nb: {
    test: 'Nieuwsblad',
    preview: 'Nieuwsblad',
    production: 'Nieuwsblad'
  },
  tc: {
    test: 'Télécran',
    preview: 'Télécran',
    production: 'Télécran'
  }
};

export default brandNameConfig;
