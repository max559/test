import type { Brand, Environment } from '../index';

const aboshopUrlConfig: Record<Brand, Record<Environment, string>> = {
  az: {
    test: 'https://testaboshop.aachener-zeitung.de',
    preview: 'https://previewaboshop.aachener-zeitung.de',
    production: 'https://aboshop.aachener-zeitung.de'
  },
  co: {
    test: 'https://testaboshop.contacto.lu',
    preview: 'https://previewaboshop.contacto.lu',
    production: 'https://aboshop.contacto.lu'
  },
  dl: {
    test: 'https://testaboshop.limburger.nl',
    preview: 'https://previewaboshop.limburger.nl',
    production: 'https://aboshop.limburger.nl'
  },
  ds: {
    test: 'https://testaboshop.standaard.be',
    preview: 'https://previewaboshop.standaard.be',
    production: 'https://aboshop.standaard.be'
  },
  gva: {
    test: 'https://testaboshop.gva.be',
    preview: 'https://previewaboshop.gva.be',
    production: 'https://aboshop.gva.be'
  },
  hbvl: {
    test: 'https://testaboshop.hbvl.be',
    preview: 'https://previewaboshop.hbvl.be',
    production: 'https://aboshop.hbvl.be'
  },
  lt: {
    test: 'https://testaboshop.luxtimes.lu',
    preview: 'https://previewaboshop.luxtimes.lu',
    production: 'https://aboshop.luxtimes.lu'
  },
  lw: {
    test: 'https://testaboshop.wort.lu',
    preview: 'https://previewaboshop.wort.lu',
    production: 'https://aboshop.wort.lu'
  },
  nb: {
    test: 'https://testaboshop.nieuwsblad.be',
    preview: 'https://previewaboshop.nieuwsblad.be',
    production: 'https://aboshop.nieuwsblad.be'
  },
  tc: {
    test: 'https://testaboshop.telecran.lu',
    preview: 'https://previewaboshop.telecran.lu',
    production: 'https://aboshop.telecran.lu'
  }
};

export default aboshopUrlConfig;
