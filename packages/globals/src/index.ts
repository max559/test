import {
  aboshopUrlConfig,
  brandNameConfig,
  customerServiceUrlConfig,
  defaultLocaleConfig,
  siteUrlConfig,
  staticUrlConfig
} from './configs';

export type Brand = 'az' | 'co' | 'dl' | 'ds' | 'gva' | 'hbvl' | 'lt' | 'lw' | 'nb' | 'tc';
export type Environment = 'test' | 'preview' | 'production';

export enum EVENT_TYPE {
  ACTION = 'action',
  CLICK = 'click',
  SHOW = 'show',
  SUBMIT = 'submit'
}

export type CustomEvent<Key, Data> = {
  type: EVENT_TYPE;
  key: Key;
  data?: Data;
};

export interface Configuration {
  aboshopUrl: string;
  brandName: string;
  customerServiceUrl: string;
  defaultLocale: string;
  siteUrl: string;
  staticUrl: string;
}

export const getConfiguration = (brand: Brand, environment: Environment): Configuration => ({
  aboshopUrl: aboshopUrlConfig[brand][environment],
  brandName: brandNameConfig[brand][environment],
  customerServiceUrl: customerServiceUrlConfig[brand][environment],
  defaultLocale: defaultLocaleConfig[brand][environment],
  siteUrl: siteUrlConfig[brand][environment],
  staticUrl: staticUrlConfig[brand][environment]
});

export enum BORDER_RADIUS {
  sm = '0.125rem',
  default = '0.25rem',
  md = '0.375rem',
  lg = '0.5rem',
  xl = '0.75rem',
  '2xl' = '1rem',
  '3xl' = '1.5rem',
  full = '9999px'
}

export type SPACING_TYPE =
  | 0
  | 0.5
  | 1
  | 1.5
  | 2
  | 2.5
  | 3
  | 3.5
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12
  | 14
  | 16
  | 20
  | 24
  | 28
  | 32
  | 36
  | 40
  | 44
  | 48
  | 52
  | 56
  | 60
  | 64
  | 72
  | 80
  | 96;

export const SPACING = Object.freeze({
  0: '0',
  0.5: '0.125rem',
  1: '0.25rem',
  1.5: '0.375rem',
  2: '0.5rem',
  2.5: '0.625rem',
  3: '0.75rem',
  3.5: '0.875rem',
  4: '1rem',
  5: '1.25rem',
  6: '1.5rem',
  7: '1.75rem',
  8: '2rem',
  9: '2.25rem',
  10: '2.5rem',
  11: '2.75rem',
  12: '3rem',
  14: '3.5rem',
  16: '4rem',
  20: '5rem',
  24: '6rem',
  28: '7rem',
  32: '8rem',
  36: '9rem',
  40: '10rem',
  44: '11rem',
  48: '12rem',
  52: '13rem',
  56: '14rem',
  60: '15rem',
  64: '16rem',
  72: '18rem',
  80: '20rem',
  96: '24rem',
  100: 15
});
