import type { Offer } from '@subscriber/services-core';
import type { Fetcher } from 'swr';

import useSWR, { preload } from 'swr';

import { useSmartwallContext } from '../components/Smartwall';

export const offerBySlugKey = 'offer-by-slug';

export const useOfferBySlug = (slug?: string) => {
  const { coreInstance } = useSmartwallContext();

  const fetcher: Fetcher<Offer, [key: string, slug: string]> = ([, slugParam]) =>
    coreInstance.offerService
      .getOfferBySlug({ slug: encodeURIComponent(slugParam) })
      .then(response => response.data);

  const offerBySlugQuery = useSWR(slug ? [offerBySlugKey, slug] : null, fetcher);

  return {
    ...offerBySlugQuery,
    prefetch: (prefetchSlug: string) => preload([offerBySlugKey, prefetchSlug], fetcher)
  };
};
