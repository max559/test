import type { Order } from '@subscriber/services-core';
import type { Fetcher } from 'swr';

import useSWR from 'swr';

import { useSmartwallContext } from '../components/Smartwall';

export const orderKey = 'order';

export const useOrder = (id?: number) => {
  const { coreInstance } = useSmartwallContext();

  const fetcher: Fetcher<Order, [key: string, id: number]> = ([, idParam]) =>
    coreInstance.orderService.getOrder({ id: idParam }).then(response => response.data);

  return useSWR(id ? [orderKey, id] : null, fetcher);
};
