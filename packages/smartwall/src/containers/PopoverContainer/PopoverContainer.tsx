import type { PropsWithChildren, ReactElement } from 'react';

import type { PopoverContainerConfigOptions } from './PopoverContainer.types';

import { Popover } from '@subscriber/subscriber-ui';
import { useState } from 'react';

interface PopoverContainerProps {
  options: PopoverContainerConfigOptions;
}

const PopoverContainer = ({
  children,
  options
}: PropsWithChildren<PopoverContainerProps>): ReactElement => {
  const [showPopover, setShowPopover] = useState<boolean>(true);

  if (showPopover) {
    return (
      <Popover
        closeOnOutsideClick={options.closeOnOutsideClick}
        hideBackdrop={options.hideBackdrop}
        show={showPopover}
        showClose={options.showClose}
        onClose={() => setShowPopover(false)}
      >
        {children}
      </Popover>
    );
  }

  return <>{children}</>;
};

export default PopoverContainer;
