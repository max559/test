export interface PopoverContainerConfig {
  options: PopoverContainerConfigOptions;
  type: 'popover';
}

export interface PopoverContainerConfigOptions {
  closeOnOutsideClick?: boolean;
  hideBackdrop?: boolean;
  showClose?: boolean;
}
