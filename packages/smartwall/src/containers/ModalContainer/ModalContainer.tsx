import type { PropsWithChildren, ReactElement } from 'react';

import type { ModalContainerConfigOptions } from './ModalContainer.types';

import { Modal } from '@subscriber/subscriber-ui';
import { useState } from 'react';

interface ModalContainerProps {
  options: ModalContainerConfigOptions;
}

const ModalContainer = ({
  children,
  options
}: PropsWithChildren<ModalContainerProps>): ReactElement => {
  const [showModal, setShowModal] = useState<boolean>(true);

  if (showModal) {
    return (
      <Modal
        closeOnOutsideClick={options.closeOnOutsideClick}
        show={showModal}
        onClose={() => setShowModal(false)}
      >
        {children}
      </Modal>
    );
  }

  return <>{children}</>;
};

export default ModalContainer;
