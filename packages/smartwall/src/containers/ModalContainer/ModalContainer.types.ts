export interface ModalContainerConfig {
  options: ModalContainerConfigOptions;
  type: 'modal';
}

export interface ModalContainerConfigOptions {
  closeOnOutsideClick?: boolean;
}
