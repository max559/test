import type { Brand, Environment } from '@subscriber/globals';
import type { AccountInfoResponse, ServicesCore } from '@subscriber/services-core';
import { createContext, useContext } from 'react';

export interface SmartwallContextValue {
  aboshopUrl: string;
  userInfo: AccountInfoResponse | null;
  brand: Brand;
  coreInstance: ServicesCore;
  environment: Environment;
  siteUrl: string;
  staticUrl: string;
}

export const SmartwallContext = createContext<SmartwallContextValue | null>(null);

export const useSmartwallContext = () => {
  const smartwallContext = useContext(SmartwallContext);

  if (!smartwallContext) {
    throw new Error(
      'The useSmartwallContext hook can only be used inside of an AuthProvider component'
    );
  }

  return smartwallContext;
};
