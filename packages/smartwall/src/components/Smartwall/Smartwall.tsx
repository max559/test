import type { Brand, Environment } from '@subscriber/globals';
import type { ReactElement } from 'react';

import type { ModalContainerConfig } from '../../containers/ModalContainer';
import type { PopoverContainerConfig } from '../../containers/PopoverContainer';
import type { DefaultCheckoutTemplateConfig } from '../../templates/DefaultCheckoutTemplate';
import type { FastCheckoutTemplateConfig } from '../../templates/FastCheckoutTemplate';
import type { HtmlTemplateConfig } from '../../templates/HtmlTemplate';
import type { SmartwallContextValue } from './Smartwall.context';

import { getConfiguration } from '@subscriber/globals';
import { AccountInfoResponse, ServicesCore } from '@subscriber/services-core';
import { useMemo } from 'react';

import { SmartwallContext } from './Smartwall.context';
import SmartwallContainer from './SmartwallContainer';
import SmartwallTemplate from './SmartwallTemplate';

interface SmartwallProps {
  userInfo: AccountInfoResponse | null;
  brand: Brand;
  container?: ModalContainerConfig | PopoverContainerConfig;
  environment: Environment;
  template: DefaultCheckoutTemplateConfig | FastCheckoutTemplateConfig | HtmlTemplateConfig;
}

const Smartwall = ({
  userInfo,
  brand,
  container,
  environment,
  template
}: SmartwallProps): ReactElement => {
  const providerValue: SmartwallContextValue = useMemo(() => {
    const { aboshopUrl, siteUrl, staticUrl } = getConfiguration(brand, environment);

    return {
      aboshopUrl,
      userInfo,
      brand,
      coreInstance: new ServicesCore(brand, environment),
      environment,
      siteUrl,
      staticUrl
    };
  }, [userInfo, brand, environment]);

  return (
    <SmartwallContext.Provider value={providerValue}>
      <SmartwallContainer config={container}>
        <SmartwallTemplate config={template} />
      </SmartwallContainer>
    </SmartwallContext.Provider>
  );
};

export default Smartwall;
