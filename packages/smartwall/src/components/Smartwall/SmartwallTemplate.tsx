import type { ReactElement } from 'react';

import type { FastCheckoutTemplateConfig } from '../../templates/FastCheckoutTemplate';
import type { HtmlTemplateConfig } from '../../templates/HtmlTemplate';
import type { DefaultCheckoutTemplateConfig } from '../../templates/DefaultCheckoutTemplate';

import { DefaultCheckoutTemplate } from '../../templates/DefaultCheckoutTemplate';
import { FastCheckoutTemplate } from '../../templates/FastCheckoutTemplate';
import { HtmlTemplate } from '../../templates/HtmlTemplate';

interface SmartwallTemplateProps {
  config: DefaultCheckoutTemplateConfig | FastCheckoutTemplateConfig | HtmlTemplateConfig;
}

const SmartwallTemplate = ({ config }: SmartwallTemplateProps): ReactElement | null => {
  if (config.type === 'default-checkout') {
    return <DefaultCheckoutTemplate data={config.data} />;
  }

  if (config.type === 'fast-checkout') {
    return <FastCheckoutTemplate data={config.data} />;
  }

  if (config.type === 'html') {
    return <HtmlTemplate html={config.data.html} />;
  }

  return null;
};

export default SmartwallTemplate;
