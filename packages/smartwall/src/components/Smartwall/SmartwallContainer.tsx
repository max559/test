import type { PropsWithChildren, ReactElement } from 'react';

import type { ModalContainerConfig } from '../../containers/ModalContainer';
import type { PopoverContainerConfig } from '../../containers/PopoverContainer';

import { ModalContainer } from '../../containers/ModalContainer';
import { PopoverContainer } from '../../containers/PopoverContainer';

interface SmartwallContainerProps {
  config?: ModalContainerConfig | PopoverContainerConfig;
}

const SmartwallContainer = ({
  children,
  config
}: PropsWithChildren<SmartwallContainerProps>): ReactElement => {
  if (config?.type === 'modal') {
    return <ModalContainer options={config.options}>{children}</ModalContainer>;
  }

  if (config?.type === 'popover') {
    return <PopoverContainer options={config.options}>{children}</PopoverContainer>;
  }

  return <>{children}</>;
};

export default SmartwallContainer;
