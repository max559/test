export interface FastCheckoutTemplateConfig {
  data: FastCheckoutTemplateConfigData;
  type: 'fast-checkout';
}

export interface FastCheckoutTemplateConfigData {
  articleTitle: string;
  orderId?: number;
}
