import type { ReactElement } from 'react';

import { Button, Heading, Loader, Paragraph, useMediaQuery } from '@mediahuis/chameleon-react';
import { SPACING } from '@subscriber/globals';
import { ORDER_STATE, VALIDATION_STATE } from '@subscriber/services-core';
import { Flex } from '@subscriber/subscriber-ui';

import { useOrder } from '../../../hooks';
import ErrorMessage from './ErrorMessage';

interface OrderVerificationProps {
  orderId: number;
  onReturnToOffer: () => void;
}

const OrderVerification = ({ orderId, onReturnToOffer }: OrderVerificationProps): ReactElement => {
  const orderQuery = useOrder(orderId);

  const containerStyle = useMediaQuery({
    xs: { margin: '0 auto', padding: SPACING[8], width: '100%' },
    md: { margin: '0 auto', padding: SPACING[8], maxWidth: '40rem', width: '100%' },
    lg: { margin: '0 auto', padding: SPACING[8], maxWidth: '60rem', width: '100%' }
  });

  if (orderQuery.data) {
    const orderState = orderQuery.data.state;
    const paymentValidation = orderQuery.data.paymentValidation;

    if (
      orderState === ORDER_STATE.Confirmed ||
      paymentValidation?.state === VALIDATION_STATE.Compliant
    ) {
      return (
        <div style={containerStyle}>
          <Flex flexDirection="column" gap={5}>
            <Heading level={4}>Gelukt, je bestelling is succesvol afgerond!</Heading>
            <Paragraph>Je ontvangt meteen een bevestiging via e-mail.</Paragraph>
            <Button>Start nu met lezen</Button>
          </Flex>
        </div>
      );
    }

    return (
      <ErrorMessage
        variant="payment"
        onReturnToOffer={() => {
          const params = new URLSearchParams(location.search);
          params.delete('sw_order_id');
          window.history.pushState({}, window.location.pathname, `/${params}`);
          onReturnToOffer();
        }}
      />
    );
  }

  return (
    <Flex
      alignItems="center"
      flexDirection="column"
      justifyContent="center"
      style={{ padding: '5rem' }}
    >
      <Loader size="xl" />
    </Flex>
  );
};

export default OrderVerification;
