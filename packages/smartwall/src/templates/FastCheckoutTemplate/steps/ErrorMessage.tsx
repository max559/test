import type { ReactElement } from 'react';

import { Button, Heading, Paragraph, useMediaQuery } from '@mediahuis/chameleon-react';
import { SPACING } from '@subscriber/globals';
import { Flex } from '@subscriber/subscriber-ui';

const defaultErrorText = 'Er ging iets mis';
const paymentErrorText = 'Je betaling werd niet succesvol uitgevoerd';
const preconditionErrorText = 'Je hebt afgelopen periode reeds genoten van een voordeelaanbod ...';

const renderErrorText = (variant: 'default' | 'payment' | 'precondition') => {
  if (variant === 'payment') {
    return paymentErrorText;
  }
  if (variant === 'precondition') {
    return preconditionErrorText;
  }

  return defaultErrorText;
};

interface ErrorMessageProps {
  variant?: 'default' | 'payment' | 'precondition';
  onReturnToOffer: () => void;
}

const ErrorMessage = ({
  variant = 'default',
  onReturnToOffer
}: ErrorMessageProps): ReactElement => {
  const containerStyle = useMediaQuery({
    xs: { margin: '0 auto', padding: SPACING[8], width: '100%' },
    md: { margin: '0 auto', padding: SPACING[8], maxWidth: '40rem', width: '100%' },
    lg: { margin: '0 auto', padding: SPACING[8], maxWidth: '60rem', width: '100%' }
  });

  return (
    <div style={containerStyle}>
      <Flex flexDirection="column" gap={4}>
        <Heading level={4}>Er liep iets mis</Heading>
        <Paragraph>{renderErrorText(variant)}</Paragraph>
        <Button
          onClick={() => {
            const params = new URLSearchParams(location.search);
            params.delete('sw_order_id');
            window.history.pushState({}, window.location.pathname, `/${params}`);
            onReturnToOffer();
          }}
        >
          Terug naar het aanbod
        </Button>
      </Flex>
    </div>
  );
};

export default ErrorMessage;
