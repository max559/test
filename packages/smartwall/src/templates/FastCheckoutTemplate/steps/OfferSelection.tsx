import type { OfferItem, SortableFeature } from '@subscriber/services-core';
import type { ReactElement } from 'react';

import {
  Caption,
  Heading,
  LinkText,
  Loader,
  Paragraph,
  useMediaQuery
} from '@mediahuis/chameleon-react';
import { SPACING } from '@subscriber/globals';
import { CompactCard, DefaultCard, Flex } from '@subscriber/subscriber-ui';
import Markdown from 'markdown-to-jsx';
import { Fragment, useEffect } from 'react';

import { useSmartwallContext } from '../../../components/Smartwall';
import { useOfferBySlug } from '../../../hooks';

export enum VITRINE_SCREEN_SIZE {
  xs = 'xs',
  sm = 'sm',
  md = 'md',
  lg = 'lg',
  xl = 'xl'
}

interface OfferSelectionProps {
  articleTitle: string;
  onSelect: (offerItem: OfferItem) => void;
}

const OfferSelection = ({ articleTitle, onSelect }: OfferSelectionProps): ReactElement => {
  const { aboshopUrl, userInfo, siteUrl, staticUrl } = useSmartwallContext();

  const containerStyle = useMediaQuery({
    xs: { margin: '0 auto', padding: SPACING[8], width: '100%' },
    md: { margin: '0 auto', padding: SPACING[8], maxWidth: '40rem', width: '100%' },
    lg: { margin: '0 auto', padding: SPACING[8], maxWidth: '60rem', width: '100%' }
  });
  const screenSize: VITRINE_SCREEN_SIZE = useMediaQuery(['xs', 'sm', 'md', 'lg', 'xl']);

  const typeOfferQuery = useOfferBySlug('porous-maxedout');

  useEffect(() => {
    if (typeOfferQuery.data) {
      typeOfferQuery.data.items?.forEach((offerItem: OfferItem) => {
        if (offerItem.nextStep) {
          typeOfferQuery.prefetch(offerItem.nextStep);
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [typeOfferQuery.data]);

  return (
    <div style={containerStyle}>
      <Flex flexDirection="column" gap={8}>
        <Heading level={4}>{articleTitle}</Heading>

        {typeOfferQuery.isLoading ? (
          <div style={{ display: 'flex', justifyContent: 'center', padding: '5rem' }}>
            <Loader size="xl" />
          </div>
        ) : (
          <>
            <Paragraph>{typeOfferQuery.data?.title}</Paragraph>
            <Flex
              flexDirection={
                screenSize === VITRINE_SCREEN_SIZE.xs || screenSize === VITRINE_SCREEN_SIZE.sm
                  ? 'column'
                  : 'row'
              }
              gap={4}
            >
              {typeOfferQuery.data?.items?.map((offerItem: OfferItem) => {
                if (
                  screenSize === VITRINE_SCREEN_SIZE.xs ||
                  screenSize === VITRINE_SCREEN_SIZE.sm
                ) {
                  return (
                    <Flex flexDirection="column" key={offerItem.id}>
                      <CompactCard
                        featuresListItems={
                          offerItem.features?.map((feature: SortableFeature) => ({
                            isAvailable: true,
                            key: feature.id,
                            text: {
                              render: value => (
                                <Markdown
                                  options={{
                                    overrides: {
                                      img: {
                                        component: 'img',
                                        props: {
                                          alt: 'FeaturesList-markdown-img',
                                          className: 'align-middle inline',
                                          height: 14,
                                          width: 14
                                        }
                                      }
                                    },
                                    wrapper: Fragment
                                  }}
                                >
                                  {value}
                                </Markdown>
                              ),
                              value: feature.description || ''
                            }
                          })) || []
                        }
                        highlight={offerItem.isHighlighted}
                        label={offerItem.label}
                        price={{
                          render: value => (
                            <Markdown options={{ forceInline: true }}>{value}</Markdown>
                          ),
                          value: offerItem.priceSentence || ''
                        }}
                        title={{
                          render: value => (
                            <Markdown options={{ forceInline: true }}>{value}</Markdown>
                          ),
                          value: offerItem.title || ''
                        }}
                        onPaperClick={() => onSelect(offerItem)}
                      />
                    </Flex>
                  );
                }
                return (
                  <div key={offerItem.id} style={{ flexGrow: 1 }}>
                    <DefaultCard
                      buttonCaption={
                        offerItem.extra
                          ? {
                              value: offerItem.extra
                            }
                          : undefined
                      }
                      buttonText={offerItem.buttonLabel || ''}
                      description={{
                        render: value => (
                          <Markdown
                            options={{
                              forceInline: true,
                              overrides: {
                                img: {
                                  component: 'img',
                                  props: {
                                    alt: 'DefaultVitrineCard-markdown-img',
                                    className: 'align-middle inline',
                                    height: 16,
                                    width: 16
                                  }
                                }
                              }
                            }}
                          >
                            {value}
                          </Markdown>
                        ),
                        value: offerItem.description || ''
                      }}
                      featuresListItems={
                        offerItem.features?.map((feature: SortableFeature) => ({
                          isAvailable: true,
                          key: feature.id,
                          text: {
                            render: value => (
                              <Markdown
                                options={{
                                  overrides: {
                                    img: {
                                      component: 'img',
                                      props: {
                                        alt: 'FeaturesList-markdown-img',
                                        className: 'align-middle inline',
                                        height: 14,
                                        width: 14
                                      }
                                    }
                                  },
                                  wrapper: Fragment
                                }}
                              >
                                {value}
                              </Markdown>
                            ),
                            value: feature.description || ''
                          }
                        })) || []
                      }
                      highlight={offerItem.isHighlighted}
                      imageProps={
                        offerItem.image
                          ? {
                              alt: offerItem.title,
                              src: `${staticUrl}/${offerItem.image}`
                            }
                          : undefined
                      }
                      label={offerItem.label}
                      // logoSrc={offerItem.logo ? `${staticUrl}/${offerItem.logo}` : undefined}
                      priceCaption={
                        offerItem.priceExtra
                          ? {
                              value: offerItem.priceExtra
                            }
                          : undefined
                      }
                      priceText={{
                        render: value => (
                          <Markdown options={{ forceInline: true }}>{value}</Markdown>
                        ),
                        value: offerItem.priceSentence || ''
                      }}
                      title={{
                        render: value => (
                          <Markdown options={{ forceInline: true }}>{value}</Markdown>
                        ),
                        value: offerItem.title || ''
                      }}
                      onButtonClick={() => onSelect(offerItem)}
                      onPaperClick={() => onSelect(offerItem)}
                    />
                  </div>
                );
              })}
            </Flex>

            <LinkText href={`${aboshopUrl}/actie/opvolgingporeus`}>
              Ontdek alle leesformules
            </LinkText>
          </>
        )}

        <Flex flexDirection="column" gap={1}>
          <Heading level={6} size="sm">
            Ben je al abonnee?
          </Heading>
          <Caption size="lg">
            Je surft nu met {userInfo?.EmailAddress}.{' '}
            <LinkText href={`${siteUrl}/account/logoff?goto=${window.location.pathname}`}>
              Klik hier
            </LinkText>{' '}
            om met een ander e-mailadres te lezen, of breng je digitale toegang{' '}
            <LinkText href={`${siteUrl}/activeer`}>hier</LinkText> in orde.
          </Caption>
        </Flex>
      </Flex>
    </div>
  );
};

export default OfferSelection;
