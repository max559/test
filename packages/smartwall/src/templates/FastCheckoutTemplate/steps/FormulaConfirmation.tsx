import type { ReactElement } from 'react';

import {
  Button,
  Caption,
  Checkbox,
  Heading,
  LinkText,
  useMediaQuery
} from '@mediahuis/chameleon-react';
import { SPACING } from '@subscriber/globals';
import {
  AddressAreaValidation,
  DateValidation,
  LoginValidation,
  Offer,
  OfferItem,
  PAYMENT_TYPE,
  Payment,
  PaymentValidation,
  SortableFeature
} from '@subscriber/services-core';
import { FeaturesList, FeaturesListItem, Flex } from '@subscriber/subscriber-ui';
import Markdown from 'markdown-to-jsx';
import { Fragment, useState } from 'react';

import { useSmartwallContext } from '../../../components/Smartwall';
import { useOfferBySlug } from '../../../hooks';

const getFeaturesListItems = (features?: Array<SortableFeature>): Array<FeaturesListItem> => {
  const defaultItems: Array<FeaturesListItem> = [
    { isAvailable: true, key: 0, text: { value: 'Toegang tot alle plusartikels' } }
  ];
  const mappedItems: Array<FeaturesListItem> = features
    ? features.map((feature: SortableFeature) => ({
        isAvailable: true,
        key: feature.id,
        text: {
          render: value => (
            <Markdown
              options={{
                overrides: {
                  img: {
                    component: 'img',
                    props: {
                      alt: 'FeaturesList-markdown-img',
                      className: 'align-middle inline',
                      height: 14,
                      width: 14
                    }
                  }
                },
                wrapper: Fragment
              }}
            >
              {value}
            </Markdown>
          ),
          value: feature.description || ''
        }
      }))
    : [];

  return [...defaultItems, ...mappedItems];
};

interface FormulaConfirmationProps {
  offerType: OfferItem;
  onError: () => void;
  onPaymentCreate: (payment: Payment) => void;
  onPreconditionError: () => void;
}

const FormulaConfirmation = ({
  offerType,
  onError,
  onPaymentCreate,
  onPreconditionError
}: FormulaConfirmationProps): ReactElement => {
  const { aboshopUrl, userInfo, brand, coreInstance } = useSmartwallContext();

  const [areConditionsAccepted, setAreConditionsAccepted] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const formulaOfferQuery = useOfferBySlug(offerType.nextStep);

  function createOrder(offer: Offer, formula: OfferItem) {
    const order = coreInstance.orderUtil.generateOrder(formula, offer.type, offer.id);
    return coreInstance.orderService.createOrder(order);
  }

  function createPayment(paymentValidation: PaymentValidation, orderId: number) {
    const redirectUrl = new URL(window.location.href);

    redirectUrl.searchParams.append('sw_order_id', orderId.toString());

    return coreInstance.paymentService.createPayment({
      amount: paymentValidation.amount,
      email: userInfo?.EmailAddress,
      currency: 'EUR',
      description: offerType.name,
      methods: ['Bancontact'],
      name:
        userInfo?.FirstName && userInfo?.Name
          ? `${userInfo.FirstName} ${userInfo.Name}`
          : undefined,
      paymentReceiver: brand,
      paymentType:
        (paymentValidation.oneShotPaymentOptions || []).length > 0
          ? PAYMENT_TYPE.OneShot
          : PAYMENT_TYPE.FirstRecurring,
      redirectUrl: redirectUrl.toString(),
      source: 'Aboshop',
      sourceReference: paymentValidation.id.toString()
    });
  }

  function handlePaymentClick() {
    setIsLoading(true);

    const formulaOffer = formulaOfferQuery.data;
    const selectedFormula = formulaOffer && formulaOffer.items && formulaOffer.items[0];

    if (formulaOffer && selectedFormula) {
      createOrder(formulaOffer, selectedFormula)
        .then(async orderResponse => {
          const addressAreaValidation = orderResponse.data.addressAreaValidation;
          const dateValidation = orderResponse.data.dateValidation;
          const emailValidation = orderResponse.data.emailValidation;
          const invoiceValidation = orderResponse.data.invoiceDetailsValidation;
          const loginValidation = orderResponse.data.loginValidation;
          const paymentValidation = orderResponse.data.paymentValidation;
          const preconditionValidation = orderResponse.data.preconditionsValidation;

          if (addressAreaValidation) {
            addressAreaValidation.data = {
              addressee: `${userInfo?.Name} ${userInfo?.FirstName}`,
              busNumber: userInfo?.Box,
              city: userInfo?.City,
              countryCode: userInfo?.Country,
              houseNumber: userInfo?.HouseNumber,
              postalCode: userInfo?.Zipcode,
              street: userInfo?.Street
            };
            addressAreaValidation.brand = orderResponse.data.brand;
          }

          if (dateValidation) {
            dateValidation.data = {};
          }

          if (emailValidation) {
            emailValidation.data = {
              value: userInfo?.EmailAddress
            };
          }

          if (loginValidation) {
            loginValidation.data.firstName = userInfo?.FirstName;
            loginValidation.data.lastName = userInfo?.Name;
            loginValidation.brand = orderResponse.data.brand;
          }

          if (preconditionValidation) {
            preconditionValidation.data = {
              boxNumber: userInfo?.Box,
              email: userInfo?.EmailAddress,
              houseNumber: userInfo?.HouseNumber,
              postalCode: userInfo?.Zipcode,
              street: userInfo?.Street
            };
          }
          Promise.all([
            addressAreaValidation
              ? coreInstance.locationValidatorService.postAddressAreaValidation({
                  ...addressAreaValidation,
                  parameters: { ...addressAreaValidation }
                } as AddressAreaValidation)
              : null,
            dateValidation
              ? coreInstance.defaultValidatorService.postDateValidation({
                  ...dateValidation,
                  parameters: { ...dateValidation }
                } as DateValidation)
              : null,
            emailValidation
              ? coreInstance.defaultValidatorService.postEmailValidation(emailValidation)
              : null,
            invoiceValidation
              ? coreInstance.locationValidatorService.postInvoiceDetailsValidation(
                  invoiceValidation
                )
              : null,
            loginValidation
              ? coreInstance.loginValidatorService.postLoginValidation({
                  ...loginValidation,
                  parameters: { ...loginValidation }
                } as LoginValidation)
              : null,
            paymentValidation ? createPayment(paymentValidation, orderResponse.data.id) : null,
            preconditionValidation
              ? coreInstance.preconditionValidatorService
                  .postPreconditionValidation(preconditionValidation)
                  .catch(() => false)
              : null
          ])
            .then(responses => {
              if (responses[6] === false) {
                onPreconditionError();
              } else if (responses[5]) {
                onPaymentCreate(responses[5].data);
              }
            })
            .catch(() => onError())
            .finally(() => setIsLoading(false));
        })
        .catch(() => {
          setIsLoading(false);
          onError();
        });
    }
  }

  const containerStyle = useMediaQuery({
    xs: { margin: '0 auto', padding: SPACING[8], width: '100%' },
    md: { margin: '0 auto', padding: SPACING[8], maxWidth: '40rem', width: '100%' },
    lg: { margin: '0 auto', padding: SPACING[8], maxWidth: '60rem', width: '100%' }
  });

  return (
    <Flex flexDirection="column">
      <div style={containerStyle}>
        <Flex flexDirection="column" gap={5}>
          <Heading level={4}>Jouw formule</Heading>

          <FeaturesList items={getFeaturesListItems(offerType.features)} />
        </Flex>
      </div>
      <div style={{ backgroundColor: 'var(--color-primary-10)' }}>
        <div style={containerStyle}>
          <Flex flexDirection="column" gap={5}>
            <Checkbox
              checked={areConditionsAccepted}
              id="GeneralConditionsCheck"
              label="Ik machtig Mediahuis nv om mijn abonnementsgeld via een doorlopende SEPA domicilieringsopdracht van mijn rekening over te schrijven en ga akkoord met de algemene voorwaarden"
              labelProps={{
                fontWeight: 'regular',
                size: 'Caption1'
              }}
              onChange={() => setAreConditionsAccepted(prevValue => !prevValue)}
            />

            <Flex flexDirection="column" gap={1}>
              <Button
                appearance="primary"
                disabled={!areConditionsAccepted}
                loading={formulaOfferQuery.isLoading || isLoading}
                onClick={handlePaymentClick}
                width="full"
              >
                Activeer domiciliering via Bancontact
              </Button>
              <Caption size="sm">
                Je betaalt een controlebedrag van € 0,02 om je rekeningnummer te verifiëren
              </Caption>
            </Flex>

            <LinkText href={`${aboshopUrl}/actie/opvolgingporeus`}>
              Andere betaalwijze kiezen
            </LinkText>
          </Flex>
        </div>
      </div>
    </Flex>
  );
};

export default FormulaConfirmation;
