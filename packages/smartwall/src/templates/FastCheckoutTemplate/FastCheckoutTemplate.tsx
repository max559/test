import type { OfferItem, Payment } from '@subscriber/services-core';
import type { WizardStep } from '@subscriber/subscriber-ui';
import type { ReactElement } from 'react';

import type { FastCheckoutTemplateConfigData } from './FastCheckoutTemplate.types';

import { Wizard } from '@subscriber/subscriber-ui';
import { useState } from 'react';

import { ErrorMessage, FormulaConfirmation, OfferSelection, OrderVerification } from './steps';

enum FAST_CHECKOUT_STEP {
  FormulaConfirmation = 'FormulaConfirmation',
  GeneralError = 'GeneralError',
  OfferSelection = 'OfferSelection',
  OrderVerification = 'OrderVerification',
  PreconditionError = 'PreconditionError'
}

interface FastCheckoutTemplateProps {
  data: FastCheckoutTemplateConfigData;
}

const FastCheckoutTemplate = ({ data }: FastCheckoutTemplateProps): ReactElement => {
  const { articleTitle } = data;

  const [selectedType, setSelectedType] = useState<OfferItem | undefined>();

  const wizardSteps: Array<WizardStep<Payment>> = [
    {
      id: FAST_CHECKOUT_STEP.OfferSelection,
      render: ({ goToNextStep }) => (
        <OfferSelection
          articleTitle={articleTitle}
          onSelect={(offerType: OfferItem) => {
            setSelectedType(offerType);
            goToNextStep(FAST_CHECKOUT_STEP.FormulaConfirmation);
          }}
        />
      )
    },
    {
      id: FAST_CHECKOUT_STEP.FormulaConfirmation,
      render: ({ completeWizard, goToNextStep }) => (
        <FormulaConfirmation
          offerType={selectedType as OfferItem}
          onError={() => goToNextStep(FAST_CHECKOUT_STEP.GeneralError)}
          onPaymentCreate={(payment: Payment) => completeWizard(payment)}
          onPreconditionError={() => goToNextStep(FAST_CHECKOUT_STEP.PreconditionError)}
        />
      )
    },
    {
      id: FAST_CHECKOUT_STEP.OrderVerification,
      render: ({ goToNextStep }) => (
        <OrderVerification
          orderId={data.orderId as number}
          onReturnToOffer={() => goToNextStep(FAST_CHECKOUT_STEP.OfferSelection)}
        />
      )
    },
    {
      id: FAST_CHECKOUT_STEP.GeneralError,
      render: ({ goToPreviousStep }) => <ErrorMessage onReturnToOffer={() => goToPreviousStep()} />
    },
    {
      id: FAST_CHECKOUT_STEP.PreconditionError,
      render: ({ goToPreviousStep }) => (
        <ErrorMessage variant="precondition" onReturnToOffer={() => goToPreviousStep()} />
      )
    }
  ];

  return (
    <Wizard
      startId={
        data.orderId ? FAST_CHECKOUT_STEP.OrderVerification : FAST_CHECKOUT_STEP.OfferSelection
      }
      steps={wizardSteps}
      onComplete={(payment: Payment) => {
        if (payment.redirectUrl) {
          window.location.href = payment.redirectUrl;
        }
      }}
    />
  );
};

export default FastCheckoutTemplate;
