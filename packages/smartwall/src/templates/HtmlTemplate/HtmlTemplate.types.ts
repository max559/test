import type { HtmlTemplateProps } from './HtmlTemplate';

export interface HtmlTemplateConfig {
  data: HtmlTemplateProps;
  type: 'html';
}
