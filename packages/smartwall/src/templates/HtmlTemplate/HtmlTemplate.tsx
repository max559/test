import type { ReactElement } from 'react';

export interface HtmlTemplateProps {
  html: string;
}

const HtmlTemplate = ({ html }: HtmlTemplateProps): ReactElement => {
  return <>{html}</>;
};

export default HtmlTemplate;
