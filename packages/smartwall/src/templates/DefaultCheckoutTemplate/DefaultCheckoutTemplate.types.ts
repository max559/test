export interface DefaultCheckoutTemplateConfig {
  data: DefaultCheckoutTemplateConfigData;
  type: 'default-checkout';
}

export interface DefaultCheckoutTemplateConfigData {
  articleTitle: string;
  offerSlug?: string;
}
