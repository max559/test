module.exports = {
  env: {
    node: true
  },
  extends: [
    'eslint:recommended',
    'prettier',
    'turbo',
    'next',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react/jsx-runtime'
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react'],
  rules: {
    'no-console': ['warn'],
    '@next/next/no-img-element': ['off']
  },
  settings: {
    next: {
      rootDir: ['apps/*/']
    }
  }
};
