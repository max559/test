import type { UserInfo } from '@subscriber/services-core';
import type { StoreApi } from 'zustand';

import type { AuthLuxStoreConfig } from '../types';

import { ServicesCore } from '@subscriber/services-core';
import { createContext } from 'react';
import { createStore } from 'zustand';

import { AUTHENTICATION_STATUS } from '../types';
import { getLuxLoginLanguage } from '../utils';

export interface AuthLuxStore {
  authenticationStatus: AUTHENTICATION_STATUS;
  config: AuthLuxStoreConfig;
  userInfo: UserInfo | null;
  getRedirectUrl: (returnUrl: string) => Promise<string>;
  getUserInfo: () => Promise<UserInfo | null>;
  redirectToSignOut: (redirectUri?: string) => void;
}

export const AuthLuxContext = createContext<StoreApi<AuthLuxStore> | null>(null);

export const createAuthLuxStore = (config: AuthLuxStoreConfig): StoreApi<AuthLuxStore> => {
  const coreInstance = new ServicesCore(config.brand, config.environment);

  return createStore<AuthLuxStore>()((set, get) => ({
    authenticationStatus: AUTHENTICATION_STATUS.UNKNOWN,
    config,
    userInfo: null,
    getRedirectUrl: (returnUrl: string) =>
      new Promise((resolve, reject) => {
        coreInstance.accountService
          .postLogin({
            lang: getLuxLoginLanguage(get().config.brand),
            redirectUrl: returnUrl
          })
          .then(response => {
            if (response.data.redirectUrl) {
              resolve(response.data.redirectUrl);
            } else {
              reject();
            }
          })
          .catch(() => {
            reject();
          });
      }),
    getUserInfo: () =>
      new Promise((resolve, reject) => {
        set({ authenticationStatus: AUTHENTICATION_STATUS.LOADING });

        coreInstance.accountService
          .getAccountInfo()
          .then(accountInfoResponse => {
            const userInfo = accountInfoResponse.data;

            set({
              authenticationStatus: AUTHENTICATION_STATUS.AUTHENTICATED,
              userInfo
            });

            resolve(userInfo);
          })
          .catch(({ status }) => {
            if (status === 401) {
              set({
                authenticationStatus: AUTHENTICATION_STATUS.UNAUTHENTICATED,
                userInfo: null
              });

              resolve(null);
            } else {
              set({
                authenticationStatus: AUTHENTICATION_STATUS.ERROR,
                userInfo: null
              });

              reject();
            }
          });
      }),
    redirectToSignOut: redirectUri => {
      const signOutUrl = new URL(
        `${
          get().config.environment === 'production'
            ? 'https://user.wort.lu'
            : 'https://sp-sso-test.wort.lu'
        }/logoutAll`
      );

      if (redirectUri) {
        signOutUrl.searchParams.set('redirectUri', redirectUri);
      }

      window.location.assign(signOutUrl);
    }
  }));
};
