import type { Environment } from '@subscriber/globals';
import type { PropsWithChildren, ReactElement } from 'react';

import type { Brand } from '../types';

import { useRef } from 'react';

import { Authentication } from '../components/Authentication';
import { AuthLuxContext, createAuthLuxStore } from './AuthLuxContext';

interface AuthLuxProviderProps {
  autoAuthentication?: boolean;
  brand: Brand;
  environment: Environment;
}

const AuthLuxProvider = ({
  autoAuthentication = true,
  brand,
  children,
  environment
}: PropsWithChildren<AuthLuxProviderProps>): ReactElement => {
  const store = useRef(
    createAuthLuxStore({
      autoAuthentication,
      brand,
      environment
    })
  ).current;

  return (
    <AuthLuxContext.Provider value={store}>
      {autoAuthentication ? <Authentication silent>{children}</Authentication> : children}
    </AuthLuxContext.Provider>
  );
};

export default AuthLuxProvider;
