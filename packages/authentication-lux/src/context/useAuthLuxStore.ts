import type { AuthLuxStore } from './AuthLuxContext';

import { useContext } from 'react';
import { useStore } from 'zustand';

import { AuthLuxContext } from './AuthLuxContext';

const useAuthLuxStore = <T>(selector: (state: AuthLuxStore) => T): T => {
  const authLuxContext = useContext(AuthLuxContext);

  if (!authLuxContext) {
    throw new Error('The useAuthLuxStore hook can only be used in AuthLuxProvider');
  }

  return useStore(authLuxContext, selector);
};

export default useAuthLuxStore;
