import type { PropsWithChildren, ReactElement } from 'react';

import { useEffect } from 'react';

import { useAuthLuxStore } from '../../context';
import { AUTHENTICATION_STATUS } from '../../types';

interface AuthenticationProps {
  fallback?: ReactElement;
  silent?: boolean;
}

const Authentication = ({
  children,
  fallback,
  silent = false
}: PropsWithChildren<AuthenticationProps>): ReactElement | null => {
  const authenticationStatus = useAuthLuxStore(state => state.authenticationStatus);
  const getUserInfo = useAuthLuxStore(state => state.getUserInfo);

  useEffect(() => {
    if (authenticationStatus === AUTHENTICATION_STATUS.UNKNOWN) {
      getUserInfo();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authenticationStatus]);

  if (
    silent ||
    authenticationStatus === AUTHENTICATION_STATUS.AUTHENTICATED ||
    authenticationStatus === AUTHENTICATION_STATUS.UNAUTHENTICATED
  ) {
    return <>{children}</>;
  }

  if (authenticationStatus === AUTHENTICATION_STATUS.LOADING && fallback) {
    return fallback;
  }

  return null;
};

export default Authentication;
