import { useEffect } from 'react';

import { useAuthLuxStore } from '../../context';

interface RedirectToSignOutProps {
  redirectUri?: string;
}

const RedirectToSignOut = ({ redirectUri }: RedirectToSignOutProps): null => {
  const redirectToSignOut = useAuthLuxStore(state => state.redirectToSignOut);

  useEffect(() => {
    redirectToSignOut(redirectUri);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return null;
};

export default RedirectToSignOut;
