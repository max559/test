import type { ReactElement } from 'react';

import { useEffect } from 'react';

import { useAuthLuxStore } from '../../context';

interface RedirectToSignInProps {
  fallback?: ReactElement;
  returnUrl?: string;
  onError?: () => void;
}

const RedirectToSignIn = ({
  fallback,
  returnUrl,
  onError
}: RedirectToSignInProps): ReactElement | null => {
  const getRedirectUrl = useAuthLuxStore(state => state.getRedirectUrl);

  useEffect(() => {
    getRedirectUrl(returnUrl || window.location.href)
      .then(redirectUrl => {
        window.location.assign(redirectUrl);
      })
      .catch(() => {
        onError && onError();
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (fallback) {
    return fallback;
  }

  return null;
};

export default RedirectToSignIn;
