import type { PropsWithChildren, ReactElement } from 'react';

import { useAuthLuxStore } from '../../context';

import { AUTHENTICATION_STATUS } from '../../types';

const SignedOut = ({ children }: PropsWithChildren): ReactElement | null => {
  const authenticationStatus = useAuthLuxStore(state => state.authenticationStatus);

  if (authenticationStatus === AUTHENTICATION_STATUS.UNAUTHENTICATED) {
    return <>{children}</>;
  }

  return null;
};

export default SignedOut;
