import { useAuthLuxStore } from '../context';

export const useAuthenticationLux = () => {
  const authenticationLuxState = useAuthLuxStore(state => ({
    authenticationStatus: state.authenticationStatus,
    userInfo: state.userInfo,
    getRedirectUrl: state.getRedirectUrl,
    getUserInfo: state.getUserInfo,
    redirectToSignOut: state.redirectToSignOut
  }));

  return authenticationLuxState;
};
