export const getLuxLoginLanguage = (brand: 'co' | 'lt' | 'lw' | 'tc'): 'de' | 'en' | 'pt' => {
  if (brand === 'co') {
    return 'pt';
  }
  if (brand === 'lt') {
    return 'en';
  }

  return 'de';
};
