import type { Environment } from '@subscriber/globals';

export type Brand = 'co' | 'lt' | 'lw' | 'tc';

export enum AUTHENTICATION_STATUS {
  AUTHENTICATED = 'authenticated',
  ERROR = 'error',
  LOADING = 'loading',
  UNAUTHENTICATED = 'unauthenticated',
  UNKNOWN = 'unknown'
}

export interface AuthLuxStoreConfig {
  autoAuthentication: boolean;
  brand: Brand;
  environment: Environment;
}
